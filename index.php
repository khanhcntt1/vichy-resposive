<?php
/**
 * @package		Joomla.Site
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// Set flag that this is a parent file.
ini_set('display_errors','off');
error_reporting(E_ERROR | E_PARSE);
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/defines.php')) {
	include_once dirname(__FILE__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__));
	require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';

// Mark afterLoad in the profiler.
JDEBUG ? $_PROFILER->mark('afterLoad') : null;

// Instantiate the application.
$app = JFactory::getApplication('site');

// Initialise the application.
$app->initialise();

// Mark afterIntialise in the profiler.
JDEBUG ? $_PROFILER->mark('afterInitialise') : null;

// Route the application.
$app->route();

// Mark afterRoute in the profiler.
JDEBUG ? $_PROFILER->mark('afterRoute') : null;

// Dispatch the application.
$app->dispatch();

// Mark afterDispatch in the profiler.
JDEBUG ? $_PROFILER->mark('afterDispatch') : null;

// Render the application.
$app->render();

// Mark afterRender in the profiler.
JDEBUG ? $_PROFILER->mark('afterRender') : null;

/******************************/


function getInfoCatOfVichy()
{
	// Get a db connection.
	$db = JFactory::getDbo();
	$lg = &JFactory::getLanguage();
	
	// Create a new query object.
	$query = $db->getQuery(true);
	$query = "SELECT id, title, alias, main_color, description, image, title_description from #__categories where published > 0 and parent_id = 1 and LOCATE('com_vichy', extension) > 0  and language = '".str_replace(' ', '', $lg->getTag())."'";
	        	$db->setQuery($sql);
	// Reset the query using our newly populated query object.
	$db->setQuery($query);
	/******************************/
	// Load the results as a list of stdClass objects (see later for more options on retrieving data).
	return $db->loadObjectList();
}

function shortDesc($str, $len, $charset='UTF-8'){
	$str = html_entity_decode($str, ENT_QUOTES, $charset);
	if(mb_strlen($str, $charset)> $len){
		$arr = explode(' ', $str);
		$str = mb_substr($str, 0, $len, $charset);
		$arrRes = explode(' ', $str);
		$last = $arr[count($arrRes)-1];
		unset($arr);
		if(strcasecmp($arrRes[count($arrRes)-1], $last)){
			unset($arrRes[count($arrRes)-1]);
		}
		return implode(' ', $arrRes)."...";
	}
	return $str;
}

function url_title($str, $separator = '-', $lowercase = FALSE)
{
	// $count = null;
	// $str = preg_replace('/<.*>/', '-', $str, -1, $count);
		
	if ($separator == 'dash') 
	{
	    $separator = '-';
	}
	else if ($separator == 'underscore')
	{
	    $separator = '_';
	}
	
	$q_separator = preg_quote($separator);

	$trans = array(		
		'<.*>'				=>$separator,
		'&.+?;'                 => '',
		'[^a-z0-9 _-]'          => '',
		'\s+'                   => $separator,
		'('.$q_separator.')+'   => $separator
	);

	// $str = strip_tags($str);

	foreach ($trans as $key => $val)
	{
		$str = preg_replace("#".$key."#i", $val, $str);
	}

	if ($lowercase === TRUE)
	{
		$str = mb_strtolower($str);
	}

	return trim($str, $separator);
}

function removesign($str)
{
    $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
    ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
        "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
        "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
        "ỳ","ý","ỵ","ỷ","ỹ",
        "đ",
        "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
        "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
        "Ì","Í","Ị","Ỉ","Ĩ",
        "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
        "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
        "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
        "Đ","ê","ù","à");
    $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
        "e","e","e","e","e","e","e","e","e","e","e",
        "i","i","i","i","i",
        "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
        "u","u","u","u","u","u","u","u","u","u","u",
        "y","y","y","y","y",
        "d",
        "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
        "E","E","E","E","E","E","E","E","E","E","E",
        "I","I","I","I","I",
        "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
        "U","U","U","U","U","U","U","U","U","U","U",
        "Y","Y","Y","Y","Y",
        "D","e","u","a");
    return str_replace($coDau,$khongDau,$str);
}
/******************************/
// Return the response.
echo $app;
