<?php
/**
 * @package		Joomla.Site
 * @subpackage	Application
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Joomla! Application define.
 */

//Global definitions.
//Joomla framework path definitions.
$parts = explode(DIRECTORY_SEPARATOR, JPATH_BASE);

//Defines.
define('JPATH_ROOT',			implode(DIRECTORY_SEPARATOR, $parts));

define('JPATH_SITE',			JPATH_ROOT);
define('JPATH_CONFIGURATION',	JPATH_ROOT);
define('JPATH_ADMINISTRATOR',	JPATH_ROOT . '/administrator');
define('JPATH_LIBRARIES',		JPATH_ROOT . '/libraries');
define('JPATH_PLUGINS',			JPATH_ROOT . '/plugins'  );
define('JPATH_INSTALLATION',	JPATH_ROOT . '/installation');
define('JPATH_THEMES',			JPATH_BASE . '/templates');
define('JPATH_CACHE',			JPATH_BASE . '/cache');
define('JPATH_MANIFESTS',		JPATH_ADMINISTRATOR . '/manifests');

define('PRODUCT_RANG', '8');
define('YOUR_NEED', '10' );
define('SKIN_CARE','14');
define('PRODUCT_RANG_WITHOUT_STEP','37');
define('STORE','18');
define('SPA','19');

define('VICHY_SPA_MENU_ST','GIỚI THIỆU');
define('VICHY_SPA_MENU_ND','DỊCH VỤ');
define('VICHY_SPA_MENU_RD','ĐỊA ĐIỂM');
define('HOME_POSITION_SLIDESHOW','1');
define('HOME_POSITION_MAIN_ROW_TOP_LEFT','2');
define('HOME_POSITION_MAIN_ROW_TOP_RIGHT','3');
define('HOME_POSITION_MAIN_ROW_BOTTOM_LEFT','4');
define('HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1','5');
define('HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2','6');

define('FB_APP_ID','774064075967000');
define('FB_SECRET','da88ef91384a2f34cdffc37790cde924');

define('VICHY','VichyVN');

// define('MAILCHIMP_KEY','8bbfa5e13a3f69135ef096e426e4197a-us8');
define('MAILCHIMP_KEY','42cfd9e26b3036c914ebfbcac84f6acd-us8');
define('ELEMENT_MC','2');