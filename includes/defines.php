<?php
/**
 * @package		Joomla.Site
 * @subpackage	Application
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Joomla! Application define.
 */

//Global definitions.
//Joomla framework path definitions.
$parts = explode(DIRECTORY_SEPARATOR, JPATH_BASE);

//Defines.
define('JPATH_ROOT',			implode(DIRECTORY_SEPARATOR, $parts));

define('JPATH_SITE',			JPATH_ROOT);
define('JPATH_CONFIGURATION',	JPATH_ROOT);
define('JPATH_ADMINISTRATOR',	JPATH_ROOT . '/administrator');
define('JPATH_LIBRARIES',		JPATH_ROOT . '/libraries');
define('JPATH_PLUGINS',			JPATH_ROOT . '/plugins'  );
define('JPATH_INSTALLATION',	JPATH_ROOT . '/installation');
define('JPATH_THEMES',			JPATH_BASE . '/templates');
define('JPATH_CACHE',			JPATH_BASE . '/cache');
define('JPATH_MANIFESTS',		JPATH_ADMINISTRATOR . '/manifests');

define('PRODUCT_RANG', '8');
define('YOUR_NEED', '10' );
define('SKIN_CARE','14');
define('PRODUCT_RANG_WITHOUT_STEP','37');
define('STORE','18');
define('SPA','19');
define('DEFAULT_DIST','5');

define('VICHY_SPA_MENU_ST','GIỚI THIỆU');
define('VICHY_SPA_MENU_ND','DỊCH VỤ');
define('VICHY_SPA_MENU_RD','ĐỊA ĐIỂM');
define('HOME_POSITION_SLIDESHOW','1');
define('HOME_POSITION_MAIN_ROW_TOP_LEFT','2');
define('HOME_POSITION_MAIN_ROW_TOP_RIGHT','3');
define('HOME_POSITION_MAIN_ROW_BOTTOM_LEFT','4');
define('HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1','5');
define('HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2','6');

define('FB_APP_ID','774064075967000');
define('FB_SECRET','da88ef91384a2f34cdffc37790cde924');

define('VICHY','VichyVN');

// define('MAILCHIMP_KEY','8bbfa5e13a3f69135ef096e426e4197a-us8');
define('MAILCHIMP_KEY','42cfd9e26b3036c914ebfbcac84f6acd-us8');
define('ELEMENT_MC','2');

// define title for page
define('VICHY_TITLE_IDEAL_SKIN','Sứ mệnh làn da lý tưởng');
define('VICHY_TITLE_WATER','Giới thiệu nước khoáng dưỡng da');
define('VICHY_TITLE_NUTRITIVE','Dưỡng sáng trị thâm nám');
define('VICHY_TITLE_REDUCE_OIL','Giảm dầu giảm mụn');
define('VICHY_TITLE_ANTI_AGING','Chống lão hóa');
define('VICHY_TITLE_TEST_SKIN','Kiểm tra da');
define('VICHY_TITLE_TEST_SKIN_EN','Skin Diagnosis');
define('VICHY_TITLE_ADVICE','Lời Khuyên');
define('VICHY_TITLE_ADVICE_EN','Advice');
define('VICHY_TITLE_FACIALS','Lời Khuyên ');
define('VICHY_TITLE_CLEAN_MOISTURIZING', 'Làm sạch & Dưỡng Ẩm');
define('VICHY_TITLE_LOTION', 'Dưỡng sáng da');
define('VICHY_TITLE_ANTI_HAIR', 'Tóc và Da Đầu');
define('VICHY_TITLE_DICTIONARY', 'Từ Điển Làn Da');
define('VICHY_TITLE_DICTIONARY_EN', 'Skin Dictionary');
define('VICHY_TITLE_INTRODUCTION', 'Giới Thiệu');
define('VICHY_TITLE_INTRODUCTION_EN', 'Introduction');
define('VICHY_TITLE_SERVICE', 'Dịch Vụ');
define('VICHY_TITLE_SERVICE_EN', 'Service');

define('VICHY_SERVICE_MAINTAIN', 'Duy trì làn da tươi trẻ');
define('VICHY_SERVICE_ENHANCE', 'Tăng cường sức khỏe làn da');
define('VICHY_SERVICE_PROVICE', 'Cung cấp oxy cho tế bào');
define('VICHY_SERVICE_RECOVERY', 'Phục hồi & Củng cố');
define('VICHY_SERVICE_TREATMENT', 'Điều trị viêm sưng tức thì');
define('VICHY_SERVICE_LOTION', 'Dưỡng sáng da tối ưu');
define('VICHY_SERVICE_REJUVENATION', 'Trẻ hóa làn da');
define('VICHY_SERVICE_CLEAN', 'Sạch bã nhờn ');
define('VICHY_SERVICE_ACNE_TREATMENT', 'Đặc trị  mụn');
define('VICHY_SERVICE_OLD_CONTROL', 'Kiểm soát dầu');
define('VICHY_SERVICE_SKIN_LIGHT', 'Dưỡng sáng da');
define('VICHY_SERVICE_ACNE_MEN', 'Đặc trị mụn nam');
define('VICHY_SERVICE_REDUCE_WRINKLES', 'Giảm quần thăm bọng mắt');
define('VICHY_SERVICE_SHOULDER_ARMS', 'Cổ vai & cánh tay');
define('VICHY_SERVICE_BLACK_WHITE', 'Mụn đầu trắng & đen');

define('VICHY_BLOG_LOTION', 'Dưỡng sáng da');
define('VICHY_BLOG_REDUCE_OIL','Giảm dầu giảm mụn');
define('VICHY_BLOG_ANTI_AGING','Chống lão hóa và săn chắc da');
define('VICHY_BLOG_ANTI_HAIR', 'Tóc và Da Đầu');
