<?php defined('_JEXEC') or die;?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?> ">
	<head>
		<link rel="shortcut icon" href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/favicon.ico" />
		<script src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/jquery.mockjax.js"></script>
    	<script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/jquery.autocomplete.js"></script>
    	<script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/countries.js"></script>
    	<script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/demo.js"></script>
    
		<link href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/css/style.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/css/styles_autocomplete.css" rel="stylesheet" type="text/css" />
		<jdoc:include type="head" />
	</head>
  	<body>
		<div class="container">
			<header class="main">
				<div class="main-center">
					<div class="logo-vichy">
						<a href="<?php echo JURI::root();?>" title="Home">
							<span>Vichy</span>
						</a>
					</div>
					<div class="nav-vichy">
						<div class="nav-vichy-top">
							<div class="nav-vichy-top-left">
								<div class="search">
			                        <!-- <input class="inputbox" type="text" name="keyword" id="keyword" placeholder="Tìm kiếm sản phẩm">
			                        <input class="btn-search" type="button" id="btn-search"> -->
			                         <input id="root-ajax" type="hidden" value="<?php echo JURI::root();?>"></input>
			                         <input class="inputbox" type="text" name="query" placeholder="Tìm kiếm sản phẩm" id="autocomplete-ajax" />
			                         <input class="btn-search"  type="button" id="btn-search">
			                         <input id="selction-ajax" type="hidden" value=""></input>
			                    </div>
							</div>
							<div class="nav-vichy-top-right">
								<jdoc:include type="modules" name="login" />
								<div class="social-link">
									<a href="https://www.facebook.com/vichy.com.vn"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/facebook.png"></a>
									<a href="https://www.youtube.com/user/VichyVietnam"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/youtube.png"></a>
								</div>
							</div>
						</div>
						<div class="nav-vichy-bottom">
							<!-- <ul class="vichy-menu">
								<li><a href="#" title="About">About</a></li>
								<li><a href="#" class="active" title="Products">Products</a></li>
								<li><a href="#" title="Advice & Diagnosis">Advice & Skin Diagnosis</a></li>
								<li><a href="#" title="Vichy sapa">Vichy Sapa</a></li>
								<li><a href="#" title="Blog">Blog</a></li>
							</ul> -->

							<jdoc:include type="modules" name="main_menu" />

							<div class="vichy-link">
								<div class="basket hover-buy-online">
									<a href="#">Mua hàng trực tuyến</a>
									<div class="buy-online">
										<a style="float: left;" href="http://www.chon.vn"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/Vichy_Home_buy_online_03.png"></a>
										<a style="float: left;" href="http://www.yes24.com"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/Vichy_Home_buy_online_04.png"></a>
										<a style="float: left;" href="http://www.lazada.vn"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/Vichy_Home_buy_online_05.png"></a>
							</div>
									<script type="text/javascript" language="javascript">
									$(document).ready(function() {
										
										// $(".hover-buy-online").mouseout(function() { $(".buy-online").css('visibility','hidden'); });
										$(".hover-buy-online").mouseover(function() { $(".buy-online").css('visibility','visible'); });
										$(".buy-online").mouseover(function() { $(".buy-online").css('visibility','visible'); });
										$(".buy-online").mouseout(function() { $(".buy-online").css('visibility','hidden'); });
									   });
 									</script>
								</div>
								<jdoc:include type="modules" name="store_locator" />
							</div>
							
							
									
						</div>
					</div>
				</div>
			</header>
			
			<jdoc:include type="modules" name="breadcrumb" />
			
			<div class="main-container" <?php echo ($_GET['option'] == 'com_vichy_home') ? 'style="padding-top:170px;"' : '' ?>>
				<a href="#" id="go-bottom" class="button-down">
			   		<img src="<?php echo JURI::root(); ?>templates/vichy/images/to-bottom-icon.png" />
				</a>
				<?php if($_GET['view']== 'featured') {?>
					<jdoc:include type="modules" name="vichy_about" />
				<?php } ?>
				<?php if($_GET['view'] == 'product_landing'){ ?>
                <jdoc:include type="modules" name="new_products" />
				<jdoc:include type="modules" name="favorite_products" />
				<?php } ?>
				<jdoc:include type="component" />
				<?php if($_GET['view'] == 'product'){ ?>
				<jdoc:include type="modules" name="vichy_comment_product" />
				<jdoc:include type="modules" name="diagnosis_advice_product" />
				<?php } ?>
				<?php if($_GET['view'] == 'product' || $_GET['view'] == 'product_range'){ ?>
				<jdoc:include type="modules" name="product_step_1" />
				<?php } ?>
				<?php if($_GET['view'] == 'product_landing'){ ?>
				<jdoc:include type="modules" name="product_without_steps" />
				<?php } ?>
				<?php if($_GET['view'] == 'advice'){ ?>
				<jdoc:include type="modules" name="product_range_diagnosis_advice" />
				<?php } ?>
				<a href="#" id="go-top" class="button-up">					
			   		<img src="<?php echo JURI::root(); ?>templates/vichy/images/to-top-icon.png" />
				</a>
			</div>

			<footer class="main">				
				<div class="top clearfix">
					<div class="main-center">
						<jdoc:include type="modules" name="vichy_footer" />
					</div>
				</div>
				<div class="bottom">
					<div class="main-center clearfix">
						<div class="copyright">© 2014 VICHY&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Điều khoản sử dụng&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Từ điển làn da</div>
					</div>
				</div>
			</footer>
		</div>		
		<script type="text/javascript">
		jQuery(window).scroll(function() {
			/*for scroll up*/
		  if (jQuery(this).scrollTop()>0) {
		   jQuery('#go-top').fadeIn(200);
		  } else {
		   jQuery('#go-top').fadeOut(50);
		  }
		  /*for scroll down*/
		  if (jQuery(this).scrollTop()>(jQuery('.main-container').height()-500)) {
		   jQuery('#go-bottom').fadeOut(50);
		  } else {
		   jQuery('#go-bottom').fadeIn(200);
		  }
		 });     
		 // Animate the scroll to top
		 jQuery('#go-top').click(function(event) {
		  event.preventDefault();      
		  jQuery('html, body').animate({scrollTop: 0}, 1000);
	 	});
		 jQuery('#go-bottom').click(function(event) {
		  event.preventDefault();      
		  jQuery('html, body').animate({scrollTop: jQuery('.main-container').height()}, 1000);
	 	});
		</script>
  	</body>
</html>





