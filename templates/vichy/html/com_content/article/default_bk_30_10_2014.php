<?php
    defined('_JEXEC') or die;
    JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');
    $document = & JFactory::getDocument();
    $document->addStyleSheet('components/com_content/views/category/css/style.css');
    $document->addStyleSheet('components/com_content/views/category/css/nivo-slider.css');
    // $document->addScript('components/com_content/views/category/js/jquery.nivo.slider.js');
    
    $db =& JFactory::getDBO();

    if(isset($_GET['catid']) && !empty($_GET['catid'])){
        
        $cat_id = mysql_escape_string(htmlentities($_GET['catid']));
        $cat_id = substr($cat_id, 0, strrpos($cat_id, ':')); 

        $query = "SELECT id, title FROM #__categories 
        WHERE parent_id = 29 order by rgt";    
        $db->setQuery($query);
        $items = $db->loadObjectList();
    }

    if(isset($_GET['id']) && !empty($_GET['id'])){
        $id = mysql_escape_string(htmlentities($_GET['id']));
        $query = 'SELECT
        u.username,
        c.id,
        c.catid, 
        c.title, 
        c.images,
        c.introtext,
        c.fulltext, 
        c.created, 
        c.language, 
        CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
        CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
        JOIN #__users u ON u.id = c.created_by
        JOIN #__categories cg ON c.catid = cg.id 
        WHERE c.state > 0 AND c.id = '.$id.'   
        ORDER BY c.created DESC';
        $db->setQuery($query);
        $blog_item = $db->loadObject();
    }
    $current_link=JURI::root().'index.php?'.$_SERVER['QUERY_STRING'];
    $tmp = explode(':', $blog_item->catid);
?>

<div class="vichy-blog">
    <div class="box">
        <ul class="list-category-blog">
            <li class="get-blog" id="0"><a href="<?php echo JROUTE::_('index.php?option=com_content&view=category&layout=blog&id=29&Itemid=109'); ?>">Tất cả</a></li>
            <?php 
                foreach ($items as $k => $v) {
                    $style = "";
                    if($k%3 == 0){
                        $style = 'style="width:322px"';
                    }
                    if(($k-1)%3 == 0){
                        $style = 'style="width:210px"';
                    }
            ?>
                <li class="get-blog" <?php echo $style; ?> id="<?php echo $v->id; ?>"><a <?php echo ($tmp[0] == $v->id)? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$v->id.'&Itemid=109'); ?>"><?php echo $v->title; ?></a></li>
            <?php } ?>
        </ul>
    </div>

    <div class="wraper-blog-detail">
        <?php if(!empty(json_decode($blog_item->images)->image_intro)){ ?>
        <div class="banner">
            <img src="<?php echo json_decode($blog_item->images)->image_intro; ?>">
        </div>
        <?php } ?>
        <div class="blog-detail">
            
            <h3 class="title"><?php echo $blog_item->title; ?></h3>
             <!--<div class="author"><span>AUTHOR: </span><?php echo $blog_item->username; ?></div>-->
            <div class="date"><?php echo date("d/m/Y", strtotime($blog_item->created)) ?></div>
           
            <div class="social">
                <!-- <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div> -->
                <iframe src="//www.facebook.com/plugins/like.php?href=<?php echo $current_link;?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                <!-- <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe> -->
            </div>
        </div>
        <div class="clear"></div>
        <div class="blog_detail_description"><?php echo $blog_item->introtext.$blog_item->fulltext; ?></div>
    </div>
</div>
<div class="clear"></div>