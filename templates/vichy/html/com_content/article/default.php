<?php
    defined('_JEXEC') or die;
    JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');
    $document = & JFactory::getDocument();
    $document->addStyleSheet('components/com_content/views/category/css/style.css');
    $document->addStyleSheet('components/com_content/views/category/css/nivo-slider.css');
        $document->addStyleSheet('components/com_vichy_store/css/spa.css');
    $document->addStyleSheet('components/com_vichy_store/css/spa_'.$active.'.css');
    // $document->addScript('components/com_content/views/category/js/jquery.nivo.slider.js');

    $db =& JFactory::getDBO();

    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();    

    if($language_tag == 'vi-VN'){
        $language = "'*',".$db->quote($language_tag);
    }else{
        $language = $db->quote($language_tag);
    }    
    
    if(isset($_GET['catid']) && !empty($_GET['catid'])){
        
        $cat_id = mysql_escape_string(htmlentities($_GET['catid']));
        $cat_id = substr($cat_id, 0, strrpos($cat_id, ':')); 

        $query = "SELECT id, title FROM #__categories 
        WHERE parent_id = 29 and published = 1 and language in(".$language.") order by rgt";    
        $db->setQuery($query);
        $items = $db->loadObjectList();
    }
    
    if(isset($_GET['id']) && !empty($_GET['id'])){
        $id = mysql_escape_string(htmlentities($_GET['id']));
        $query = 'SELECT
        u.username,
        c.id,
        c.catid, 
        c.title, 
        c.images,
        c.introtext,
        c.fulltext, 
        c.created, 
        c.language, 
        CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
        CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
        JOIN #__users u ON u.id = c.created_by
        JOIN #__categories cg ON c.catid = cg.id 
        WHERE c.state > 0 AND c.id = '.$id.'   
        ORDER BY c.created DESC';
        $db->setQuery($query);
        $blog_item = $db->loadObject();
    }
    //$current_link=JURI::root().'index.php?'.$_SERVER['QUERY_STRING'];
    $current_link = 'index.php?option=com_content&view=article&catid='.$blog_item->catid.'&id='.$blog_item->id.'&Itemid=109&lang=vi';
    
    $tmp = explode(':', $blog_item->catid);
    
    $current_cate = $_GET['catid'];
?>

<nav class="navbar navbar-default mobile-style" style="display: none" role="navigation">
        <div class="container">
            <div class="navbar-header sub-menu-spa-header blog-menu">
                <a class="navbar-brand" href="#" style="color:white;">Blog</a>
                <button type="button" class="navbar-toggle collapsed sub-menu-spa-btn plus1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                </button>
            </div>
            <div class="collapse navbar-collapse over-breadrum" id="bs-example-navbar-collapse-1" style="margin-left:0 !important;margin-right:0 !important;">
                <ul class="nav navbar-nav" style="margin-top:0px;background: #123f83;">
                    <!--                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
                    <!--                    <li><a href="#">Link</a></li>-->
            <!--         <li class="bg-li-menu-mobile1" style="margin-bottom:1px;"> <a class="sub-menu-a"  href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=3&Itemid=108'); ?>"><?php echo $title; ?></a></li> -->
                    <?php 
                        foreach ($items as $k => $v) {
                            $style = "";
                            if($k%3 == 0){
                                $style = 'width:322px';
                            }
                            if(($k-1)%3 == 0){
                                $style = 'width:210px';
                            }
                    ?>
                        <li id="<?php echo $v->id; ?>" class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"><a  class="sub-menu-a" <?php echo ($cat_id == $v->id)? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$v->id.'&Itemid=109'); ?>"><?php echo $v->title; ?></a></li>
                    <?php } ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
 <ul class="mobile-style" style="display: none;">
 <?php 
foreach ($items as $row) :
    $temp= $row->id;
    if($temp==$cat_id){
        $tempname = $row->title;
    }
endforeach;

 ?>
    <li class="bg-li-menu-mobile active" id="<?php echo $cat_id ?>" >
    <div style="overflow:hidden;max-width:85%; height:40px;float:left;background:#3c72c7">
        <a class="sub-menu-a active"  href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$cat_id.'&Itemid=109'); ?>"><?php echo $tempname; ?></a>
    </div>
    <div style="">
        <img style="margin-left:-0.7px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" />   
    </div>
    </li>
<!--     <li class="bg-li-menu-mobile active" id="0" > <a <?php echo ($cat_id == '29')? 'class = "cat_active"' : ''; ?>  class="sub-menu-a active"  href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$v->id.'&Itemid=109'); ?>"><?php echo ($language_tag=='en-GB') ? 'All' : 'Tất cả'; ?></a></li> -->
</ul>
<div class="vichy-blog">
    <div class="box pc-style">
        <ul class="list-category-blog">
            <li class="get-blog" id="0"><a href="<?php echo JROUTE::_('index.php?option=com_content&view=category&layout=blog&id=29&Itemid=109'); ?>"><?php echo ($language_tag=='en-GB') ? 'All' : 'Tất cả'; ?></a></li>
            <?php 
                foreach ($items as $k => $v) {
                    $style = "";
                    if($k%3 == 0){
                        $style = 'style="width:322px"';
                    }
                    if(($k-1)%3 == 0){
                        $style = 'style="width:210px"';
                    }
            ?>
                <li class="get-blog" <?php echo $style; ?> id="<?php echo $v->id; ?>"><a <?php echo ($current_cate == $v->id)? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$v->id.'&Itemid=109'); ?>"><?php echo $v->title; ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="wraper-blog-detail">
        <?php if(!empty(json_decode($blog_item->images)->image_intro)){ ?>
        <div class="banner pc-style">
            <img src="<?php echo ($language_tag == 'en-GB') ? 'images/contents/' : ''; echo json_decode($blog_item->images)->image_intro; ?>">
        </div>
        <?php } ?>
        <div class="blog-detail">
            
            <h3 class="title"><?php echo $blog_item->title; ?></h3>
             <!--<div class="author"><span>AUTHOR: </span><?php echo $blog_item->username; ?></div>-->
            <div class="date"><?php echo date("d/m/Y", strtotime($blog_item->created)) ?></div>
            <?php if(!empty(json_decode($blog_item->images)->image_intro)){ ?>
            <div class="banner mobile-style">
                <img src="<?php echo ($language_tag == 'en-GB') ? 'images/contents/' : ''; echo json_decode($blog_item->images)->image_intro; ?>">
            </div>
            <?php } ?>
            <div class="social">
                <!-- <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div> -->
                <!--<iframe src="//www.facebook.com/plugins/like.php?href=<?php //echo $current_link;?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>-->
                <div class="fb-like" data-href="<?php echo JROUTE::_($current_link); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                <!-- <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe> -->
            </div>
        </div>
        <div class="clear"></div>
        <div class="blog_detail_description"><?php echo $blog_item->introtext.$blog_item->fulltext; ?></div>
    </div>
</div>
<div class="clear"></div>
<script>
    $(".sub-menu-spa-btn").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
</script>