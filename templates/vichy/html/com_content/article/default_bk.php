<?php
    defined('_JEXEC') or die;
    JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');
    $document = & JFactory::getDocument();
    $document->addStyleSheet('components/com_content/views/category/css/style.css');
    $document->addStyleSheet('components/com_content/views/category/css/nivo-slider.css');
    $document->addScript('components/com_content/views/category/js/jquery.nivo.slider.js');
    
?>

<div class="vichy-blog">
    <div class="box">
        <div id="slider" class="nivoSlider">
            <a href="javascript:void(0);"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/banner-blog1.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/banner-blog1.jpg" alt="Banner" width="926" height="336"></a>
            <a href="javascript:void(0);"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/up.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/up.jpg" alt="Banner" width="926" height="336" ></a>
            <a href="javascript:void(0);"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/toystory.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/toystory.jpg" alt="Banner" width="926" height="336" ></a>
            <a href="javascript:void(0);"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/walle.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/walle.jpg" alt="Banner" width="926" height="336" ></a>
        </div>
    </div> <!-- slider -->

     <div class="box">
        <ul class="list-category-blog">
            <li><a href="javascript:void(0);">ALL</a></li>
            <li><a href="javascript:void(0);">ANTI - AGEING</a></li>
            <li><a href="javascript:void(0);">FOUNDATION MAKE UP</a></li>
            <li><a href="javascript:void(0)">HYDRATION</a></li>
            <li><a href="javascript:void(0)">BODYCARE AND DEODORANTS</a></li>
            <li><a href="javascript:void(0)">SUN CARE</a></li>
             <li><a href="javascript:void(0)">HYDRATION</a></li>
            <li><a href="javascript:void(0)">BODYCARE AND DEODORANTS</a></li>
            <li><a href="javascript:void(0)">SUN CARE</a></li>
        </ul>
    </div> <!-- list blog -->
    <div class="wraper-blog-detail">
        <div class="banner">
            <a href="javascript:void(0)"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/banner-blog2.jpg" alt="Blog"></a>
        </div>
        
        <div class="blog-detail">
            <div class="title">
                <h3>BLOG TITLE</h3>
                <div class="social">
                    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                </div>
            </div>
            
             <div class="author"><span>AUTHOR: </span>LOREM IPSUM </div>
            
            <div class="date">21/05/2014</div>
           
            <div class="description">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
            </div>
        </div>
    </div> <!-- blog detail -->

</div>
<script type="text/javascript">
    $(window).load(function() {
         $('#slider').nivoSlider({
            prevText: '',
            nextText: ''
                    
            });
        });
</script>



