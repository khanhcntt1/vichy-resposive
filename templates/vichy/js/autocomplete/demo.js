﻿/*jslint  browser: true, white: true, plusplus: true */
/*global $, countries */

$(function () {
    'use strict';

    var countriesArray = $.map(countries, function (value, key) { return { value: value, data: key }; });
    var lang_code = $("#language_tag").val();
    if(lang_code == 'en-GB'){
        lang_code = 'en';
    }else{
        lang_code = 'vi';
    }

    $('#btn-search').click(function() {
        var root = $('#root-ajax').val();
        var value_search = $('#autocomplete-ajax').val();
    	//window.location.href = $('#selction-ajax').val();
        if(value_search != ''){
            window.location.href = root+'index.php?option=com_vichy_product&view=product_search&filter='+value_search+'&Itemid=103&lang='+lang_code;
        }else{
            window.location.href = $('#selction-ajax').val();
        }
    });
    $('#btn-search3').click(function() {
        var root = $('#root-ajax3').val();
        var value_search = $('#autocomplete-ajax3').val();
        //window.location.href = $('#selction-ajax').val();
        if(value_search != ''){
            window.location.href = root+'index.php?option=com_vichy_product&view=product_search&filter='+value_search+'&Itemid=103&lang='+lang_code;
        }else{
            window.location.href = $('#selction-ajax3').val();
        }
    });
    $('#btn-search2').click(function() {
        var root = $('#root-ajax').val();
        var value_search = $('#autocomplete-ajax-popup').val();
    	//window.location.href = $('#selction-ajax').val();
        if(value_search != ''){
            window.location.href = root+'index.php?option=com_vichy_product&view=product_search&filter='+value_search+'&Itemid=103&lang='+lang_code;
        }else{
            window.location.href = $('#selction-ajax').val();
        }        
    });
    
    $('#btn-search-comp').click(function() {
        var root = $('#root-ajax').val();
        var value_search = $('#autocomplete-ajax-comp').val();
        //window.location.href = $('#selction-ajax').val();
        if(value_search != ''){
            window.location.href = root+'index.php?option=com_vichy_product&view=product_search&filter='+value_search+'&Itemid=103&lang='+lang_code;
        }else{
            window.location.href = $('#selction-ajax').val();
        }        
    });

    // Setup jQuery ajax mock:
    $.mockjax({
        url: '*',
        responseTime: 500,
        response: function (settings) {
            var query = settings.data.query,
                queryLowerCase = query.toLowerCase(),
                re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi'),
//                suggestions = $.grep(countriesArray, function (country) {
//                     // return country.value.toLowerCase().indexOf(queryLowerCase) === 0;
//                    return re.test(country.value);
//                }),
                response = {
                    query: query,
                    suggestions: suggestions
                };

            this.responseText = JSON.stringify(response);
        }
    });

    // Initialize ajax autocomplete:
    var root = $('#root-ajax').val();
   
    $('#autocomplete-ajax').autocomplete({
         serviceUrl: root+'index.php?option=com_vichy_product&view=product&task=autocomplete',
         minChars:1,
     	 delimiter: /(,|;)\s*/, // regex or character
        //lookup: countriesArray,
     	//params: { q:'Gel' }, //aditional parameters
    	noCache: false, //default is false, set to true to disable caching
        lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            return re.test(suggestion.value);
        },
        onSelect: function(suggestion) {
        	if(suggestion.isrange == 1)
        	{
        		// $('#selction-ajax').val(root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+suggestion.cid+'&Itemid=103&lang=vi');
        		window.location.href = root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+suggestion.cid+'&Itemid=103&lang='+lang_code;
        	}else
        	{
        		// $('#selction-ajax').val(root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&Itemid=103&lang=vi');
        		window.location.href = root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+'&Itemid=103&lang='+lang_code;
        	}
            
            
        },
        onHint: function (hint) {
            $('#autocomplete-ajax-x').val(hint);
        },
        onInvalidateSelection: function() {
            $('#selction-ajax').html('You selected: none');
        }
    });
    
    $('#autocomplete-ajax-comp').autocomplete({
        serviceUrl: root+'index.php?option=com_vichy_product&view=product&task=autocomplete',
        containerClass: 'autocomplete-suggestions-comp',
        minChars:1,
        appendTo:'.search-comp',
         delimiter: /(,|;)\s*/, // regex or character
       //lookup: countriesArray,
        //params: { q:'Gel' }, //aditional parameters
    noCache: false, //default is false, set to true to disable caching
       lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
           var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
           return re.test(suggestion.value);
       },
       onSelect: function(suggestion) {
        if(suggestion.isrange == 1)
        {
            // $('#selction-ajax').val(root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+suggestion.cid+'&Itemid=103&lang=vi');
            window.location.href = root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+suggestion.cid+'&Itemid=103&lang='+lang_code;
        }else
        {
            // $('#selction-ajax').val(root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&Itemid=103&lang=vi');
            window.location.href = root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+'&Itemid=103&lang='+lang_code;
        }
           
           
        },
        onHint: function (hint) {
            $('#autocomplete-ajax-x').val(hint);
        },
        onInvalidateSelection: function() {
            $('#selction-ajax').html('You selected: none');
        }
    });

    $('#autocomplete-ajax-popup').autocomplete({
        serviceUrl: root+'index.php?option=com_vichy_product&view=product&task=autocomplete',
        containerClass: 'autocomplete-suggestions-popup',
        minChars:1,
        appendTo:'.search-box',
    	 delimiter: /(,|;)\s*/, // regex or character
       //lookup: countriesArray,
    	//params: { q:'Gel' }, //aditional parameters
   	noCache: false, //default is false, set to true to disable caching
       lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
           var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
           return re.test(suggestion.value);
       },
       onSelect: function(suggestion) {
       	if(suggestion.isrange == 1)
       	{
       		// $('#selction-ajax').val(root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+suggestion.cid+'&Itemid=103&lang=vi');
       		window.location.href = root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+suggestion.cid+'&Itemid=103&lang='+lang_code;
       	}else
       	{
       		// $('#selction-ajax').val(root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&Itemid=103&lang=vi');
       		window.location.href = root+'index.php?option=com_vichy_product&view=product&id='+suggestion.data+'&rid='+'&Itemid=103&lang='+lang_code;
       	}
           
           
        },
        onHint: function (hint) {
            $('#autocomplete-ajax-x').val(hint);
        },
        onInvalidateSelection: function() {
            $('#selction-ajax').html('You selected: none');
        }
    });

    // Initialize autocomplete with local lookup:
    $('#autocomplete').autocomplete({
        lookup: countriesArray,
        minChars: 0,
        onSelect: function (suggestion) {
            $('#selection').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
        }
    });
    
    // Initialize autocomplete with custom appendTo:
    $('#autocomplete-custom-append').autocomplete({
        lookup: countriesArray,
        appendTo: '#suggestions-container'
    });

    // Initialize autocomplete with custom appendTo:
    $('#autocomplete-dynamic').autocomplete({
        lookup: countriesArray
    });
});