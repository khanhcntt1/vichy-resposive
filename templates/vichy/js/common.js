jQuery(window).scroll(function() {
	/*for scroll up*/
	if (jQuery(this).scrollTop()>0) {
		jQuery('#go-top').fadeIn(200);
	// 	if(jQuery(this).scrollTop()>=50)
	// 	{
	// 		jQuery('header .main-center').attr('style','height:100px;');
	// 		jQuery('.vichy-breadcrumb').attr('style','top:104px;');	
	// 		jQuery('.border_home').attr('style','top:104px;');
	// 		jQuery('.social-link').attr('style','position: absolute; top:30px;right:0px;');
	// 		jQuery('.nav-vichy-top-left').attr('style','margin-top:3px;');
	// 		jQuery('.logo-vichy >a').attr('style','height:71px;');
	// 		jQuery('.vichy-menu').attr('style','margin-top:1px;');
	// 		jQuery('.main-container').css('background-position','center 109px');
	// 	}		
	} else {
		jQuery('#go-top').fadeOut(50);
		// jQuery('header .main-center').removeAttr('style');
		// jQuery('.vichy-breadcrumb').removeAttr('style');	
		// jQuery('.social-link').removeAttr('style');
		// jQuery('.nav-vichy-top-left').removeAttr('style');	
		// jQuery('.logo-vichy >a').removeAttr('style');
		// jQuery('.border_home').removeAttr('style');
		// jQuery('.vichy-menu').removeAttr('style');
		// jQuery('.main-container').css('background-position','center 116px');
	}
	/*for scroll down*/
	if (jQuery(this).scrollTop()>(jQuery('.main-container').height()-500)) {
		jQuery('#go-bottom').fadeOut(50);
	} else {
		jQuery('#go-bottom').fadeIn(200);
	}
});     
// Animate the scroll to top
jQuery('#go-top').click(function(event) {
	event.preventDefault();      
	jQuery('html, body').animate({scrollTop: 0}, 1000);
});
jQuery('#go-bottom').click(function(event) {
	event.preventDefault();      
	jQuery('html, body').animate({scrollTop: jQuery('.main-container').height()}, 1000);
});
width_window = jQuery(window).width();
width = (width_window/2 + 470 + 21);

jQuery('.button-up').css('left',width);
jQuery('.button-down').css('left',width);
about_vichy_width=jQuery('.vichy-menu li:first-child').width();
vichy_menu_width=jQuery('.vichy-menu').width();
pop_width=vichy_menu_width - about_vichy_width - 921 - 14;
jQuery('.messagepop').css('left',pop_width);