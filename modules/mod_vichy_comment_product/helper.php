<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_feed
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class modCommentHelper
{
	static function getComment($product_id)
	{
		$db = & JFactory::getDBO();
		$sql = "SELECT cp.*, u.avatar,u.usertype,u.name,pr.rating from vc_vichy_comment_product as cp  join vc_users as u on u.username = cp.username left join vc_vichy_product_rating as pr on pr.username = cp.username and pr.product_id = cp.product_id where cp.published = '1' and cp.product_id = $product_id order by cp.id DESC";
		$db->setQuery($sql);
		$result = $db->loadObjectList();
		return $result;
	}
	// public static function captcha(){
	// 	//Let's generate a totally random string using md5 
	//     $md5_hash = md5(rand(0,999)); 
	//     //We don't need a 32 character long string so we trim it down to 5 
	//     $security_code = substr($md5_hash, 15, 5); 

	//     //Set the session to store the security code
	//     $session =& JFactory::getSession();
 //    	$session->set('security_code', $security_code);
 //    	//$this->setSecure($security_code);
 //    	return $security_code;
	// }
}
