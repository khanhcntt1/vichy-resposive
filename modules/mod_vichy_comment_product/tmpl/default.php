<?php
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
$document = & JFactory::getDocument();
$document->addStyleSheet('modules/mod_vichy_comment_product/css/style.css');
$document->addStyleSheet('templates/vichy/css/jquery.rating.css');
$user = & JFactory::getUser();
$username = $user->username;
$db =& JFactory::getDBO();
$sql = "Select avatar, usertype from #__users where id = ".$user->id;
$db->setQuery ($sql);
$avatar = $db->loadObject();
$rid = $_GET['rid'];
$color_sql = "Select main_color from vc_categories where id = ".$rid;
$db->setQuery ($color_sql);
$current_color = $db->loadObject();

$color = '';	 
		switch ($current_color->main_color) {
			case 'normaderm':
				$color = '#197205';
				break;
			case 'capital':
				$color = '#ff8b06';
				break;
			case 'thermal':
				$color = '#82add8';
				break;
			case 'aqualia_thermal':
				$color = '#448ac9';
				break;
			case 'bi_white':
				$color = '#717e85';
				break;
			case 'destock':
				$color = '#099575';
				break;
			case 'dercos':
				$color = '#8b0305';
				break;
			case 'liftactiv':
				$color = '#1a74a3';
				break;
			case 'purete_thermal':
				$color = '#00a0db';
				break;
			case 'aera-mineral':
				$color = '#e2c8af';
				break;
            case 'idealia':
                $color = '#e55482';
                break;
			default :
				$color = '#2b716f';
				break;
		}		

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();
if($language_tag == 'vi-VN'){	
	$lbl_rating = "Đánh giá";
	$lbl_comment = "Bình luận";
	$lbl_submit_comment = "Gửi nhận xét";

	$error_login = "Lỗi : Bạn chưa đăng nhập !";
	$error_comment = "Lỗi : Bình luận của bạn không được để trống !";
}else{
	$lbl_rating = "Rating";
	$lbl_comment = "Comment";
	$lbl_submit_comment = "Submit comment";

	$error_login = "Error : You are not logged in !";
	$error_comment = "Error : Your comment can not be empty !";
}


?>
<!-- rating -->

<script language="javascript" type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/jquery.rating.pack.js"></script>
<script>
	(function($){
		$(window).load(function(){
			$(".comment-product").mCustomScrollbar({
				autoHideScrollbar:false,
				theme:"light-thin"
			});
		});
	})(jQuery);
	$( document ).ready(function() {
	  if(window.innerWidth<992){
	  	$("#collapse2").addClass("collapse in");
	  }
	});
</script>
<div class="btn-mobi btn-mobi2 mobile-style" style="background:<?php echo $color;?>;width:98%;margin-left:1%" aria-expanded="true" data-toggle="collapse" data-target="#collapse2">ĐÁNH GIÁ<img class="plus" src="<?php echo JURI::root() ?>components/com_vichy_product/images/btn-minus2.png" /></div>
<div id="collapse2" class="wrap_comment box_shadow">
	<div class="customer_review" style="background:<?php echo $color;?>"><?php echo $lbl_rating; ?> (<span id="no_customer_review" style="font-weight:bold"></span> <?php echo $lbl_comment; ?>)</div>
	<?php if(count($feed)){ ?>
	<div class='comment-product'>
		<?php
			foreach($feed as $k => $v){
				$data .= '<div class="comment-item">';
				$data .= '<div class="profile-user">';
				$data .= '<div class="thumb-pic">';
				$src = ($v->avatar == '1') ? 'vichy_default.jpg' : $v->avatar ;
				$src = JURI::root().'timbthumb.php?src=media/com_users/image/'.$src.'&w=109&q=100';
				if($v->usertype != '' && strpos($v->usertype, 'face_') !== false){
					$face_id = substr($v->usertype, 5);
					$src = "http://graph.facebook.com/$face_id/picture?type=large&width=109&height=109";
				}
				$data .= '<img src="'.$src.'" />';
				$data .= '</div>';
				$data .= '<div class="product-comment-rating">';
				$data .= '<div class="profile-user-comment">';
				
				$data .= '<div class="username">by '.$v->name.'</div>';
				$data .= '<div class="datetime">'.date("d F Y", strtotime($v->date)).'</div>';
				$data .= '<div class="rating">';
				for($j=1; $j<=5; $j++){
	                if(!empty($v->rating) && $v->rating >0 && $j<=$v->rating )
	                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/small_star_blue.png">';
	                else{
	                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/small_star_grey.png">';
	                }
	            }
	            $data .= '</div>';// end rating
				$data .= '</div>';// end profile-user-comment
				$data .= '</div>';// end product-comment-rating
				$data .= '</div>';//end profile-user
				$data .= '<div class="comment-content">';
				$data .= $v->comment;
				$data .= '</div>';//end comment-content
				$data .= '</div>';
			}
			echo $data;
		?>
	</div>
	<?php } ?>
	<div class='comment_product_input' id="scroll_comment">
		<div class="profile-user">
			<?php if($username!=''){ ?>
				<div class="thumb-pic">
					<?php 
						if($avatar->usertype != '' && strpos($avatar->usertype, 'face_') !== FALSE){
							$face_id = substr($avatar->usertype, 5);
							$src = "http://graph.facebook.com/$face_id/picture?type=large&width=109&height=109";
						?>
						<img src="<?php echo $src; ?>" />
						<?php }else{ ?>
						<img width='109' src="media/com_users/image/<?php echo ($avatar->avatar == '1') ? 'vichy_default.jpg' : $avatar->avatar; ?>" />
					<?php } ?>
				</div>
			<?php } ?>
			<div class="product-rating">
				<div class="title"><?php echo $lbl_rating; ?></div>
				<div class="rating">
                	<input type="hidden" id="id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ""; ?>" />
                    <input name="star1" type="radio" class="star" value="1" />
                    <input name="star1" type="radio" class="star" value="2" />
                    <input name="star1" type="radio" class="star" value="3" />
                    <input name="star1" type="radio" class="star" value="4" />
                    <input name="star1" type="radio" class="star" value="5" />
				</div>
			</div>
		</div>
		<div class="form_comment">
			<div class="form-error">
				<span class="error-text" style="color:red;"></span>
			</div>
			<form action="#" method="post">
				<input type="hidden" name="product_id" value="<?php echo $_GET['id']; ?>" />
				<input type="hidden" name="range_id" value="<?php echo $_GET['rid']; ?>" />
				<input type="hidden" name="username" value="<?php echo $username; ?>" />
				<div class="user-input">
					<textarea class="comment-input" rows="3" cols="70" name="feedback" id="comment_text" placeholder="<?php echo $lbl_comment; ?>"></textarea>
				</div>
				<div class="user-submit">
				<input type="submit" class='top_add_review' style="background:<?php echo $color;?>" id="submit_comment" value="<?php echo JText::_($lbl_submit_comment); ?>"/>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	var product_id = "<?php echo $_GET['id']; ?>";
	jQuery(document).ready(function(){
		// pagination(1);  
	 //    jQuery(".comment-product").on('click',".paging > a",function(){
		//     var page = jQuery(this).attr('page');
		//     pagination(page);
		// });

		jQuery.ajax ({
	        type: "POST",
	        url: "<?php echo JURI::root(); ?>index.php?option=com_vichy_comment_product&task=count_comment",
	        data: "product_id="+product_id,
	        success: function(kq) { 
	            jQuery("#no_customer_review").html(kq); 
	        }
	    });

		// function pagination(page){
		//     // jQuery('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast');
		    
		//     jQuery.ajax ({
		//         type: "POST",
		//         url: "<?php echo JURI::root(); ?>index.php?option=com_vichy_comment_product&task=comment_product_pagination",
		//         data: "page="+page+"&product_id="+product_id,
		//         success: function(data_page) { 
		//             // jQuery('#loading').fadeOut('fast');
		//             if(data_page != '1'){
		//             	jQuery(".comment-product").html(data_page); 
		//             }else{
		//             	jQuery(".comment-product").html('<h3>Chưa có bình luận nào</h3>');
		//             }
		//         }
		//     });
		// }

		/* Rating */
		jQuery(".star-rating a").click(function(){
			var val 	= jQuery(this).attr('title');
			var id		= jQuery("#id").val();
			jQuery.ajax({
                type: "POST",
                url: "<?php echo JURI::root();?>index.php?option=com_vichy_comment_product&task=rating",
                dataType:"text",
                data:"id="+id+"&val="+val,
                success:function(response){
                	alert(response);
                	location.reload(true);
                },
                error:function (xhr, ajaxOptions, thrownError){
                    console.log(thrownError);
                }
            });
			return false;
		});

		jQuery("#submit_comment").click(function(){

			var username =  jQuery("input[name=username]").val();
			var feedback = jQuery(".comment-input").val();
			var product_id = jQuery('input[name=product_id]').val();
			var range_id = jQuery('input[name=range_id]').val();
			if(username == ""){
				jQuery(".error-text").text("<?php echo $error_login; ?>");
				return false;
			}else{
				jQuery(".error-text").text("");
			}

			if(feedback == ""){
				jQuery(".error-text").text("<?php echo $error_comment; ?>");
				return false;
			}else{
				jQuery(".error-text").text("");
			}

			var myData = 'product_id='+ product_id + '&range_id='+ range_id+'&username='+ username + '&feedback='+ feedback;
			jQuery.ajax({
                type: "POST",
                url: "<?php echo JURI::root();?>index.php?option=com_vichy_comment_product&task=saveFeedback",
                dataType:"text",
                data:myData,
                success:function(response){
                	alert(response);
                	location.reload(true);
                },
                error:function (xhr, ajaxOptions, thrownError){
                    console.log(thrownError);
                }
            });
			return false;
		});
	})
    $(".btn-mobi2").click(function(){
        if($(this).attr('aria-expanded') == "false")
        {
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-minus2.png');
        }else
        {
//            $('.btn-mobi').find('img').attr("src",'<?php //echo JURI::root(); ?>//components/com_vichy_product/images/btn-plus.png');
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus2.png');
        }
    });
</script>