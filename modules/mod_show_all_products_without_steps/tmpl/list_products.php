<?php 
	defined('_JEXEC') or die('Restricted access'); 
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_show_all_products_without_steps/css/list_products_without_steps.css');
	$class="class_layout_product_new_without_steps";
	$paging_name="paging_new_without_steps";
	if($layout == 1){
		$class="class_layout_product_favorite";
		$paging_name="paging_favorite";
	}
?>
<?php
//	if(count($list_products)>0)
//	{
 ?>
<div class = "list_products_without_steps">
	<div>
		<div class="list_products_title_without_steps"><?php echo $module->title;?></div>

		<div id="wrap_product_lists_without_steps" class ="list_products_list_without_steps <?php echo $class; ?>">
		
		</div>
	</div>
	<script type="text/javascript" src="modules/mod_show_all_products_without_steps/js/my_script_without_steps.js"></script>
	<div id="com-vichy-product-url_without_steps" data-class="<?php echo $class; ?>" data-layout="<?php echo $layout; ?>" data-paging-name="<?php echo $paging_name; ?>" data-url="<?php echo JURI::root() ?>index.php?option=com_vichy_product&view=product_without_steps&task=getProductWithoutSteps"></div>
</div>
<?php 
//}
?>