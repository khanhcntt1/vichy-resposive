﻿<?php 
	defined('_JEXEC') or die('Restricted access'); 
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_show_all_products/css/list_products.css');
	$class="class_layout_product_new";
	$paging_name="paging_new";
    $check=0;
	$style = 'style="border-top-left-radius:5px; border-top-right-radius:5px;"';
	if($layout == 1){
		$class="class_layout_product_favorite";
		$paging_name="paging_favorite";
		$style = 'style="border-top:none"';
	}
	$session =& JFactory::getSession();
	$lang = $session->get('lang');

	if($lang == 'vi-VN'){
		$lbl_title = $module->title;
		$lbl_text_new = "Mới";
		$lbl_text_favorite = "Yêu thích";
		$lbl_number_review = "người đánh giá";
	}else{		
		$lbl_title = ($module->position == 'new_products') ? "TOP NEW PRODUCTS" : "TOP FAVOURITE PRODUCTS";
		$lbl_text_new = "NEW";
		$lbl_text_favorite = "Top Favorite";
		$lbl_number_review = "reviews";
	}
?>


<?php
	if(count($list_products)>0)
	{
 ?>
 <script>
    $(".sub-menu-spa-btn").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
    $(".dropdown-a").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
        }else
        {
            $('.dropdown-a').find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-minus.png');
        }
    });

</script>
<div class = "list_products" <?php echo $style; ?>>
	<div>
		<div class="list_products_title"><?php echo $lbl_title;?></div>
		<div id="wrap_product_lists" class ="list_products_list <?php echo $class; ?>">
			<?php
			$data="";

			foreach($list_products as $k => $v){
					$rid;$skincare;$product_range;$parent_id;
					$tmp_skincare = array();
					$rids = explode(",",$v->cid);
					foreach ($rids as $pair) {
						$tmp=explode("|",$pair);//parent-child
						// print_r($pair);die();
						if($tmp[0]==PRODUCT_RANG||$tmp[0]==PRODUCT_RANG_WITHOUT_STEP)
							$rid=$tmp[1];
					}
					$p_rs = explode(",",$v->product_range);
					foreach ($p_rs as $r_pair) {
						$tmp=explode("|",$r_pair);//parent-child
						if($tmp[0]==PRODUCT_RANG||$tmp[0]==PRODUCT_RANG_WITHOUT_STEP)
							{
								$product_range=$tmp[1];
								$parent_id=$tmp[0];
							}
							else
							{
								if($tmp[1]!=null&&!empty($tmp[1]))
								$tmp_skincare[]=$tmp[1];
							}
					}
					$link='index.php?option=com_vichy_product&view=product&id='.$v->pid.'&rid='.$rid.'&Itemid=103';
					if($parent_id==PRODUCT_RANG_WITHOUT_STEP)
					 $link.='&no_step=1';
					$tmp_skincare=array_unique($tmp_skincare);
					$skincare=implode(" & ", $tmp_skincare);
					$reviews = !empty( $v->reviews)? $v->reviews:0 ;
					$data .='<div class = "product_item" style="margin-bottom:10px">';
					$data .='<a href="'.JROUTE::_($link).'" style="display:block;">';
				 	$data .='<div class="image_product">';
				 	$data .='<img width="160" src="'.JURI::root().'components/com_vichy_product/uploads/products/'.$v->image.'" />';
				 	$data .='</div>';
				 	//R=#197205 N=#acd180
				 	$data .='<div class="product_range">'.$product_range.'</div>';
				 	$data .='<div class="product_name">'.shortDesc($v->product_name,500).'</div>';
				 	if($v->is_new&&$layout==0)
					{
						$data .='<div class="product_new">'.$lbl_text_new.'</div>';
					}
					if($v->is_favorite&&$layout==1)
					{
						$data .='<div class="product_favorite"><div class="favorite_featured">'.$lbl_text_favorite.'</div></div>';
					}

				 	// $data .='<div class="product_skin_care">'.$skincare.'</div>';
				 	$data.='<div class="product_box_detail">';
				 	$data .='<div class="product_star_review">';
				 	for($j=1; $j<=5; $j++){
			                if(!empty($v->rating) && $v->rating >0 && $j<=$v->rating )
			                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/small_star_blue.png">';
			                else{
			                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/small_star_grey.png">';
			                }
			            }
			        $data .='</div>';//start view end
			        $data .='<div class="product_number_review">'.$reviews.' '.$lbl_number_review.'</div>';
			        $data.='</div>';//box detail end
					$data .='<div style="clear:both;"></div>';
					$data .='</a>';
					$data .='</div>';
				}
				if($pages>1)
				{
					$data .= '<div class="paging '.$paging_name.'" style="clear:both;">';

	 				for($i = 1;$i<=$pages;$i++ )
	 				{
	 					$data.='<a href="javascript:void(0);"'." page=$i ";
	 					if($i==$current_page)
	 						$data.='class ="active"';
	 					$data.=">$i</li>";
	 				}

					$data = $data . "</div>";
				}
				echo $data;
			?>
		</div>
	</div>
	<script type="text/javascript" src="modules/mod_show_all_products/js/my_script.js"></script>
	<div id="com-vichy-product-url" data-class="<?php echo $class; ?>" data-layout="<?php echo $layout; ?>" data-paging-name="<?php echo $paging_name; ?>" data-url="<?php echo JURI::root() ?>index.php?option=com_vichy_product&view=product_landing&task=getProductLanding"></div>
</div>
<?php }?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		var sessionLang = "<?php echo $lang; ?>";
		if(sessionLang == 'en-GB'){
			jQuery('.favorite_featured').css({'font-size':'9px', 'margin-top':'18px'});
		}
	});
</script>