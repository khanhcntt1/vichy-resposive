<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_archive
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once JPATH_SITE.'/components/com_vichy_product/helpers/helper.php';

$product_rang = Vichy_productHelper::getPRODUCT_RANG();

$group_id = $product_rang;
$list = Vichy_productHelper::getCategoriesByGroup($group_id);

require JModuleHelper::getLayoutPath('mod_product_range_diagnosis_advice', 'list_products');

