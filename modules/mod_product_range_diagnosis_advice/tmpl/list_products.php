<?php 
	defined('_JEXEC') or die('Restricted access'); 
	$document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_product/css/style.css');
?>
<div class="wrap_list_product_ranges">
<div class="wrap_list_product_ranges_content" style="border-top-left-radius:5px;border-top-right-radius:5px;">
	<?php
	$color = '';
	foreach($list as $k => $v){ 
		switch ($v->main_color) {
			case 'normaderm':
				$color = '#197205';
				break;
			case 'capital':
				$color = '#ff8b06';
				break;
			case 'thermal':
				$color = '#82add8';
				break;
			case 'aqualia_thermal':
				$color = '#95c7ea';
				break;
			case 'bi_white':
				$color = '#c7c7c7';
				break;
			case 'destock':
				$color = '#32d2ae';
				break;
			case 'dercos':
				$color = '#8e090c';
				break;
			case 'liftactiv':
				$color = '#6cafc9';
				break;
			case 'purete_thermal':
				$color = '#04a0db';
				break;
			default :
				$color = '#197205';
				break;
		}
		$data = json_decode($v->params,true);
		$image = (!empty($data['image'])) ? $data['image']: 'components/com_vichy_product/uploads/product_range/bg.png';
	?>
	<a href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->id.'&Itemid=103'); ?>" style="display:block;">
		<div class="wrap_product_range_item" <?php echo ($k==0) ? 'style="padding-top:7px"' : ''; ?>>
			
			<div class="product_range_name" style="background:<?php echo $color; ?>">
				<span><?php echo $v->title; ?></span>
				<span class="desc_range"><?php echo $v->short_desc; ?></span>
			</div>
			<div class="product_range_image">
				<img width="924" src="<?php echo JURI::root().$image; ?>" />
			</div>
		</div>
	</a>
	<?php } ?>
</div>
</div>
