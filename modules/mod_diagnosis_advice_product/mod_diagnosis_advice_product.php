<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_archive
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

// $layout = $params->get('tmp_layout',0);

// if($layout==0){
// 		$list_products = modDiagnosisAdviceProduct::getNewProducts($params);
// 	}else if($layout == 1){
// 		$list_products = modDiagnosisAdviceProduct::getFavoriteProducts($params);
// 	}

// $params->def('count', 10);
// $moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));


// $list = modArchiveHelper::getList($params);

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();
if($language_tag == 'vi-VN'){
	$title_advice = $params->get('title_advice','Lời khuyên');
	$title_diagnosis = $params->get('title_diagnosis','Kiểm tra da');

	$text_advice = $params->get('text_advice','');
	$text_diagnosis = $params->get('text_diagnosis','');
}else{
	$title_advice = $params->get('title_advice_en','Advice');
	$title_diagnosis = $params->get('title_diagnosis_en','Check your skin');

	$text_advice = $params->get('text_advice_en','');
	$text_diagnosis = $params->get('text_diagnosis_en','');
}

require JModuleHelper::getLayoutPath('mod_diagnosis_advice_product', 'list_products');

