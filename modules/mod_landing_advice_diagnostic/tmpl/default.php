<?php 
	defined('_JEXEC') or die('Restricted access'); 
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_landing_advice_diagnostic/css/style.css');
?>
<div class="wrap_advice_content">
	<div class="wrap_advice_content_left">
		<div class = "vc_advice_main box_shadow">
			<a class ="vc_advice_start_button" href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107'); ?>">
				<div class ="vc_advice_content" style="background: url('<?php echo $bg_skin_test?>') no-repeat" >
					<div class = "vc_advice_text">
						<div class ="vc_advice_title"><?php echo $title_skin_test; ?></div>
						<div class ="vc_advice_short_description"><?php echo $text_skin_test; ?></div>
						<div class ="vc_advice_full_description"></div>
					</div>
				</div>
			</a>	
		</div>

		<div class = "vc_advice_main box_shadow">
			<a class ="vc_advice_start_button" href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice&Itemid=107'); ?>">
				<div class ="vc_advice_content" style="background: url('<?php echo $bg_advice?>') no-repeat" >
					<div class = "vc_advice_text">
						<div class ="vc_advice_title"><?php echo $title_advice; ?></div>
						<div class ="vc_advice_short_description"><?php echo $text_advice; ?></div>
						<div class ="vc_advice_full_description"></div>
					</div>
				</div>
			</a>
		</div>

		<div class = "vc_advice_main box_shadow">
			<a href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=dictionary&Itemid=107'); ?>">
				<div class ="vc_advice_content" style="background: url('<?php echo $bg_dictionary?>') no-repeat" >
					<div class = "vc_advice_text">
						<div class ="vc_advice_title"><?php echo $title_dictionary; ?></div>
						<div class ="vc_advice_short_description"><?php echo $text_dictionary; ?></div>
						<div class ="vc_advice_full_description"></div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="wrap_advice_content_right">
		<div class="buy_product box_shadow">
			<div class="buy_product_inner">
				<div class="top_buy_product_online"><?php echo $lbl_buy_product_online; ?></div>
				<div class="bottom_buy_product_online" style="positon:relative">
					<div class="buy_product_inner_1"><?php echo $lbl_buy_product_inner_1; ?></div>
					<a id="muahangonl2" class="buy_product_inner_offline"><?php echo $lbl_link_buy_online; ?></a>
					<div class="muaonline">
                      <a href="http://www.chon.vn/cua-hang/vichy.aspx?ref=home-merchant" target="_blank"><img src="<?php echo JURI::root();?>templates/vichy/images/tmp/chonvn.png"></a>
                      <a href="http://www.yes24.vn/khuyen-mai/471450/vichy-khuyen-mai.html" target="_blank"><img src="<?php echo JURI::root();?>templates/vichy/images/tmp/yes24com.png"></a>
                      <a href="http://www.lazada.vn/vichy/" target="_blank"><img src="<?php echo JURI::root();?>templates/vichy/images/tmp/lazada.png"></a>
                      <a href="http://tiki.vn/thuong-hieu/vichy.html" target="_blank"><img src="<?php echo JURI::root();?>templates/vichy/images/tmp/tiki.png"></a>
        			</div>
					<div class="buy_product_inner_1" style="font-weight:bold"><?php echo $lbl_message_buy; ?></div>
					<a class="buy_product_inner_offline" href="<?php echo JRoute::_('index.php?option=com_vichy_store&Itemid=113'); ?>"><?php echo $lbl_link_store; ?></a>
					
				</div>
			</div>
		</div>
		
		<script type="text/javascript" language="javascript">
                                $(document).ready(function(e) {
                                    $(".muaonline").hide();
                                    $("#muahangonl2").click(function(){
                                        $(".muaonline").toggle("slow");
                                    });
                                    // $("#muahangonl").click(function(){
                                    //     $(".common-buy-online-menu").toggle("slow");
                                    // });
                                    // $("#login-mobi").click(function(){
                                    //     $(".login-popup-mobi").toggle("slow");
                                    // });
                               });
                  </script>
		<?php 
			$user = JFactory::getUser();
			$text = $lbl_mess_my_skin;
			$link = JRoute::_('index.php?option=com_users&view=registration');
			if(!empty($user->username)){
				// $text = 'trả lời thêm khảo sát trong tài khoản my skin để nhận mẩu thử';
				$link = JRoute::_('index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107');
			}
		?>
		<div class="buy_product box_shadow" style="margin-top:12px;height:277px;">
			<a href="<?php echo $link; ?>">
				<img src="<?php echo JURI::root(); ?>timbthumb.php?src=templates/vichy/images/myskin.jpg&w=208" />
				<div class="myskin"><?php echo $lbl_myskin; ?></div>
				<div class="myskin_desc"><?php echo $lbl_myskin_desc; ?></div>
				<div class="myskin_btn"><?php echo $lbl_join_now; ?></div>
			</a>
		</div>
	</div>
</div>