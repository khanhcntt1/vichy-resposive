<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_archive
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$layout = $params->get('tmp_layout',0);

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();
if($language_tag == 'vi-VN'){
	$title_skin_test = $params->get('title_skin_test','Kiểm tra da');
	$text_skin_test = $params->get('text_skin_test','');
	$content_skin_test = $params->get('content_skin_test','');
	$detail_skin_test = $params->get('detail_skin_test','');
	$bg_skin_test = $params->get('bg_skin_test','');

	$title_advice = $params->get('title_advice','Lời khuyên');
	$text_advice = $params->get('text_advice','');
	$content_advice = $params->get('content_advice','');
	$detail_advice = $params->get('detail_advice','');
	$bg_advice = $params->get('bg_advice','');

	$title_dictionary = $params->get('title_dictionary','Từ điển da');
	$text_dictionary = $params->get('text_dictionary','');
	$content_dictionary = $params->get('content_dictionary','');
	$detail_dictionary = $params->get('detail_dictionary','');
	$bg_dictionary = $params->get('bg_dictionary','');


    $lbl_buy_product_online = "BẠN CÓ THỂ MUA SẢN PHẨM VICHY TẠI";
    $lbl_buy_product_inner_1 = "MUA HÀNG TRỰC TUYẾN";
    $lbl_link_buy_online = "MUA ONLINE";
    $lbl_message_buy = "HAY ĐẾN CỬA HÀNG GẦN NHẤT ĐỂ ĐƯỢC TƯ VẤN CHUYÊN SÂU";
    $lbl_link_store = "CỬA HÀNG";
    $lbl_mess_my_skin = 'Tham gia câu lạc bộ MYSKIN để cập nhật thông tin mới nhất và nhận nhiều ưu đãi đặc biệt.';
    $lbl_myskin = "MY SKIN";
    $lbl_myskin_desc = "Tham gia câu lạc bộ MYSKIN để cập nhật thông tin mới nhất và nhận nhiều ưu đãi đặc biệt.";
    $lbl_join_now = "THAM GIA NGAY";
}else{

	$title_skin_test = $params->get('title_skin_test_en','Skin diagnostis');
	$text_skin_test = $params->get('text_skin_test_en','');
	$content_skin_test = $params->get('content_skin_test_en','');
	$detail_skin_test = $params->get('detail_skin_test_en','');
	$bg_skin_test = $params->get('bg_skin_test','');

	$title_advice = $params->get('title_advice_en','Advice');
	$text_advice = $params->get('text_advice_en','');
	$content_advice = $params->get('content_advice_en','');
	$detail_advice = $params->get('detail_advice_en','');
	$bg_advice = $params->get('bg_advice','');

	$title_dictionary = $params->get('title_dictionary_en','Skin dictionary');
	$text_dictionary = $params->get('text_dictionary_en','');
	$content_dictionary = $params->get('content_dictionary_en','');
	$detail_dictionary = $params->get('detail_dictionary_en','');
	$bg_dictionary = $params->get('bg_dictionary','');


    $lbl_buy_product_online = "YOU CAN BUY PRODUCTS IN VICHY";
    $lbl_buy_product_inner_1 = "CLICK TO BUY ONLINE";
    $lbl_link_buy_online = "BUY ONLINE";
    $lbl_message_buy = "OR TO NEAREST STORE FOR INTENSIVE COUNSELING";
    $lbl_link_store = "STORE";
    $lbl_mess_my_skin = 'Join the MYSKIN Club to update the latest information and receive many special privileges.';
    $lbl_myskin = "MY SKIN";
    $lbl_myskin_desc = "Join the MYSKIN Club to update the latest information and receive many special privileges.";
    $lbl_join_now = "JOIN NOW";        
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_landing_advice_diagnostic', 'default');

