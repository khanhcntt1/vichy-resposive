<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_archive
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();
if($language_tag == 'vi-VN'){
	$text = $params->get('text','Cùng Vichy chia sẻ những bí quyết làm đẹp và những câu chuyện thú vị về cuộc sống.');
}else{
	$text = $params->get('text','Vichy share the same mystique and interesting stories about life.');
}

$bg = $params->get('image','components/com_content/views/category/images/bg_banner_blog.jpg');

require JModuleHelper::getLayoutPath('mod_banner_blog', 'default');

