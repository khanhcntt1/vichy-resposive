jQuery(document).ready(function() {
	base_url = jQuery('#com-vichy-product-url').attr('data-url');
	$(".list_products").each(function(){
		layout = jQuery(this).find("#com-vichy-product-url").attr('data-layout');	
		cl = jQuery(this).find("#com-vichy-product-url").attr('data-class');
		paging_name=jQuery(this).find("#com-vichy-product-url").attr('data-paging-name');
		
		pagination(1,layout,cl,paging_name);  		
		
	});

jQuery(".class_layout_product_new").on('click',".paging_new >a",function(){
	    page = jQuery(this).attr('page');
	    	pagination(page,0,'class_layout_product_new','paging_new');	    	
	    	
	}); 
jQuery(".class_layout_product_favorite").on('click',".paging_favorite >a",function(){
	   		page = jQuery(this).attr('page');
	    	pagination(page,1,'class_layout_product_favorite','paging_favorite');	    	
	    	
		});

	function pagination(page,layout,cl,paging_name){
	    // jQuery('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast');                 
	    jQuery.ajax ({
	        type: "POST",
	        url: base_url,
	        data: "page="+page+'&layout='+layout+'&paging_name='+paging_name,
	        success: function(data_page) { 
	            // jQuery('#loading').fadeOut('fast');
	      
	            jQuery("."+cl).html(data_page);  
	        }
	    });
	}
});