<?php 
	defined('_JEXEC') or die('Restricted access'); 
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_banner_blog/css/style.css');
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo FB_APP_ID; ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div class="vichy-blog">
	<div class="box">
	    <div id="carousel" style="background:url(<?php echo JURI::root().$bg ?>)">
	        <img id="skin-life" src="<?php echo JURI::root();?>components/com_content/views/category/images/skin_life.png" alt="Banner">
	        <div class="desc_banner">
	        	<div class="text"><?php echo $text; ?></div>
	        	<div class="social">
	        		<div class="fb-like" data-href="<?php echo JROUTE::_('index.php?option=com_content&view=category&layout=blog&id=29&Itemid=109'); ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
	        	</div>
	        </div>
	    </div>
	</div> <!-- slider -->
</div>