﻿<?php
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
$document = & JFactory::getDocument();
$document->addStyleSheet('modules/mod_vichy_product_step/css/style.css');
// $color1 = '';
// $color2 = '';
foreach($steps as $v){
	if(!empty($v->main_color)){
		switch ($v->main_color) {
// 			case 'normaderm':
// 				$color1 = '#197205';
// 				$color2 = '#9cc46e';
// 				break;
// 			case 'capital':
// 				$color1 = '#ff8b06';
// 				$color2 = '#9cc46e';
// 				break;
// 			case 'thermal':
// 				$color1 = '#2f4c9d';
// 				$color2 = '#9cc46e';
// 				break;
			case 'normaderm':
				//$color1 = '#197205';
				$border_color='#197205';
				break;
			case 'capital':
				//$color1 = '#ff7200';
				$border_color='#ff8b06';
				break;
			case 'thermal':
				//$color1 = '#448ac9';
				$border_color='#82add8';
				break;
			case 'aqualia_thermal':
			    	//$color1 = '#448ac9';
			    	$border_color='#95c7ea';
			   	 break;
			case 'bi_white':
			    	$color1 = '#717e85';
			    	$border_color='#c7c7c7';
			    	break;
			case 'destock':
			    	//$color1 = '#099575';
			    	$border_color='#32d2ae';
			    	break;
			case 'dercos':
			    	//$color1= '#8b0305';
			    	$border_color='#8e090c';
			    	break;
			case 'liftactiv':
			    	//$color1 = '#1a74a3';
			    	$border_color='#6cafc9';
			    	break;
			case 'purete_thermal':
			    	//$color1 = '#448ac9';
			    	$border_color='#04a0db';
			    	break;
		}
		// $bg1 = $v->main_color.'_1.png';
		// $bg2 = $v->main_color.'_2.png';
		// break;
	}
}
$current_product_id = $_GET['id'];

if($language_tag == 'vi-VN'){
	$text_reviews = "người đánh giá";
}else{
	$text_reviews = "reviews";
}

?>
	<link href="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
	<!-- custom scrollbars plugin -->
	<script src="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
	<script>
		(function($){
			$(window).load(function(){
				$("#product_steps_id").mCustomScrollbar({
					autoHideScrollbar:false,
					theme:"light-thin"
				});
			});
		})(jQuery);
	</script>


<?php if(count($steps)){ ?>
<div class="wrapper_product_step">
<div class="wrapper_product_step_content box_shadow">
	<div class="product_steps" id="product_steps_id">
		<?php $c=1; ?>
		<?php foreach($steps as $k => $v){ ?>
		<?php 
			$arr = explode(',', $v->product_range);
			$product_range = '';
			foreach ($arr as $value) {
					$titlelist = explode('|', $value);
					if(count($titlelist)>1 && $titlelist[0]==$_product_rang_without_step)
					{
						$product_range = $titlelist[1];
						break;					
					}
				
			}
		?>
		<!-- product item -->
		<a href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$v->product_id.'&rid='.$v->category_id.'&no_step=1'); ?>">
		<div class="product_item border_step" style="float:left;">
			<?php 	
					if($v->product_id==$current_product_id)
					{ 
				 ?>
				 <div class='wrap_current_product' style="border: 1px solid <?php echo $border_color;?>;">
				 <?php 
					}
				 ?>
			<div class="image_product">
				<img width="147" height="290" src="<?php echo JURI::root() ?>components/com_vichy_product/uploads/products/<?php echo $v->image; ?>" />
			</div>
			<div class="product_range" >
			<?php 
			
			
			
			echo $product_range;
			
			?>
			</div>
			<div class="product_name" ><?php echo shortDesc($v->product_name,500); ?></div>
			
			<!-- <div class="product_skin_care"> -->
			<?php 
			
			// $arr = explode(',', $v->product_range);
			// $skincare = null;
			// foreach ($arr as $value) {
			// 		$skincarelist = explode('|', $value);
			// 		if(count($skincarelist)>1 && $skincarelist[0]==SKIN_CARE)
			// 		{
			// 			  if(empty($skincare))
			// 			  {
			// 			  	 $skincare .=$skincarelist[1];
			// 			  }else {
			// 			  	$skincare .=','.$skincarelist[1];
			// 			  }
						  					
			// 		}
				
			// }
			// echo $skincare; ?>
			
			<!-- </div> -->
			<?php if($v->is_new){ ?>
			<div class="product_new">Mới</div>
			<?php }?>
			<div class="product_box_detail">
			<div class="product_star_review">
	 		<?php for($j=1; $j<=5; $j++){ ?>
                <?php if(!empty($v->rating) && $v->rating >0 && $j<=$v->rating ){ ?>
				<img style="border:none;float:left;" src="<?php echo JURI::root() ?>media/small_star_blue.png">
                <?php }else{ ?>
				<img style="border:none;float:left;" src="<?php echo JURI::root() ?>media/small_star_grey.png">
                <?php } ?> 
            <?php } ?>
			</div>
			<div style="clear:both;"></div>
			<div class="product_number_review"><?php  echo !empty( $v->reviews)? $v->reviews:0 ;?> <?php echo $text_reviews; ?></div>
		</div>
		<?php
					if($v->product_id==$current_product_id)
					{
				 ?>
				</div>
				 <?php 
					}
				 ?>
		</div>
		</a>
		<!-- end product item -->

		<?php if($c==5){ ?>
		<div style="clear:both;"></div>
		<?php } ?>

		<?php } //end foreach ?>
	</div>
</div>
</div>
<?php } ?>