<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_feed
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class modVichyStepHelper
{
	static function getStep($range_id, $language_tag)
	{
		$db = & JFactory::getDbo();
        // $query = "SELECT s.product_id,s.category_id,s.no_of_steps,p.name, p.is_new, p.rating, p.reviews, c.main_color, sn.step_name FROM `vc_vichy_step` as s join `vc_vichy_product` as p on p.id = s.product_id join `vc_categories` as c on c.id = s.category_id join `vc_vichy_step_name` as sn on s.no_of_steps = sn.no_of_steps where s.category_id = $range_id";
     //    $query ="SELECT tb1.name as product_name,tb1.title as product_range,tb1.main_color,tb1.rating,tb1.reviews,tb1.image,tb1.step_name,tb1.pid as product_id,tb1.category_id,tb1.no_of_steps,tb1.is_new,tb2.skincare
					// from (
					// 		(
					// 			select sl.*,vt.parent_id as type 
					// 				from (
					// 						select p.id as pid,p.name,c.title,p.rating,p.reviews,p.image,p.is_new,s.no_of_steps,sn.step_name,c.id as cid, c.main_color,s.category_id as category_id
					// 							from vc_vichy_product p 
					// 							join vc_vichy_product_category pc on p.id=pc.product_id 
					// 							join vc_categories c on c.id=pc.category_id 

					// 							join vc_vichy_step s on p.id=s.product_id

					// 							join vc_vichy_step_name sn on s.no_of_steps = sn.no_of_steps and sn.category_id=pc.category_id

					// 							where s.category_id=$range_id												
					// 						) as sl 
					// 				join vc_categories vt on sl.cid = vt.id 
					// 				where vt.parent_id in (8)
					// 		)as tb1 
					// left join (
					// 	select p.id as pid,c.parent_id,
					// 			case when c.parent_id=14 then Group_concat(c.title SEPARATOR ' & ') end as skincare
					// 			,c.id as cid
					// 					from vc_vichy_product p 
					// 					join vc_vichy_product_category pc on p.id=pc.product_id 
					// 					join vc_categories c on c.id=pc.category_id                                                                                
     //                                    where c.parent_id=14
     //                                    group by p.id,c.parent_id                                                                               
                                              					
					// 			) as tb2 on tb1.pid =tb2.pid
					// 	) order by tb1.no_of_steps asc
     //    ";
		//$query = "SELECT s.*, c.title as product_range, p.is_new, c.main_color, p.name as product_name, p.rating, p.reviews, p.image, sn.step_name from vc_vichy_step as s left join vc_vichy_product as p on s.product_id = p.id join vc_vichy_step_name as sn on sn.category_id = s.category_id and sn.no_of_steps = s.no_of_steps join vc_categories as c on c.id = s.category_id where s.category_id = $range_id order by s.no_of_steps";
        
		//fixed
		$query = "(SELECT s.id, a.id AS product_id,a.is_new,a.is_favorite, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( c.parent_id, '|', b.category_id ) AS category_id, GROUP_CONCAT( DISTINCT (
					c.parent_id
					) ) AS parent_id, GROUP_CONCAT( c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( DISTINCT(c.main_color) ) as main_color , s.no_of_steps, sn.step_name
					FROM vc_vichy_product AS a
					INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.id
					INNER JOIN vc_categories AS c ON b.category_id = c.id
					INNER JOIN vc_vichy_step AS s ON s.product_id = a.id
					INNER JOIN vc_vichy_step_name AS sn ON sn.category_id = s.category_id
					AND s.no_of_steps = sn.no_of_steps
					WHERE s.category_id = $range_id
					AND a.published =1
					AND a.language
					IN (
					 'vi-VN',  '*'
					)
					GROUP BY a.id
					)
UNION
(SELECT s.id, 0 AS product_id, '' AS is_new,'' AS is_favorite, '' AS product_name,  '' AS created_product,  '' AS rating,  '' AS reviews,  '' AS image, '' AS category_id,  '' AS parent_id,  '' AS product_range,  '' AS main_color, s.no_of_steps, sn.step_name
FROM  `vc_vichy_step` AS s
JOIN vc_vichy_step_name AS sn ON sn.category_id = s.category_id
AND sn.no_of_steps = s.no_of_steps
WHERE s.`product_id` =0
AND s.`category_id` =$range_id
)
order by no_of_steps, id

";				
        if($language_tag == 'vi-VN'){
            $query = $query;
        }else{
        	$jfManager      = JoomFishManager::getInstance();
		    $table = 'categories';
		    $reference_id = $jfManager->getReferenceID($language_tag, $range_id, $table);
		    $range_id = $reference_id;

            $query = "(SELECT s.id, a.id AS product_id,a.is_new,a.is_favorite, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( c.parent_id, '|', b.category_id ) AS category_id, GROUP_CONCAT( DISTINCT (
					c.parent_id
					) ) AS parent_id, GROUP_CONCAT( c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( DISTINCT(c.main_color) ) as main_color , s.no_of_steps, sn.step_name
					FROM #__vichy_product AS a
					INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.origin_id
					INNER JOIN vc_categories AS c ON b.category_id = c.id
					INNER JOIN vc_vichy_step AS s ON s.product_id = a.origin_id
					INNER JOIN vc_vichy_step_name AS sn ON sn.category_id = s.category_id
					AND s.no_of_steps = sn.no_of_steps and sn.language='en-GB'
					WHERE s.category_id = $range_id
					AND a.published =1
					AND a.language
					IN (
					 'en-GB',  '*'
					)
					GROUP BY a.id
					)
UNION
(SELECT s.id, 0 AS product_id, '' AS is_new,'' AS is_favorite, '' AS product_name,  '' AS created_product,  '' AS rating,  '' AS reviews,  '' AS image, '' AS category_id,  '' AS parent_id,  '' AS product_range,  '' AS main_color, s.no_of_steps, sn.step_name
FROM  `vc_vichy_step` AS s
JOIN vc_vichy_step_name AS sn ON sn.category_id = s.category_id
AND sn.no_of_steps = s.no_of_steps
WHERE s.`product_id` =0
AND s.`category_id` =$range_id and sn.language='en-GB'
)
order by no_of_steps, id";
        }
		
		$db->setQuery($query);
        
        $result = $db->loadObjectList();
        return $result;
	}

	static function getRangeNoStep($range_id, $lang_code){
		$db = & JFactory::getDbo();	        

		$query = "SELECT a.id AS product_id, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( b.category_id ) AS category_id, GROUP_CONCAT( DISTINCT (
					c.parent_id
					) ) AS parent_id, GROUP_CONCAT( c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( c.main_color ) as main_color
					FROM #__vichy_product AS a
					INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.id
					INNER JOIN vc_categories AS c ON b.category_id = c.id
					WHERE c.id = $range_id
					AND a.published =1
					AND a.language
					IN (
					 '".$lang_code."',  '*'
					)
					GROUP BY a.id
					";
        $db->setQuery($query);
		$result = $db->loadObjectList();
		
        return $result;
	}
}
