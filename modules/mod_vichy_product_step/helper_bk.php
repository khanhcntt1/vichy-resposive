<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_feed
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class modVichyStepHelper
{
	static function getStep($range_id)
	{
		$db = & JFactory::getDbo();
        // $query = "SELECT s.product_id,s.category_id,s.no_of_steps,p.name, p.is_new, p.rating, p.reviews, c.main_color, sn.step_name FROM `vc_vichy_step` as s join `vc_vichy_product` as p on p.id = s.product_id join `vc_categories` as c on c.id = s.category_id join `vc_vichy_step_name` as sn on s.no_of_steps = sn.no_of_steps where s.category_id = $range_id";
        $query ="SELECT tb1.name as product_name,tb1.title as product_range,tb1.main_color,tb1.rating,tb1.reviews,tb1.image,tb1.step_name,tb1.pid as product_id,tb1.category_id,tb1.no_of_steps,tb1.is_new,tb2.skincare
					from (
							(
								select sl.*,vt.parent_id as type 
									from (
											select p.id as pid,p.name,c.title,p.rating,p.reviews,p.image,p.is_new,s.no_of_steps,sn.step_name,c.id as cid, c.main_color,s.category_id as category_id
												from vc_vichy_product p 
												join vc_vichy_product_category pc on p.id=pc.product_id 
												join vc_categories c on c.id=pc.category_id 

												join vc_vichy_step s on p.id=s.product_id

												join vc_vichy_step_name sn on s.no_of_steps = sn.no_of_steps and sn.category_id=pc.category_id

												where s.category_id=$range_id												
											) as sl 
									join vc_categories vt on sl.cid = vt.id 
									where vt.parent_id in (8)
							)as tb1 
					left join (
						select p.id as pid,c.parent_id,
								case when c.parent_id=14 then Group_concat(c.title SEPARATOR ' & ') end as skincare
								,c.id as cid
										from vc_vichy_product p 
										join vc_vichy_product_category pc on p.id=pc.product_id 
										join vc_categories c on c.id=pc.category_id                                                                                
                                        where c.parent_id=14
                                        group by p.id,c.parent_id                                                                               
                                              					
								) as tb2 on tb1.pid =tb2.pid
						) order by tb1.no_of_steps asc
        ";
        $db->setQuery($query);
        
        $result = $db->loadObjectList();
        return $result;
	}
}
