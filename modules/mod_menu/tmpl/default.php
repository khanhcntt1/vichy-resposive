<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
$document = & JFactory::getDocument();
$document->addStyleSheet('modules/mod_menu/css/style.css');

$lang_code = $arr_option['lang_code'];
if($lang_code == 'vi-VN'){
	$lbl_search = "Tìm kiếm sản phẩm";
	$lbl_new_product = "Sản phẩm mới nhất";
	$lbl_favorite_product = "Sản phẩm yêu thích";
	$lbl_skin_needs = "NHU CẦU CỦA BẠN";
	$lbl_skin_care = "CHỨC NĂNG SẢN PHẨM";
	$lbl_vichy_ranges = "CÁC DÒNG SẢN PHẨM";
	$lbl_all_products = "TẤT CẢ DÒNG SẢN PHẨM";
	$title_all_products = "TẤT CẢ DÒNG SẢN PHẨM";
}else{
	$lbl_search = "Search a product";
	$lbl_new_product = "TOP NEW PRODUCTS";
	$lbl_favorite_product = "TOP FAVOURITE PRODUCTS";
	$lbl_skin_needs = "SKIN NEEDS";
	$lbl_skin_care = "SKIN CARE";
	$lbl_vichy_ranges = "VICHY RANGES";
	$lbl_all_products = "ALL PRODUCTS";
	$title_all_products = "SEE ALL VICHY PRODUCTS";
}

$arr = array();
foreach ($list_cate_yourneed as $v) {
	$pa=$v->parent_id;
	$arr[$pa][] = $v;
}

// Note. It is important to remove spaces between elements.
?>
<script type="text/javascript">
	$(document).ready(function(e) {
		if(window.innerWidth>992) {
			$('#popup-product').hover(function (e) {
				var el = $(this).find('.messagepop').css({
					'visibility': 'visible',
					'opacity': '1',
					'transition-delay': '0s'
				});

			}, function (e) {
				var el = $(this).find('.messagepop').css({
					'visibility': 'hidden',
					'opacity': '0',
					'transition': 'visibility 0s linear 0.3s,opacity 0.3s linear'
				});
			});
		}
    });
</script>
<ul class="menu<?php echo $class_sfx;?>"<?php
	$tag = '';
	if ($params->get('tag_id')!=NULL) {
		$tag = $params->get('tag_id').'';
		echo ' id="'.$tag.'"';
	}
?>>
<?php
foreach ($list as $i => &$item) :
	$class = 'item-'.$item->id;
	if ($item->id == $active_id) {
		$class .= ' current';
	}

	if (in_array($item->id, $path)) {
		$class .= ' active';
	}
	elseif ($item->type == 'alias') {
		$aliasToId = $item->params->get('aliasoptions');
		if (count($path) > 0 && $aliasToId == $path[count($path)-1]) {
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path)) {
			$class .= ' alias-parent-active';
		}
	}

	if ($item->deeper) {
		$class .= ' deeper';
	}

	if ($item->parent) {
		$class .= ' parent';
	}

	if (!empty($class)) {
		$class = ' class="'.trim($class) .'"';
	}

	if(strpos($item->link, 'com_vichy_product')){
		$popTag = 'id="popup-product" style="height:45px;"';
		echo '<li'.$class.''.$popTag.'>';
	}else{
		echo '<li'.$class.'>';
	}

	

	// Render the menu item.
	switch ($item->type) :
		case 'separator':
		case 'url':
		case 'component':
			require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
			break;

		default:
			require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
			break;
	endswitch;

	if(strpos($item->link, 'com_vichy_product')){
?>
<div class="messagepop pop">
	   <div class="search-box">
		 <!-- <input type="text" placeholder="Search a product" id="keyword" name="keyword" class="inputbox">
		<input type="button" id="btn-search" class="btn-search">-->
		<input class="inputbox" type="text" name="query" placeholder="<?php echo $lbl_search; ?>" id="autocomplete-ajax-popup" />
		<input class="btn-search"  type="button" id="btn-search2">
		<div class="clear"></div>
			<div class="link-box">
				<div class="link_box_sub">
					<a class="link_box_sub_a" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing&Itemid=103'); ?>"><?php echo $lbl_new_product; ?></a>
				</div>
				<div class="link_box_sub">
					<a class="link_box_sub_a" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing&Itemid=103'); ?>"><?php echo $lbl_favorite_product; ?></a>
				</div>		   		
		   </div>
	   </div>
		


		<div class="product-content">
			<div class="str_cl" style="width:246px;">
				<h4><?php echo $lbl_skin_needs; ?></h4>
				<?php foreach($arr[$arr_option['your_need_id']] as $v){ ?>
					<?php if(isset($arr[$v->id])){ ?>
					<div class="sub_1"><b><?php echo $v->child_title; ?></b></div>
					<?php foreach($arr[$v->id] as $v1){ ?>
					<div class="sub_2"><a class="sub_2_a" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$v1->id.'&Itemid=103'); ?>"><?php echo $v1->child_title; ?></a></div>
				<?php }}} ?>

				<?php foreach($arr[$arr_option['your_need_id']] as $v){ ?>
					<?php if(!isset($arr[$v->id])){ ?>
					<div class="sub_1">
						<a class="sub_1_a" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$v->id.'&Itemid=103'); ?>"><?php echo $v->child_title; ?></a>
					</div>
				<?php }}?>
				
				
			</div>
			<div class="str_cl">
				<h4><?php echo $lbl_skin_care; ?></h4>
				<?php foreach ($list_cate as $r) {?>
				<?php if($r->parent_id == $arr_option['skin_care_id']){?>
					<div class="sub_main">
						<a class="sub_main_a" title="<?php echo $r->title; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$r->id.'&Itemid=103'); ?>"><?php echo $r->title; ?></a>
					</div>
					<?php } ?>
				<?php } ?>
			
			</div>
			<div class="str_cl pos">
				<h4><?php echo $lbl_vichy_ranges; ?></h4>
				<?php foreach ($list_cate as $r) {?>
					<?php if($r->parent_id == $arr_option['product_rang_id']){?>
						<div class="sub_main">
							<a class="sub_main_a" title="<?php echo $r->title; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$r->id.'&Itemid=103'); ?>"><?php echo mb_strtoupper($r->title); ?></a>
						</div>
					<?php }?>
				<?php } ?>
				<?php foreach ($list_cate as $r) {?>
					<?php if($r->parent_id == $arr_option['product_rang_without_step_id']){?>
						<div class="sub_main">
							<a class="sub_main_a" title="<?php echo $r->title; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$r->id.'&no_step=1&Itemid=103'); ?>"><?php echo mb_strtoupper($r->title); ?></a>
						</div>
					<?php }?>
				<?php } ?>
				<div class="sub_main">
					<a class="sub_main_a menu_all_product" style="font-weight: bold;" title="<?php echo $title_all_products; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing&Itemid=103'); ?>"><?php echo $lbl_all_products; ?></a>
				</div>
			</div>
		</div>

</div>
<?php
	}

	// The next item is deeper.
	if ($item->deeper) {
		echo '<ul>';
	}
	// The next item is shallower.
	elseif ($item->shallower) {
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	// The next item is on the same level.
	else {
		echo '</li>';
	}
endforeach;
?></ul>
