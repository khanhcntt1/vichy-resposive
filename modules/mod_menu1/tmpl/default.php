<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
$document = & JFactory::getDocument();
//$document->addStyleSheet('modules/mod_menu/css/style.css');

$lang_code = $arr_option['lang_code'];
if($lang_code == 'vi-VN'){
	$lbl_search = "Tìm kiếm sản phẩm";
	$lbl_new_product = "Sản phẩm mới nhất";
	$lbl_favorite_product = "Sản phẩm yêu thích";
	$lbl_skin_needs = "NHU CẦU CỦA BẠN";
	$lbl_skin_care = "CHỨC NĂNG SẢN PHẨM";
	$lbl_vichy_ranges = "CÁC DÒNG SẢN PHẨM";
	$lbl_all_products = "TẤT CẢ DÒNG SẢN PHẨM";
	$title_all_products = "TẤT CẢ DÒNG SẢN PHẨM";
}else{
	$lbl_search = "Search a product";
	$lbl_new_product = "TOP NEW PRODUCTS";
	$lbl_favorite_product = "TOP FAVOURITE PRODUCTS";
	$lbl_skin_needs = "SKIN NEEDS";
	$lbl_skin_care = "SKIN CARE";
	$lbl_vichy_ranges = "VICHY RANGES";
	$lbl_all_products = "ALL PRODUCTS";
	$title_all_products = "SEE ALL VICHY PRODUCTS";
}

$arr = array();
foreach ($list_cate_yourneed as $v) {
	$pa=$v->parent_id;
	$arr[$pa][] = $v;
}

// Note. It is important to remove spaces between elements.
?>
<nav class="navbar mobile-style" style="display: none" role="navigation">
	<div class="container">
		<!--       --><?php // var_dump($list)?>
		<div class="navbar-header sub-menu-spa-header">
			<a class="navbar-brand" href="#" style="color:white;">Sản phẩm</a>
			<button type="button" class="navbar-toggle collapsed sub-menu-spa-btn plus1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			</button>
		</div>
		<div class="collapse navbar-collapse over-breadrum" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav" style="margin-top:0px;background: #123f83;">
				<!--                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
				<!--                    <li><a href="#">Link</a></li>-->
				<li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a <?php echo ($active=='3') ? 'active' : '' ?>"  href="<?php echo JURI::root(); ?>index.php?option=com_vichy_product&view=product_landing&Itemid=103">Sản phẩm mới nhất
					</a></li>
				<li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a <?php echo ($active=='3') ? 'active' : '' ?>"  href="<?php echo JURI::root(); ?>index.php?option=com_vichy_product&view=product_landing&Itemid=103">Sản phẩm yêu thích
					</a></li>
				<li  class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">
					<a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $lbl_skin_needs?> <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png"></a>
					<ul class="dropdown-menu">
						<?php foreach($arr[$arr_option['your_need_id']] as $v){ ?>
							<?php if(isset($arr[$v->id])){ ?>
								<?php foreach($arr[$v->id] as $v1){ ?>
<!--									<div class="sub_2"><a class="sub_2_a" href="--><?php //echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$v1->id.'&Itemid=103'); ?><!--">--><?php //echo $v1->child_title; ?><!--</a></div>-->
									<li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$v1->id.'&Itemid=103'); ?>"><?php echo $v1->child_title; ?></a></li>

								<?php }}} ?>

						<?php foreach($arr[$arr_option['your_need_id']] as $v){ ?>
							<?php if(!isset($arr[$v->id])){ ?>
								<li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$v->id.'&Itemid=103'); ?>"><?php echo $v->child_title; ?></a></li>
							<?php }}?>

					</ul>
				</li>
				<li  class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">
					<a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $lbl_skin_care?> <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png"></a>
					<ul class="dropdown-menu">
						<?php foreach ($list_cate as $r) {?>
							<?php if($r->parent_id == $arr_option['skin_care_id']){?>
<!--									<a class="sub_main_a" title="--><?php //echo $r->title; ?><!--" href="--><?php //echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$r->id.'&Itemid=103'); ?><!--">--><?php //echo $r->title; ?><!--</a>-->
								<li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$r->id.'&Itemid=103'); ?>"><?php echo $r->title; ?></a></li>

							<?php } ?>
						<?php } ?>
					</ul>
				</li>
				<li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">
					<a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $lbl_vichy_ranges; ?> <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png"></a>
					<ul class="dropdown-menu">
						<li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="<?php echo JURI::root(); ?>index.php?option=com_vichy_product&view=product_range&rid=9&Itemid=103">NORMADERM</a></li>
<!--						<h4>--><?php //echo $lbl_vichy_ranges; ?><!--</h4>-->
						<?php foreach ($list_cate as $r) {?>
							<?php if($r->parent_id == $arr_option['product_rang_id']){?>
								<li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$r->id.'&Itemid=103'); ?>"><?php echo mb_strtoupper($r->title); ?></a></li>
							<?php }?>
						<?php } ?>
						<?php foreach ($list_cate as $r) {?>
							<?php if($r->parent_id == $arr_option['product_rang_without_step_id']){?>
								<li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$r->id.'&no_step=1&Itemid=103'); ?>"><?php echo mb_strtoupper($r->title); ?></a></li>
							<?php }?>
						<?php } ?>
					</ul>
				</li>
				<li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a <?php echo ($active=='3') ? 'active' : '' ?>"  href="<?php echo JURI::root(); ?>index.php?option=com_vichy_product&view=product_landing&Itemid=103">Tất cả các dòng sản phẩm
					</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>