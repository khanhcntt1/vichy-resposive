<?php
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

class vichyFooterHelper
{
	function getCategoryWithParent(){
		$db = & JFactory::getDbo();
		$lg = &JFactory::getLanguage();
        $sql = "SELECT id, parent_id, title, alias, main_color, description, image, title_description from #__categories where published > 0 and LOCATE('com_vichy', extension) > 0 and (parent_id = ".PRODUCT_RANG." or parent_id = ".YOUR_NEED.") and language = '".str_replace(' ', '', $lg->getTag())."'";
        $db->setQuery($sql);
        $db->query();
        return $db->loadObjectList();
	}
}
