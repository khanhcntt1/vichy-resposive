<?php
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

class vichyFooterHelper extends JModuleHelper
{
	function getCategoryWithParent($params, $arr_option){
		$db = & JFactory::getDbo();		
        //$sql = "SELECT id, parent_id, title, alias, main_color, description, image, title_description from #__categories where published > 0 and LOCATE('com_vichy', extension) > 0 and (parent_id = ".PRODUCT_RANG." or parent_id = ".YOUR_NEED.") and language = '".str_replace(' ', '', $lg->getTag())."'";
        $sql = "SELECT id, parent_id, title, alias, main_color, description, image, title_description 
		        from #__categories 
		        where published > 0 and 
		        LOCATE('com_vichy', extension) > 0 and 
		        (parent_id = ".$arr_option['product_rang_id']." or parent_id = ".$arr_option['product_rang_without_step_id']." or parent_id = ".$arr_option['your_need_id']." or parent_id = ".$arr_option['skin_care_id'].") and 
		        language = '".str_replace(' ', '', $arr_option['lang_code'])."' 
		        ORDER BY lft, rgt";
        $db->setQuery($sql);
        $db->query();
        return $db->loadObjectList();
	}
	 function getCategoryWithParent_YourNeed($arr_option){
		$db = & JFactory::getDbo();		
        $sql = "SELECT g.id,c.parent_id as grand_id,g.title as child_title,g.parent_id,c.title from #__categories as c Join #__categories as g On c.id=g.parent_id where c.published > 0 and LOCATE('com_vichy', c.extension) > 0 and (c.parent_id=".$arr_option['your_need_id']." OR g.parent_id=".$arr_option['your_need_id'].") and g.language = '".str_replace(' ', '', $arr_option['lang_code'])."' ORDER BY c.rgt ";
		
        $db->setQuery($sql);
        $db->query();
        return $db->loadObjectList();

    }

}
