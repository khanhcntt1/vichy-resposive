<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_productViewProduct_landing extends JView{
    	function display($tpl=null){
    		// $group_id = $_GET['gid'];
            $session =& JFactory::getSession();
            $lang = $session->get('lang');
            $this->lang = $lang;
            if($lang == 'vi-VN'){
                $this->lang = $lang;            
                $this->product_rang_without_step = PRODUCT_RANG_WITHOUT_STEP;
                $group_id = PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP;
            }else{
                $this->lang = $lang;
                $product_rang = Vichy_productHelper::get_translation_id($lang, PRODUCT_RANG, 'categories');
                $this->product_rang_without_step = Vichy_productHelper::get_translation_id($lang, PRODUCT_RANG_WITHOUT_STEP, 'categories');
                $group_id = $product_rang.','.$this->product_rang_without_step;
            }           
            
    		$items = Vichy_productHelper::getCategoriesByGroup($group_id);
    		$this->items = $items;
    		parent::display($tpl);
    	}
    }
?>