<?php
defined('_JEXEC') or die;
$document = & JFactory::getDocument();

$title = $this->title;
$title_item = $this->title_item;
if(!empty($title_item)){
    $document->setTitle($title.' | '.$title_item);
}else{
    $document->setTitle($title);
}
$document->addStyleSheet('components/com_vichy_product/css/style.css');
$color1 = '';
$color2 = '';
if(strpos($this->items->main_color,',') !== FALSE){
    $this->items->main_color = str_replace(',', '', $this->items->main_color);
}
switch ($this->items->main_color) {
    case 'normaderm':
        $color_large_font = '#197205';
        $color_small_font = '#197205';
        $color = '#197205';
        $color2 = '#8cbf4f';
        break;
    case 'normadermnormaderm':
        $color_large_font = '#197205';
        $color_small_font = '#197205';
        $color = '#197205';
        $color2 = '#8cbf4f';
        break;
    case 'capital':
        $color_large_font = '#ff7200';
        $color_small_font = '#ff7200';
        $color = '#ff8b06';
        $color2 = '#ffa146';
        break;
    case 'capitalcapital':
        $color_large_font = '#ff7200';
        $color_small_font = '#ff7200';
        $color = '#ff8b06';
        $color2 = '#ffa146';
        break;
    case 'thermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color = '#82add8';
        $color2 = '#8cbae7';
        break;
    case 'thermalthermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color = '#82add8';
        $color2 = '#8cbae7';
        break;
    case 'aqualia_thermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color = '#448ac9';
        $color2 = '#57a5df';
        break;
    case 'aqualia_thermalaqualia_thermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color = '#448ac9';
        $color2 = '#57a5df';
        break;
    case 'bi_white':
        $color_large_font = '#717e85';
        $color_small_font = '#717e85';
        $color = '#717e85';
        $color2 = '#9aa6af';
        break;
    case 'blue-2':
        $color_large_font = '#c20c5c';
        $color_small_font = '#c20c5c';
        $color = '#c20c5c';
        $color2 = '#9aa6af';
        break;
    case 'bi_whitebi_white':
        $color_large_font = '#717e85';
        $color_small_font = '#717e85';
        $color = '#717e85';
        $color2 = '#9aa6af';
        break;
    case 'destock':
        $color_large_font = '#099575';
        $color_small_font = '#099575';
        $color = '#099575';
        $color2 = '#0fb18d';
        break;
    case 'destockdestock':
        $color_large_font = '#099575';
        $color_small_font = '#099575';
        $color = '#099575';
        $color2 = '#0fb18d';
        break;
    case 'dercos':
        $color_large_font = '#8b0305';
        $color_small_font = '#8b0305';
        $color = '#8b0305';
        $color2 = '#dd1734';
        break;
    case 'dercosdercos':
        $color_large_font = '#8b0305';
        $color_small_font = '#8b0305';
        $color = '#8b0305';
        $color2 = '#dd1734';
        break;
    case 'liftactiv':
        $color_large_font = '#1a74a3';
        $color_small_font = '#1a74a3';
        $color = '#1a74a3';
        $color2 = '#418db3';
        break;
    case 'liftactivliftactiv':
        $color_large_font = '#1a74a3';
        $color_small_font = '#1a74a3';
        $color = '#1a74a3';
        $color2 = '#418db3';
        break;
    case 'purete_thermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color = '#00a0db';
        $color2 = '#57a5df';
        break;
    case 'purete_thermalpurete_thermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color = '#00a0db';
        $color2 = '#57a5df';
        break;

        case 'aqualia_thermalpurete_thermal':
            $color_large_font = '#448ac9';
            $color_small_font = '#448ac9';
            $color = '#00a0db';
            $color2 = '#57a5df';
            break;
    case 'aera-mineral':
        $color_large_font = '#e2c8af';
        $color_small_font = '#e2c8af';
        $color = '#e2c8af';
        $color2 = '#f1d6bc';
        break;
    case 'aera-mineralaera-mineral':
        $color_large_font = '#e2c8af';
        $color_small_font = '#e2c8af';
        $color = '#e2c8af';
        $color2 = '#f1d6bc';
        break;
    case 'idealia':
        $color_large_font = '#e55482';
        $color_small_font = '#e55482';
        $color = '#e55482';
        $color2 = '#ed88a7';
        break;
    case 'idealiaidealia':
        $color_large_font = '#e55482';
        $color_small_font = '#e55482';
        $color = '#e55482';
        $color2 = '#ed88a7';
        break;
    case 'idealialiftactiv':
        $color_large_font = '#e55482';
        $color_small_font = '#e55482';
        $color = '#e55482';
        $color2 = '#ed88a7';
        break;
    default :
        $color_large_font = '#2f4c9d';
        $color_small_font = '#2f4c9d';
        $color = '#2b716f';
        break;

}
$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();
if($language_tag == 'vi-VN'){
    $lbl_text_new = "Mới";
    $lbl_text_favorite = "Yêu thích";
    $lbl_number_review = "người đánh giá";
    $lbl_your_review = "Nhận xét của bạn";
    $lbl_efficacy = "Thành phần";
    $lbl_tolerance = "Hiệu quả";
    $lbl_pleasure = "Độ an toàn";
    $lbl_video = "Cách sử dụng";
    $lbl_no_data = "Không có dữ liệu";
    $lbl_buy_product_online = "BẠN CÓ THỂ TÌM MUA SẢN PHẨM VICHY TẠI:";
    $lbl_buy_product_inner_1 = "Mua hàng trực tuyến";
    $lbl_buy_product_inner_offline_1 = "Mua Online";
    $lbl_buy_product_inner_2 = "Hay đến cửa hàng gần nhất để được tư vấn chuyên sâu";
    $lbl_buy_product_inner_offline_2 = "cửa hàng";
    $lbl_text_register = 'Hãy đăng ký tài khoản my skin để nhận mẫu thử';
    $lbl_text_answer = 'trả lời thêm khảo sát trong tài khoản my skin để nhận mẩu thử';
    $lbl_message = "Sản phẩm đang được cập nhật...Vui lòng chọn mục khác.";

    $img_vichy_myskin = "vichy_myskin.png";
}else{
    $lbl_text_new = "NEW";
    $lbl_text_favorite = "Top Favorite";
    $lbl_number_review = "reviews";
    $lbl_your_review = "Your review";
    $lbl_efficacy = "Efficacy";
    $lbl_tolerance = "Tolerance";
    $lbl_pleasure = "Pleasure";
    $lbl_video = "Video";
    $lbl_no_data = "No data";
    $lbl_buy_product_online = "WHERE WOULD YOU LIKE TO BUY THE PRODUCT?";
    $lbl_buy_product_inner_1 = "Online";
    $lbl_buy_product_inner_offline_1 = "BUY ONLINE";
    $lbl_buy_product_inner_2 = "GO TO STORE FOR DEEPER CONSULTANCY";
    $lbl_buy_product_inner_offline_2 = "Store";
    $lbl_text_register = 'Please sign up to receive my skin specimen';
    $lbl_text_answer = 'Answer the survey added my account to receive pieces of skin test';
    $lbl_message = "Our products are being updated ... Please choose another item.";

    $img_vichy_myskin = "vichy_myskin_en.png";
}
$rid= $_GET['rid'];
$step= $_GET['no_step'];

?>
<script>
    $(document).ready(function(){
        $('.sub-tabs-product1,.sub-tabs-product').first().find('a').css({'background':"<?php echo $color;?>"});
        $('.sub-tabs-product').first().find('a').css({'background':"<?php echo $color;?>"});
        $('.sub-tabs-product1 a,.sub-tabs-product a').click(function(){
            $('.sub-tabs-product1 a,.sub-tabs-product a').css({'background':"transparent"});
            $(this).css({'background':"<?php echo $color;?>"});
        });
    });
</script>

<ul class="mobile-style" style="display:none;">
    <li class="bg-li-menu-mobile active breadrumsp" >
        <div style="overflow:hidden;max-width:45%; height:40px;float:left;background:#3c72c7;padding-left:8px !important;">
            <a class="sub-menu-a active" style="padding-right:0" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing'); ?>">Tất cả sản phẩm</a>
        </div>
        <div>
            <img style="margin-left:-0.7px;float:left" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu3.png" />   
        </div>
        <div style="overflow:hidden;max-width:45%; height:40px;float:left;background:#50a5cd">
            <a class="sub-menu-a active" style="padding-right:0; padding-left:10px !important; line-height:20px" href="<?php
            if(isset($step)){
                 echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$rid.'&no_step=1');
            }else{
                echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$rid);
            }?>"><?php echo $title_item ?></a>
        </div>
        <div style="border-bottom: 1px solid #fff;">
            <img style="margin-left:-0.9px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu2.png" />   
        </div>
        <div style="overflow:hidden;max-width:93%; height:40px;float:left;background:#3c72c7;padding-left: 8px !important;">
            <a class="sub-menu-a active" style="padding-right:0; line-height:26px" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing'); ?>"><div class="aaa" style="width:100%;height:50px;"><?php  $str=$this->items->name; echo strip_tags($str);?></div></a>
        </div>
        <div>
            <img style="margin-left:-0.9px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" />   
        </div>
    </li> 

</ul>
<script>
    $(document).ready(function() {
        $(".aaa").dotdotdot({
        });
//        function getUrlParameter(sParam)
//        {
//            var sPageURL = window.location.search.substring(1);
//            var sURLVariables = sPageURL.split('&');
//            for (var i = 0; i < sURLVariables.length; i++)
//            {
//                var sParameterName = sURLVariables[i].split('=');
//                if (sParameterName[0] == sParam)
//                {
//                    return sParameterName[1];
//                }
//            }
//        }
//        var r_id = getUrlParameter('rid');
//        alert(r_id);

    });
</script>
<script>
    $(".sub-menu-spa-btn").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
    $(".dropdown-a").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
        }else
        {
            $('.dropdown-a').find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-minus.png');
        }
    });

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo FB_APP_ID; ?>";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>
<div class="wrap_product_detail">
    <div class="product_detail box_shadow">
        <div class="product_detail_left">
            <div class="product_detail_range_name" style="color:<?php echo $color_small_font; ?>"><?php echo $this->items->product_range; ?></div>
            <div class="product_detail_name" style="color:<?php echo $color_large_font;?>"><?php echo $this->items->name; ?></div>
            <div class="product_detail_desc"><?php echo strip_tags($this->items->description); ?></div>
            <div class="product_detail_rating">
                <?php for($j=1; $j<=5; $j++){ ?>
                    <?php if(!empty($this->items->rating) && $this->items->rating >0 && $j<=$this->items->rating ){ ?>
                        <img style="border:none;float:left;" src="<?php echo JURI::root() ?>media/small_star_blue.png">
                    <?php }else{ ?>
                        <img style="border:none;float:left;" src="<?php echo JURI::root() ?>media/small_star_grey.png">
                    <?php } ?>
                <?php } ?>
            </div>
            <div style="clear:both;"></div>
            <div class="product_detail_review"><?php echo (!empty($this->items->reviews)) ? $this->items->reviews : '0' ?> <?php echo $lbl_number_review; ?></div>
            <a class="top_add_review" style="background-color:<?php echo $color;?>"><?php echo $lbl_your_review; ?></a>
            <div class="product_detail_facebook">
                <!-- <img src="<?php //echo JURI::root() ?>media/facebook.png" /> -->
                <div class="fb-like" data-href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$this->items->pid.'&rid='.$this->items->range_id.'&Itemid=103'); ?>" data-layout="button_count" data-send="false" data-action="like" data-show-faces="false" data-share="false"></div>
            </div>
        </div>
        <div class="product_detail_image">
            <img src="<?php echo JURI::root() ?>components/com_vichy_product/uploads/products/<?php echo $this->items->image; ?>" />
        </div>
        <div class="buy-btn-block">
            <a href="<?php echo $this->items->linkbuy;?>" target="_blank">Mua</a>
        </div>
    </div>
    <div class="buy_product box_shadow pc-style">
        <div class="buy_product_inner">
            <div class="top_buy_product_online"><?php echo $lbl_buy_product_online; ?></div>
            <!--<img src="<?php echo JURI::root() ?>components/com_vichy_product/images/map_product.jpg" />-->
            <div class="bottom_buy_product_online">
                <div class="buy_product_inner_1"><?php echo $lbl_buy_product_inner_1; ?></div>
                <div class="basket hover-buy-online product-detail-hover-buy-online">
                    <a class="buy_product_inner_offline" href="#"><?php echo $lbl_buy_product_inner_offline_1; ?></a>
                    <div class="buy-online product-detail-buy-online">
                        <a href="http://www.chon.vn/cua-hang/vichy.aspx?ref=home-merchant" target="_blank"><img src="<?php echo JURI::root();?>images/chonvn.png"></a>
                        <a href="http://www.yes24.vn/khuyen-mai/471450/vichy-khuyen-mai.html" target="_blank"><img src="<?php echo JURI::root();?>images/yes24com.png"></a>
                        <a href="http://www.lazada.vn/vichy/" target="_blank"><img src="<?php echo JURI::root();?>images/lazada.png"></a>
                        <a href="http://tiki.vn/thuong-hieu/vichy.html" style="margin-right: 0px;"><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/tiki.png"></a>
                    </div>

                    <script type="text/javascript" language="javascript">
                        $(document).ready(function(e) {
                            $('.product-detail-hover-buy-online').hover(function(e) {
                                var padding_root = 3*$(this).parent().parent().parent().css('padding').replace("px","");
                                var box_border = parseInt($(this).find('.buy-online').css('border-right-width').replace("px",""));
                                var width_root = $(this).width() + padding_root + box_border;

                                var el=$(this).find('.buy-online').css({'visibility':'visible','opacity':'1','transition-delay':'0s','right':width_root});

                                // $(this).find('.product-detail-buy-online').css('right':right);
                            },function(e){
                                var el=$(this).find('.buy-online').css({'visibility':'hidden','opacity':'0','transition':'visibility 0s linear 0.3s,opacity 0.3s linear'});
                                $('.buy_product_inner_offline').removeAttr('style');
                            });
                        });
                    </script>
                </div><!--end of hover buy online-->
                <div class="buy_product_inner_2" style="font-weight:bold"><?php echo $lbl_buy_product_inner_2; ?></div>
                <a class="buy_product_inner_offline" href="<?php echo JRoute::_('index.php?option=com_vichy_store&Itemid=113'); ?>"><?php echo $lbl_buy_product_inner_offline_2; ?></a>
            </div>
        </div>
    </div>
    <div style="clear:both;"></div>
    <div class="description box_shadow mobile-style">
        <div class="btn-mobi" style="background: <?php echo $color;?>;" data-toggle="collapse" data-target="#collapse1">THÔNG TIN CHI TIẾT<img class="plus" src="<?php echo JURI::root() ?>components/com_vichy_product/images/btn-plus2.png" /></div>
        <div id="collapse1" class="collapse">
            <ul class="tabs">
                <li class="sub-tabs-product1" style="background: <?php echo $color2.'!important';?>">
                    <a href="#efficacy-mobi" class="active"><?php echo $lbl_efficacy; ?></a>
                </li>
                <li class="sub-tabs-product1" style="background: <?php echo $color2.'!important';?>">
                    <a href="#tolerance-mobi"><?php echo $lbl_tolerance; ?></a>
                </li>
                <li class="sub-tabs-product1" style="background: <?php echo $color2.'!important';?>">
                    <a href="#pleasure-mobi"><?php echo $lbl_pleasure; ?></a>
                </li>
                <li class="sub-tabs-product1" style="background: <?php echo $color2.'!important';?>">
                    <a href="#video-mobi"><?php echo $lbl_video; ?></a>
                </li>
            </ul>
            <div class="tab-container">
                <div id="efficacy-mobi" class="tab-contentmobi">
                    <?php echo (!empty($this->items->effectiveness)) ? $this->items->effectiveness : $lbl_no_data; ?>
                </div>
                <div id="tolerance-mobi" class="tab-contentmobi">
                    <?php echo (!empty($this->items->tolerance)) ? $this->items->tolerance : $lbl_no_data; ?>
                </div>
                <div id="pleasure-mobi" class="tab-contentmobi">
                    <?php echo (!empty($this->items->pleasure)) ? $this->items->pleasure : $lbl_no_data; ?>
                </div>
                <div id="video-mobi" class="tab-contentmobi">
                    <?php echo (!empty($this->items->video)) ? $this->items->video : $lbl_no_data; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="description box_shadow pc-style">
        <ul class="tabs">
            <li class="sub-tabs-product" style="background: <?php echo $color2;?>">
                <a href="#efficacy" class="active"><?php echo $lbl_efficacy; ?></a>
            </li>
            <li class="sub-tabs-product" style="background: <?php echo $color2;?>">
                <a href="#tolerance"><?php echo $lbl_tolerance; ?></a>
            </li>
            <li class="sub-tabs-product" style="background: <?php echo $color2;?>">
                <a href="#pleasure"><?php echo $lbl_pleasure; ?></a>
            </li>
            <li class="sub-tabs-product" style="background: <?php echo $color2;?>">
                <a href="#video"><?php echo $lbl_video; ?></a>
            </li>
        </ul>
        <div class="tab-container">
            <div id="efficacy" class="tab-content">
                <?php echo (!empty($this->items->effectiveness)) ? $this->items->effectiveness : $lbl_no_data; ?>
            </div>
            <div id="tolerance" class="tab-content">
                <?php echo (!empty($this->items->tolerance)) ? $this->items->tolerance : $lbl_no_data; ?>
            </div>
            <div id="pleasure" class="tab-content">
                <?php echo (!empty($this->items->pleasure)) ? $this->items->pleasure : $lbl_no_data; ?>
            </div>
            <div id="video" class="tab-content">
                <?php echo (!empty($this->items->video)) ? $this->items->video : $lbl_no_data; ?>
            </div>
        </div>
    </div>

    <?php
    $user = JFactory::getUser();
    $text = $lbl_text_register;
    $link = JRoute::_('index.php?option=com_users&view=registration');
    if(!empty($user->username)){
        $text = $lbl_text_answer;
        $link = JRoute::_('index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107');
    }
    ?>
    <div class="buy_product box_shadow pc-style" style="margin-top:12px;">
        <a href="<?php echo $link; ?>">
            <img src="<?php echo JURI::root() ?>timbthumb.php?src=components/com_vichy_product/images/<?php echo $img_vichy_myskin; ?>&w=207&h=365&xz=0" />
        </a>
        <!-- <div class="buy_product_inner">
			<img src="<?php echo JURI::root() ?>components/com_vichy_product/images/vichy_product.jpg" />
			<div class="buy_product_inner_1" style="color:#6e86ac;font-size:13px;"><?php echo $text; ?></div>
			<a class="buy_product_inner_offline" href="<?php echo $link; ?>" style="margin-top:39px;">Đăng ký</a>
		</div> -->
    </div>
    <div style="clear:both;"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        if(window.innerWidth < 992){
            jQuery('.tab-contentmobi:not(:first)').hide();
            (function($){
                $(window).load(function(){
                    $(".tab-contentmobi").mCustomScrollbar({
                        autoHideScrollbar:false,
                        theme:"light-thin"
                    });
                });
            })(jQuery);

            jQuery('.tabs>li>a').click(function(){
                jQuery('.tabs>li>a').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.tab-contentmobi').hide();

                var activeTab = jQuery(this).attr('href');
                jQuery(activeTab).fadeIn();
                $(".tab-contentmobi").mCustomScrollbar({
                    autoHideScrollbar:false,
                    theme:"light-thin"
                });
                return false;
            });
            jQuery('.top_add_review').click(function(event) {

                jQuery('html, body').animate({scrollTop: $("#scroll_comment").position().top - 180}, 500);
                //jQuery('html, body').scrollTo('#scroll_comment');
            });
        }
        else{
            jQuery('.tab-content:not(:first)').hide();
            (function($){
                $(window).load(function(){
                    $(".tab-content").mCustomScrollbar({
                        autoHideScrollbar:false,
                        theme:"light-thin"
                    });
                });
            })(jQuery);

            jQuery('.tabs>li>a').click(function(){
                jQuery('.tabs>li>a').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.tab-content').hide();

                var activeTab = jQuery(this).attr('href');
                jQuery(activeTab).fadeIn();
                $(".tab-content").mCustomScrollbar({
                    autoHideScrollbar:false,
                    theme:"light-thin"
                });
                return false;
            });
            jQuery('.top_add_review').click(function(event) {

                jQuery('html, body').animate({scrollTop: $("#scroll_comment").position().top - 180}, 500);
                //jQuery('html, body').scrollTo('#scroll_comment');
            });
        }
    });



    $(".btn-mobi").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus2.png');
        }else
        {
//            $('.btn-mobi').find('img').attr("src",'<?php //echo JURI::root(); ?>//components/com_vichy_product/images/btn-plus.png');
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-minus2.png');
        }
    });
</script>

