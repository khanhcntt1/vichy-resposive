<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_product/css/style.css');
    
?>
<div class="wrap_list_product_ranges">
	<?php
	$color = '';
	foreach($this->items as $v){ 
		switch ($v->main_color) {
			case 'normaderm':
				$color = '#197205';
				break;
			case 'capital':
				$color = '#ff8b06';
				break;
			case 'thermal':
				$color = '#2f4c9d';
				break;
		}
		$data = json_decode($v->params,true);
		$image = (!empty($data['image'])) ? $data['image']: 'components/com_vichy_product/uploads/product_range/bg.png';
	?>
	<a href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->id); ?>" style="display:block;">
		<div class="wrap_product_range_item">
			
			<div class="product_range_name" style="background:<?php echo $color; ?>">
				<span><?php echo $v->title; ?></span>
			</div>
			<div class="product_range_image">
				<img width="940" src="<?php echo JURI::root().$image; ?>" />
			</div>
		</div>
	</a>
	<?php } ?>
</div>