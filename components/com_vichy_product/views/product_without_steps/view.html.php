<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_productViewProduct_without_steps extends JView{
    	function display($tpl=null){
    		// $group_id = $_GET['gid'];
            $session =& JFactory::getSession();
            $lang = $session->get('lang');
            $this->lang = $lang;
            if($lang == 'vi-VN'){ 
                $group_id = PRODUCT_RANG;
            }else{
                $group_id = Vichy_productHelper::get_translation_id($lang, PRODUCT_RANG, 'categories');                
            }

    		$items = Vichy_productHelper::getCategoriesByGroup($group_id);
    		$this->items = $items;
    		parent::display($tpl);
    	}
    }
?>