<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
    
    $title = $this->title;
    $title_item = $this->title_item;
    $document->setTitle($title.' | '.$title_item);

	$document->addStyleSheet('components/com_vichy_product/css/style.css');	
	$list=$this->list_yourneed;
	$item=$this->yourneed;
	$item_img=array();
	$item_img=json_decode($item->image);
	$link_prod = "index.php?option=com_vichy_product&view=product";
?>
<div class="wrap_your_need">
	<!-- <div class="wrap_full_left box_shadow">
		<div class="your_need_left" style="background: url(<?php echo $item_img->background_landing;?>) no-repeat;">
			<div class="title_your_need_left"><?php echo $item->title; ?></div>
		</div>
	</div>
	<div class="buy_product">
		<div class="buy_product_inner">
			<div class="top_buy_product_online">BẠN CÓ THỂ TÌM MUA SẢN PHẨM VICHY TẠI:</div>
			<img src="<?php echo JURI::root() ?>components/com_vichy_product/images/map_product.jpg" />
			<div class="bottom_buy_product_online">
				<div class="buy_product_inner_1">Mua hàng trực tuyến tại website</div>
				<a class="buy_product_inner_offline">Mua Online</a>
				<div class="buy_product_inner_2" style="font-weight:bold">Hay đến cửa hàng gần nhất để được tư vấn chuyên sâu</div>
				<a class="buy_product_inner_offline" href="<?php echo JRoute::_('index.php?option=com_vichy_store&view=store&Itemid=113'); ?>">Đến cửa hàng</a>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	 -->
	<link href="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
	<!-- custom scrollbars plugin -->
	<script src="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
	<script>
		(function($){
			$(window).load(function(){
				$("#scroll_yourneed").mCustomScrollbar({
					autoHideScrollbar:false,
					theme:"light-thin"
				});
			});
		})(jQuery);
	</script>


	<div class="list_yourneed box_shadow">
		<div class="title_list_yourneed"><?php echo $item->title; ?></div>
		<div class="wrap_yourneed" id="scroll_yourneed">
			<?php
			if(!empty($list)){
				foreach($list as $k => $v){
					$link_prod.="&rid=$v->product_range_id&id=$v->product_id";
					if($v->product_range_id==37)
						$link_prod.="&nostep=1";

					$link_prod .= '&Itemid=103';

			?>
			<div class="item_yourneed">
				<?php if($v->is_new){ ?>
						<div class="product_new">Mới</div>
						<?php }
						else
						{
							if($v->is_favorite)
							{ ?>
						<div class="product_favorite">
							<div class="favorite_featured">Yêu thích</div>
						</div>
					<?php }
					}
					?>
				<a href="<?php echo JRoute::_($link_prod) ;?>">
					<div class="your_img">
						<img src="<?php echo JURI::root()?>components/com_vichy_product/uploads/products/<?php echo $v->image;?>">
					</div>
					<div class="your_range"><?php echo $v->product_range ;?></div>
					<div class="your_name"><?php echo $v->product_name ; ?></div>
					<div class="your_skin_care"></div>
					
					<div class="your_box_detail">
						<div class="product_star_review">
							<?php for($j=1; $j<=5; $j++){
								 if(!empty($v->rating) && $v->rating >0 && $j<=$v->rating ){?>
								 	<img style="border:none;float:left;" src="<?php echo JURI::root()?>/media/small_star_blue.png">
								 <?php } else {?>
								 	<img style="border:none;float:left;" src="<?php echo JURI::root()?>/media/small_star_grey.png">
								 
							<?php }}?>
						</div>
						<div class="product_number_review"><?php echo (empty($v->reviews))?0 : $v->reviews;?> người đánh giá</div>
					</div>
					<div class="clear"></div>
					
				</a>
			</div>
			<?php }}
			else {?>
				<div>Sản phẩm đang được cập nhật...Vui lòng chọn mục khác.</div>
			<?php }?>

			<div class="clear"></div>
		</div>
	</div>
</div>
