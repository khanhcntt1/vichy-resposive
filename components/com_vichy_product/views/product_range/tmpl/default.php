<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();

    $title = $this->title;
    $title_item = $this->title_item;
    $document->setTitle($title.' | '.$title_item);
    
	$document->addStyleSheet('components/com_vichy_product/css/style.css');	
	$color1 = $color2 = $color3 = '';
	switch ($this->items->main_color) {
		case 'normaderm':
			$color1 = '#a8dc6a';
			$color2 = '#8cbf4f';
			$color3 = '#3b872a';
			break;
		// case 'capital':
		// 	$color1 = '#fcdd85';
		// 	$color2 = '#ffdb46';
		// 	$color3 = '#ff8800';
		// 	break;
		case 'capital':
			$color1 = '#fcc085';
			$color2 = '#ffa146';
			$color3 = '#ff7200';
			break;
		case 'thermal':
			// $color1 = '#99c9f7';
			// $color2 = '#7ba9d6';
			// $color3 = '#5f96cc';
			$color1 = '#8bc1e9';
			$color2 = '#57a5df';
			$color3 = '#448ac9';
			break;
		case 'aqualia_thermal':
			$color1 = '#8bc1e9';
			$color2 = '#57a5df';
			$color3 = '#448ac9';
			break;
		// case 'bi_white':
		// 	$color1 = '#d7dade';
		// 	$color2 = '#c0c4c6';
		// 	$color3 = '#a5a9b0';
		// 	break;
        case 'blue-2':
            $color1 = '#fe7eb4';
            $color2 = '#ee5898';
            $color3 = '#c20c5c';
            break;
		case 'bi_white':
			$color1 = '#babfc3';
			$color2 = '#9aa6af';
			$color3 = '#717e85';
			break;
		case 'destock':
			$color1 = '#2dd1ad';
			$color2 = '#0fb18d';
			$color3 = '#099575';
			break;
		case 'dercos':
			$color1 = '#dc4b60';
			$color2 = '#dd1734';
			$color3 = '#8b0305';
			break;
		case 'liftactiv':
			$color1 = '#63acc6';
			$color2 = '#418db3';
			$color3 = '#1a74a3';
			break;
		case 'purete_thermal':
			// $color1 = '#9dd5f9';
			// $color2 = '#77c2f1';
			// $color3 = '#00a0db';
			$color1 = '#8bc1e9';
			$color2 = '#57a5df';
			$color3 = '#448ac9';
			break;
		case 'aera-mineral':
			$color1 = '#f8e1cb';
			$color2 = '#f0d7bf';
			$color3 = '#e2c8af';
			break;
        case 'idealia':
            $color1 = '#ff9cbb';
            $color2 = '#ed88a7';
            $color3 = '#e55482';
            break;  
		default :
			$color1 = '#8cbf4f';
			$color2 = '#3b872a';
			$color3 = '#2b716f';
			break;
	}
	$image = json_decode($this->items->image);
	$image->background_range = (!empty($image->background_range)) ? $image->background_range : 'images/background/product_range/Product-Range_bg.jpg';
	$image->background_landing = (!empty($image->background_landing)) ? $image->background_landing : 'images/background/product_landing/Vichy_Product-Range.jpg';
	//$yourneed_id = $this->items->yourneed_id;
	$yourneed_id = $this->facial_skin_care;
	if($this->items->main_color=='purete_thermal'||$this->items->main_color=='thermal'||$this->items->main_color=='aqualia_thermal')
		$yourneed_id = $this->facial_skin_care;
	$advice_link=JROUTE::_("index.php?option=com_vichy_advice&view=advice&Itemid=107&catid=".$yourneed_id);

	if($this->lang == 'vi-VN'){		
		$lbl_text_new = "Mới";
		$lbl_text_favorite = "Yêu thích";
		$lbl_number_review = "người đánh giá";
		$lbl_title_skin = "Kiểm tra da";
		$lbl_text_skin = "Hiểu hơn làn da của bạn và chọn lựa sản phẩm phù hợp";
		$lbl_title_advice = "Lời khuyên";
		$lbl_text_advice = "Giải mã những thắc mắc phổ biến nhất về các vấn đề về da";
		$lbl_select_other_range = "CHỌN DÒNG SẢN PHẨM";
	}else{
		$lbl_text_new = "NEW";
		$lbl_text_favorite = "Top Favorite";
		$lbl_number_review = "reviews";
		$lbl_title_skin = "Skin diagnosis";
		$lbl_text_skin = "Understand your skin and choose the suitable";
		$lbl_title_advice = "Advice";
		$lbl_text_advice = "Decoding the most common questions about skin problems";
		$lbl_select_other_range = "CHOOSE A PRODUCT RANGE";
	}
?>
<!--<nav class="navbar navbar-default mobile-style" style="display: none" role="navigation">-->
<!--    <div class="container">-->
<!--        <div class="navbar-header sub-menu-spa-header">-->
<!--            <a class="navbar-brand" href="#" style="color:white;padding-left:7%">Sản phẩm</a>-->
<!--            <button type="button" class="navbar-toggle collapsed sub-menu-spa-btn plus1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">-->
<!--            </button>-->
<!--        </div>-->
<!--        <div class="collapse navbar-collapse over-breadrum" id="bs-example-navbar-collapse-1">-->
<!--            <ul class="nav navbar-nav" style="margin-top:0px;background: #123f83;">-->
<!--                <!--                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->-->
<!--                <!--                    <li><a href="#">Link</a></li>-->-->
<!--                <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a --><?php //echo ($active=='3') ? 'active' : '' ?><!--"  href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_landing&Itemid=103">Sản phẩm mới nhất-->
<!--                    </a></li>-->
<!--                <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a --><?php //echo ($active=='3') ? 'active' : '' ?><!--"  href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_landing&Itemid=103">Sản phẩm yêu thích-->
<!--                    </a></li>-->
<!--                <li  class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">-->
<!--                    <a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nhu cầu của bạn <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="--><?php //echo JURI::root(); ?><!--components/com_vichy_product/images/btn-plus.png"></a>-->
<!--                    <ul class="dropdown-menu">-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=62&Itemid=103">Dưỡng sắng da và trị thâm, nám</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=63&Itemid=103">Giảm dầu,giảm mụn, se khít lỗ chân lông</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=64&Itemid=103">Chống lão hóa và làm săn chắc da</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=65&Itemid=103">Chống nắng</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=61&Itemid=103">Làm sạch và dưỡng ẩm</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=56&Itemid=103">Chăm sóc cơ thể</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=58&Itemid=103">Chăm sóc tóc và da đầu</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=59&Itemid=103">Trang điểm</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li  class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">-->
<!--                    <a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Chức năng sản phầm <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="--><?php //echo JURI::root(); ?><!--components/com_vichy_product/images/btn-plus.png"></a>-->
<!--                    <ul class="dropdown-menu">-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=15&Itemid=103">Sữa rửa mặt</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=16&Itemid=103">Nước cân bằng</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=50&Itemid=103">Tẩy tế bào chết</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=51&Itemid=103">Nước khoáng dưỡng da</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=52&Itemid=103">Tính chất đặc trị</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=53&Itemid=103">Kem dưỡng</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=54&Itemid=103">Chống nắng</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_yourneed&yid=55&Itemid=103">Mặt nạ</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">-->
<!--                    <a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Các dòng sản phẩm <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="--><?php //echo JURI::root(); ?><!--components/com_vichy_product/images/btn-plus.png"></a>-->
<!--                    <ul class="dropdown-menu">-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=9&Itemid=103">NORMADERM</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=33&Itemid=103">BI-WHITE</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=36&Itemid=103">AQUALIA THERMAL</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=34&Itemid=103">LIFTACTIV</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=47&Itemid=103">UV PRO SECURE</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=172&Itemid=103">IDEALIA</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=44&no_step=1&Itemid=103">DERCOS</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=46&no_step=1&Itemid=103">BODYCARE</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&amp;view=product&amp;rid=48&amp;id=57&amp;Itemid=103&amp;no_step=1&amp">AERAMINERAL</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=13&no_step=1&Itemid=103">CAPITAL SOLEIL</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_range&rid=35&no_step=1&Itemid=103">PURETE THERMAL</a></li>-->
<!--                        <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&amp;view=product&amp;rid=21&amp;id=16&amp;Itemid=103&amp;no_step=1&amp">THERMAL SPA WATER</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a --><?php //echo ($active=='3') ? 'active' : '' ?><!--"  href="--><?php //echo JURI::root(); ?><!--index.php?option=com_vichy_product&view=product_landing&Itemid=103">Tất cả các dòng sản phẩm-->
<!--                    </a></li>-->
<!--            </ul>-->
<!--        </div><!-- /.navbar-collapse -->-->
<!--    </div>-->
<!--</nav>-->
<script>
    $(".sub-menu-spa-btn").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
    $(".dropdown-a").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
        }else
        {
            $('.dropdown-a').find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-minus.png');
        }
    });

</script>
<ul class="mobile-style" style="display:none;">
    <li class="bg-li-menu-mobile active breadrumsp" >
        <div style="overflow:hidden;max-width:45%; height:40px;float:left;background:#3c72c7">
            <a class="sub-menu-a active" style="padding-left: 8px !important;padding-right:0" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing'); ?>">Tất cả sản phẩm</a>
        </div>
        <div>
            <img style="margin-left:-0.7px;float:left" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu3.png" />   
        </div>
        <div style="overflow:hidden;max-width:45%; height:40px;float:left;background:#50a5cd">
            <a class="sub-menu-a active" style="padding-right:0;padding-left:10px !important;" href="#"><?php echo $title_item ?></a>
        </div>
        <div>
            <img style="margin-left:-0.9px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu2.png" />   
        </div>
    </li> 
</ul> 
<div class="wrap_product_range box_shadow row no-width-height">
<div class="wrap_product_range box_shadow row no-width-height no-margin">
	<div class="col-xs-12 col-sm-8 product_range_left no-width-height" style="background:url(<?php echo JURI::root().'timbthumb.php?src='.$image->background_range.'&w=658&h=315&zc=0'; ?>) no-repeat; background-position-y: 0; min-height: 200px;">
        <div class="product-racroi">
            <div class="product_range_left_name no-width-height" style="color:<?php echo $color3; ?>;"><?php echo $this->items->title; ?></div>
            <div class="product_range_left_short_desc no-width-height" style="color:<?php echo $color3; ?>"><?php echo $this->items->title_description; ?></div>
        </div>
            <div class="product_range_left_full_desc no-width-height"><?php echo $this->items->description; ?></div>
	</div>
	<div class="col-xs-12 product_range_right no-width-height no-border no-padding" style="padding:0">
		<div class="product_range_right_top no-width-height" style="background:<?php echo $color1; ?>;">
			<a style="color:#ffffff;" href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107'); ?>">
			<div class="product_range_content no-width-height">
				<div class="title_right no-width-height"><?php echo $lbl_title_skin; ?></div>
				<span><?php echo $lbl_text_skin; ?></span>
			</div>
			</a>
		</div>
		<a href="<?php echo $advice_link;?>">
			<div class="product_range_right_middle no-width-height" style="background:<?php echo $color2; ?>">
				<!-- <img src="<?php echo JURI::root(); ?>components/com_vichy_product/images/bt_mid.png"> -->
				
				<div class="product_range_content no-width-height">
					
						<div class="title_right no-width-height"><?php echo $lbl_title_advice; ?></div>
						<span><?php echo $lbl_text_advice; ?></span>
					
				</div>
				
			</div>
		</a>
		<div class="product_range_right_bottom no-width-height pc-style" style="background:<?php echo $color3; ?>">
			<div class="other_product_ranges">
				<div class="title_select_other_range">
					<?php echo $lbl_select_other_range; ?>
				</div>
				<select id="select_other_range" class="select_other_range">
					<option value="javascript:void(0);"></option>
					<?php
					$link='index.php?option=com_vichy_product&view=product_range&Itemid=103';
					$other_ranges=$this->other_ranges;					
					foreach ($other_ranges as $k => $v) {
						$other_range_link="";
						$other_range_link=$link."&rid=$v->id";
						if($v->parent_id == $this->product_rang_without_step_id)
							$other_range_link.="&no_step=1";
					?>
					<option value="<?php echo JROUTE::_($other_range_link);?>"><?php echo shortDesc(trim($v->title),19);?></option>
					<?php }
					?>
				<select/>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		// jQuery('div.main-container').css('background', 'url(<?php echo JURI::root().$image->background_landing ?>) no-repeat fixed center');

		jQuery('#select_other_range').change(function(){
			location.href = $(this).val();
		});

		//set css for favorite with lang=en-GB
		var sessionLang = "<?php echo $lang; ?>";
		if(sessionLang == 'en-GB'){
			jQuery('.favorite_featured').css({'font-size':'9px', 'margin-top':'18px'});
		}
	});
</script>
	<div class="clear"></div>
