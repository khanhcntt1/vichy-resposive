<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_productViewProduct_range extends JView{
    	function display($tpl=null){
            $document = & JFactory::getDocument();
            $title = $document->getTitle();
            $title_item = ucwords(strtolower($this->getTitleByCategory($_GET['rid'])));
            
            $this->assignRef('title', $title);
            $this->assignRef('title_item', $title_item);

            $session =& JFactory::getSession();
            $lang = $session->get('lang');
            $this->lang = $lang;
            if($lang == 'vi-VN'){
                $this->lang = $lang;            
                $this->product_rang_without_step_id = PRODUCT_RANG_WITHOUT_STEP;
                $this->facial_skin_care = 63;
            }else{
                $this->lang = $lang;
                $this->product_rang_without_step_id = Vichy_productHelper::get_translation_id($lang, PRODUCT_RANG_WITHOUT_STEP, 'categories');
                $this->facial_skin_care = Vichy_productHelper::get_translation_id($lang, 63, 'categories');
            }

    		$range_id = $_GET['rid'];
    		$items = Vichy_productHelper::getProductRangById($range_id);
            $other_ranges=Vichy_productHelper::getOtherRanges($range_id);
    		$this->items = $items;
            $this->other_ranges = $other_ranges;
    		parent::display($tpl);
    	}

        function getTitleByCategory($id){
            $db = & JFactory::getDbo();
            $sql = "SELECT title FROM #__categories where id=$id";
            $db->setQuery($sql);
            $title = $db->loadResult();
            return $title;
        }
    }
?>