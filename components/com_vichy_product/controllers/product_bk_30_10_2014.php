<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_productControllerProduct extends JController{
    	function __construct(){
    		parent::__construct();
    	}

    	function display(){
			JRequest::setVar('view', 'product');
			$product_id = $_GET['id'];
			$range_id =$_GET['rid'];

			$Breadcrumb_Item = $this->getBreadcrumbs($product_id,$range_id);
			$title_link="index.php?option=com_vichy_product&view=product_range&rid=$range_id";
			if($Breadcrumb_Item->parent_id==37)
				$title_link.="&no_step=1";
			
			$app	= JFactory::getApplication();
			$pathway = $app->getPathway();
			$pathway->addItem("Tất cả sản phẩm",'javascript:void(0);');
			$pathway->addItem(shortDesc($Breadcrumb_Item->title,20), JROUTE::_($title_link));
			$pathway->addItem(shortDesc(strip_tags($Breadcrumb_Item->name),17), '');
			parent::display();
		}

		function getBreadcrumbs($product_id,$range_id)
		{
			$db = JFactory::getDbo();
			
			$query="SELECT p.name,c.title,c.parent_id from vc_vichy_product p
							join vc_vichy_product_category pc on pc.product_id=p.id
							join vc_categories c on c.id = pc.category_id
							where p.id= $product_id and c.id=$range_id";

			$db->setQuery($query);
			$result = $db->loadObject();

			return $result;
		}

		public function registerEmail()
		{
			$db		= JFactory::getDbo();
			$email  = $_POST['email'];

			$query = " INSERT INTO vc_vichy_subscribe_email (email) values ('$email')";
					  
			
			$db->setQuery($query);
			$res =  $db->query();

			if($res)
				echo "Cảm ơn bạn đã đăng ký nhận bản tin.\nChúng tôi sẽ gởi đến bạn những thông tin mới nhất \nvề sản phẩm của Vichy \ntheo e-mail $email .";
			else
			{
				
				$query ="SELECT count(id) from vc_vichy_subscribe_email where email=$email";
				$db->setQuery($query);
				$res =  $db->query();
				if($res)
					echo "E-mail đã đăng ký từ trước .\nChúng tôi sẽ gởi đến bạn những thông tin mới nhất \nvề sản phẩm của Vichy \ntheo e-mail $email .";
					else
						echo "Đăng ký không thành công .\n Xin vui lòng thử lại sau.";
			}
			
			exit();
		}
		
    	function autocomplete(){
	    	if (isset($_GET['query'])) {
			  $text = $_GET['query'];
			}
			else {
			  $text = $_POST['query'];
			}
			if(!empty($text))
			{
				$rows = Vichy_productHelper::onProductSearch($text,'exact','alpha');
				
				
				$suggestions = array();
				//$suggestionids = array();
				
				foreach($rows as $k => $v) {
					$a = null;
					foreach($rows[$k] as $key => $value)
					{
						switch ($key) {
							case 'name':
								$a->value=$value;
								break;
							case 'id':
								$a->data=$value;
								break;
							case 'catid':
								$a->cid=$value;
								break;
							case 'pcatid':
							{
								$pos = strpos(PRODUCT_RANG, $value);
								if ($pos === false) {
								    $a->isrange=0;
								} else {
								    $a->isrange=1;
								}
								
								break;
							}
							default:
								break;
						}
//						if($key == 'name')
//						{
//							$a->value=$value;
//						}
//						if($key == 'id')
//						{
//							$a->data=$value;
//							
//						}
//						if($key == 'catid')
//						{
//							$a->rid=$value;						
//						}
							
					}
					$suggestions[] = $a;
				}

				$result = array(
				    "query" => "Unit",
				    "suggestions" => $suggestions
				);
				echo(json_encode($result));
				exit();
				
			}

			
		}
     }
?>