<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_productControllerProduct_without_steps extends JController{
    	function __construct(){
    		parent::__construct();
    		JRequest::setVar('view','product_without_steps');
    	}

    	public function getProductWithoutSteps(){

    		if($_POST['page']) {
				$page = $_POST['page'];				
				$layout =  $_POST['layout'];
				$paging_name =  $_POST['paging_name'];				
				$current_page = $page;
				$page -= 1;				
				$display = 5;
				$start = $page * $display;
				 
				$data = "";
				$result;
				$count;
				$pages;				

				if($layout==0)
				{
					$result = Vichy_productHelper::getProductsWithoutStep($start,$display);
					$count = Vichy_productHelper::countProductsWithoutSteps();											
					$pages = ceil($count / $display);						
				}
					

				foreach($result as $k => $v){
					$reviews = !empty( $v->reviews)? $v->reviews:0 ;

					$data .='<div class = "product_item" style="margin-bottom:10px">';
					$data .='<a href="'.JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$v->pid.'&rid='.$v->cid.'&Itemid=103').'" style="display:block;">';
				 	$data .='<div class="image_product">';
				 	$data .='<img width="160" src="'.JURI::root().'components/com_vichy_product/uploads/products/'.$v->image.'" />';	
				 	$data .='</div>';
				 	$data .='<div class="product_range" style="color:#197205;">'.$v->product_range.'</div>';
				 	$data .='<div class="product_name" style="color:#acd180;">'.shortDesc($v->product_name,32).'</div>';
//				 	if($v->is_new)
//					{							
//						$data .='<div class="product_new">Mới</div>';
//					}
				 	// $data .='<div class="product_skin_care">'.$v->skincare.'</div>';
				 	$data .='<div class="product_star_review">';
				 	for($j=1; $j<=5; $j++){
			                if(!empty($v->rating) && $v->rating >0 && $j<=$v->rating )
			                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/product_star_blue.png">';
			                else{
			                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/product_star_grey.png">';
			                }
			            }
			        $data .='</div>';
					$data .='<div style="clear:both;"></div>';					
					$data .='<div class="product_number_review">'.$reviews.' người đánh giá</div>';
					$data .='</a>';
					$data .='</div>';
				}				 


				 
				// $data .= "<nav role='page' style='clear:both;'><ul>";
				if($pages>1)
				{
					$data .= '<div class="paging '.$paging_name.'" style="clear:both;">';	
					
	 				for($i = 1;$i<=$pages;$i++ )
	 				{
	 					$data.='<a href="javascript:void(0);"'." page=$i ";
	 					if($i==$current_page)
	 						$data.='class ="active"';
	 					$data.=">$i</li>";
	 				}
					 
					$data = $data . "</div>";
				}
				echo $data;
				exit();
			}
		} 
     }
?>