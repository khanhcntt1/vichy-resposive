<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_productControllerProduct_range extends JController{
    
        function __construct(){
    		parent::__construct();
    		// JRequest::setVar('view','product_range');
    		
    		$this->check();
    	}
    	function check()
    	{
            self::set_category_id();
            
    		$group_id = $this->_product_rang.','.$this->_product_rang_without_step;
    		$product_range_id = $_GET['rid'];
    		$cate=Vichy_productHelper::checkCategoryForRedirect($group_id,$product_range_id);
    		if($cate->total_product==1){  

    			$link = "index.php?option=com_vichy_product&view=product&rid=$cate->id&id=$cate->product_id&Itemid=103";
    			if($cate->parent_id == $this->_product_rang_without_step)
    			{
    				$link.="&no_step=1";
    			}
    			$this->setRedirect(JROUTE::_($link));
    		}
    	}
    	function display(){
			JRequest::setVar('view','product_range');
			
            $curLanguage = JFactory::getLanguage();
            $language_tag = $curLanguage->getTag();
            if($language_tag == 'vi-VN'){
                $title_item = "Tất cả sản phẩm";
            }else{
                $title_item = "Our products";
            }

			$product_range_id = $_GET['rid'];
			$Breadcrumb_Item = $this->getProduct_Range_Name($product_range_id);
			
			$app	= JFactory::getApplication();
			$pathway = $app->getPathway();
			$pathway->addItem($title_item,'javascript:void(0);');
			$pathway->addItem(shortDesc(trim($Breadcrumb_Item),20), '');
			parent::display();
		}

		function getProduct_Range_Name($rid)
		{
			$db = JFactory::getDbo();
			
			$query="select title from vc_categories where id= $rid";

			$db->setQuery($query);
			$result = $db->loadObject();

			return $result->title;
		}

     }


?>