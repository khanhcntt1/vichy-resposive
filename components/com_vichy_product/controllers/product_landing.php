﻿<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_productControllerProduct_landing extends JController{
    	
    	function __construct(){
    		parent::__construct();
    		JRequest::setVar('view','product_landing');
    	}

    	public function getProductLanding(){

    		self::set_category_id();

    		if($_POST['page']) {
				$page = $_POST['page'];
				$layout =  $_POST['layout'];
				$paging_name =  $_POST['paging_name'];
				$current_page = $page;
				$page -= 1;
				$display = 5;
				$start = $page * $display;

				$data = "";
				$result;
				$count;
				$pages;

				if($layout==0)
					{
						$result = Vichy_productHelper::getNewProducts($start,$display);
						$count 	= Vichy_productHelper::countNewProducts();
						$pages  = ceil($count / $display);
					}
				else
					{
						$result = Vichy_productHelper::getFavoriteProducts($start,$display);
						$count = Vichy_productHelper::countFavoriteProducts();
						$pages = ceil($count / $display);

					}

				foreach($result as $k => $v){
					$reviews = !empty( $v->reviews)? $v->reviews:0 ;


				foreach($list_products as $k => $v){
						$rid;
                    $skincare;
                    $product_range;
                    $parent_id;

						$tmp_skincare = array();
						$rids = explode(",",$v->cid);
						foreach ($rids as $pair) {
							$tmp=explode("|",$pair);//parent-child
							// print_r($pair);die();
							if($tmp[0]==$this->_product_rang||$tmp[0]==$this->_product_rang_without_step)
								$rid=$tmp[1];
						}
						$p_rs = explode(",",$v->product_range);
						foreach ($p_rs as $r_pair) {
							$tmp=explode("|",$r_pair);//parent-child
							if($tmp[0]==$this->_product_rang||$tmp[0]==$this->_product_rang_without_step)
								{
									$product_range=$tmp[1];
									$parent_id=$tmp[0];
								}
								else
								{
									if($tmp[1]!=null&&!empty($tmp[1]))
									$tmp_skincare[]=$tmp[1];
								}
						}
						$skincare=implode(" & ", $tmp_skincare);
						$reviews = !empty( $v->reviews)? $v->reviews:0 ;
						$link='index.php?option=com_vichy_product&view=product&id='.$v->pid.'&rid='.$rid.'&Itemid=103';
						if($parent_id==$this->_product_rang_without_step)
							$link.='&no_step=1';
					$data .='<div class = "product_item" style="margin-bottom:10px">';
						$data .='<a href="'.JROUTE::_($link).'" style="display:block;">';
				 	$data .='<div class="image_product">';
				 	$data .='<img width="160" src="'.JURI::root().'components/com_vichy_product/uploads/products/'.$v->image.'" />';
				 	$data .='</div>';
					 	//R=#197205 N=#acd180
					 	$data .='<div class="product_range">'.$product_range.'</div>';
				 	$data .='<div class="product_name">'.shortDesc($v->product_name,500).'</div>';
				 	if($v->is_new&&$layout==0)
					{
						$data .='<div class="product_new">Mới</div>';
					}
					if($v->is_favorite&&$layout==1)
					{
						$data .='<div class="product_favorite"><div class="favorite_featured">Yêu thích</div></div>';
					}

					 	// $data .='<div class="product_skin_care">'.$skincare.'</div>';
				 	$data.='<div class="product_box_detail">';
				 	$data .='<div class="product_star_review">';
				 	for($j=1; $j<=5; $j++){
			                if(!empty($v->rating) && $v->rating >0 && $j<=$v->rating )
			                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/small_star_blue.png">';
			                else{
			                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/small_star_grey.png">';
			                }
			            }
			         $data .='</div>';//start view end
			        $data.='</div>';//box detail end
					$data .='<div style="clear:both;"></div>';
					$data .='<div class="product_number_review">'.$reviews.' người đánh giá</div>';
					$data .='</a>';
					$data .='</div>';
				}
					if($pages>1)
					{
						$data .= '<div class="paging '.$paging_name.'" style="clear:both;">';

		 				for($i = 1;$i<=$pages;$i++ )
		 				{
		 					$data.='<a href="javascript:void(0);"'." page=$i ";
		 					if($i==$current_page)
		 						$data.='class ="active"';
		 					$data.=">$i</li>";
		 				}

						$data = $data . "</div>";
					}
					}



				// $data .= "<nav role='page' style='clear:both;'><ul>";
				if($pages>1)
				{
					$data .= '<div class="paging '.$paging_name.'" style="clear:both;">';

	 				for($i = 1;$i<=$pages;$i++ )
	 				{
	 					$data.='<a href="javascript:void(0);"'." page=$i ";
	 					if($i==$current_page)
	 						$data.='class ="active"';
	 					$data.=">$i</li>";
	 				}

					$data = $data . "</div>";
				}
				echo $data;
				exit();
			}
		}

     }
?>