<?php

	defined('_JEXEC') or die;

	class Vichy_productHelper {
		function getCategoriesByGroup($group){
			$db = & JFactory::getDbo();
			$lg = &JFactory::getLanguage();
        	$sql = "SELECT c.id, c.title, c.alias, c.main_color,c.description, c.image, c.title_description,c.short_desc, c.params,c.parent_id,count(pc.product_id) as total_product,
                            case when count(pc.product_id)=1 then pc.product_id end as product_id                           
                            from #__categories as c 
                            JOIN vc_vichy_product_category pc on c.id = pc.category_id
                             where published > 0 and parent_id in ($group) and language = '".str_replace(' ', '', $lg->getTag())."' GROUP BY c.id ORDER BY order_on_layout ";
                            
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObjectList();
		}
        function checkCategoryForRedirect($group,$id){
            $db = & JFactory::getDbo();
            $lg = &JFactory::getLanguage();
            $sql = "SELECT c.id,c.parent_id,count(pc.product_id) as total_product,
                            case when count(pc.product_id)=1 then pc.product_id end as product_id                           
                            from #__categories as c 
                            JOIN vc_vichy_product_category pc on c.id = pc.category_id
                             where published > 0 and c.id=$id and parent_id in ($group) and language = '".str_replace(' ', '', $lg->getTag())."'ORDER BY order_on_layout ";
                            
            $db->setQuery($sql);
            $db->query();
            return $db->loadObject();
        }
        function getProductByCate($cate_id){
        	$db = & JFactory::getDbo();
        	$sql = "SELECT p.id, p.name, p.image, p.rating, p.reviews, sl.title as category from vc_vichy_product as p left join (select pc.product_id, pc.category_id, c.title from vc_vichy_product_category as pc inner join vc_categories as c on pc.category_id = c.id where c.parent_id = 8) as sl on p.id = sl.product_id where p.published = 1 and sl.category_id = ".$cate_id;
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObjectList();
        }
        function getProduct($product_id){
            $db = & JFactory::getDbo();
            
            $query = "SELECT tb1.id as pid ,tb1.name,tb1.image,Group_Concat(tb1.main_color) as main_color,tb1.rating,tb1.reviews,tb1.description,tb1.effectiveness,tb1.tolerance,tb1.pleasure,tb1.video,
                            Group_Concat(tb1.skincare) as skincare,
                            Group_Concat(tb1.product_range) as product_range,
                            Group_Concat(tb1.range_id) as range_id
                        from  (
                                select p.*,c.main_color,
                                case when c.parent_id in(".PRODUCT_RANG.",".PRODUCT_RANG_WITHOUT_STEP.") then c.id end as range_id,
                                        case when c.parent_id =".SKIN_CARE." then Group_Concat(c.title) end as skincare,
                                        case when c.parent_id in(".PRODUCT_RANG.",".PRODUCT_RANG_WITHOUT_STEP.") then c.title end as product_range
                                        from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c
                                        where p.id = pc.product_id 
                                            and c.id = pc.category_id
                                            and p.id=$product_id
                                        Group by c.parent_id
                               ) as tb1 
                               group by tb1.id
                     ";
                     
            $db->setQuery($query);
            
            $result = $db->loadObject();
            return $result;
        }
       
		function getProductRangById($productrange_id){
            $db = & JFactory::getDbo();
			$lg = &JFactory::getLanguage();
        	$sql = "SELECT id, title, title_description, alias, description, image, thumbnail, main_color from #__categories where published > 0 and id = $productrange_id";
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObject();
        }
        

        function getOtherRanges($productrange_id){
            $db = & JFactory::getDbo();
            $lg = &JFactory::getLanguage();
            $sql = "SELECT id, title,parent_id from #__categories 
                    where published > 0 and id <> $productrange_id and parent_id in (".PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP.") ORDER BY title asc";
            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();
        }

		function getProductsWithoutStep($start,$display)
        {
            $db = & JFactory::getDbo();
            
            $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_new,tb1.rating,tb1.reviews,
                                Group_Concat(tb1.skincare) as skincare,
                                Group_Concat(tb1.product_range) as product_range
                            from  (
                                    select p.*,c.main_color,c.id as cid,
                                        case when c.parent_id =".SKIN_CARE." then Group_Concat(c.title) end as skincare,
                                        case when c.parent_id=".PRODUCT_RANG_WITHOUT_STEP." then c.title end as product_range
                                    from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

                                    where p.id = pc.product_id 
                                        and c.id = pc.category_id
                                        and c.parent_id = ".PRODUCT_RANG_WITHOUT_STEP."
                                        Group by p.id,c.parent_id
                                   ) as tb1 
                                 group by tb1.id
                                 limit ".$start.",".$display 
                            ;

            $db->setQuery($query_new);
            $list_data = $db->loadObjectList();
            
            return $list_data;
        }
        
        function getNewProducts($start,$display)
        {
            $db = & JFactory::getDbo();
            
            // $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_new,tb1.rating,tb1.reviews,
            //                     Group_Concat(tb1.skincare) as skincare,
            //                     Group_Concat(tb1.product_range) as product_range
            //                 from  (
            //                         select p.*,c.main_color,c.id as cid,
            //                             case when c.parent_id =".SKIN_CARE." then Group_Concat(c.title) end as skincare,
            //                             case when c.parent_id in(".PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP.") then c.title end as product_range
            //                         from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

            //                         where p.id = pc.product_id 
            //                             and c.id = pc.category_id
            //                             and p.is_new=1
            //                         Group by p.id,c.parent_id
            //                        ) as tb1 
            //                      group by tb1.id
            //                      limit ".$start.",".$display 
            //                 ;
              $query_new="SELECT  a.description,a.id AS pid,a.is_new,a.is_favorite, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( c.parent_id, '|', b.category_id ) AS cid, GROUP_CONCAT( DISTINCT (
                    c.parent_id
                    ) ) AS parent_id, GROUP_CONCAT( c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( DISTINCT(c.main_color) ) as main_color 
                    FROM vc_vichy_product AS a
                    INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.id
                    INNER JOIN vc_categories AS c ON b.category_id = c.id                    
                    where a.is_new=1
                    AND a.published =1
                    AND a.language
                    IN (
                     'vi-VN',  '*'
                    )
                    GROUP BY a.id
                    limit ".$start.",".$display;
        
            $db->setQuery($query_new);
            $list_data = $db->loadObjectList();
            
            return $list_data;
        }
        function getYourNeedById($yourneed_id){
            $db= &JFactory::getDbo();
            $lg=&JFactory::getLanguage();
            $sql="select * from #__categories where id=$yourneed_id";
            $db->setQuery($sql);
            $db->query();
            return $db->loadObject();
        }
        function getProductYourNeedById($yourneed_id){
            $db=& JFactory::getDbo();
            $lg=& JFactory::getLanguage();
            $sql="select info.*, tmp.title as product_range,tmp.product_range_id
                from 
                (SELECT a.id AS product_id, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( DISTINCT (
                b.category_id
                ) ) AS category_id, GROUP_CONCAT( DISTINCT (
                c.parent_id
                ) ) AS parent_id, GROUP_CONCAT( c.title ) AS your_need, GROUP_CONCAT( DISTINCT (
                c.main_color
                ) ) AS main_color
                FROM #__vichy_product AS a
                INNER JOIN #__vichy_product_category AS b ON b.product_id = a.id
                INNER JOIN #__categories AS c ON b.category_id = c.id
                WHERE c.id =$yourneed_id
                GROUP BY a.id) as info

                left join 

                (select b.product_id, c.title,c.id as product_range_id from vc_vichy_product_category as b  
                INNER JOIN vc_categories AS c ON b.category_id = c.id
                where c.parent_id =8
                )AS tmp

                ON tmp.product_id = info.product_id";
            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();
        }
        function getFavoriteProducts($start,$display)
        {
            $db = & JFactory::getDbo();
            
            // $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_favorite,tb1.rating,tb1.reviews,
            //                     Group_Concat(tb1.skincare) as skincare,
            //                     Group_Concat(tb1.product_range) as product_range
            //                 from  (
            //                         select p.*,c.main_color,c.id as cid,
            //                             case when c.parent_id =".SKIN_CARE." then Group_Concat(c.title) end as skincare,
            //                             case when c.parent_id in(".PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP.") then c.title end as product_range
            //                         from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

            //                         where p.id = pc.product_id 
            //                             and c.id = pc.category_id
            //                             and p.is_favorite=1
            //                         Group by p.id,c.parent_id
            //                        ) as tb1 
            //                      group by tb1.id
            //                      limit ".$start.",".$display 
            //                 ;

            $query_new="SELECT  a.description,a.id AS pid,a.is_new,a.is_favorite, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( c.parent_id, '|', b.category_id ) AS cid, GROUP_CONCAT( DISTINCT (
                    c.parent_id
                    ) ) AS parent_id, GROUP_CONCAT( c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( DISTINCT(c.main_color) ) as main_color 
                    FROM vc_vichy_product AS a
                    INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.id
                    INNER JOIN vc_categories AS c ON b.category_id = c.id                    
                    where a.is_new=1
                    AND a.published =1
                    AND a.language
                    IN (
                     'vi-VN',  '*'
                    )
                    GROUP BY a.id
                    limit ".$start.",".$display;
        
            $db->setQuery($query_new);
            $list_data = $db->loadObjectList(); 
            
            return $list_data;
        }

		function countProductsWithoutSteps(){

            $db = & JFactory::getDbo();


            
            $query= "SELECT COUNT(pc.product_id) as count from vc_vichy_product_category pc, vc_categories  c where c.parent_id = ".PRODUCT_RANG_WITHOUT_STEP." AND pc.category_id = c.id";

           
            $db->setQuery($query);
            $result = $db->loadObject();
            
            return $result->count;
            
        }
        
        function countNewProducts(){

            $db = & JFactory::getDbo();

            $query= "SELECT COUNT(p.id) as count from vc_vichy_product p where p.is_new =1";

            $db->setQuery($query);
            $result = $db->loadObject();
           
            return $result->count;
            

        }

        function countFavoriteProducts(){

            $db = & JFactory::getDbo();

            $query= "SELECT COUNT(p.id) as count from vc_vichy_product p where p.is_favorite =1";

            $db->setQuery($query);
            $result = $db->loadObject();
           
            return $result->count;
        }
        
		/**
		* Products Search method
		*
		* The sql must return the following fields that are used in a common display
		* routine: href, title, section, created, text, browsernav
		* @param string Target search string
		* @param string matching option, exact|any|all
		* */
		function onProductSearch($text, $phrase='', $ordering='')
		{
			$db		= JFactory::getDbo();
			$app	= JFactory::getApplication();
			$user	= JFactory::getUser();
			$groups	= implode(',', $user->getAuthorisedViewLevels());
	
			
	
			$sContent		= 1;
			$sArchived		= 1;
			$limit			= 50;
			$state = array();
			if ($sContent) {
				$state[]=1;
			}
			if ($sArchived) {
				$state[]=2;
			}
	
			$text = trim($text);
			if ($text == '') {
				return array();
			}
	
			switch($phrase) {
			case 'exact':
				$text		= $db->Quote('%'.$db->escape($text, true).'%', false);
				$wheres2 	= array();
				$wheres2[]	= 'a.name LIKE '.$text;
				$where		= '(' . implode(') OR (', $wheres2) . ')';
				break;

			case 'any':
			case 'all';
			default:
				$words = explode(' ', $text);
				$wheres = array();
				foreach ($words as $word) {
					$word		= $db->Quote('%'.$db->escape($word, true).'%', false);
					$wheres2 	= array();
					$wheres2[]	= 'a.name LIKE '.$word;
					$wheres2[]	= 'a.description LIKE '.$word;
					$wheres2[]	= 'a.effectiveness LIKE '.$word;
					$wheres2[]	= 'a.tolerance LIKE '.$word;
					$wheres2[]	= 'a.pleasure LIKE '.$word;
					$wheres[]	= implode(' OR ', $wheres2);
				}
				$where = '(' . implode(($phrase == 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
				break;
			}
	
			switch ($ordering) {
				case 'alpha':
					$order = 'a.name ASC';
					break;
	
				case 'category':
				case 'popular':
				case 'newest':
				case 'oldest':
				default:
					$order = 'a.name DESC';
			}
	
			$return = array();
			if (!empty($state)) {
				$query	= $db->getQuery(true);
				$query->select('a.name, a.description AS text, a.create_date AS created, a.id, GROUP_CONCAT(b.category_id) as catid, GROUP_CONCAT( c.parent_id ) as pcatid');
				$query->from('#__vichy_product AS a');
				$query->innerJoin('#__vichy_product_category as b ON b.product_id = a.id');
				$query->innerJoin('#__categories as c ON b.category_id = c.id');
				$query->where('('. $where .')' . ' AND a.published = 1' );
				$query->group('a.id');
				$query->order($order);
				if ($app->isSite() && $app->getLanguageFilter()) {
					$query->where('a.language in (' . $db->Quote(JFactory::getLanguage()->getTag()) . ',' . $db->Quote('*') . ')');
				}
				
				$db->setQuery($query, 0, $limit);
				$rows = $db->loadObjectList();
	
				
			}
			return $rows;
		}
    }
    
?>