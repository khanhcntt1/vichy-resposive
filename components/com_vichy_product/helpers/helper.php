<?php

	defined('_JEXEC') or die;

	class Vichy_productHelper extends JComponentHelper {
        
		function getCategoriesByGroup($group){
			$db = & JFactory::getDbo();
			$lg = &JFactory::getLanguage();
            $language_tag = str_replace(' ', '', $lg->getTag());
        	$sql = "SELECT c.id, c.title, c.alias, c.main_color,c.description, c.image, c.title_description,c.short_desc, c.params,c.parent_id,count(pc.product_id) as total_product,
                            case when count(pc.product_id)=1 then pc.product_id end as product_id                           
                            from #__categories as c 
                            JOIN vc_vichy_product_category pc on c.id = pc.category_id
                             where published > 0 and parent_id in ($group) and language = '".$language_tag."' GROUP BY c.id ORDER BY order_on_layout ";

        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObjectList();
		}
        function checkCategoryForRedirect($group,$id){
            $db = & JFactory::getDbo();
            $lg = &JFactory::getLanguage();
            $sql = "SELECT c.id,c.parent_id,count(pc.product_id) as total_product,
                            case when count(pc.product_id)=1 then pc.product_id end as product_id                           
                            from #__categories as c 
                            JOIN vc_vichy_product_category pc on c.id = pc.category_id
                             where published > 0 and c.id=$id and parent_id in ($group) and language = '".str_replace(' ', '', $lg->getTag())."'ORDER BY order_on_layout ";
                            
            $db->setQuery($sql);
            $db->query();
            return $db->loadObject();
        }
        function getProductByCate($cate_id){
        	$db = & JFactory::getDbo();
        	$sql = "SELECT p.id, p.name, p.image, p.rating, p.reviews, sl.title as category from vc_vichy_product as p left join (select pc.product_id, pc.category_id, c.title from vc_vichy_product_category as pc inner join vc_categories as c on pc.category_id = c.id where c.parent_id = 8) as sl on p.id = sl.product_id where p.published = 1 and sl.category_id = ".$cate_id;
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObjectList();
        }
        function getProduct($product_id){
            self::set_category_id();

            $db = & JFactory::getDbo();
            
            $query = "SELECT tb1.id as pid ,tb1.name,tb1.image,Group_Concat(tb1.main_color) as main_color,tb1.rating,tb1.reviews,tb1.description,tb1.effectiveness,tb1.tolerance,tb1.pleasure,tb1.video,tb1.link as linkbuy,
                            Group_Concat(tb1.skincare) as skincare,
                            Group_Concat(tb1.product_range) as product_range,
                            Group_Concat(tb1.range_id) as range_id
                        from  (
                                select p.*,c.main_color,
                                case when c.parent_id in(".self::$_product_rang.",".self::$_product_rang_without_step.") then c.id end as range_id,
                                        case when c.parent_id =".self::$_skin_care." then Group_Concat(c.title) end as skincare,
                                        case when c.parent_id in(".self::$_product_rang.",".self::$_product_rang_without_step.") then c.title end as product_range
                                        from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c
                                        where p.id = pc.product_id 
                                            and c.id = pc.category_id
                                            and p.id=$product_id
                                        Group by c.parent_id
                               ) as tb1 
                               group by tb1.id
                     ";
                     
            $db->setQuery($query);
            
            $result = $db->loadObject();
            return $result;
        }
       
		function getProductRangById($productrange_id){
            $db = & JFactory::getDbo();
			$lg = &JFactory::getLanguage();
        	// $sql = "SELECT id, title, title_description, alias, description, image, thumbnail, main_color from #__categories where published > 0 and id = $productrange_id";
            $language_tag = str_replace(' ', '', $lg->getTag());
            if($language_tag == 'vi-VN'){
            $sql = "SELECT c.id, c.title, c.title_description, c.alias, c.description, c.image, c.thumbnail, c.main_color,ar.yourneed_id
                    from #__categories c
                    left join vc_vichy_advice_range ar on ar.range_id=c.id
                    where c.published > 0 and c.id = $productrange_id
                    ";
            }else{
                $sql = "SELECT c.id, c.title, c.title_description, c.alias, c.description, c.image, c.thumbnail, c.main_color,ar.yourneed_id
                        from #__categories c                    
                        inner join vc_jf_translationmap as tm on c.id=tm.translation_id and tm.language='".$language_tag."' and reference_table='categories'
                        left join vc_vichy_advice_range ar on ar.range_id=tm.reference_id
                        where c.published > 0 and c.id = $productrange_id
                        ";
            }
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObject();
        }
        

        function getOtherRanges($productrange_id){
            self::set_category_id();

            $db = & JFactory::getDbo();
            $lg = &JFactory::getLanguage();            
            $language_tag = str_replace(' ', '', $lg->getTag());
            $sql = "SELECT id, title,parent_id from #__categories 
                    where published > 0 and id <> $productrange_id and parent_id in (".self::$_product_rang.','.self::$_product_rang_without_step.") and language = '".$language_tag."' ORDER BY title asc";
            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();
        }

		function getProductsWithoutStep($start,$display)
        {
            self::set_category_id();

            $db = & JFactory::getDbo();
            
            $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_new,tb1.rating,tb1.reviews,
                                Group_Concat(tb1.skincare) as skincare,
                                Group_Concat(tb1.product_range) as product_range
                            from  (
                                    select p.*,c.main_color,c.id as cid,
                                        case when c.parent_id =".self::$_skin_care." then Group_Concat(c.title) end as skincare,
                                        case when c.parent_id=".self::$_product_rang_without_step." then c.title end as product_range
                                    from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

                                    where p.id = pc.product_id 
                                        and c.id = pc.category_id
                                        and c.parent_id = ".self::$_product_rang_without_step."
                                        Group by p.id,c.parent_id
                                   ) as tb1 
                                 group by tb1.id
                                 limit ".$start.",".$display 
                            ;

            $db->setQuery($query_new);
            $list_data = $db->loadObjectList();
            
            return $list_data;
        }
        
        function getNewProducts($start,$display)
        {
            $db = & JFactory::getDbo();
            
            // $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_new,tb1.rating,tb1.reviews,
            //                     Group_Concat(tb1.skincare) as skincare,
            //                     Group_Concat(tb1.product_range) as product_range
            //                 from  (
            //                         select p.*,c.main_color,c.id as cid,
            //                             case when c.parent_id =".SKIN_CARE." then Group_Concat(c.title) end as skincare,
            //                             case when c.parent_id in(".PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP.") then c.title end as product_range
            //                         from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

            //                         where p.id = pc.product_id 
            //                             and c.id = pc.category_id
            //                             and p.is_new=1
            //                         Group by p.id,c.parent_id
            //                        ) as tb1 
            //                      group by tb1.id
            //                      limit ".$start.",".$display 
            //                 ;
              $query_new="SELECT  a.description,a.id AS pid,a.is_new,a.is_favorite, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( c.parent_id, '|', b.category_id ) AS cid, GROUP_CONCAT( DISTINCT (
                    c.parent_id
                    ) ) AS parent_id, GROUP_CONCAT( c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( DISTINCT(c.main_color) ) as main_color 
                    FROM vc_vichy_product AS a
                    INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.id
                    INNER JOIN vc_categories AS c ON b.category_id = c.id                    
                    where a.is_new=1
                    AND a.published =1
                    AND a.language
                    IN (
                     'vi-VN',  '*'
                    )
                    GROUP BY a.id
                    limit ".$start.",".$display;
        
            $db->setQuery($query_new);
            $list_data = $db->loadObjectList();
            
            return $list_data;
        }
        function getYourNeedById($yourneed_id){
            $db= &JFactory::getDbo();
            $lg=&JFactory::getLanguage();
            $sql="select * from #__categories where id=$yourneed_id";
            $db->setQuery($sql);
            $db->query();
            return $db->loadObject();
        }
        function getProductYourNeedById($yourneed_id){
            self::set_category_id();

            $db=& JFactory::getDbo();           
            $sql="select info.*, tmp.title as product_range,tmp.product_range_id
                from 
                (SELECT a.id AS product_id, a.is_new,a.is_favorite,a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( DISTINCT (
                b.category_id
                ) ) AS category_id, GROUP_CONCAT( DISTINCT (
                c.parent_id
                ) ) AS parent_id, GROUP_CONCAT( c.title ) AS your_need, GROUP_CONCAT( DISTINCT (
                c.main_color
                ) ) AS main_color
                FROM #__vichy_product AS a
                INNER JOIN #__vichy_product_category AS b ON b.product_id = a.id
                INNER JOIN #__categories AS c ON b.category_id = c.id
                WHERE c.id =$yourneed_id
                GROUP BY a.id) as info

                left join 

                (select b.product_id, c.title,c.id as product_range_id from vc_vichy_product_category as b  
                INNER JOIN vc_categories AS c ON b.category_id = c.id
                where c.parent_id in (".self::$_product_rang.','.self::$_product_rang_without_step.")
                )AS tmp

                ON tmp.product_id = info.product_id";
            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();
        }
        function getFavoriteProducts($start,$display)
        {
            $db = & JFactory::getDbo();
            
            // $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_favorite,tb1.rating,tb1.reviews,
            //                     Group_Concat(tb1.skincare) as skincare,
            //                     Group_Concat(tb1.product_range) as product_range
            //                 from  (
            //                         select p.*,c.main_color,c.id as cid,
            //                             case when c.parent_id =".SKIN_CARE." then Group_Concat(c.title) end as skincare,
            //                             case when c.parent_id in(".PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP.") then c.title end as product_range
            //                         from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

            //                         where p.id = pc.product_id 
            //                             and c.id = pc.category_id
            //                             and p.is_favorite=1
            //                         Group by p.id,c.parent_id
            //                        ) as tb1 
            //                      group by tb1.id
            //                      limit ".$start.",".$display 
            //                 ;

            $query_new="SELECT  a.description,a.id AS pid,a.is_new,a.is_favorite, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( c.parent_id, '|', b.category_id ) AS cid, GROUP_CONCAT( DISTINCT (
                    c.parent_id
                    ) ) AS parent_id, GROUP_CONCAT( c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( DISTINCT(c.main_color) ) as main_color 
                    FROM vc_vichy_product AS a
                    INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.id
                    INNER JOIN vc_categories AS c ON b.category_id = c.id                    
                    where a.is_new=1
                    AND a.published =1
                    AND a.language
                    IN (
                     'vi-VN',  '*'
                    )
                    GROUP BY a.id
                    limit ".$start.",".$display;
        
            $db->setQuery($query_new);
            $list_data = $db->loadObjectList(); 
            
            return $list_data;
        }

		function countProductsWithoutSteps(){
            self::set_category_id();

            $db = & JFactory::getDbo();


            
            $query= "SELECT COUNT(pc.product_id) as count from vc_vichy_product_category pc, vc_categories  c where c.parent_id = ".self::$_product_rang_without_step." AND pc.category_id = c.id";

           
            $db->setQuery($query);
            $result = $db->loadObject();
            
            return $result->count;
            
        }
        
        function countNewProducts(){

            $db = & JFactory::getDbo();

            $query= "SELECT COUNT(p.id) as count from vc_vichy_product p where p.is_new =1";

            $db->setQuery($query);
            $result = $db->loadObject();
           
            return $result->count;
            

        }

        function countFavoriteProducts(){

            $db = & JFactory::getDbo();

            $query= "SELECT COUNT(p.id) as count from vc_vichy_product p where p.is_favorite =1";

            $db->setQuery($query);
            $result = $db->loadObject();
           
            return $result->count;
        }
        
		/**
		* Products Search method
		*
		* The sql must return the following fields that are used in a common display
		* routine: href, title, section, created, text, browsernav
		* @param string Target search string
		* @param string matching option, exact|any|all
		* */
		function onProductSearch($text, $phrase='', $ordering='')
		{
			$db		= JFactory::getDbo();
			$app	= JFactory::getApplication();
			$user	= JFactory::getUser();
			$groups	= implode(',', $user->getAuthorisedViewLevels());
	
			$session =& JFactory::getSession();
            $lang = $session->get('lang');
	
			$sContent		= 1;
			$sArchived		= 1;
			$limit			= 50;
			$state = array();
			if ($sContent) {
				$state[]=1;
			}
			if ($sArchived) {
				$state[]=2;
			}
	
			$text = trim($text);
			if ($text == '') {
				return array();
			}
	
			switch($phrase) {
			case 'exact':
				$text		= $db->Quote('%'.$db->escape($text, true).'%', false);
				$wheres2 	= array();
				$wheres2[]	= 'a.name LIKE '.$text;
				$where		= '(' . implode(') OR (', $wheres2) . ')';
				break;

			case 'any':
			case 'all';
			default:
				$words = explode(' ', $text);
				$wheres = array();
				foreach ($words as $word) {
					$word		= $db->Quote('%'.$db->escape($word, true).'%', false);
					$wheres2 	= array();
					$wheres2[]	= 'a.name LIKE '.$word;
					$wheres2[]	= 'a.description LIKE '.$word;
					$wheres2[]	= 'a.effectiveness LIKE '.$word;
					$wheres2[]	= 'a.tolerance LIKE '.$word;
					$wheres2[]	= 'a.pleasure LIKE '.$word;
					$wheres[]	= implode(' OR ', $wheres2);
				}
				$where = '(' . implode(($phrase == 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
				break;
			}
	
			switch ($ordering) {
				case 'alpha':
					$order = 'a.name ASC';
					break;
	
				case 'category':
				case 'popular':
				case 'newest':
				case 'oldest':
				default:
					$order = 'a.name DESC';
			}
	
			$return = array();
			if (!empty($state)) {
				$query	= $db->getQuery(true);
				$query->select('a.name, a.description AS text, a.create_date AS created, a.id, GROUP_CONCAT(b.category_id) as catid, GROUP_CONCAT( c.parent_id ) as pcatid');
				$query->from('#__vichy_product AS a');
				$query->innerJoin('#__vichy_product_category as b ON b.product_id = a.id');
				$query->innerJoin('#__categories as c ON b.category_id = c.id');
				$query->where('('. $where .')' . ' AND a.published = 1 AND a.language="'.$lang.'" ' );
				$query->group('a.id');
				$query->order($order);
				if ($app->isSite() && $app->getLanguageFilter()) {
					$query->where('a.language in (' . $db->Quote(JFactory::getLanguage()->getTag()) . ',' . $db->Quote('*') . ')');
				}
				
				$db->setQuery($query, 0, $limit);
				$rows = $db->loadObjectList();
	
				
			}
			return $rows;
		}

        function getPRODUCT_RANG(){
            self::set_category_id();
            return self::$_product_rang;
        }

        function get_translation_id($lang_code, $reference_id, $reference_table){
            $db  = & JFactory::getDBO();
            $translation_id = null;
            $sql = "select translation_id from #__jf_translationmap where language='".$lang_code."' and reference_id=".$reference_id." and reference_table='".$reference_table."'";
            $db->setQuery($sql);
            $translation_id = $db->loadResult();
            return $translation_id;
        }

        static function getList(&$params)
        {
            $lg = &JFactory::getLanguage();
            $app = JFactory::getApplication();
            $menu = $app->getMenu();

            // If no active menu, use default
            $active = ($menu->getActive()) ? $menu->getActive() : $menu->getDefault();

            $user = JFactory::getUser();
            $levels = $user->getAuthorisedViewLevels();
            asort($levels);
            $key = 'menu_items'.$params.implode(',', $levels).'.'.$active->id;
            $cache = JFactory::getCache('mod_menu', '');
            if (!($items = $cache->get($key)))
            {
                // Initialise variables.
                $list		= array();
                $db			= JFactory::getDbo();

                $path		= $active->tree;
                $start		= (int) $params->get('startLevel');
                $end		= (int) $params->get('endLevel');
                $showAll	= $params->get('showAllChildren');
                $items 		= $menu->getItems('menutype', $params->get('menutype'));

                $lastitem	= 0;

                if ($items) {
                    foreach($items as $i => $item)
                    {
                        if (($start && $start > $item->level)
                            || ($end && $item->level > $end)
                            || (!$showAll && $item->level > 1 && !in_array($item->parent_id, $path))
                            || ($start > 1 && !in_array($item->tree[$start-2], $path))
                        ) {
                            unset($items[$i]);
                            continue;
                        }

                        $item->deeper = false;
                        $item->shallower = false;
                        $item->level_diff = 0;

                        if (isset($items[$lastitem])) {
                            $items[$lastitem]->deeper		= ($item->level > $items[$lastitem]->level);
                            $items[$lastitem]->shallower	= ($item->level < $items[$lastitem]->level);
                            $items[$lastitem]->level_diff	= ($items[$lastitem]->level - $item->level);
                        }

                        $item->parent = (boolean) $menu->getItems('parent_id', (int) $item->id, true);

                        $lastitem			= $i;
                        $item->active		= false;
                        $item->flink = $item->link;

                        // Reverted back for CMS version 2.5.6
                        switch ($item->type)
                        {
                            case 'separator':
                                // No further action needed.
                                continue;

                            case 'url':
                                if ((strpos($item->link, 'index.php?') === 0) && (strpos($item->link, 'Itemid=') === false)) {
                                    // If this is an internal Joomla link, ensure the Itemid is set.
                                    $item->flink = $item->link.'&Itemid='.$item->id;
                                }
                                break;

                            case 'alias':
                                // If this is an alias use the item id stored in the parameters to make the link.
                                $item->flink = 'index.php?Itemid='.$item->params->get('aliasoptions');
                                break;

                            default:
                                $router = JSite::getRouter();
                                if ($router->getMode() == JROUTER_MODE_SEF) {
                                    $item->flink = 'index.php?Itemid='.$item->id;
                                }
                                else {
                                    $item->flink .= '&Itemid='.$item->id;
                                }
                                break;
                        }

                        if (strcasecmp(substr($item->flink, 0, 4), 'http') && (strpos($item->flink, 'index.php?') !== false)) {
                            $item->flink = JRoute::_($item->flink, true, $item->params->get('secure'));
                        }
                        else {
                            $item->flink = JRoute::_($item->flink);
                        }

                        $item->title = htmlspecialchars($item->title, ENT_COMPAT, 'UTF-8', false);
                        $item->anchor_css   = htmlspecialchars($item->params->get('menu-anchor_css', ''), ENT_COMPAT, 'UTF-8', false);
                        $item->anchor_title = htmlspecialchars($item->params->get('menu-anchor_title', ''), ENT_COMPAT, 'UTF-8', false);
                        $item->menu_image   = $item->params->get('menu_image', '') ? htmlspecialchars($item->params->get('menu_image', ''), ENT_COMPAT, 'UTF-8', false) : '';
                    }

                    if (isset($items[$lastitem])) {
                        $items[$lastitem]->deeper		= (($start?$start:1) > $items[$lastitem]->level);
                        $items[$lastitem]->shallower	= (($start?$start:1) < $items[$lastitem]->level);
                        $items[$lastitem]->level_diff	= ($items[$lastitem]->level - ($start?$start:1));
                    }
                }

                $cache->store($items, $key);
            }
            return $items;
        }


        function getCategoryWithParent($arr_option){
            $db = & JFactory::getDbo();

            $sql = "SELECT id, parent_id, title, alias, main_color, description, image, title_description from #__categories where published > 0 and LOCATE('com_vichy', extension) > 0 and (parent_id = ".$arr_option['product_rang_id']." or parent_id = ".$arr_option['product_rang_without_step_id']." or parent_id = ".$arr_option['your_need_id']." or parent_id = ".$arr_option['skin_care_id'].") and language = '".str_replace(' ', '', $arr_option['lang_code'])."' ORDER BY rgt";
            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();

        }

        function getCategoryWithParent_YourNeed($arr_option){
            $db = & JFactory::getDbo();

            $sql = "SELECT g.id,c.parent_id as grand_id,g.title as child_title,g.parent_id,c.title from #__categories as c Join #__categories as g On c.id=g.parent_id where c.published > 0 and LOCATE('com_vichy', c.extension) > 0 and (c.parent_id=".$arr_option['your_need_id']." OR g.parent_id=".$arr_option['your_need_id'].") and g.language = '".str_replace(' ', '', $arr_option['lang_code'])."' ORDER BY c.rgt ";

            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();

        }
    }
    
?>
