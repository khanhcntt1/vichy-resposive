<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_adviceViewAdvice extends JView{
    	function display($tpl=null){
    		$curLanguage = JFactory::getLanguage();
            $lang = $curLanguage->getTag();
            $this->lang = $lang;

            $youneedid=(!isset($_GET['catid'])||empty($_GET['catid'])) ? ($lang == 'vi-VN'?63:104) : $_GET['catid'];
            
            if($youneedid == 61){
                $title_item = VICHY_TITLE_CLEAN_MOISTURIZING;
            }else if($youneedid == 62){
                $title_item = VICHY_TITLE_LOTION;
            }else if($youneedid == 63){
                $title_item = VICHY_TITLE_REDUCE_OIL;
            }else if($youneedid == 64){
                $title_item = VICHY_TITLE_ANTI_AGING;
            }else if($youneedid == 58){
                $title_item = VICHY_TITLE_ANTI_HAIR;
            }else{
                $title_item = $this->getTitleByCategory($youneedid);    
            }
            
            $this->assignRef('title', $title);
            $this->assignRef('title_item', $title_item);

            if($lang == 'vi-VN'){
                //$catid = 63;
                $path_name = "Lời khuyên";
                $title = VICHY_TITLE_ADVICE;
                $this->yourneed_parent = YOUR_NEED;
            }else{
                //$catid = 104;
                $path_name = "Advice";
                $title = VICHY_TITLE_ADVICE_EN;
                $this->yourneed_parent = $this->get_translation_id($lang, YOUR_NEED, 'categories');
            }

            //$youneedid=(!isset($_GET['catid'])||empty($_GET['catid']))? $catid : $_GET['catid'];

    		$list = Vichy_adviceHelper::getContentByType('advice',$youneedid);
            $menu_left= Vichy_adviceHelper::getCategoryWithParent_YourNeed($this->yourneed_parent);
    		$new_arr = array();
    		foreach ($list as $k => $v) {
                if(empty($v->type) && empty($v->published))
                {
                    $new_arr[0][0]=$v;
                    break;
                }
    			$pa = $v->parent_id;
    			$new_arr[$pa][] = $v;
    		}
            
            $items = $this->getCategoriesByGroup($youneedid, $lang);
            $this->items = $items;
    		$this->data = $new_arr;         
            $this->menu_left=$menu_left;

            $app    = JFactory::getApplication();
            $pathway = $app->getPathway();
            $pathway->addItem($path_name,'javascript:void(0);');
            $pathway->addItem(shortDesc($this->data[0][0]->title,20), '');
    		parent::display($tpl);
    	}

        function getTitleByCategory($id){
            $db = & JFactory::getDbo();
            $sql = "SELECT title FROM #__categories where id=$id";
            $db->setQuery($sql);
            $title = $db->loadResult();
            return $title;
        }

        function get_translation_id($lang_code, $reference_id, $reference_table){
            $db  = & JFactory::getDBO();
            $translation_id = null;
            $sql = "select translation_id from #__jf_translationmap where language='".$lang_code."' and reference_id=".$reference_id." and reference_table='".$reference_table."'";
            $db->setQuery($sql);
            $translation_id = $db->loadResult();
            return $translation_id;
        }

        function getCategoriesByGroup($yourneed, $language='vi-VN')
        {
            $db = & JFactory::getDbo();
            if($language == 'vi-VN'){
                $sql =  "SELECT a.*,c_r.main_color,c_r.title,c_r.params,c_r.title_description FROM #__vichy_advice_range a
                        
                        Join vc_categories c_r on a.range_id = c_r.id

                        where a.yourneed_id=$yourneed

                        ";
            }else{
                $sql = "SELECT a.*, c_r.main_color,c_r.title,c_r.params,c_r.title_description
                        FROM
                        (select yourneed_id, tm2.translation_id as range_id
                        from
                        (Select translation_id as yourneed_id, range_id from vc_jf_translationmap as tm inner join vc_vichy_advice_range as a_r on a_r.yourneed_id = tm.reference_id and reference_table='categories') a_r_trans
                        inner join vc_jf_translationmap as tm2 on a_r_trans.range_id = tm2.reference_id and tm2.reference_table='categories') as a join vc_categories as c_r on a.range_id = c_r.id
                        where a.yourneed_id = $yourneed";
            }            
            
            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();
        }
    }
?>