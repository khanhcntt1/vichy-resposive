<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_advice/css/style.css');
?>
<div class = "vc_advice_main">
	<div class ="vc_advice_content" style="background: url('<?php echo JURI::root();?>components/com_vichy_advice/images/bg_vichy_diagnosis.png') no-repeat" >
		<div class = "vc_advice_text">
			<div class ="vc_advice_title">
				DIAGNOSTICS
			</div>
			<div class ="vc_advice_short_description">
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
			</div>
			<div class ="vc_advice_full_description">
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut .
			</div>
		</div>
			<a class ="vc_advice_start_button" href="">START YOUR DIAGNOSTICS</a>
	</div>
</div>

<div class = "vc_advice_main">
	<div class ="vc_advice_content" style="background: url('<?php echo JURI::root();?>components/com_vichy_advice/images/bg_vichy_advice.png') no-repeat" >
		<div class = "vc_advice_text">
			<div class ="vc_advice_title">
				ADVICE
			</div>
			<div class ="vc_advice_short_description">
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
			</div>
			<div class ="vc_advice_full_description">
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut .
			</div>
		</div>
			<a class ="vc_advice_start_button" href="">START YOUR ADVICE</a>
	</div>
</div>

<div class = "vc_advice_main">
	<div class ="vc_advice_content" style="background: url('<?php echo JURI::root();?>components/com_vichy_advice/images/bg_vichy_skin_encyclopedia.png') no-repeat" >
		<div class = "vc_advice_text">
			<div class ="vc_advice_title">
				SKIN ENCYCLOPEDIA
			</div>
			<div class ="vc_advice_short_description">
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
			</div>
			<div class ="vc_advice_full_description">
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut .
			</div>
		</div>
			<a class ="vc_advice_start_button" href="">SKIN ENCYCLOPEDIA</a>
	</div>
</div>
