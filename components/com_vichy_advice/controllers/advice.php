<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
   
    class Vichy_adviceControllerAdvice extends JController{
        private $item;
    	function __construct(){
    		parent::__construct();
    		JRequest::setVar('view','advice');
            $catid=(!isset($_POST['catid'])||empty($_POST['catid']))?63:$_POST['catid'];
            $this->item=Vichy_adviceHelper::getCategoriesByGroup($catid);
    	}
    	function getAdvice()
    	{
    		$type = $_POST['type'];
    		$catid=$_POST['catid'];

    		$data="";

    		$content=array();
    		$advices=Vichy_adviceHelper::getContentByType('advice',$catid);
            

    		foreach ($advices as $k => $v) {
    			if(empty($v->type) && empty($v->published))
    			{
    				$content[0][0]=$v;
                    $data.='<div class="comming-soon">COMING SOON</div>';
                    $response['question'] = $data; 
                    $response['range'] = ''; 
                    $response['comming_soon']=1;
                    echo json_encode($response); 
                    exit();
    				break;
    			}
                else
                {                    
                    $response['comming_soon']=0;
                }
    			$pa = $v->parent_id;
    			$content[$pa][] = $v;
    		}
            // print_r($content);
            $items = $this->item;

			if(count($content)>0)
			{	
				$tmp =json_decode($content[0][0]->params);
				$background = $tmp->image;

                $color_0= '';$color_1= '';$color_2= '';$color_3= '';
                $color_large_font = '';
                $color_small_font = '';
                foreach($items as $v){ 
                    switch ($v->main_color) {
                       case 'normaderm':
                        $color_large_font = '#197205';
                        $color_small_font = '#197205';
                        $color_3 = '#70D759';
                        $color_2 = '#50B33B';
                        $color_1= '#35951F';
                        $color_0 = '#197205';
                        break;
                    case 'capital':
                        $color_large_font = '#ff7200';
                        $color_small_font = '#ff7200';
                        $color_3 = '#FFF257';
                        $color_2 = '#FFC93A';
                        $color_1 = '#FFA720';
                        $color_0 = '#FF8B06';
                        break;
                    case 'thermal':
                        $color_large_font = '#448ac9';
                        $color_small_font = '#448ac9';
                        $color_3 = '#E2FFFF';
                        $color_2 = '#BCFAFF';
                        $color_1 = '#9CD0FF';
                        $color_0 = '#82ADD8';
                        break;
                    case 'aqualia_thermal':
                        $color_large_font = '#448ac9';
                        $color_small_font = '#448ac9';
                        $color_3 = '#FFFFFF';
                        $color_2 = '#D7FFFF';
                        $color_1 = '#B3EEFF';
                        $color_0 = '#95C6EA';
                        break;
                    case 'bi_white':
                        $color_large_font = '#717e85';
                        $color_small_font = '#717e85';
                        $color_0 = '#B5B5B5';
                        $color_1 = '#CFCFCF';
                        $color_2 = '#E3E3E3';
                        $color_3 = '#F7F7F7';
                        break;
                    case 'destock':
                        $color_large_font = '#099575';
                        $color_small_font = '#099575';
                        $color_0 = '#099574';
                        $color_1 = '#24B397';
                        $color_2 = '#41D7B6';
                        $color_3 = '#5BFFDB';
                        break;
                    case 'dercos':
                        $color_large_font = '#8b0305';
                        $color_small_font = '#8b0305';
                        $color_0 = '#8B0306';
                        $color_1 = '#A71D20';
                        $color_2 = '#C9383A';
                        $color_3 = '#F25457';
                        break;
                    case 'liftactiv':
                        $color_large_font = '#1a74a3';
                        $color_small_font = '#1a74a3';
                        $color_0 = '#1A73A3';
                        $color_1 = '#3496C4';
                        $color_2 = '#4EB4EC';
                        $color_3 = '#6ED8FF';
                        break;
                    case 'purete_thermal':
                        $color_large_font = '#448ac9';
                        $color_small_font = '#448ac9';
                        $color_0 = '#00A1DB';
                        $color_1 = '#1BC2FF';
                        $color_2 = '#36E9FF';
                        $color_3 = '#51FFFF';
                        break;
                    case 'aera-mineral':
                        $color_large_font = '#e2c8af';
                        $color_small_font = '#e2c8af';
                        $color_0 = '#E2C7AF';
                        $color_1 = '#FFEFD2';
                        $color_2 = '#FFFFFC';
                        $color_3 = '#FFFFFF';
                        break;
                    case 'idealia':
                        $color_large_font = '#e55482';
                        $color_small_font = '#e55482';
                        $color_0 = '#e55482';
                        $color_1 = '#ed88a7';
                        $color_2 = '#ff9cbb';
                        $color_3 = '#FFFFFF';
                        break;

                    default :       
                        $color_large_font = '#2f4c9d';
                        $color_small_font = '#2f4c9d';  
                        $color_0 = '#2B716F';
                        $color_1 = '#459391';
                        $color_2 = '#61B1AE';
                        $color_3 = '#7FD5D1';
                        break;
                    }
                }
                $background_btn="background:$color_0;";
                            // background: -moz-linear-gradient(bottom, $color_0 0%, $color_1 50%, $color_2 75%, $color_3  100%);
                            // background: -o-linear-gradient(bottom, $color_0 0%, $color_1 50%, $color_2 75%, $color_3  100%);
                            // background: -webkit-gradient(linear, left bottom, left top, color-stop(0, $color_0), color-stop(50, $color_1), color-stop(60, $color_2), color-stop(100, $color_3));
                            // background: -webkit-linear-gradient(bottom, $color_0 0%, $color_1 50%, $color_2 75%, $color_3  100%);
                            // background: linear-gradient(to top,$color_0 0%, $color_1 50%, $color_2 75%, $color_3  100%);                            
                            // ";
				$data.='<div class="banner" style="background:url('.JURI::root().'timbthumb.php?src='.$background.'&w=672&h=234&zc=0) no-repeat;">';
				$data.='<a href="'.JROUTE::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107').'">';
                $data.='<div class="content-banner-left">';
                $data.='<div class="content-banner-left-title" style="color:'.$color_large_font.';">'.$content[0][0]->title.'</div>';
                $data.='<div class="content-banner-left-description" style="color:'.$color_small_font.';">'.$content[0][0]->title_description.'</div>';
    			$data.='<div class="content-banner-left-button" style="'.$background_btn.'">KIỂM TRA DA</div>';
                $data.='</div>';
            	$data.='</a>';
				$data.='</div>';
				$data.='<div class="content">';        		
            	$data.='<ul class="question">';
                foreach($content[0] as $k => $v){
                	if(isset($content[$v->id])){
		            	$data.='<li>';
		                $data.='<a href="javascript:void(0);">'.$v->text.'</a>';
		                $data.='<div class="answer">';
		                $data.='<h3>BẠN CÓ BIẾT</h3>';
		                $data.=$content[$v->id][0]->text;
		                $data.='</div>';
		                $data.='</li>';
	                } 
                }
            	$data.='</ul>';
        		$data.='</div>';                
			}
			$response['question'] = '<div class="content-right">'.$data.'</div>';


            
            $html = '';
            $color = '';
            foreach($items as $v){ 
                switch ($v->main_color) {
                    case 'normaderm':
                        $color = '#197205';
                        break;
                    case 'capital':
                        $color = '#ff8b06';
                        break;
                    case 'thermal':
                        $color = '#82add8';
                        break;
                    case 'aqualia_thermal':
                        $color = '#95c7ea';
                        break;
                    case 'bi_white':
                        $color = '#c7c7c7';
                        break;
                    case 'destock':
                        $color = '#32d2ae';
                        break;
                    case 'dercos':
                        $color = '#8e090c';
                        break;
                    case 'liftactiv':
                        $color = '#6cafc9';
                        break;
                    case 'purete_thermal':
                        $color = '#04a0db';
                        break;
                    case 'aera-mineral':
                        $color = '#e2c8af';
                        break;
                    case 'idealia':
                        $color = '#e55482';
                        break;
                    default :
                        $color = '#197205';
                        break;
                }
                $tmp = json_decode($v->params,true);
                $image = (!empty($tmp['image'])) ? $tmp['image']: 'components/com_vichy_product/uploads/product_range/bg.png';
                $html .= '<a href="';
                            if($v->parent_id==37) 
                                $html .= JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->id.'&no_step=1'); 
                            else
                                $html .= JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->id); 
                $html .= '" style="display:block;">';         
                $html .= '<div class="wrap_product_range_item">';
                    
                $html .= '<div class="product_range_name" style="background:'.$color.'">';
                $html .= '<span>'.$v->title.'</span>';
                $html .= '<span class="desc_range">'.$v->short_desc.'</span>';
                $html .= '</div>';
                $html .= '<div class="product_range_image">';
                $html .= '<img width="915" src="'.JURI::root().'timbthumb.php?src='.$image.'&w=915&zc=0" />';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</a>';
            }
            $response['range'] = $html;
            echo json_encode($response);
			exit();
            
    	}
    }
?>