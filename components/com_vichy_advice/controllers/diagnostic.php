<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_adviceControllerDiagnostic extends JController{
    	function __construct(){
    		parent::__construct();
    		JRequest::setVar('view','diagnostic');
    	}

    	function storeResult(){
    		$result_string = $_POST['result'];
            $user_id = $_POST['user_id'];

            $check = Vichy_adviceHelper::isUserDiagnostic($user_id);
            if($check){
                $link = JROUTE::_("index.php?option=com_vichy_advice&view=diagnostic");
                $html = '<div class="warning_skin_diagnostic">
                            Bạn đã làm kiểm tra da rồi. Click vào <a href="'.$link.'">đây</a> để xem kết quả. 
                        </div>';

                $respone['status'] = 'false';
                $respone['content'] = $html;
                echo json_encode($respone);
                
                exit();
            }

            $db = & JFactory::getDbo();
            $sql = "INSERT INTO vc_vichy_qa_user(id, question_id, answer_id, user_id, created_date, modified_date, is_registration_question) values ";
            
            $result = explode(',', $result_string);
            $values = array();
            $answer_t_id = null;
            $answer_u_id = null;

            foreach ($result as $key => $value) {
                $item = explode('|', $value);
                if($item[0] == 187){
                    $answer_t_id = $item[1];
                }else if($item[0] == 192){
                    $answer_u_id = $item[1];
                }
                if(!empty($item[0])){
                    $values[] = "(null, ".$item[0].",". $item[1].",". $user_id.", null, null, 0)";
                }                
            }
            $values_string = implode(',', $values);
            $sql .= $values_string;
            
            $db->setQuery($sql);
            $db->query();

            //show result
            
            $result_answer_1 = Vichy_adviceHelper::getResultFromT_U($user_id, $answer_t_id, $answer_u_id);
            $update_myskin = Vichy_adviceHelper::updateMyskinFromResult($user_id, $result_answer_1->result_id);
            $result_section_1 = Vichy_adviceHelper::getQuestionSectionOne($user_id, 167);
            $result_condition = Vichy_adviceHelper::getResultByCate($user_id, 165);
            $result_lifestyle = Vichy_adviceHelper::getResultByCate($user_id, 166);
            $result_condition_end = Vichy_adviceHelper::getResultSectionOne($user_id, 167);
            $result_condition_end_1 = Vichy_adviceHelper::getResultQuestionEnd($user_id, 168, 202);
            $result_condition_end_2 = Vichy_adviceHelper::getResultQuestionEnd($user_id, 168, 203);

            //$background = "background_result.gif";
            $bg_diagnostic_top = "bg_diagnostic_top.jpg";
            $bg_diagnostic_cen = "bg_diagnostic_cen.jpg";
            $bg_diagnostic_bot = "bg_diagnostic_bot.jpg";

            $html = '<div class="note">
                        <span>Trả lời các câu hỏi sau đây để hiểu hơn về loại da của bạn.</span>
                    </div>
                    <div class="cate-diagnostic"><h2>3. Kết quả</h2></div>                    
                    <div class="diagnostic-result" style="background:url('.JURI::root().'components/com_vichy_advice/images/diagnostics/'.$bg_diagnostic_top.') no-repeat;">
                        <div class="header-result">
                            <div class="icon-result">
                                <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/images/diagnostics/icon_result.png&w=58&h=64&zc=0">
                            </div>
                            <div class="title-result">
                                KẾT QUẢ PHÂN TÍCH DA
                            </div>
                            <div class="text-result">
                                <span>
                                    Chúc mừng bạn đã hoàn thành các câu hỏi kiểm tra da sơ lược. Dưới đây là kết qủa<br/>
                                    và lời khuyên của Vichy dành cho bạn. Hãy để Vichy đồng hành với bạn suốt hành<br/>
                                    trình đạt được làn da lý tưởng nhé
                                </span>
                            </div>
                        </div>
                        <div class="content-result">
                            <div class="top-section">
                                PHẦN 1: LOẠI DA
                            </div>
                            <div class="section-1">
                                <div class="section-1-left">';
            $num = 1;
            foreach ($result_section_1 as $k => $v) {
               
            $html .=                '<div class="question">
                                        <div class="icon-small-question">
                                            <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/uploads/diagnostics/'. $v->icon .'&w=49&h=70&zc=0">
                                        </div>
                                        <div class="text-question">
                                            '.$num++.'. '. strip_tags($v->question_text) .'
                                        </div>
                                        <div class="text-answer">
                                            '. strip_tags($v->answer_text) .'
                                        </div>
                                    </div>';
            }
            
            $html .=           '</div>

                                <div class="section-1-cen">
                                    <img src="'.JURI::root().'components/com_vichy_advice/images/diagnostics/line_part_1.jpg" />
                                </div>

                                <div class="section-1-right">
                                    <div class="column-1">
                                        <div class="column-1-title">
                                            Kết quả:
                                        </div>
                                        <div class="column-1-myskin"> 
                                            Da của bạn là '. strip_tags($result_answer_1->result_name) .'
                                        </div>
                                        <div class="column-1-step">
                                            BƯỚC 1
                                        </div>
                                        <div class="column-1-advice">
                                            Hãy rửa mặt với:
                                        </div>
                                        <div class="column-1-link">
                                            <a href="'. JROUTE::_("index.php?option=com_vichy_product&view=product&id=".$result_answer_1->product_id."&Itemid=103").'">'. $result_answer_1->product_name .'</a>
                                        </div>
                                        <div class="buy_now" id="buy_now_1">
                                            <a href="javascript:void(0);">Mua ngay</a>';
                                            $html .= $this->getBuyOnline(1);
            $html .=                    '</div>
                                    </div>
                                    <div class="column-2">
                                        <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_product/uploads/products/'. $result_answer_1->product_image .'&w=120&h=200&zc=0">
                                    </div>
                                </div>
                            </div>
                            <!-- SECTION 02 -->
                            <div class="clear"></div>

                            <div class="top-section-devider">
                                PHẦN 2: MÔI TRƯỜNG SỐNG                        
                            </div>';
        $i = 0;
        foreach ($result_condition as $k => $v) {            
        
            if($i != 0){
                $html .= '<div class="divider">
                            <img src="'.JURI::root().'components/com_vichy_advice/images/diagnostics/dotted.png">                          
                        </div>';
            }
            $i++;
            $html .=        '<div class="section-2">
                                <div class="section-2-row">
                                    <div class="section-2-left">
                                        <div class="section-2-left-icon">
                                            <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/uploads/diagnostics/'. $v->icon .'&w=250&zc=0">
                                        </div>
                                    </div>
                                    <div class="section-2-right">
                                        <div class="section-2-right-ques">
                                            '. $num++ .'. '. strip_tags($v->question_text) .'
                                        </div>
                                        <div class="section-2-right-ans">
                                            '. strip_tags($v->answer_text) .'
                                        </div>
                                        <div class="section-2-advice">
                                            <strong>Lời khuyên: </strong>'. strip_tags($v->result_text) .'
                                        </div>
                                    </div>
                                </div>
                            </div>';
        }

        $html .=       '<!-- SECTION 03 -->
                            <div class="clear"></div>

                            <div class="top-section-devider">
                                PHẦN 3: THÓI QUEN SỐNG                       
                            </div>';
        $html_cen = '';
        $used_checkbox = false;
        $i = 0;
        foreach ($result_lifestyle as $k => $v) {
            $ques_id_current = $v->id;
            if($v->type_answer == "checkbox"){
                $ques_id = $v->id;
                $used_checkbox = true;
            }else{
                $ques_id = null;
                if($used_checkbox == true){
                    $html .= $html_top.$html_cen.$html_bot;
                    $used_checkbox = false;
                    $num++;
                }
            }
            if(!empty($ques_id) && $ques_id == $ques_id_current){

                if($i != 0){
                    $html_top .= '<div class="divider">
                                    <img src="'.JURI::root().'components/com_vichy_advice/images/diagnostics/dotted.png">                          
                                </div>';
                }   

                $html_top .=    '<div class="section-2">
                                <div class="section-2-row">
                                    <div class="section-2-left">
                                        <div class="section-2-left-icon">
                                            <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/uploads/diagnostics/'. $v->icon .'&w=250&zc=0">
                                        </div>
                                    </div>
                                    <div class="section-2-right">
                                        <div class="section-2-right-ques">
                                            '. $num .'. '. strip_tags($v->question_text) .'
                                        </div>';

            $html_cen .=               '<div class="section-2-right-ans">
                                            '. strip_tags($v->answer_text) .'
                                        </div>
                                        <div class="section-2-advice">
                                            <strong>Lời khuyên: </strong>'. strip_tags($v->result_text) .'
                                        </div>';

            $html_bot =           '</div>
                                </div>
                            </div>';
                                        
            }else{

            if($i != 0){
                    $html .= '<div class="divider">
                                <img src="'.JURI::root().'components/com_vichy_advice/images/diagnostics/dotted.png">                          
                            </div>';
                }
            

            $html .=        '<div class="section-2">
                                <div class="section-2-row">
                                    <div class="section-2-left">
                                        <div class="section-2-left-icon">
                                            <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/uploads/diagnostics/'. $v->icon .'&w=250&zc=0">
                                        </div>
                                    </div>
                                    <div class="section-2-right">
                                        <div class="section-2-right-ques">
                                            '. $num++ .'. '. strip_tags($v->question_text) .'
                                        </div>
                                        <div class="section-2-right-ans">
                                            '. strip_tags($v->answer_text) .'
                                        </div>
                                        <div class="section-2-advice">
                                            <strong>Lời khuyên: </strong>'. strip_tags($v->result_text) .'
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }
            $i++;
        }

        $html .=           '<div class="divider">
                                <img src="'.JURI::root().'components/com_vichy_advice/images/diagnostics/dotted.png">                          
                            </div>

                        <div class="section-2" style="background:url('.JURI::root().'components/com_vichy_advice/images/diagnostics/'.$bg_diagnostic_cen.') repeat-y;">
                            <div class="section-2-row">
                                <div class="section-2-left">
                                    <div class="section-2-left-icon">
                                        <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/uploads/diagnostics/'. $result_condition_end[0]->icon .'&w=130&zc=0">
                                    </div>
                                </div>
                                <div class="section-2-right">
                                    <div class="section-2-right-ques">
                                        '. $num++ .'. '. strip_tags($result_condition_end[0]->question_text) .'
                                    </div>';
        foreach($result_condition_end as $k=>$v){
        $html .=                    '<div class="section-2-right-ans">
                                        '. strip_tags($v->answer_text) .'
                                    </div>
                                    <div class="section-2-advice">
                                        <strong>Lời khuyên: </strong>'. strip_tags($v->result_text) .'
                                    </div>';
        }
        $html .=                '</div>
                            </div>
                        </div>';

        $html .=           '<div class="divider">
                                <img src="'.JURI::root().'components/com_vichy_advice/images/diagnostics/dotted.png">                          
                            </div>

                                <div class="section-end-row-1" style="background:url('.JURI::root().'components/com_vichy_advice/images/diagnostics/'.$bg_diagnostic_cen.') repeat-y;">
                                    <div class="section-end-left">
                                        <div class="left-col-1">
                                            <div class="left-col-1-ques">
                                                10a. Mối quan tâm về da quan trọng nhất hiện nay:
                                            </div>
                                            <div class="left-col-1-ans">
                                                '. strip_tags($result_condition_end_1->answer_text) .'
                                            </div>
                                            <div class="left-col-1-step">
                                                BƯỚC 2
                                            </div>
                                            <div class="left-col-1-advice">
                                                <strong>Sử dụng sản phẩm giúp chuyển hóa làn da, đặc trị chuyên sâu:</strong>
                                            </div>
                                            <div class="left-col-1-link">
                                                <a href="'. JROUTE::_("index.php?option=com_vichy_product&view=product&id=".$result_condition_end_1->product_id."&Itemid=103").'">'. $result_condition_end_1->product_name .'</a>
                                            </div>
                                            <div class="buy_now" id="buy_now_2">
                                                <a href="javascript:void(0);">Mua ngay</a>';
                                                $html .= $this->getBuyOnline(2);   
        $html .=                            '</div>
                                        </div>
                                        <div class="left-col-2">
                                            <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_product/uploads/products/'. $result_condition_end_1->product_image .'&w=120&h=200&zc=0">
                                        </div>
                                    </div>
                                    <div class="section-end-right">
                                        <div class="left-col-1">
                                            <div class="left-col-1-ques">
                                                Bảo vệ là bước cuối cùng trong quy trình chăm sóc da giúp bảo vệ và giữ gìn sức khỏe làn da trước các tác động từ tia UV, ô nhiễm, khói bụi,...
                                            </div>                                            
                                            <div class="left-col-1-step">
                                                BƯỚC 3
                                            </div>
                                            <div class="left-col-1-advice">
                                                <strong>Chống nắng với</strong>
                                            </div>
                                            <div class="left-col-1-link">
                                                <a href="'. JROUTE::_("index.php?option=com_vichy_product&view=product&id=".$result_condition_end_1->product_id_2."&Itemid=103").'">'. $result_condition_end_1->product_name_2 .'</a>
                                            </div>
                                            <div class="buy_now" id="buy_now_3">
                                                <a href="javascript:void(0);">Mua ngay</a>';
                                                $html .= $this->getBuyOnline(3);   
        $html .=                            '</div>
                                        </div>
                                        <div class="left-col-2">
                                            <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_product/uploads/products/'. $result_condition_end_1->product_image_2 .'&w=120&h=200&zc=0">
                                        </div>
                                    </div>
                                </div>
                                <div class="divider">
                                    <img src="'.JURI::root().'components/com_vichy_advice/images/diagnostics/dotted.png">                          
                                </div>
                                <div class="section-end-row-2" style="background:url('.JURI::root().'components/com_vichy_advice/images/diagnostics/'.$bg_diagnostic_cen.') repeat-y;">
                                    <div class="section-end-row-1">
                                        <div class="left-col-1">
                                            <div class="left-col-1-ques">
                                                10b. Mối quan tâm về da quan trọng tiếp theo với bạn:
                                            </div>
                                            <div class="left-col-1-ans">
                                                '. strip_tags($result_condition_end_2->answer_text) .'
                                            </div>
                                            
                                            <div class="left-col-1-advice">
                                                <strong>Sản phẩm khuyên dùng:</strong>
                                            </div>
                                            <div class="left-col-1-link">
                                                <a href="'. JROUTE::_("index.php?option=com_vichy_product&view=product&id=".$result_condition_end_2->product_id."&Itemid=103").'">'. $result_condition_end_2->product_name .'</a>
                                            </div>
                                            <div class="buy_now" id="buy_now_4">
                                                <a href="javascript:void(0);">Mua ngay</a>';
                                                $html .= $this->getBuyOnline(4);   
        $html .=                            '</div>
                                            <div class="left-col-1-advice" style="margin-top:-20px;margin-bottom:-15px;">
                                                <br/>
                                                Hãy gọi hoặc đến trung tâm chăm sóc da Vichy để tư vấn cụ thể hơn với các chuyên gia và nhận mẫu thử.
                                            </div>
                                        </div>
                                        <div class="left-col-2">
                                            <img src="'.JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_product/uploads/products/'. $result_condition_end_2->product_image .'&w=120&h=200&zc=0">
                                        </div>
                                    </div>
                                </div>

                                <div class="divider" style="margin-bottom:0px;">
                                    <img src="'.JURI::root().'components/com_vichy_advice/images/diagnostics/dotted.png">                          
                                </div>

                            </div>
                            
                            <div class="footer-result" style="padding-top:30px;background:url('.JURI::root().'components/com_vichy_advice/images/diagnostics/'.$bg_diagnostic_bot.') no-repeat;">
                                <div class="btn_bottom">
                                    <div class="print"><a href="javascript:void(0);" id="print_result">In kết quả</a></div>

                                    <div class="store_system"><a href="'. JROUTE::_('index.php?option=com_vichy_store&view=store') .'">Hệ thống cửa hàng</a></div>
                                </div>
                            </div>

                        </div>                

                    </div>
                    <style>
                        div.main-container{
                            min-height: 3500px !important;
                        }
                    </style>                    
                    ';
            $respone['status'] = 'true';
            $respone['content'] = $html;
            echo json_encode($respone);
            
            exit();
    	}

        function getBuyOnline($num=null){
            $html = '<div class="buy-online common-buy-online" id="common-buy-online_'.$num.'">
                        <a href="http://www.chon.vn/cua-hang/vichy.aspx?ref=home-merchant" target="_blank"><img src="'.JURI::root().'/templates/vichy/images/tmp/chonvn.png"></a>
                        <a href="http://www.yes24.vn/khuyen-mai/471450/vichy-khuyen-mai.html" target="_blank"><img src="'.JURI::root().'/templates/vichy/images/tmp/yes24com.png"></a>
                        <a href="http://www.lazada.vn/vichy/" target="_blank"><img src="'.JURI::root().'/templates/vichy/images/tmp/lazada.png"></a>
                        <a href="http://tiki.vn/thuong-hieu/vichy.html" target="_blank"><img src="'.JURI::root().'/templates/vichy/images/tmp/tiki.png"></a>
                    </div>
                            
                        <script type="text/javascript" language="javascript">
                            $(document).ready(function(e) {
                                $("#buy_now_'.$num.'").hover(function(e) {
                                    var left = $("#buy_now_'.$num.'").offset().left;
                                    
                                    var el=$(this).find("#common-buy-online_'.$num.'").css({"visibility":"visible","opacity":"1","transition-delay":"0s","left":left,"margin-top":"8px","border-top":"1px solid #ccc"});
                                                            
                                },function(e){
                                    var el=$(this).find("#common-buy-online_'.$num.'").css({"visibility":"hidden","opacity":"0","transition":"visibility 0s linear 0.3s,opacity 0.3s linear"});
                                });
                           });
                        </script>';
            return $html;
        }
    }
?>