<?php
/**
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * @package		Joomla.Site
 * @subpackage	com_content
 */
class ContentControllerArticle extends JControllerForm
{
	/**
	 * @since	1.6
	 */
	protected $view_item = 'form';

	/**
	 * @since	1.6
	 */
	protected $view_list = 'categories';

	/**
	 * Method to add a new record.
	 *
	 * @return	boolean	True if the article can be added, false if not.
	 * @since	1.6
	 */
	public function add()
	{
		if (!parent::add()) {
			// Redirect to the return page.
			$this->setRedirect($this->getReturnPage());
		}
	}

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param	array	An array of input data.
	 *
	 * @return	boolean
	 * @since	1.6
	 */
	protected function allowAdd($data = array())
	{
		// Initialise variables.
		$user		= JFactory::getUser();
		$categoryId	= JArrayHelper::getValue($data, 'catid', JRequest::getInt('catid'), 'int');
		$allow		= null;

		if ($categoryId) {
			// If the category has been passed in the data or URL check it.
			$allow	= $user->authorise('core.create', 'com_content.category.'.$categoryId);
		}

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd();
		}
		else {
			return $allow;
		}
	}

	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param	array	$data	An array of input data.
	 * @param	string	$key	The name of the key for the primary key.
	 *
	 * @return	boolean
	 * @since	1.6
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Initialise variables.
		$recordId	= (int) isset($data[$key]) ? $data[$key] : 0;
		$user		= JFactory::getUser();
		$userId		= $user->get('id');
		$asset		= 'com_content.article.'.$recordId;

		// Check general edit permission first.
		if ($user->authorise('core.edit', $asset)) {
			return true;
		}

		// Fallback on edit.own.
		// First test if the permission is available.
		if ($user->authorise('core.edit.own', $asset)) {
			// Now test the owner is the user.
			$ownerId	= (int) isset($data['created_by']) ? $data['created_by'] : 0;
			if (empty($ownerId) && $recordId) {
				// Need to do a lookup from the model.
				$record		= $this->getModel()->getItem($recordId);

				if (empty($record)) {
					return false;
				}

				$ownerId = $record->created_by;
			}

			// If the owner matches 'me' then do the test.
			if ($ownerId == $userId) {
				return true;
			}
		}

		// Since there is no asset tracking, revert to the component permissions.
		return parent::allowEdit($data, $key);
	}

	/**
	 * Method to cancel an edit.
	 *
	 * @param	string	$key	The name of the primary key of the URL variable.
	 *
	 * @return	Boolean	True if access level checks pass, false otherwise.
	 * @since	1.6
	 */
	public function cancel($key = 'a_id')
	{
		parent::cancel($key);

		// Redirect to the return page.
		$this->setRedirect($this->getReturnPage());
	}

	/**
	 * Method to edit an existing record.
	 *
	 * @param	string	$key	The name of the primary key of the URL variable.
	 * @param	string	$urlVar	The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return	Boolean	True if access level check and checkout passes, false otherwise.
	 * @since	1.6
	 */
	public function edit($key = null, $urlVar = 'a_id')
	{
		$result = parent::edit($key, $urlVar);

		return $result;
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 * @param	array	$config	Configuration array for model. Optional.
	 *
	 * @return	object	The model.
	 *
	 * @since	1.5
	 */
	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Gets the URL arguments to append to an item redirect.
	 *
	 * @param	int		$recordId	The primary key id for the item.
	 * @param	string	$urlVar		The name of the URL variable for the id.
	 *
	 * @return	string	The arguments to append to the redirect URL.
	 * @since	1.6
	 */
	protected function getRedirectToItemAppend($recordId = null, $urlVar = 'a_id')
	{
		// Need to override the parent method completely.
		$tmpl		= JRequest::getCmd('tmpl');
		$layout		= JRequest::getCmd('layout', 'edit');
		$append		= '';

		// Setup redirect info.
		if ($tmpl) {
			$append .= '&tmpl='.$tmpl;
		}

		// TODO This is a bandaid, not a long term solution.
//		if ($layout) {
//			$append .= '&layout='.$layout;
//		}
		$append .= '&layout=edit';

		if ($recordId) {
			$append .= '&'.$urlVar.'='.$recordId;
		}

		$itemId	= JRequest::getInt('Itemid');
		$return	= $this->getReturnPage();
		$catId = JRequest::getInt('catid', null, 'get');

		if ($itemId) {
			$append .= '&Itemid='.$itemId;
		}

		if($catId) {
			$append .= '&catid='.$catId;
		}

		if ($return) {
			$append .= '&return='.base64_encode(urlencode($return));
		}

		return $append;
	}

	/**
	 * Get the return URL.
	 *
	 * If a "return" variable has been passed in the request
	 *
	 * @return	string	The return URL.
	 * @since	1.6
	 */
	protected function getReturnPage()
	{
		$return = JRequest::getVar('return', null, 'default', 'base64');

		if (empty($return) || !JUri::isInternal(urldecode(base64_decode($return)))) {
			return JURI::base();
		}
		else {
			return urldecode(base64_decode($return));
		}
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param	JModel	$model		The data model object.
	 * @param	array	$validData	The validated data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function postSaveHook(JModel &$model, $validData)
	{
		$task = $this->getTask();

		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_content&view=category&id='.$validData['catid'], false));
		}
	}

	/**
	 * Method to save a record.
	 *
	 * @param	string	$key	The name of the primary key of the URL variable.
	 * @param	string	$urlVar	The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return	Boolean	True if successful, false otherwise.
	 * @since	1.6
	 */
	public function save($key = null, $urlVar = 'a_id')
	{
		// Load the backend helper for filtering.
		require_once JPATH_ADMINISTRATOR.'/components/com_content/helpers/content.php';

		$result = parent::save($key, $urlVar);

		// If ok, redirect to the return page.
		if ($result) {
			$this->setRedirect($this->getReturnPage());
		}

		return $result;
	}

	/**
	 * Method to save a vote.
	 *
	 * @return	void
	 * @since	1.6.1
	 */
	function vote()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$user_rating = JRequest::getInt('user_rating', -1);

		if ( $user_rating > -1 ) {
			$url = JRequest::getString('url', '');
			$id = JRequest::getInt('id', 0);
			$viewName = JRequest::getString('view', $this->default_view);
			$model = $this->getModel($viewName);

			if ($model->storeVote($id, $user_rating)) {
				$this->setRedirect($url, JText::_('COM_CONTENT_ARTICLE_VOTE_SUCCESS'));
			} else {
				$this->setRedirect($url, JText::_('COM_CONTENT_ARTICLE_VOTE_FAILURE'));
			}
		}
	}

	function getEventById(){
		$id = $_POST['id'];
		$db = & JFactory::getDbo();
		$db->setQuery('Select title, introtext, images from vc_content where id = '.$id);
		$result = $db->loadObject();
		$result->introtext = strip_tags($result->introtext);
		$image = json_decode($result->images);
		$result->img_src = JURI::root().'timbthumb.php?src='.$image->image_intro.'&w=198&h=198&q=100&zc=0';
		$result->img_alt = $image->image_intro_alt;
		echo json_encode($result);
		die();
	}

	function getListBlog(){

		$id = $_POST['id'];

  		$db =& JFactory::getDBO();
		$query = 'SELECT
		c.id, 
        c.title, 
        c.images, 
        c.introtext, 
        c.created, 
        c.language, 
		CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
		CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
		JOIN #__categories cg ON c.catid = cg.id 
		WHERE catid = '.$id.' AND c.state > 0 
        ORDER BY c.created DESC';


        if(empty($id)){

			// $query = 'SELECT
			// c.id, 
			// c.catid,
	  //       c.title, 
	  //       c.images, 
	  //       c.introtext, 
	  //       c.created, 
	  //       c.language, 
			// CASE WHEN CHAR_LENGTH(c.alias) 
	  //       THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
			// CASE WHEN CHAR_LENGTH(cg.alias) 
	  //       THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
	  //       FROM #__content c 
			// JOIN #__categories cg ON c.catid = cg.id 
			// WHERE c.state > 0 
	  //       ORDER BY c.created DESC';
        	$query = 'SELECT
		    c.id,
		    c.catid, 
		    c.title, 
		    c.images, 
		    c.introtext, 
		    c.created, 
		    c.language, 
		    CASE WHEN CHAR_LENGTH(c.alias) 
		    THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
		    CASE WHEN CHAR_LENGTH(cg.alias) 
		    THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
		    FROM #__content c 
		    JOIN #__categories cg ON c.catid = cg.id 
		    WHERE cg.parent_id = 29 AND c.state > 0 
		    ORDER BY c.created DESC';
		}

		$db->setQuery($query);
        $items = $db->loadObjectList();
        $html_code = '';
        foreach ($items as $v) {

            $html_code .= '<li>';
            $html_code .= '<div class="image"><a href="'.JROUTE::_('index.php?option=com_content&view=article&catid='.$v->catid.'&id='.$v->id.'&Itemid=109').'"><img src="'.JURI::root().json_decode($v->images)->image_intro.'"></a></div>';
            $html_code .= '<div class="social">';
            $html_code .= '<div class="fb-like" data-href="#" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>';
            $html_code .= '</div>';
            $html_code .= '<div class="title"><span>ARTICLE TITLE: </span>'.$v->title.'</div>';
            $html_code .= '<div class="date">'.date("d/m/Y", strtotime($v->created)).'</div>';
            $html_code .= '<p>';
            $html_code .= shortDesc(strip_tags($v->introtext),147);
            $html_code .= '</p>';
            $html_code .= '<div class="view-more"><a href="'.JROUTE::_('index.php?option=com_content&view=article&catid='.$v->catid.'&id='.$v->id.'&Itemid=109').'">XEM CHI TIẾT</a></div>';
            $html_code .= '</li>';
        }

        echo $html_code;
       
        exit();
	}
}
