<?php
	defined('_JEXEC') or die;

    $document = & JFactory::getDocument();
    $title = $this->title;
    $title_item = $this->title_item;
    $document->setTitle($title.' | '.$title_item);
    
	function getArticles(){
        $db = & JFactory::getDbo();
        $lang = JFactory::getLanguage();
        $language_tag = str_replace(' ', '', $lang->getTag());
        if($language_tag == 'vi-VN'){
            $language = "'*',".$db->quote($language_tag);
        }else{
            $language = $db->quote($language_tag);
        }

        $sql = "SELECT * FROM #__vichy_press_corner where published = 1 and language in ($language)";
        $db->setQuery($sql);
        $ret = $db->loadObjectList();        
        return $ret;
    }
    $listArticle = getArticles();
?>
<script type="text/javascript">
    $(document).ready(function(e) {
         jQuery('.item').hover(function(){
             var el=$(this).find('.item_text').animate({ height: "+=40px"},250)
        },function(){
            var el=$(this).find('.item_text').animate({ height: "-=40px"},250)
        });
    });
</script>
<div class="press_corner">
    <?php 
    foreach($listArticle as $k => $v){ 
        $flag = $k%2;
        $class = '';
        if($flag == '1'){
            $class = 'no_margin_right';
        }
    ?>
    <div class="item <?php echo $class; ?>">
        <div class="item_img">
            <a target="_blank" href="<?php echo JURI::root() ?>components/com_vichy_about/uploads/press_corner/<?php echo $v->file_pdf; ?>">
            <img width="284" height="238" src="<?php echo JURI::root().'components/com_vichy_about/uploads/images/'.$v->image; ?>">
            <div class="item_text" style="height:55px;">
                <div class="title_text"><?php echo $v->title; ?></div>
                <div class="content_text"><?php echo $v->description; ?></div>
            </div>
            </a>
        </div>
    </div>
    <?php } ?>
</div>