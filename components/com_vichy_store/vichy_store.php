<?php 
	
	defined('_JEXEC') or die;

	$controller = JRequest::getCmd('view','store');
	require_once(JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php');
    require_once(JPATH_COMPONENT.DS.'helpers'.DS.'helper.php');
    
	$controller = ucwords(strtolower($controller));
	
	$classname = 'Vichy_storeController'.$controller;
	$controller = new $classname();
    
	$task = JRequest::getVar('task');
	$controller->execute($task);
	$controller->redirect();

?>
