<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_storeControllerStore extends JController{
        function __construct(){
            parent::__construct();
            JRequest::setVar('view','store');
        }

        function getStoresByType(){
            $curLanguage = JFactory::getLanguage();
            $language_tag = $curLanguage->getTag();

            if($language_tag == 'vi-VN'){
                $lbl_address = "Địa chỉ:";
                $lbl_phone_number = "Điện thoại:";
                $lbl_no_data = "Không có dữ liệu";
            }else{
                $lbl_address = "Address:";
                $lbl_phone_number = "Phone:";
                $lbl_no_data = "Data not available";
            }

            if($_POST['type']){
                $type = $_POST['type'];
                $long = $_POST['long'];
                $lat = $_POST['lat'];
                $list = '';
                if(!empty($long) && !empty($lat)){
                    $list = Vichy_storeHelper::getStoreByAddress($lat,$long,$type);
                }else{
                    $list = Vichy_storeHelper::getStoresByType($type);
                }
                $data = '';
                
                if(count($list)){
                    foreach ($list as $key => $item) {
                        $data .= '<li>';
                        $data .= '<a class="map-location" data-long="'.$item->lng.'" data-lat="'.$item->lat.'"  href="javascript:void(0);">';
                        $data .= '<div class="image">'.$item->name.'</div>';
                        $data .= '<div class="info">';
                        $data .= '<div class="addr"><span>'.$lbl_address.'</span> '.$item->address.'</div>';
                        $data .= '<div class="addr"><span>'.$lbl_phone_number.'</span> '.$item->phone.'</div>';
                        $data .= '</div>';
                        $data .= '</a>';
                        $data .= '</li>';
                    }
                }else{
                    $data .= '<li>'.$lbl_no_data.'</li>';
                }
                echo $data;
                exit();
            }
        }

        function getStoreByAddress(){
            $curLanguage = JFactory::getLanguage();
            $language_tag = $curLanguage->getTag();

            if($language_tag == 'vi-VN'){
                $lbl_address = "Địa chỉ:";
                $lbl_phone_number = "Điện thoại:";
            }else{
                $lbl_address = "Address:";
                $lbl_phone_number = "Phone:";
            }
            
            $long = $_POST['long'];
            $lat = $_POST['lat'];
            $list = Vichy_storeHelper::getStoreByAddress($lat,$long,90);
            $data = '';
            foreach ($list as $key => $item) {
                $data .= '<li>';
                $data .= '<a class="map-location" data-long="'.$item->lng.'" data-lat="'.$item->lat.'"  href="javascript:void(0);">';
                $data .= '<div class="image">'.$item->name.'</div>';
                $data .= '<div class="info">';
                $data .= '<div class="addr"><span>'.$lbl_address.'</span> '.$item->address.'</div>';
                $data .= '<div class="addr"><span>'.$lbl_phone_number.'</span> '.$item->phone.'</div>';
                $data .= '</div>';
                $data .= '</a>';
                $data .= '</li>';
            }
            echo $data;
            exit();
        }

        function getStores()
        {
            if($_POST['page']) {
                $page = $_POST['page'];           
                            
                $current_page = $page;
                $page -= 1;             
                $display = 5;
                $start = $page * $display; 

                $data .='<ul id="store-content">';

                $result = Vichy_storeHelper::getStores($start,$display);   
                $count = Vichy_storeHelper::countStores();                                           
                $pages = ceil($count / $display);               
                
                foreach($result as $k => $v){
                    $reviews = !empty( $row->reviews)? $row->reviews:0 ;

                    $data.='<li>';
                    $data.='<a class="map-location" data-long="'.$v->lng.'" data-lat="'.$v->lat.'" href="javascript:void(0);">';
                    $data.='<div class="image"><img src="'.JURI::root().'timbthumb.php?src=components/com_vichy_store/uploads/stores/'.$v->image.'&w=190&h=120&q=100&zc=0" width="190" alt="Spa name"></div>';
                    $data.='<div class="info">';
                    $data.='<h3>'.$v->name.'</h3>';
                    $data.='<div class="addr"><span>Address:</span>'.$v->address.'</div>';
                    $data.='<div class="addr"><span>Phone:</span>'.$v->phone.'</div>';
                    $data.='</div>';
                    $data.='</a>';
                    $data.='</li>';
                   
                }  
                 $data.='</ul>';//end of loading store

                // $data .= "<nav role='page' style='clear:both;'><ul>";
                 if($pages>1)
                 {
                    $data .= '<div class="paging" style="clear:both;">';
                    for($i = 1;$i<=$pages;$i++ )
                    {
                        $data.='<a href="javascript:void(0);"'." page=$i ";
                        if($i==$current_page)
                            $data.='class ="active"';
                        $data.=">$i</li>";
                    }
                     
                    $data = $data . "</div>";
                 }
                echo $data;
                exit();
            }
        }  
     }
?>