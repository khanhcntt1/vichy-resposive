<?php
defined( '_JEXEC' ) or die;

function Vichy_storeBuildRoute(&$query){
	$segments = array();
	if(isset($query['view'])){

		$segments[] = $query['view'];
		unset($query['view']);
    }
    if(isset($query['cid'])){
    	$curLanguage = JFactory::getLanguage();
		$language_tag = $curLanguage->getTag();

		if($language_tag == 'vi-VN'){
			$url_service = 'dich-vu';
			$url_introduction = 'gioi-thieu';
			$url_service_detail = 'chi-tiet-dich-vu';
		}else{
			$url_service = 'service';
			$url_introduction = 'introduction';
			$url_service_detail = 'service-detail';
		}

    	switch ($query['cid']) {
    		case 2:
    			$segments[] = $url_service;
    			break;
    		case 3:
    			$segments[] = $url_introduction;
    			break;
    		case 4:
    			$segments[] = $url_service_detail;
    			# code...
    			break;
    	}
		// $segments[] = $query['cid'];
		unset($query['cid']);
    }
    if(isset($query['id'])){
    	$db = JFactory::getDbo();
		$db->setQuery("SELECT `alias` from #__categories where id = ".$query['id']);
		$alias =$db->loadObject()->alias;
		$segments[] = $alias;
		unset($query['id']);
    }
    return $segments;
}

function Vichy_storeParseRoute($segments){
	$vars = array();
	// echo '<pre>';
	// print_r($segments);
	// echo '</pre>';
	// die();
	foreach ($segments as $k => $v) {
		$segments[$k]=str_replace(':','-',$segments[$k]);
	}
	
	switch ($segments[0]) {
		case 'store':
			$vars['view'] = $segments[0];
			break;
		
		case 'spa':
			$vars['view'] = $segments[0];
			// $segments[1]=str_replace(':','-',$segments[1]);
			switch ($segments[1]) {
				case 'gioi-thieu':
				case 'introduction':
					$vars['cid']=3;
					break;
				case 'dich-vu':
				case 'service':
					$vars['cid']=2;
					break;
				case 'chi-tiet-dich-vu':
				case 'service-detail':
					$vars['cid']=4;
					break;
			}
			$db = JFactory::getDbo();
			$db->setQuery("SELECT `id` from #__categories where alias = '{$segments[2]}' and extension ='com_vichy_services'");
			$vars['id'] =$db->loadObject()->id;
			// $vars['id'] = $segments[2];
			break;
	}
	
	return $vars;
}