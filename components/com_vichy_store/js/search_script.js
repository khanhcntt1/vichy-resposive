jQuery(document).ready(function() {
	(function($){
		$(window).load(function(){
			$(".store-content").mCustomScrollbar({
				autoHideScrollbar:false,				
				theme:"light-thin",
				setTop:"0px",
				advanced:{ updateOnContentResize: true }
			});
		});
	})(jQuery);
	/****************this is location of first store in the list*******************/
	var first_store = $("#store-content li:first-child > a");
	var mylng = first_store.attr('data-long'); 
    var mylat = first_store.attr('data-lat');   

    var base_url = jQuery('#com-vichy-store-url').attr('data-url');
    var redirect_url = jQuery('#com-vichy-store-url').attr('redirect_url');
    var image = base_url+'components/com_vichy_store/images/icon_map.png';
    var current_long = jQuery('#com-vichy-store-url').attr('data-current-long');
    var current_lat = jQuery('#com-vichy-store-url').attr('data-current-lat');
    var address = '';
    var address_submit_form = $('#submit_address');

    var search_lat=$('#search_latitude');
    var search_lng=$('#search_longtitude');
    var search_address=$("#search_address");

    jQuery("#geocomplete").geocomplete({
        map: "#map",
        mapOptions: {
            zoom: 14
        },
        location: [mylat,mylng], 
        markerOptions: {
            draggable: false,
            icon:image
        },
        country:'vn'
    }).bind("geocode:result",function(event,result){
    	console.log(result);
    	search_lat.val(result.geometry.location.lat());
    	search_lng.val(result.geometry.location.lng());
    	search_address.val(result.formatted_address);
    	getStoreByAddress(search_lng.val(),search_lat.val(),search_address.val());
    });  
    jQuery(".address").on('click',".map-location",function(){
        var longtitude = jQuery(this).attr('data-long');
        var latitude = jQuery(this).attr('data-lat');
  		var map = jQuery("#geocomplete").geocomplete("map"),
        center = new google.maps.LatLng(latitude, longtitude);
        // map.clearOverlays();
        map.setCenter(center);

        var beachMarker = new google.maps.Marker({
        position: center,
        map: map,
        icon: image
    	});    	

	});
    /*this called before submit*/
	address_submit_form.on('submit',function(e){
		e.preventDefault();
		geocoder = new google.maps.Geocoder();
		console.log(geocoder);
		    address = jQuery("#geocomplete").val();
		    
		    geocoder.geocode( { 'address': address}, function(results, status) {
		    	console.log(results);
		    	if (status == google.maps.GeocoderStatus.OK) {
		    		console.log(results[0]);
		    		new_lat = results[0].geometry.location.lat();
			    	new_long = results[0].geometry.location.lng();
			    	search_lat.val(new_lat);
    				search_lng.val(new_long);
    				search_address.val(results[0].formatted_address);
			    	getStoreByAddress(new_long,new_lat,address);
			    	return false;
		        }else {
		        	// alert("Geocode was not successful for the following reason: " + status);
		        	getStoreByAddress(false,false,address);
		        	return false;
		        }
		    });

	});


	jQuery('#geocomplete').keypress(function(e) {
		if(e.which == 13){
	        // jQuery("#geocomplete").trigger("geocode");
	        // jQuery('#address').val(jQuery("#geocomplete").val());
	        geocoder = new google.maps.Geocoder();
		    address = jQuery("#geocomplete").val();
		    geocoder.geocode( { 'address': address}, function(results, status) {
		    	console.log(results);
		    	if (status == google.maps.GeocoderStatus.OK) {
			    	new_lat = results[0].geometry.location.lat();
			    	new_long = results[0].geometry.location.lng();
			    	search_lat.val(results[0].geometry.location.lat());
    				search_lng.val(results[0].geometry.location.lng());
    				search_address.val(results[0].formatted_address);
			    	getStoreByAddress(new_long,new_lat,address);
			    	return false;
		        }else {
		        	// alert("Geocode was not successful for the following reason: " + status);
		        	getStoreByAddress(false,false,false);
		        }
		    });
	    }
    });
    function getStoreByAddress(mylong,mylat,address){
		// jQuery('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast');
	    // jQuery.ajax ({
	    //     type: "POST",
	    //     url: base_url+'index.php?option=com_vichy_store&view=store&task=getStoreByAddress',
	    //     data: "long="+mylong+'&lat='+mylat,
	    //     success: function(data_page) { 
	    //         // jQuery('#loading').fadeOut('fast');	            
	    //         jQuery("#store-content").html(data_page);
	    //         longtitude = jQuery("#store-content li:first-child > a").attr('data-long');
		   //      latitude = jQuery("#store-content li:first-child > a").attr('data-lat');
		  	// 	map = jQuery("#geocomplete").geocomplete("map"),
		   //      center = new google.maps.LatLng(latitude, longtitude);
		   //      map.setCenter(center);
		   //      beachMarker = new google.maps.Marker({
			  //       position: center,
			  //       map: map,
			  //       icon: image
		   //  	});
		   //  }
	    // });
		if(!mylong && !mylat && !address)
		{
			$().redirect(redirect_url, {
	          'no_location_suggest': true,
	          'address': address
	       });
		}
		else
		{
			$().redirect(redirect_url, {
	          'lng': mylong,
	          'lat': mylat,
	          'address': address
	       });
		}
				// $().redirect(base_url+'index.php?option=com_vichy_store&view=store&Itemid=113&lang=vi', {
	   //        'lng': mylong,
	   //        'lat': mylat,
	   //        'address': address
	   //     });

		// window.location = base_url+'index.php?option=com_vichy_store&view=store&Itemid=113&lng='+mylong+'&lat='+mylat+'&address='+address;
	}

	jQuery('.tabs a').click(function(){
		type = jQuery(this).attr('data-id');
		image_type_store = jQuery(this).attr('data-image');
		getStoreByType(type,current_long,current_lat);
		jQuery('.tabs li').removeClass('active');
		jQuery(this).parent('li').addClass('active');
		jQuery('.image_type_store').children('img').attr('src',base_url+'timbthumb.php?src='+image_type_store+'&w=444&h=210&q=100&zc=0');
	})

	function getStoreByType(type,mylong,mylat){
		// jQuery('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast');
	    jQuery.ajax ({
	        type: "POST",
	        url: base_url+'index.php?option=com_vichy_store&view=store&task=getStoresByType',
	        data: "type="+type+'&long='+mylong+'&lat='+mylat,
	        success: function(data_page) { 
	   //          jQuery('#mCSB_1 .mCSB_container').css('top','0px');
				// jQuery('#mCSB_1 .mCSB_dragger').css('top','0px');

	            // jQuery('#loading').fadeOut('fast');	            
	            jQuery("#store-content").html(data_page);
	            var longtitude = jQuery("#store-content li:first-child > a").attr('data-long');
		        var latitude = jQuery("#store-content li:first-child > a").attr('data-lat');
		  		var map = jQuery("#geocomplete").geocomplete("map"),
		        center = new google.maps.LatLng(latitude, longtitude);
		        map.clearOverlays();
		        map.setCenter(center);

		        var beachMarker = new google.maps.Marker({
			        position: center,
			        map: map,
			        icon: image
		    	});

		        jQuery(".store-content").mCustomScrollbar('update');
		  		jQuery(".store-content").mCustomScrollbar("scrollTo",0);

		    }
	    });
	}
})