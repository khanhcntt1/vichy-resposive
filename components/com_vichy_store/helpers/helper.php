<?php

	defined('_JEXEC') or die;

	class Vichy_storeHelper extends JComponentHelper {

		function getStoresByType($type,$start=0,$display=5)
		{						
			self::set_category_id();					
			$db = JFactory::getDBO();
			$sql="SELECT s.*, c.parent_id, c.params FROM `vc_vichy_store` as s join vc_categories as c on c.id = s.type where c.parent_id =  ". self::$_store ." and s.published=1 and s.type = $type";
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			return $result;
		}

		function getTypeStore(){
			$db = JFactory::getDBO();
			self::set_category_id();
			
		    $language_tag = self::$_lang;
		    if($language_tag == 'vi-VN'){
		        $language = "'*',".$db->quote($language_tag);
		    }else{
		        $language = $db->quote($language_tag);
		    }

			$sql="SELECT id, parent_id, title, params from vc_categories where parent_id = ". self::$_store ." and published = 1 and language in($language)";
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			return $result;
		}

		function getStoreByAddress($lat,$long,$type){
			$db = JFactory::getDBO();
			$sql="Call geodist($lat,$long,".DEFAULT_DIST.",0,$type)";
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			$link = $db->getConnection();
			while ($link->more_results()) {
				$link->next_result();
				if ($result1 = $link->store_result()) {
					$result1->free();
				}
			}
			return $result;
		}

		function getAllStoreByAddress($lat,$long){
			$db = JFactory::getDBO();

			self::set_category_id();
			$store = self::$_store;
			$sql="Call geodist($lat,$long,".DEFAULT_DIST.",$store,0)";
			// $sql="Call geodist($store,$lat,$long,1)";
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			$link = $db->getConnection();
			while ($link->more_results()) {
				$link->next_result();
				if ($result1 = $link->store_result()) {
					$result1->free();
				}
			}
			return $result;
		}

		// private function clearStoredResults() {
		// 	$db = JFactory::getDBO();
		// 	$link = $db->getConnection();
		// 	while ($link->more_results()) {
		// 		$link->next_result();
		// 		if ($result = $link->store_result()) {
		// 			$result->free();
		// 		}
		// 	}
		// }

		function countStores(){
			$db = JFactory::getDBO();

			self::set_category_id();
			$sql="select count(s.id) as count from vc_vichy_store as s join vc_categories as c on c.id = s.type where c.parent_id = ". self::$_store ." and published=1";
			$db->setQuery($sql);
			$result = $db->loadObject();
			return $result->count;        
		}

		function getSpa($province,$start=0,$display=4)
		{						
			$db = JFactory::getDBO();
			
			self::set_category_id();
	        $language_tag = self::$_lang;
			if($language_tag == 'vi-VN'){
	            $language = "'*',".$db->quote($language_tag);            
	        }else{
	            $language = $db->quote($language_tag);            
	        }

			$sql="select s.name,s.address,s.image,s.phone, p.name as province from vc_vichy_store s join vc_provinces as p on s.province = p.id where s.type = ". self::$_spa ." and s.province = $province and s.published=1 and s.language in($language) limit ". $start.','.$display;
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			return $result;
		}
		function getIntroductionSpa()
		{						
			$db = JFactory::getDBO();
			
			self::set_category_id();
	        $language_tag = self::$_lang;
	        if($language_tag == 'vi-VN'){
	            $language = "'*',".$db->quote($language_tag);            
	        }else{
	            $language = $db->quote($language_tag);            
	        }

			$sql="select id,title,introtext,catid from vc_content where catid=43 and state>0 and language in($language) order by id limit 0,1";
			$db->setQuery($sql);
			$result = $db->loadObject();
			return $result;
		}
        
    }
    
?>