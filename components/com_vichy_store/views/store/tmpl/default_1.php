<?php
defined('_JEXEC') or die;

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();

if($language_tag == 'vi-VN'){
	$boutique_id = 92;
	$lbl_address = "Địa chỉ:";
	$lbl_phone_number = "Điện thoại:";
	$lbl_no_data = "Không có dữ liệu";
	$lbl_buy_online = "Mua hàng trực tuyến";
	$lbl_input_address = "Nhập địa chỉ của bạn";
}else{
	$boutique_id = 101;
	$lbl_address = "Address:";
	$lbl_phone_number = "Phone:";
	$lbl_no_data = "Data not available";
	$lbl_buy_online = "CLICK TO BUY ONLINE";
	$lbl_input_address = "Enter your address";
}
$document->addStyleSheet('components/com_vichy_product/css/style.css');

$type_store = Vichy_storeHelper::getTypeStore();
$type_store_image = json_decode($type_store[0]->params);
$list = Vichy_storeHelper::getStoresByType($boutique_id);
$address = '';
if(isset($_REQUEST['map_address']))
	$address = $_REQUEST['map_address'];
if(empty($address))
	 $address = $_REQUEST['address'];
?>
<script>
    $( document ).ready(function() {
//        if(window.innerWidth<992){
//            $("#wrap-store-location").addClass("collapse in");
//        }else{
//            $("#wrap-store-location").removeClass("collapse");
//            $("#buy-collapse").removeClass("collapse");
//            $("#map-collapse").removeClass("collapse");
//        }

             $("#locationmobi").hide();

    });

</script>
<style>
	#img-location{
		display: none !important;
	}
</style>
<div class="store-locator">
<div style="margin:0 auto 0;overflow:hidden;">

    <div class="btn-mobi mobile-style" id="hethong-collapse" style="margin-bottom:3px;width:100%; background:#0f3e87; color:white; padding:3%;" data-toggle="collapse" aria-expanded="false" data-target="#wrap-store-location">HỆ THỐNG CỬA HÀNG<img class="plus" src="<?php echo JURI::root() ?>components/com_vichy_product/images/btn-plus.png" /></div>

	<div class="address collapse" id="wrap-store-location">
		<ul class="tabs">
			<?php foreach ($type_store as $k => $v) { $image = json_decode($v->params); ?>
			<li <?php echo ($k==0) ? 'class="active"' : ''; echo ($k==2)? 'style="margin-right:0px;width:148px;"': ''; ?>>
				<a href="javascript:void(0);" data-image="<?php echo $image->image; ?>" data-id="<?php echo $v->id; ?>"><?php echo mb_strtoupper($v->title); ?></a>
			</li>
			<?php } ?>
		</ul>
		<div class="clear"></div>
		<div class="image_type_store">
			<img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo $type_store_image->image;?>&w=444&h=210&q=100&zc=0" />
		</div>
		<div class="tab-container">
			<div class="store-content" style="height: 380px;">
				<ul id="store-content">
		    		<?php
		            if(!empty($list)){
		    			foreach ($list as $key => $item) {
		    		?>
			        	<li>
		                    <a class="map-location" data-long="<?php echo $item->lng; ?>" data-lat="<?php echo $item->lat; ?>" href="javascript:void(0);">
			            	<div class="image"><?php echo $item->name; ?></div>
			                <div class="info">
			                    <div class="addr"><span><?php echo $lbl_address; ?></span> <?php echo $item->address; ?></div>
			                    <div class="addr"><span><?php echo $lbl_phone_number; ?></span> <?php echo $item->phone; ?></div>
			                </div>
		                    </a>
		                </li>

		            <?php
		            	}
		            }else{
		            	echo $lbl_no_data;
		            }
		            ?>

		        </ul>
	        </div>
		</div>
	</div><!-- address -->

    <div class="content mobile-style" id="muahang-collapse" style="margin-bottom:3px;clear:both">
        <div class="btn-mobi mobile-style" style="width:100%; background:#0f3e87; color:white; padding:3%;" data-toggle="collapse" aria-expanded="false" data-target="#buy-collapse">MUA HÀNG TRỰC TUYẾN<img class="plus" src="<?php echo JURI::root() ?>components/com_vichy_product/images/btn-plus.png" /></div>
        <div class="buy-offline collapse" id="buy-collapse">
            	<h3><?php echo $lbl_buy_online; ?></h3>
                <div class="buy-offline-image">
                	<a href="http://www.chon.vn/cua-hang/vichy.aspx?ref=home-merchant"><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/chonvn.png"></a>
					<a href="http://www.yes24.vn/khuyen-mai/471450/vichy-khuyen-mai.html"><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/yes24com.png"></a>
					<a href="http://www.lazada.vn/vichy/" ><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/lazada.png"></a>
					<a href="http://tiki.vn/thuong-hieu/vichy.html" style="margin-right: 0px;"><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/tiki.png"></a>
                </div>
            </div>
        </div>
	<div class="store-locator-right">
        <div class="mobile-style" style="text-align:center;padding-right:9% !important;width:100%; background:#0f3e87; color:white; padding:3%;" >TÌM CỬA HÀNG GẦN NHẤT  <!--<img class="plus" src="<?php echo JURI::root() ?>components/com_vichy_product/images/btn-minus.png" />--></div>
        <div class="content" style="position:relative;">
    		<form id="submit_address" method="post" action="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=store&task=search&Itemid=113'); ?>">
    			<input type="text" id="geocomplete" placeholder="<?php echo $lbl_input_address; ?>" name="map_address" class="inputbox" value="" />
    			<input type="submit" class="google-search" value="" />
    			<input id="search_latitude" type="hidden" name="lat" value="" />
        		<input id="search_longtitude" type="hidden" name="lng" value="" />
        		<input id="search_address" type="hidden" name="address" value="" />
    		</form>
        	<div id="map" style="position: relative"></div>
        </div>
        <!-- <input id="geocomplete" type="hidden" name="geocomplete" /> -->
        <div class="content pc-style">
        	<div class="buy-offline ">
            	<h3><?php echo $lbl_buy_online; ?></h3>
                <div class="buy-offline-image">
                	<a href="http://www.chon.vn/cua-hang/vichy.aspx?ref=home-merchant"><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/chonvn.png"></a>
					<a href="http://www.yes24.vn/khuyen-mai/471450/vichy-khuyen-mai.html"><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/yes24com.png"></a>
					<a href="http://www.lazada.vn/vichy/" ><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/lazada.png"></a>
					<a href="http://tiki.vn/thuong-hieu/vichy.html" style="margin-right: 0px;"><img src="<?php echo JURI::root();?>/components/com_vichy_store/images/tiki.png"></a>
                </div>
            </div>
        </div>
    
    </div>
    <div id="com-vichy-store-url" redirect-url="<?php echo 'http://'.JROUTE::_('index.php?option=com_vichy_store&Itemid=113'); ?>" data-url="<?php echo JURI::root() ?>" data-current-long="<?php echo $long; ?>" data-current-lat="<?php echo $lat; ?>"></div>
</div>
</div>
<link href="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
<!-- custom scrollbars plugin -->
<script src="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<script src="<?php echo JURI::root();?>templates/vichy/js/jquery.redirect.min.js" type="text/javascript"></script>
<script type="text/javascript" src="components/com_vichy_store/js/my_script.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script src="<?php echo JURI::root(); ?>administrator/templates/bluestork/js/jquery.geocomplete.min.js"></script>
<script>
    $(".btn-mobi").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
        }else
        {
//            $('.btn-mobi').find('img').attr("src",'<?php //echo JURI::root(); ?>//components/com_vichy_product/images/btn-plus.png');
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-minus.png');
        }
    });
</script>