<div class="content">
    <h2>HỆ THỐNG VICHY SPA</h2>
    <div class="list-spa left">
        <h3 class="title"><?php echo $this->spa_hcm[0]->province; ?></h3>
        <ul>
        <?php foreach ($this->spa_hcm as $k => $v) { ?>
            <li>
                <div class="image"><a href="javascript:void(0);"><img src="<?php echo JURI::root() ?>components/com_vichy_store/uploads/spa/<?php echo $v->image; ?>" width="120" height="120" alt="Spa"></a></div>
                <div class="info">
                    <h3><?php echo $v->name; ?></h3>
                    <div class="addr"><span>Địa chỉ:</span> <?php echo $v->address; ?></div>
                    <div class="addr"><span>Điện thoại:</span> <?php echo $v->phone; ?></div>
                </div>
            </li> <!-- item -->  
        <?php } ?>                 
        </ul>
    </div> <!-- list spa -->
    
     <div class="list-spa right">
        <h3 class="title"><?php echo $this->spa_hn[0]->province; ?></h3>
        <ul>
        <?php foreach ($this->spa_hn as $k => $v) { ?>
            <li>
                <div class="image"><a href="javascript:void(0);"><img src="<?php echo JURI::root() ?>components/com_vichy_store/uploads/spa/<?php echo $v->image; ?>" width="120" height="120" alt="Spa"></a></div>
                <div class="info">
                    <h3><?php echo $v->name; ?></h3>
                    <div class="addr"><span>Địa chỉ:</span> <?php echo $v->address; ?></div>
                    <div class="addr"><span>Điện thoại:</span> <?php echo $v->phone; ?></div>
                </div>
            </li> <!-- item -->  
        <?php } ?>
        </ul>
    </div> <!-- list spa -->
    
    <div class="clearfix"></div>
    
    <div class="box-contact">
        <p>ĐỂ ĐẶT LỊCH HẸN SPA , HÃY GỌI ĐẾN <span>1800 545 463 </span><br>
        HOẶC E-MAIL TỚI <span><a href="mailto:INFO@VICHY.COM.VN">INFO@VICHY.COM.VN</a></span></p>
    </div>
    
    
</div>