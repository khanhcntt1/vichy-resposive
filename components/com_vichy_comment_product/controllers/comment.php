<?php
defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/controller.php';
class Vichy_comment_productControllerComment extends JController{
	public function __construct(){
		parent::__construct();
	}
	public function saveFeedback(){
		$curLanguage = JFactory::getLanguage();
		$language_tag = $curLanguage->getTag();
		if($language_tag == 'vi-VN'){
			$warning_login = "Vui lòng đăng nhập để đăng bình luận";
			$warning_comment = "Bình luận không được để trống";
			$send_comment = "Bình luận đã được gửi";
		}else{
			$warning_login = "Please login to post a comment";
			$warning_comment = "Comment can not be empty";
			$send_comment = "Comment has been sent";
		}

		$username = JRequest::getVar('username');
		$product_id = JRequest::getVar('product_id');
		$range_id = JRequest::getVar('range_id');
		if($username == ''){
			$this->setMessage(JText::_($warning_login),'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_vichy_product&view=product&id='.$product_id.'&rid='.$range_id.'&Itemid=103', false));
			return false;
		}
		$feedback = trim(JRequest::getVar('feedback'));
		if($feedback == ''){
			$this->setMessage(JText::_($warning_comment),'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_vichy_product&view=product&id='.$product_id.'&rid='.$range_id.'&Itemid=103', false));
		}else{
			$db = & JFactory::getDBO();
			$date = date('Y-m-d H:i:s');
			$sql = "INSERT INTO vc_vichy_comment_product VALUES (null,'$username','$product_id','$feedback',null,'$date','0')";
			$db->setQuery($sql);
			$db->query();

			// $activity_id = $db->insertid();

			// $type = 'com_vichy_comment_product';
			// $query = "INSERT INTO win_notification VALUES (null,$activity_id,2,'$username','$type','$date')";
			// $db->setQuery($query);
			// $db->query();
			

			//Gui mail thong bao cho admin
			// $config = JFactory::getConfig();
   //      	$data['fromname'] = $config->get('fromname');
			// $data['mailfrom'] = $config->get('mailfrom');
			// $emailSubject	= 'Khách hàng đăng bình luận trong Vichy';
			// $emailBody = "Chào admin, có bình luận mới trong trang sản phẩm của Vichy. Bạn hãy vào trang quản trị bình luận để kiểm tra";
   //      	$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['mailfrom'], $emailSubject, $emailBody,1);

			// $this->setMessage(JText::_('Bình luận của bạn đã được gửi'));
			// $this->setRedirect(JRoute::_('index.php?option=com_vichy_product&view=product&id='.$product_id.'&rid='.$range_id.'&Itemid=103',false));
			echo "$send_comment";
			exit();
		}
	}
	function rating(){
		$curLanguage = JFactory::getLanguage();
		$language_tag = $curLanguage->getTag();
		if($language_tag == 'vi-VN'){
			$warning_login = "Bạn chưa đăng nhập";
			$warning_rating = "Bạn đã đánh giá rồi";
			$message_rating = "Cám ơn bạn đã đánh giá sản phẩm này";
		}else{
			$warning_login = "You are not logged in";
			$warning_rating = "You have already rated";
			$message_rating = "Thank you for evaluating this product";
		}

		$product_id = $_POST['id'];
		$val = $_POST['val'];
		$user = & JFactory::getUser();
		$username = $user->username;
		$db = & JFactory::getDBO();
		if($username == ""){
			echo "$warning_login";
		}else{
			$sql = "Select count(id) as count from vc_vichy_product_rating where username = '$username' and product_id = $product_id";
			$db->setQuery($sql);
			$ret = $db->loadObject();
			if($ret->count > 0){
				echo "$warning_rating";
			}else{
				$db->setQuery("INSERT into vc_vichy_product_rating VALUES (null, $product_id, $val, '$username',null)");
				$db->query();
				$db->setQuery("UPDATE vc_vichy_product SET reviews = reviews + 1, rating = ROUND(((rating * reviews)+$val)/(reviews+1)) where id = $product_id");
				$db->query();
				echo "$message_rating";
			}
		}
		die();
	}
	function comment_product_pagination(){
		if($_POST['page']) {
		$page = $_POST['page'];
		$product_id = $_POST['product_id'];
		$current_page = $page;
		$page -= 1;
		$display = 3;
		$start = $page * $display;
		 
		$data = "";
		$db = & JFactory::getDBO();
		$sql = "SELECT cp.*, u.avatar,u.usertype,u.name,pr.rating from vc_vichy_comment_product as cp  join vc_users as u on u.username = cp.username left join vc_vichy_product_rating as pr on pr.username = cp.username and pr.product_id = cp.product_id where cp.published = '1' and cp.product_id = $product_id order by cp.id DESC LIMIT $start, $display";
		$db->setQuery($sql);
		$result = $db->loadObjectList();
		if(count($result) == 0){
			echo '1';exit();
		}
		foreach($result as $k => $v){
			$data .= '<div class="comment-item">';
			$data .= '<div class="profile-user">';
			$data .= '<div class="thumb-pic">';
			$src = ($v->avatar == '1') ? 'vichy_default.jpg' : $v->avatar ;
			$src = JURI::root().'timbthumb.php?src=media/com_users/image/'.$src.'&w=109&q=100';
			if($v->usertype != '' && strpos($v->usertype, 'face_') !== false){
				$face_id = substr($v->usertype, 5);
				$src = "http://graph.facebook.com/$face_id/picture?type=large&width=109&height=109";
			}
			$data .= '<img src="'.$src.'" />';
			$data .= '</div>';
			$data .= '<div class="product-comment-rating">';
			$data .= '<div class="rating-detail">';
			$data .= '<div class="rating">';
			for($j=1; $j<=5; $j++){
                if(!empty($v->rating) && $v->rating >0 && $j<=$v->rating )
                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/product_star_blue.png">';
                else{
                    $data .='<img style="border:none;float:left;" src="'.JURI::root().'/media/product_star_grey.png">';
                }
            }
            $data .= '</div>';// end rating
			$data .= '</div>';//end rating-detail
			$data .= '<div class="profile-user-comment">';
			$data .= '<div class="username">by '.$v->name.'</div>';
			$data .= '<div class="datetime">'.date("d F Y", strtotime($v->date)).'</div>';
			$data .= '</div>';// end profile-user-comment
			$data .= '</div>';// end product-comment-rating
			$data .= '</div>';//end profile-user
			$data .= '<div class="comment-content">';
			$data .= $v->comment;
			$data .= '</div>';//end comment-content
			$data .= '</div>';
		}
		$query_page = "Select count(id) as count from vc_vichy_comment_product where published = '1' and product_id = '$product_id'";
		$db->setQuery($query_page);
		$result2 = $db->loadObject();
		$count = $result2->count;
		$pages = ceil($count / $display);
		
		if($pages > 1){
			$data .= '<div class="paging" style="clear:both;">';	
	 		for($i = 1;$i<=$pages;$i++ ){
	 			$data.='<a href="javascript:void(0);"'." page=$i ";
	 			if($i==$current_page)
	 				$data.='class ="active"';
	 			$data.=">$i</li>";
 			} 
			$data = $data . "</div>";
		}
		echo $data;
		exit();
		}
	}

	function count_comment(){
		$product_id = $_POST['product_id'];
		$db = & JFactory::getDBO();
		$query_page = "Select count(id) as count from vc_vichy_comment_product where published = '1' and product_id = '$product_id'";
		$db->setQuery($query_page);
		$result2 = $db->loadObject();
		echo $result2->count;
		exit();
	}
}