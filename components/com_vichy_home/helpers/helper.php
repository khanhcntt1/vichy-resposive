<?php

	defined('_JEXEC') or die;

	abstract class Vichy_homeHelper {
		function getSlideShow()
		{
			$db = JFactory::getDbo();

			$curLanguage = JFactory::getLanguage();
        	$language_tag = $curLanguage->getTag();
			if($language_tag == 'vi-VN'){
		        $language = "'*',".$db->quote($language_tag);
		    }else{
		        $language = $db->quote($language_tag);
		    }

			$query = "SELECT image,link from vc_vichy_home where position = 1 and publish=1 and language in ($language) ";
			$db->setQuery($query);

			$result = $db->loadObjectList();

			return $result;
		}

		function getOtherImages()
		{
			
			$db = JFactory::getDbo();

			$curLanguage = JFactory::getLanguage();
        	$language_tag = $curLanguage->getTag();
			if($language_tag == 'vi-VN'){
		        $language = "'*',".$db->quote($language_tag);
		    }else{
		        $language = $db->quote($language_tag);
		    }

			$query = "SELECT position,image,link from vc_vichy_home where position != 1 and publish=1 and language in ($language) ";
			$db->setQuery($query);

			$result = $db->loadObjectList();

			return $result;
		}
		function getCategory()
		{
			$db= JFactory::getDbo();
			$query = 'SELECT c.id, c.catid,  c.title, c.images, c.introtext, c.created, c.language, 
	        CASE WHEN CHAR_LENGTH(c.alias) 
	        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
	        CASE WHEN CHAR_LENGTH(cg.alias) 
	        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
	        FROM vc_content c JOIN vc_categories cg ON c.catid = cg.id 
	        WHERE cg.parent_id =29 AND c.state > 0 
	        ORDER BY c.created DESC limit 0,1';
		   	$db->setQuery($query);
		    $result = $db->loadObject();
		    return $result;
		}
    }
    
?>