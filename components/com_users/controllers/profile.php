<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';
// require_once 'SimpleImage.php';

/**
 * Profile controller class for Users.
 *
 * @package		Joomla.Site
 * @subpackage	com_users
 * @since		1.6
 */
class UsersControllerProfile extends UsersController
{
	/**
	 * Method to check out a user for editing and redirect to the edit form.
	 *
	 * @since	1.6
	 */
	public function edit()
	{
		$app			= JFactory::getApplication();
		$user			= JFactory::getUser();
		$loginUserId	= (int) $user->get('id');

		// Get the previous user id (if any) and the current user id.
		$previousId = (int) $app->getUserState('com_users.edit.profile.id');
		$userId	= (int) JRequest::getInt('user_id', null, '', 'array');

		// Check if the user is trying to edit another users profile.
		if ($userId != $loginUserId) {
			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_users.edit.profile.id', $userId);

		// Get the model.
		$model = $this->getModel('Profile', 'UsersModel');

		// Check out the user.
		if ($userId) {
			$model->checkout($userId);
		}

		// Check in the previous user.
		if ($previousId) {
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function save()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app	= JFactory::getApplication();
		$model	= $this->getModel('Profile', 'UsersModel');
		$user	= JFactory::getUser();
		$userId	= (int) $user->get('id');

		// Get the user data.
		$data = JRequest::getVar('jform', array(), 'post', 'array');

		// Force the ID to this user.
		$data['id'] = $userId;

		// Validate the posted data.
		$form = $model->getForm();
		if (!$form) {
			JError::raiseError(500, $model->getError());
			return false;
		}

		// Validate the posted data.
		$data = $model->validate($form, $data);

		// Check for errors.
		if ($data === false) {
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if ($errors[$i] instanceof Exception) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Save the data in the session.
			$app->setUserState('com_users.edit.profile.data', $data);

			// Redirect back to the edit screen.
			$userId = (int) $app->getUserState('com_users.edit.profile.id');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit&user_id='.$userId, false));
			return false;
		}

		// Attempt to save the data.
		$return	= $model->save($data);

		// Check for errors.
		if ($return === false) {
			// Save the data in the session.
			$app->setUserState('com_users.edit.profile.data', $data);

			// Redirect back to the edit screen.
			$userId = (int)$app->getUserState('com_users.edit.profile.id');
			$this->setMessage(JText::sprintf('COM_USERS_PROFILE_SAVE_FAILED', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit&user_id='.$userId, false));
			return false;
		}

		// Redirect the user and adjust session state based on the chosen task.
		switch ($this->getTask()) {
			case 'apply':
				// Check out the profile.
				$app->setUserState('com_users.edit.profile.id', $return);
				$model->checkout($return);

				// Redirect back to the edit screen.
				$this->setMessage(JText::_('COM_USERS_PROFILE_SAVE_SUCCESS'));
				$this->setRedirect(JRoute::_(($redirect = $app->getUserState('com_users.edit.profile.redirect')) ? $redirect : 'index.php?option=com_users&view=profile&layout=edit&hidemainmenu=1', false));
				break;

			default:
				// Check in the profile.
				$userId = (int)$app->getUserState('com_users.edit.profile.id');
				if ($userId) {
					$model->checkin($userId);
				}

				// Clear the profile id from the session.
				$app->setUserState('com_users.edit.profile.id', null);

				// Redirect to the list screen.
				$this->setMessage(JText::_('COM_USERS_PROFILE_SAVE_SUCCESS'));
				$this->setRedirect(JRoute::_(($redirect = $app->getUserState('com_users.edit.profile.redirect')) ? $redirect : 'index.php?option=com_users&view=profile&user_id='.$return, false));
				break;
		}

		// Flush the data from the session.
		$app->setUserState('com_users.edit.profile.data', null);
	}

	public function saveData(){
		$field = $_POST['field'];
		$value = $_POST['text'];
		$user_id = $_POST['user_id'];
		$db = & JFactory::getDbo();
		if($field == 'email'){
			//Check email exist
			$query = "Select count(id) as count from #__users where email = '$value'";
	        $db->setQuery($query);
	        $ret = $db->loadObject();
	        if($ret->count){
	        	echo "error_email";
	        	exit();
	        }
		}
		$sql = "UPDATE vc_users set $field = '$value' where id = $user_id";
		$db->setQuery($sql);
		$db->query();
		echo '1';
		die();
	}

	public function add_info(){
		$answer = $_POST['info'];
		$user_id = $_POST['user_id'];
		$db = & JFactory::getDbo();
		foreach ($answer as $k => $v) {
			if(!empty($v)){
				$tmp = explode('|', $v);
				$db->setQuery("SELECT count(id) as count from vc_vichy_qa_user where user_id = $user_id and question_id = $tmp[0]");
				$count = $db->loadObject();
				if($count->count == 1){
					$db->setQuery("UPDATE vc_vichy_qa_user set answer_id = $tmp[1], modified_date = '".date('Y-m-d h:i:s')."' WHERE user_id = $user_id and question_id = $tmp[0]");
					$db->query();
				}else{
					$db->setQuery("INSERT into vc_vichy_qa_user values (null, $tmp[0], $tmp[1],$user_id, null, null, 1)");
					$db->query();
				}
			}
		}
		//$link = JROUTE::_('index.php?option=com_users&view=profile');
		$link = JROUTE::_('index.php?option=com_vichy_advice&view=diagnostic');
		$this->setRedirect($link);
	}

	// public function saveimage(){
	// 	$f = false; 
	// 	$nw = 120; # image with # height
	// 	$nh = 120;
	// 	$path = '';
	// 	$userId = JFactory::getUser()->id;
	// 	$file_name = substr($_SESSION['tmp_image'][$userId], 3);
		
	// 	$path = 'media/com_users/image/' . $file_name;
	// 	$size = getimagesize($_SESSION['tmp_image'][$userId]);

	// 	$x = (int) $_POST['x'];
	// 	$y = (int) $_POST['y'];
	// 	$w = (int) $_POST['w'] ? $_POST['w'] : $size[0];
	// 	$h = (int) $_POST['h'] ? $_POST['h'] : $size[1];

	// 	$data = file_get_contents('media/com_users/image/'.$_SESSION['tmp_image'][$userId]);
	// 	$vImg = imagecreatefromstring($data);
	// 	$dstImg = imagecreatetruecolor($nw, $nh);
	// 	imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
	// 	imagejpeg($dstImg, $path);
	// 	imagedestroy($dstImg);
	// 	if(file_exists('media/com_users/image/'.$_SESSION['tmp_image'][$userId])){
	// 		unlink('media/com_users/image/'.$_SESSION['tmp_image'][$userId]);
	// 	}
	// 	unset($_SESSION['tmp_image'][$userId]);

	// 	$db =& JFactory::getDBO();
	// 	$sql1 = "Select avatar from #__users where id = $userId";
	// 	$db->setQuery ($sql1);
	// 	$avatar = $db->loadObject();
	// 	if($avatar->avatar != '1'){
	// 		$filename = 'media/com_users/image/'.$avatar->avatar;
	// 		if(file_exists($filename)){
	// 			unlink($filename);
	// 		}
	// 	}
	// 	$sql = "UPDATE vc_users  SET avatar = '$file_name' where id = $userId";
	// 	$db->setQuery ($sql);
	// 	$db->query();
	// 	$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
	// 	return false;
	// }

	// public function savetmpimage(){
	// 	$valid_exts = array('jpeg', 'jpg', 'png');
	// 	$max_file_size = 150 * 1024; #200kb
	// 	$userId = JFactory::getUser()->id;

	// 	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	// 		if ( isset($_FILES['image']) ) {
	// 			if (! $_FILES['image']['error'] && $_FILES['image']['size'] < $max_file_size) {
	// 				$ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
	// 				if (in_array($ext, $valid_exts)) {
	// 					$file_tmp = 'UM_'.time().$_FILES['image']['name'];
	// 					$arr_tmp = getimagesize($_FILES['image']['tmp_name']);
	// 					if($arr_tmp[0] > 500){
	// 						$image = new SimpleImage(); 
	// 						$image->load($_FILES['image']['tmp_name']);
	// 						$image->resizeToWidth(500); 
	// 						$image->save('media/com_users/image/'.$file_tmp);
	// 					}else{
	// 						move_uploaded_file($_FILES['image']['tmp_name'], 'media/com_users/image/'.$file_tmp);
	// 					}
	// 					$_SESSION['tmp_image'][$userId] = $file_tmp;
	// 					$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
	// 					return false;
	// 				} else {
	// 					$this->setMessage(JText::_('File không đúng định dạng'),'warning');
	// 					$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
	// 					return false;
	// 				}
	// 			} else {
	// 				$this->setMessage(JText::_('File quá lớn'),'warning');
	// 				$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
	// 				return false;
	// 			}
	// 		} else {
	// 			$this->setMessage(JText::_('Không có file nào'),'warning');
	// 			$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
	// 			return false;
	// 		}
	// 	}
	// }

	// public function cancelcrop(){
	// 	$userId = JFactory::getUser()->id;
	// 	if(file_exists('media/com_users/image/'.$_SESSION['tmp_image'][$userId])){
	// 		unlink('media/com_users/image/'.$_SESSION['tmp_image'][$userId]);
	// 	}
	// 	unset($_SESSION['tmp_image'][$userId]);
	// 	$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
	// 	return false;
	// }
}
