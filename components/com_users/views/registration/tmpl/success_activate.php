<?php
defined('_JEXEC') or die;
$document = & JFactory::getDocument();
$document->addStyleSheet('components/com_users/views/registration/css/style.css');
$document->addStyleSheet('components/com_users/views/registration/css/complete.css');

$session =& JFactory::getSession();
$lang = $session->get('lang');
if($lang == 'vi-VN'){
	$alert_message_active = "Đã kích hoạt tài khoản";
	$alert_message_check_active = "Tài khoản của bạn đã được kích hoạt. <br />Vui lòng kiểm tra lại thông tin tài khoản trong email kích hoạt hoặc liên hệ với Vichy để được hướng dẫn chi tiết";
	$btn_return_homepage = "Về trang chủ";
}else{
	$alert_message_active = "You activated your account";
	$alert_message_check_active = "Your account is activated. <br />Please check your information in activated email again or contact with Vichy to guided detail";
	$btn_return_homepage = "Return home page";
}

?>
<div class="registration">
	<div class="sign-box">
		<h1><?php echo $alert_message_active; ?></h1>
		<div class="intro-text"><?php echo $alert_message_check_active; ?></div>
		<a class="return" href="<?php echo JURI::root(); ?>"><?php echo $btn_return_homepage; ?></a>
	</div>
</div>