<?php
	defined('_JEXEC') or die;
	$document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_users/views/registration/css/style.css');
	$db = & JFactory::getDbo();
	
	$curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();
    if($language_tag == 'vi-VN'){
        $language = "'*',".$db->quote($language_tag);
        $catid = 60;  
    }else{
        $language = $db->quote($language_tag);
        $catid = 155;         
    }

	$sql = "SELECT id,name FROM `vc_provinces` order by name";
	$db->setQuery($sql);
	$listCity = $db->loadObjectList();
	$sql2 = "SELECT id, `text`, parent_id, is_question FROM `vc_vichy_qa` where type = 'diagnostic' and published = 1 and catid = $catid and language in ($language)";
	$db->setQuery ($sql2);
	$listQA = $db->loadObjectList();
	if(count($listQA)){
		$listQA_new = array();
		foreach ($listQA as $k => $v) {
			$pa = $v->parent_id;
			$listQA_new[$pa][] = $v;
		}
	}
	
	if($language_tag == 'vi-VN'){		
		$lbl_sign_box_top = "Tham gia câu lạc bộ my skin để được tư vấn và nhận nhiều ưu đãi";
		$lbl_info_register = "Thông tin đăng ký";
		$lbl_note = "Những ô đánh dấu * là bắt buộc";
		$lbl_username = "Tên đăng nhập *";
		$lbl_password = "Mật khẩu *";
		$lbl_confirm_password = "Xác nhận mật khẩu *";
		$lbl_fullname = "Tên đầy đủ *";
		$lbl_gender = "Giới tính *";
		$lbl_male = "Nam";
		$lbl_female = "Nữ";
		$lbl_date_of_birth = "Ngày tháng năm sinh *";
		$lbl_phone_number = "Số điện thoại *";
		$lbl_provice = "Thành phố *";
		$lbl_select_province = "Chọn tên thành phố";
		$lbl_add_info = "<span>Thông tin bổ sung</span> (Cập nhật thông tin chi tiết để tiếp tục bước tư vấn da)";
		$lbl_btn_register = "ĐĂNG KÍ";
	}else{
		$lbl_sign_box_top = "Join my club skin for advice and received many incentives";
		$lbl_info_register = "Indentification information";
		$lbl_note = "Fields with an * are mandatory";
		$lbl_username = "Username *";
		$lbl_password = "Password *";
		$lbl_confirm_password = "Password confirmation*";
		$lbl_fullname = "Full name *";
		$lbl_gender = "Gender *";
		$lbl_male = "Male";
		$lbl_female = "Female";
		$lbl_date_of_birth = "Your date of birth *";
		$lbl_phone_number = "Your phone number *";
		$lbl_provice = "City *";
		$lbl_select_province = "Choose a city";
		$lbl_add_info = "<span>Additional information</span> (Update information to continue steps skin consulting)";
		$lbl_btn_register = "Sign up";
	}
?>
<script src="<?php echo JURI::root(); ?>templates/vichy/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/bday-picker.js"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_users/views/registration/js/script.js"></script>
<div class="registration no-width-height">
	<div class="sign-box no-width-height">
		<div class="sign_box_top no-width-height">
			<?php echo $lbl_sign_box_top; ?>
		</div>
		<div class="title">
			<h3>MY VICHY SPACE</h3>
			<span><?php echo $lbl_info_register; ?></span>
			<!-- <div class="intro-text">
				By registering, you can share your opinion on our products and keep your diagnoses results.
				My Vichy Space also allows you to keep an eye on products suggested for your skin and on advice from our experts. Do not wait any longer!
			</div> -->
			<div class="note"><?php echo $lbl_note; ?></div>
		</div>
		<div class="sign-form no-width-height">
		<div>
			<div class="form-error">
				<span class="error-text"></span>
			</div>
			<div class="left-form no-width-height">
				<div class="row no-width-height">
					<div class="title"><?php echo $lbl_username; ?></div>
					<div class="content no-width-height">
						<input type="text" class="input-data" name="user_name" id="user_name"/>
					</div>
				</div>
				<div class="row no-width-height">
					<div class="title"><?php echo $lbl_password; ?></div>
					<div class="content no-width-height">
						<input type="password" class="input-data" name="password" id="password"/>
					</div>
				</div>
				<div class="row no-width-height">
					<div class="title"><?php echo $lbl_confirm_password; ?></div>
					<div class="content no-width-height">
						<input type="password" class="input-data" name="repass" id="repass"/>
					</div>
				</div>
				<div class="row no-width-height">
					<div class="title"><?php echo $lbl_fullname; ?></div>
					<div class="content no-width-height">
						<input type="text" class="input-data" name="full-name" id="full-name"/>
					</div>
				</div>
				<div class="row no-width-height">
					<div class="title"><?php echo $lbl_gender; ?></div>
					<div class="content no-width-height">
						<div class="left-content">
							<input type="radio" value="1" name="gender" class="gender"  style="width: auto;">
							<?php echo $lbl_male; ?>
						</div>
						<div class="left-content">
							<input type="radio" value="0" name="gender" class="gender"  style="width: auto;">
							<?php echo $lbl_female; ?>
						</div>
					</div>
				</div>
				<!-- <input type="text" class="input-data" placeholder="Ngày tháng năm sinh *" 
				name="birthday" id="birthday"/> -->
				<div class="row no-width-height">
					<div class="title"><?php echo $lbl_date_of_birth; ?></div>
					<div class="content no-width-height" id="picker1"></div>
				</div>
				<div class="row no-width-height">
					<div class="title email-title">Email *</div>
					<div class="content no-width-height">
						<input type="text" class="input-data" name="email" id="email"/>
					</div>
				</div>
			<!-- </div>
			<div class="right-form"> -->
				<div class="row no-width-height">
					<div class="title"><?php echo $lbl_phone_number; ?></div>
					<div class="content no-width-height">
						<input type="text" class="input-data" name="phone" id="phone"/>
					</div>
				</div>
				<!-- <input type="text" class="input-data" placeholder="Địa chỉ" name="address" id="address"/> -->
				<div class="row no-width-height">
					<div class="title"><?php echo $lbl_provice; ?></div>
					<div class="content">
						<select class="select-data" name="address" id="address">
							<option value=""><?php echo $lbl_select_province; ?></option>
							<?php foreach($listCity as $v){ ?>
							<option value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
		</div>
			<div class="clear"></div>
			<?php if(count($listQA)){ ?>
			<div class="add_info">
				<div class="add_title"><?php echo $lbl_add_info; ?></div>
					<?php foreach($listQA_new[0] as $k => $v){ ?>
					<div class="add_item" <?php echo ($k==count($listQA_new[0])-1) ? 'style="border-bottom:none;"': ''; ?>>
						<div class="add_question"><?php echo strip_tags($v->text); ?></div>
						<input type="hidden" name="info[]" class="info" id="info_<?php echo $v->id; ?>" />
						<?php foreach($listQA_new[$v->id] as $v1){ ?>
						<div class="add_answer">
							<div class="image_common image_off" data-qid="<?php echo $v1->parent_id; ?>" data-id="<?php echo $v1->id; ?>"><?php echo strip_tags($v1->text); ?></div>
						</div>
						<?php } ?>
					</div>
					<?php } ?>
			</div>
			<?php } ?>
			<div class="bottom-form no-width-height">
				<div class="box">
					<input type="button" class="submit" id="btn-register" value="<?php echo $lbl_btn_register; ?>"/>
				</div>
			</div>
		</div>
		<div class="base_url" style="display:none;"><?php echo JURI::root(); ?></div>
	</div>
</div><!-- end div registration -->

