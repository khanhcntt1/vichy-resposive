jQuery(document).ready(function(){
	var language_tag = jQuery("#language_tag").val();
    if(language_tag == 'vi-VN'){
        placeYear = "Năm";
        placeMonth = "Tháng";
        placeDay = "Ngày";
        errorUsername = "Lỗi : Tên đăng nhập không được bỏ trống !";
        errorPass = "Lỗi : Vui lòng nhập mật khẩu !";
        errorConfirmPass = "Lỗi : Xác nhận mật khẩu không trùng với mật khẩu !";
        errorName = "Lỗi : Họ tên không được bỏ trống !";
        errorGender = "Lỗi : Vui lòng chọn giới tính !";
        errorDOB = "Lỗi : Ngày tháng năm sinh không được bỏ trống !";
        errorEmail = "Lỗi : Email không được bỏ trống !";
        errorFormatEmail = "Lỗi : Email không đúng định dạng !";
        errorPhone = "Lỗi : Số điện thoại không được bỏ trống !";
        errorAddress = "Lỗi : Vui lòng chọn thành phố !";
        errorEmailExist = "Lỗi : Email này đã được sử dụng !";
        errorUsernameExist = "Lỗi : Tên đăng nhập này đã được sử dụng !";
		errorSendMail = "Hệ thống không gửi được email cho bạn";

    }else{
    	jQuery('.row .title').css('width',"175px");
    	
        placeYear = "Year";
        placeMonth = "Month";
        placeDay = "Day";
        errorUsername = "Error : Username is required !";
        errorPass = "Error : Please input a password !";
        errorConfirmPass = "Error :Passwords did not match !";
        errorName = "Error : Your name is required !";
        errorGender = "Error : Please select your gender !";
        errorDOB = "Error : Please select your date of birth !";
        errorEmail = "Error : Your email is required !";
        errorFormatEmail = "Error : That is not a valid email. Please input a valid email !";
        errorPhone = "Error : Your phone number is required !";
        errorAddress = "Error : Please select your city !";
        errorEmailExist = "Error : This email address already exist !";
        errorUsernameExist = "Error : The username is already being used !";
		errorSendMail = "The system does not send emails to you";
    }

	var base_url = jQuery('.base_url').text();
	jQuery('.add_item').on('click','.image_off',function(){
		qid = jQuery(this).attr('data-qid');
		aid = jQuery(this).attr('data-id');
		jQuery('#info_'+qid).val(qid+'|'+aid);
		jQuery(this).parent().siblings().children('.image_common').removeClass('image_on');
		jQuery(this).parent().siblings().children('.image_common').addClass('image_off');
		jQuery(this).removeClass('image_off');
		jQuery(this).addClass('image_on');
	});

	jQuery("#picker1").birthdaypicker({
    	dateFormat: "littleEndian",
    	placeYear: placeYear,
    	placeMonth: placeMonth,
    	placeDay: placeDay
    });
	jQuery("#btn-register").click(function(){

		var fullname =  jQuery("#full-name").val();
		var user_name = jQuery("#user_name").val();
		var arr = new Array();
		jQuery('.info').each(function(i,e){
			if(jQuery(e).val() != ''){
				arr[i] = jQuery(this).val();
			}else{
				arr[i] = 'a';
			}
		})
		var str_arr = arr.join('#');

		if(user_name == ""){
			jQuery(".error-text").text(errorUsername);
			return false;
		}else{
			jQuery(".error-text").text("");
		}

		var password =  jQuery("#password").val();
		if(password == ""){
			jQuery(".error-text").text(errorPass);
			return false;
		}else{
			jQuery(".error-text").text("");
		}


		var repass =  jQuery("#repass").val();
		if(repass != password){
			jQuery(".error-text").text(errorConfirmPass);
			return false;
		}else{
			jQuery(".error-text").text("");
		}

		if(fullname == ""){
			jQuery(".error-text").text(errorName);
			return false;
		}else{
			jQuery(".error-text").text("");
		}

		var gender =  '';
		if(jQuery("input[name=gender]").is(":checked")){
			gender = jQuery("input[name=gender]:checked").val();
			jQuery(".error-text").text("");
		}else{
			jQuery(".error-text").text(errorGender);
			return false;
		}

		var birthday =  jQuery("#birthdate").val();
		if(birthday == ""){
			jQuery(".error-text").text(errorDOB);
			return false;
		}else{
			jQuery(".error-text").text("");
		}

		var email =  jQuery("#email").val();
		if(email == ""){
			jQuery(".error-text").text(errorEmail);
			return false;
		}else{
			jQuery(".error-text").text("");
		}

		if(!isEmailAddress(email)){
			jQuery(".error-text").text(errorFormatEmail);
			return false;
		}else{
			jQuery(".error-text").text("");
		}

		var phone =  jQuery("#phone").val();
		if(phone == ""){
			jQuery(".error-text").text(errorPhone);
			return false;
		}else{
			jQuery(".error-text").text("");
		}

		var address = jQuery("#address").val();
		if(address == ""){
			jQuery(".error-text").text(errorAddress);
			return false;
		}else{
			jQuery(".error-text").text("");
		}

		var myData = 'fullname='+ fullname + '&user_name='+ user_name + '&gender='+ gender + '&birthday='+ birthday + '&email='+ email + '&phone='+ phone + '&address='+ address + '&password='+ password+'&question='+str_arr;
		
		jQuery.ajax({
            type: "POST",
            url: base_url+"index.php?option=com_users&task=registration.register_user",
            dataType:"text",
            data:myData,
            success:function(response){
            	if(response == 'error_email'){
            		jQuery(".error-text").text(errorEmailExist);
					return false;
            	}
            	if(response == 'error_user'){
            		jQuery(".error-text").text(errorUsernameExist);
					return false;
            	}
            	if(response.indexOf("user_") != -1){
            		// alert("Bạn đã đăng kí thành công! Vui lòng kiểm tra email của bạn và kích hoạt tài khoản");
            		userid = response.substring(response.indexOf("_")+1);
            		window.location = base_url+"index.php?option=com_users&view=registration&layout=mycomplete&user="+userid;
            		// window.location = base_url;
            	}else{
            		alert(errorSendMail);
            	}
            }
        });
		return false;
	});
	function isEmailAddress(str) {
	   var pattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	   return pattern.test(str);  // returns a boolean 
	}
});