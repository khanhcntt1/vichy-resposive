<?php
defined('_JEXEC') or die;
$document = & JFactory::getDocument();
$document->addStyleSheet('components/com_users/views/profile/css/style.css');
//Get data of user
$db =& JFactory::getDBO();

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();
if($language_tag == 'vi-VN'){
    $language = "'*',".$db->quote($language_tag);
    $catid = 60;  
}else{
    $language = $db->quote($language_tag);
    $catid = 155;         
}

$user_id = JFactory::getUser()->id;
$sql = "Select u.name, u.gender, u.avatar, u.birthday, u.phone, u.email, u.usertype, p.name as address, u.address as city_id from #__users as u left join #__provinces as p on u.address = p.id where u.id = ".$user_id;
$db->setQuery ($sql);
$result = $db->loadObject();

$db->setQuery("Select answer_id,question_id from vc_vichy_qa_user where user_id = $user_id");
$question_id = $db->loadObjectList();

if(!empty($question_id)){
	$language_register = getLanguageWhenRegister($question_id[0]->question_id);
	if(($language_tag == 'vi-VN' && $language_register == 'vi-VN') || ($language_tag == 'en-GB' && $language_register == 'en-GB')){
		$question_id = $question_id;
	}else{
		$question_id = getQuesAnsID($question_id, $language_register);
	}
}

$sql2 = "SELECT id, `text`, parent_id, is_question FROM `vc_vichy_qa` where type = 'diagnostic' and published = 1 and catid = $catid and language in ($language)";
$db->setQuery ($sql2);
$listQA = $db->loadObjectList();
if(count($listQA)){
	$listQA_new = array();
	foreach ($listQA as $k => $v) {
		$pa = $v->parent_id;
		$listQA_new[$pa][] = $v;
	}
}

$sql3 = "SELECT id, name from #__provinces order by name";
$db->setQuery ($sql3);
$listCity = $db->loadObjectList();

function getLanguageWhenRegister($question_id)
{
	$language = 'vi-VN';
	$db =& JFactory::getDBO();
	$sql = "SELECT language FROM `vc_vichy_qa` where id = $question_id";
	$db->setQuery($sql);
	$language = $db->loadResult();
	return $language;
}

function getQuesAnsID($ques_ans, $language_register='vi-VN'){
	$ques = array();
	$ans = array();
	$result = array();
	
	foreach($ques_ans as $value){
		$ques[] = $value->question_id;
		$ans[] = $value->answer_id;		
	}
	
	$str_ques = implode(',', $ques);
	$str_ans = implode(',', $ans);

	$db =& JFactory::getDBO();
	if($language_register == 'vi-VN'){
		$sql = "SELECT q1.id as question_id, q2.id as answer_id FROM `vc_vichy_qa` as q1 inner join `vc_vichy_qa` as q2 on q1.id = q2.parent_id where q2.origin_id in ($str_ans)";
		$db->setQuery($sql);
		$result = $db->loadObjectList();
	}else{
		$sql = "SELECT q1.origin_id as question_id, q2.origin_id as answer_id FROM `vc_vichy_qa` as q1 inner join `vc_vichy_qa` as q2 on q1.id = q2.parent_id where q2.id in ($str_ans)";
		$db->setQuery($sql);
		$result = $db->loadObjectList();	
	}
	
	return $result;
}

if($language_tag == 'vi-VN'){			
	$lbl_avartar = "Chọn ảnh đại diện";
	$lbl_fullname = "Tên của bạn";	
	$lbl_gender = "Giới tính";
	$lbl_male = "Nam";
	$lbl_female = "Nữ";
	$lbl_date_of_birth = "Ngày sinh";
	$lbl_email = "Email";
	$lbl_phone_number = "Số điện thoại";
	$lbl_provice = "Thành phố";
	$lbl_add_info = "<span>Thông tin bổ sung</span> (Cập nhật thông tin chi tiết để tiếp tục bước tư vấn da)";
	$lbl_btn_skin_diagnosis = "KIỂM TRA DA";
	$lbl_cancel = "Hủy";
	$lbl_edit = "Chỉnh sửa";
	$lbl_save = "Lưu";
}else{	
	$lbl_avartar = "Choose your avatar";
	$lbl_fullname = "Full name";	
	$lbl_gender = "Gender *";
	$lbl_male = "Male";
	$lbl_female = "Female";
	$lbl_date_of_birth = "Your date of birth";
	$lbl_email = "Email";
	$lbl_phone_number = "Your phone number";
	$lbl_provice = "City";
	$lbl_add_info = "<span>Additional information</span> (Update information to continue steps skin consulting)";
	$lbl_btn_skin_diagnosis = "SKIN DIAGNOSTIS";
	$lbl_cancel = "Cancel";
	$lbl_edit = "Edit";
	$lbl_save = "Save";
}

?>
<div class="profile">
	<div class="sign-box">
		<div class="sign_box_top">
			<p><?php echo JText::_('COM_USERS_JOIN_MYSKIN'); ?></p>
		</div>
		<div class="title">
			<h3>MY SKIN</h3>
			<!-- <div class="intro-text">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
			</div> -->
		</div>
		<div class="sign-form">
			<div class="form-error">
				<span class="error-text"></span>
			</div>
			<div class="left-form">
				<div class="image_profile">
				<?php $image = ($result->avatar!='1') ? $result->avatar : 'vichy_default.jpg'; ?>
					<img src="<?php echo JURI::root().'timbthumb.php?src=media/com_users/image/'.$image.'&w=120&h=120&q=100&zc=0'; ?>" />
				</div>
				<!-- <a href="javascript:void(0);" class="upload_avatar">Upload avatar</a> -->
				<span class="file-wrapper">
					  <input type="file" name="photo" id="photo" title="Nhấn để chọn ảnh tải lên" />
					  <span class="button" id="upload"><?php echo $lbl_avartar; ?></span>
				</span>
				<div style="color:#FF0000;padding:3px 0;" id="status"></div>
			</div>
			<div class="right-form">
				<table>
					<tr class="row">
						<td class="col1"><?php echo $lbl_fullname; ?></td>
						<td class="col2 name"><?php echo $result->name; ?></td>
						<td class="col3"><a href="javascript:void(0)" data-value="<?php echo $result->name; ?>" data-id="name" class="cancel_profile"><?php echo $lbl_cancel; ?></a></td>
						<td class="col4"><a href="javascript:void(0)" id="name" class="edit_profile"><?php echo $lbl_edit; ?></a><a href="javascript:void(0)" data-id="name" data-value="<?php echo $result->name; ?>" class="save_profile"><?php echo $lbl_save; ?></a></td>
					</tr>
					<tr class="row">
						<td class="col1"><?php echo $lbl_gender; ?></td>
						<td class="col2 gender"><?php echo ($result->gender=='1') ? $lbl_male : $lbl_female; ?></td>
						<td class="col3"><a href="javascript:void(0)" data-id="gender" data-value="<?php echo $result->gender; ?>" class="cancel_profile"><?php echo $lbl_cancel; ?></a></td>
						<td class="col4"><a href="javascript:void(0)" id="gender" data-value="<?php echo $result->gender; ?>" class="edit_profile"><?php echo $lbl_edit; ?></a><a href="javascript:void(0)" data-id="gender" class="save_profile"><?php echo $lbl_save; ?></a></td>
					</tr>
					<tr class="row">
						<td class="col1"><?php echo $lbl_date_of_birth; ?></td>
						<td class="col2 birthday"><?php echo $result->birthday; ?></td>
						<td class="col3"><a href="javascript:void(0)" data-value="<?php echo $result->birthday; ?>" data-id="birthday" class="cancel_profile"><?php echo $lbl_cancel; ?></a></td>
						<td class="col4"><a href="javascript:void(0)" id="birthday" class="edit_profile"><?php echo $lbl_edit; ?></a><a href="javascript:void(0)" data-value="<?php echo $result->birthday; ?>" data-id="birthday" class="save_profile"><?php echo $lbl_save; ?></a></td>
					</tr>
					<tr class="row">
						<td class="col1"><?php echo $lbl_email; ?></td>
						<td class="col2 email"><?php echo $result->email; ?></td>
						<td class="col3"><a href="javascript:void(0)" data-value="<?php echo $result->email; ?>" data-id="email" class="cancel_profile"><?php echo $lbl_cancel; ?></a></td>
						<td class="col4"><a href="javascript:void(0)" id="email" class="edit_profile"><?php echo $lbl_edit; ?></a><a href="javascript:void(0)" data-id="email" data-value="<?php echo $result->email; ?>" class="save_profile"><?php echo $lbl_save; ?></a></td>
					</tr>
					<tr class="row">
						<td class="col1"><?php echo $lbl_phone_number; ?></td>
						<td class="col2 phone"><?php echo $result->phone; ?></td>
						<td class="col3"><a href="javascript:void(0)" data-value="<?php echo $result->phone; ?>" data-id="phone" class="cancel_profile"><?php echo $lbl_cancel; ?></a></td>
						<td class="col4"><a href="javascript:void(0)" id="phone" class="edit_profile"><?php echo $lbl_edit; ?></a><a href="javascript:void(0)" data-id="phone" data-value="<?php echo $result->phone; ?>" class="save_profile"><?php echo $lbl_save; ?></a></td>
					</tr>
					<tr class="row">
						<td class="col1"><?php echo $lbl_provice; ?></td>
						<td class="col2 address"><?php echo $result->address; ?></td>
						<td class="col3"><a href="javascript:void(0)" data-value="<?php echo $result->address; ?>" data-id="address" class="cancel_profile"><?php echo $lbl_cancel; ?></a></td>
						<td class="col4"><a href="javascript:void(0)" id="address" class="edit_profile"><?php echo $lbl_edit; ?></a><a href="javascript:void(0)" data-value="<?php echo $result->address; ?>" data-id="address" class="save_profile"><?php echo $lbl_save; ?></a></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="clear"></div>
		<?php if(count($listQA)){ ?>
		<div class="add_info">
			<div class="add_title"><?php echo $lbl_add_info; ?></div>
			<form action="<?php echo JROUTE::_('index.php?option=com_users&task=profile.add_info'); ?>" method="post" name="check_skin">
				<?php 
					foreach($listQA_new[0] as $k => $v){
						$value = "";
						foreach ($question_id as $qid) {
							if($v->id == $qid->question_id){
								$value = $v->id.'|'.$qid->answer_id;
							}
						}
				?>
				<div class="add_item" <?php echo ($k==count($listQA_new[0])-1) ? 'style="border-bottom:none;"': ''; ?>>
					<div class="add_question"><?php echo strip_tags($v->text); ?></div>
					<input type="hidden" name="info[]" id="info_<?php echo $v->id; ?>" value="<?php echo $value; ?>" />
					<?php 
						foreach($listQA_new[$v->id] as $v1){
							$class = "image_off";
							foreach ($question_id as $qid) {
								if($v1->id == $qid->answer_id){
									$class = "image_on";
								}
							}
					?>
					<div class="add_answer">
						<div class="image_common <?php echo $class; ?>" data-qid="<?php echo $v1->parent_id; ?>" data-id="<?php echo $v1->id; ?>"><?php echo strip_tags($v1->text); ?></div>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
				<input type="hidden" name="user_id" value="<?php echo $user_id ?>" />
				<input type="submit" id="submit_form" name="submit_form" value="<?php echo $lbl_btn_skin_diagnosis; ?>" />
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="base_url" data-user="<?php echo $user_id; ?>" style="display:none;"><?php echo JURI::root(); ?></div>
	<select name="address" class="list_city" style="display:none;">
		<?php 
			foreach ($listCity as $k => $v) {
				$selected = '';
				if($result->city_id == $v->id){
					$selected = 'selected = "selected"';
				}
				echo '<option value="'.$v->id.'" '.$selected.'>'.$v->name.'</option>';
			}
		?>
	</select>
	<div class="select_birthday" id="picker1" style="display:none;"></div>
</div>
<link rel="stylesheet" href="<?php echo JURI::root(); ?>templates/vichy/css/jquery-ui.css">
<script src="<?php echo JURI::root(); ?>templates/vichy/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/bday-picker.js"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_users/views/profile/js/script.js"></script>