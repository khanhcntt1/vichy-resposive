<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

/**
 * Registration model class for Users.
 *
 * @package		Joomla.Site
 * @subpackage	com_users
 * @since		1.6
 */
class UsersModelRegistration extends JModelForm
{
	/**
	 * @var		object	The user registration data.
	 * @since	1.6
	 */
	protected $data;

	/**
	 * Method to activate a user account.
	 *
	 * @param	string		The activation token.
	 * @return	mixed		False on failure, user object on success.
	 * @since	1.6
	 */
	public function activate($token)
	{
		$config	= JFactory::getConfig();
		$userParams	= JComponentHelper::getParams('com_users');
		$db		= $this->getDbo();

		// Get the user id based on the token.
		$db->setQuery(
			'SELECT '.$db->quoteName('id').' FROM '.$db->quoteName('#__users') .
			' WHERE '.$db->quoteName('activation').' = '.$db->Quote($token) .
			' AND '.$db->quoteName('block').' = 1' .
			' AND '.$db->quoteName('lastvisitDate').' = '.$db->Quote($db->getNullDate())
		);
		$userId = (int) $db->loadResult();
		// Check for a valid user id.
		if (!$userId) {
			$this->setError(JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'));
			return false;
		}


		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Activate the user.
		$user = JFactory::getUser($userId);

						$body_pre='
<table class="wrap_email" style="width:800px;
            table-layout:fixed;
			margin:0 auto;
			background:#3d80b0;
            border-collapse:collapse;" cellspacing="0" cellpadding="0">
                <tbody class="wrap_main_email_content">
                    <!--header-->
                <tr>
                    <td class="column_padding_left">
                    </td>
                    <td class="wrap_header_email" style="width:600px;background-repeat: no-repeat;height:62px;background:#eef4fa;">
                                                 <table background="" style="height:62px;table-layout:fixed;
                                                 width:100%;
								border-collapse:collapse;
							" cellpadding="0">
                            <tbody>
                            <tr>
                            <td style="width:20%;padding-left:2%;">
                        				<div class="header_email_home_icon" mc:edit="vc_vichy_confirm_icon_home_header_email_template">
					                 	 <a href="http://www.vichy.com.vn" target="_blank">
					                           <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/bac767e8-6c03-48c3-915a-23e516f700d5.png"></a>
					                           </div>
                        			</td>
                                    
                                                            			<td style="width:65%;">
                        				<div class="header_line_icon">
				                          <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/14557d45-7723-42f8-9d7b-12687f308908.png">
				                        </div>
                        			</td>
                                    
                                    <td style="width:7%;">
                        				<div class="header_email_facebook_icon" mc:edit="vc_vichy_confirm_icon_facebook_header_email_template">
			                          <a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/21014a26-5458-4c36-b30c-806fffe33167.png"></a></div>
                        			</td>
                                    
                                    <td>
                        				                            <div class="header_email_youtube_icon" mc:edit="vc_vichy_confirm_icon_youtube_header_email_template">
                                           <a href="https://www.youtube.com/user/VichyVietnam" target="_blank">       <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/936eacc9-e5ab-4fae-a03a-a4cc1ab34969.png"></a>
                                                  </div>
                        			</td>
                            </tr>
                            </tbody>
                        	</table>
                            </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--main-->
                <!--top-->
                <tr>
                    <td class="column_padding_left">
                    </td>
                     <td class="content-top" style="width:600px;
												margin:0 auto;background:#ffffff;">
                     <div class="wrap_image_top" style="height:180px;"><a href="http://www.vichy.com.vn/vi/san-pham/dong-san-pham/aqualia-thermal" target="_blank">
                       <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/fcb3dbef-c4a7-4f53-82d7-307f49856d14.png" mc:edit="imagebannertoptemplatenhanbantin"></a>
                       </div>
                    </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--center-->
                <tr class="content-center">
                    <td class="column_padding_left">
                    </td>
                     <td>
                     <table style="width:600px;border-collapse:collapse;background:#ffffff;font-family:arial">
                     	<tbody>
                        <tr>
                        <td style="
			width:307px;
			padding-left:4%;
			margin-top:-12px;
			font-size:8pt;
            " mc:edit="content_center_left_email_template_nhan_ban_tin">
                         <p>';
		$body_suf='</p>
   
                        </td>
                       <td>
                        <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/6b4fe5e9-48c5-4e12-a133-64b95345aa54.png" alt="" border="0" style="width: 100%;" mc:edit="image_center_info_email_subscribe_template">
                        </td>
                        </tr>
                        </tbody>
                       </table>
                     	
                     </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--logo-->
                <tr class="content-bottom" style="">
                    <td class="column_padding_left">
                    </td>
                    <td>
                     <table style="width:600px;
								margin:0 auto;
								table-layout:fixed;
								border-collapse:collapse;
                                height:95px;
                                background:#ffffff;
							" cellpadding="0">
                        	<tbody>
                        		<tr>
                        			<td style="width:21%;
														overflow:hidden;">
                        				<div class="bottom_left" style="font-size:10pt;">
                    <a href="http://www.vichy.com.vn/vi/my-skin/dang-ky" target="_blank" style="height:17px"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/22ec1c78-4e9a-481b-951b-a43c1fc8c60a.png" mc:edit="content_image_bottom_left_email_template_nhan_ban_tin"> </a>
                    </div><!--end of image-->
                        			</td>

                        			<td style="width:21%;
                        						padding-left:1%;
														overflow:hidden;">
                        				<div class="bottom_center">
                    <a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/d948158f-e21a-4d4a-b17d-5a4c36313e83.png" mc:edit="content_image_bottom_center_email_template_nhan_ban_tin"> </a>
                    </div><!--end of image-->
                        			</td>

                        			<td style="width:21%;
                        						padding-left:2%;
														overflow:hidden;">
		                             <div class="bottom_right" style="
                    
                    display:inline-block;
			background:#ffffff;
			padding:5px 0px 1px 0px;">
                    <a href="http://www.vichy.com.vn/vi/he-thong-cua-hang" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/f72ce317-4c75-456f-b600-9daff4fd5747.png" mc:edit="content_image_bottom_right_email_template_nhan_ban_tin"> </a>
                    </div>    <!--end of image-->
	
                        			</td>
                        		</tr>
                        	</tbody>
                        </table>
                    </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--footer-->
                <tr>
                     <td class="column_padding_left">
                    </td>
                 <td style="background:#ffffff;width:600px;">
						
						<table style="width:600px;
								margin:0 auto;
								height:90px;
								table-layout:fixed;
								border-collapse:collapse;
							" cellpadding="0">
							<tbody>
<tr>
        <td><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/755409e6-0408-4348-84eb-5653dd1ed34c.png"></td>
</tr>
								<tr>
	
									<td>
				                         <table style="width:600px;
								margin:0 auto;
								height:90px;
								table-layout:fixed;
								border-collapse:collapse;
							" cellpadding="0">
							<tbody>
                              <tr>
									<td style="width:60%;height:90px;"></td>
                                    <td>
                                                        
                                    </td>
                                    <td>
                                        <a class="go_gome" href="http://www.vichy.com.vn" target="_blank">Về Vichy</a>
                                    </td>

                                </tr>
                            </tbody>
                            </table>               
                                    </td>

								</tr>
<tr>
<td><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/755409e6-0408-4348-84eb-5653dd1ed34c.png"></td>
</tr>
	                </tbody>
	            </table>

					</td>
                     
                    <td class="column_padding_right">
                </td></tr>

            </tbody>
            </table>
';

		$mailer =	JFactory::getMailer();
		$mailer->isHTML(true);
		$mailer->Encoding = 'base64';
		// Admin activation is on and user is verifying their email
		if (($userParams->get('useractivation') == 2) && !$user->getParam('activate', 0))
		{
			$uri = JURI::getInstance();

			// Compile the admin notification mail values.
			$data = $user->getProperties();
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$user->set('activation', $data['activation']);
			$data['siteurl']	= JUri::base();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$user->setParam('activate', 1);
			$emailSubject	= JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody_content = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_BODY',
				$data['sitename'],
				$data['name'],
				$data['email'],
				$data['username'],
				$data['activate']
			);

			$emailBody=$body_pre.$emailBody_content.$body_suf;

			// get all admin users
			$query = 'SELECT name, email, sendEmail, id' .
						' FROM #__users' .
						' WHERE sendEmail=1';

			$db->setQuery( $query );
			$rows = $db->loadObjectList();
			

			// Send mail to all users with users creating permissions and receiving system emails
			foreach( $rows as $row )
			{
				$usercreator = JFactory::getUser($id = $row->id);
				if ($usercreator->authorise('core.create', 'com_users'))
				{
					$return = $mailer->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBody);

					// Check for an error.
					if ($return !== true) {
						$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
						return false;
					}
				}
			}
		}

		//Admin activation is on and admin is activating the account
		elseif (($userParams->get('useractivation') == 2) && $user->getParam('activate', 0))
		{
			$user->set('activation', '');
			$user->set('block', '0');

			$uri = JURI::getInstance();

			// Compile the user activated notification mail values.
			$data = $user->getProperties();
			$user->setParam('activate', 0);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$data['siteurl']	= JUri::base();
			$emailSubject	= JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody_content = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_BODY',
				$data['name'],
				$data['siteurl'],
				$data['username']
			);

			$emailBody=$body_pre.$emailBody_content.$body_suf;

			$return = $mailer->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

			// Check for an error.
			if ($return !== true) {
				$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
				return false;
			}
		}
		else
		{
			$user->set('activation', '');
			$user->set('block', '0');

			$uri = JURI::getInstance();
			
			// Compile the user activated notification mail values.
			$data = $user->getProperties();
			// echo '<pre>';
			// print_r($data);
			// echo '</pre>';
			// die();
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$data['siteurl']	= JURI::root();
			$data['fburl'] = 'www.facebook.com/vichy.com.vn';

			//send email welcome
			$emailSubject	= JText::sprintf(
				'COM_USERS_EMAIL_WELCOME_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody_content = JText::sprintf(
				'COM_USERS_EMAIL_WELCOME_BODY',
				$data['siteurl'],
				$data['fburl']
			);

			$emailBody=$body_pre.$emailBody_content.$body_suf;

			$return = $mailer->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

			// Check for an error.
			if ($return !== true) {
				$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
				return false;
			}

			//send email Reminding of fullfiling the information
			//count number of question that user has not answered yet
			$db->setQuery("Select question_id from vc_vichy_qa_user where user_id = $data[id]");
			$question_id = $db->loadObjectList();
			if(count($question_id)){
				$where = "";
				foreach ($question_id as $v) {
					$where .= ','.$v->question_id;
				}
				$where = substr($where, 1);
				$where = " and id not in ($where) and parent_id not in ($where)";
				$sql2 = "SELECT id FROM `vc_vichy_qa` where type = 'diagnostic' and published = 1 and catid = 60 and is_question = 1 $where";
				// die($sql2);
				$db->setQuery ($sql2);
				$listQA = $db->loadObjectList();
				if(count($listQA)){
					//send email Reminding of fullfiling the information
					$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
					$data['profile_url'] = $base.JROUTE::_('index.php?option=com_users&view=profile',false);
					$data['diagnostic_url'] = $base.JRoute::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107', false);
					$emailSubject	= JText::sprintf(
						'COM_USERS_EMAIL_REMINDING_SUBJECT'
					);

					$emailBody_content = JText::sprintf(
						'COM_USERS_EMAIL_REMINDING_BODY',
						$data['profile_url'],
						$data['diagnostic_url']
					);

					$emailBody=$body_pre.$emailBody_content.$body_suf;

					$return = $mailer->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

					// Check for an error.
					if ($return !== true) {
						$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
						return false;
					}
				}
			}else{
				//send email Reminding of fullfiling the information
				$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
				$data['profile_url'] = $base.JROUTE::_('index.php?option=com_users&view=profile',false);
				$data['diagnostic_url'] = $base.JRoute::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107', false);
				$emailSubject	= JText::sprintf(
					'COM_USERS_EMAIL_REMINDING_SUBJECT'
				);

				$emailBody_content = JText::sprintf(
					'COM_USERS_EMAIL_REMINDING_BODY',
					$data['profile_url'],
					$data['diagnostic_url']
				);

				$emailBody=$body_pre.$emailBody_content.$body_suf;

				$return = $mailer->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

				// Check for an error.
				if ($return !== true) {
					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
					return false;
				}
			}
		}

		// Store the user object.
		if (!$user->save()) {
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_ACTIVATION_SAVE_FAILED', $user->getError()));
			return false;
		}

		return $user;
	}

	/**
	 * Method to get the registration form data.
	 *
	 * The base form data is loaded and then an event is fired
	 * for users plugins to extend the data.
	 *
	 * @return	mixed		Data object on success, false on failure.
	 * @since	1.6
	 */
	public function getData()
	{
		if ($this->data === null) {

			$this->data	= new stdClass();
			$app	= JFactory::getApplication();
			$params	= JComponentHelper::getParams('com_users');

			// Override the base user data with any data in the session.
			$temp = (array)$app->getUserState('com_users.registration.data', array());
			foreach ($temp as $k => $v) {
				$this->data->$k = $v;
			}

			// Get the groups the user should be added to after registration.
			$this->data->groups = array();

			// Get the default new user group, Registered if not specified.
			$system	= $params->get('new_usertype', 2);

			$this->data->groups[] = $system;

			// Unset the passwords.
			unset($this->data->password1);
			unset($this->data->password2);

			// Get the dispatcher and load the users plugins.
			$dispatcher	= JDispatcher::getInstance();
			JPluginHelper::importPlugin('user');

			// Trigger the data preparation event.
			$results = $dispatcher->trigger('onContentPrepareData', array('com_users.registration', $this->data));

			// Check for errors encountered while preparing the data.
			if (count($results) && in_array(false, $results, true)) {
				$this->setError($dispatcher->getError());
				$this->data = false;
			}
		}

		return $this->data;
	}

	/**
	 * Method to get the registration form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 * for users plugins to extend the form with extra fields.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_users.registration', 'registration', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		return $this->getData();
	}

	/**
	 * Override preprocessForm to load the user plugin group instead of content.
	 *
	 * @param	object	A form object.
	 * @param	mixed	The data expected for the form.
	 * @throws	Exception if there is an error in the form event.
	 * @since	1.6
	 */
	protected function preprocessForm(JForm $form, $data, $group = 'user')
	{
		$userParams	= JComponentHelper::getParams('com_users');

		//Add the choice for site language at registration time
		if ($userParams->get('site_language') == 1 && $userParams->get('frontend_userparams') == 1)
		{
			$form->loadFile('sitelang', false);
		}

		parent::preprocessForm($form, $data, $group);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		// Get the application object.
		$app	= JFactory::getApplication();
		$params	= $app->getParams('com_users');

		// Load the parameters.
		$this->setState('params', $params);
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array		The form data.
	 * @return	mixed		The user id on success, false on failure.
	 * @since	1.6
	 */
	public function register($temp)
	{
		$config = JFactory::getConfig();
		$db		= $this->getDbo();
		$params = JComponentHelper::getParams('com_users');

		// Initialise the table with JUser.
		$user = new JUser;
		$data = (array)$this->getData();

		// Merge in the registration data.
		foreach ($temp as $k => $v) {
			$data[$k] = $v;
		}

		// Prepare the data for the user object.
		$data['email']		= $data['email1'];
		$data['password']	= $data['password1'];
		$useractivation = $params->get('useractivation');
		$sendpassword = $params->get('sendpassword', 1);

		// Check if the user needs to activate their account.
		if (($useractivation == 1) || ($useractivation == 2)) {
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$data['block'] = 1;
		}

		// Bind the data.
		if (!$user->bind($data)) {
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));
			return false;
		}

		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Store the data.
		if (!$user->save()) {
			$this->setError($user->getError());
			return false;
		}

		// Compile the notification mail values.
		$data = $user->getProperties();
		$data['fromname']	= $config->get('fromname');
		$data['mailfrom']	= $config->get('mailfrom');
		$data['sitename']	= $config->get('sitename');
		$data['siteurl']	= JUri::root();

										$body_pre='
<table class="wrap_email" style="width:800px;
            table-layout:fixed;
			margin:0 auto;
			background:#3d80b0;
            border-collapse:collapse;" cellspacing="0" cellpadding="0">
                <tbody class="wrap_main_email_content">
                    <!--header-->
                <tr>
                    <td class="column_padding_left">
                    </td>
                    <td class="wrap_header_email" style="width:600px;background-repeat: no-repeat;height:62px;background:#eef4fa;">
                                                 <table background="" style="height:62px;table-layout:fixed;
                                                 width:100%;
								border-collapse:collapse;
							" cellpadding="0">
                            <tbody>
                            <tr>
                            <td style="width:20%;padding-left:2%;">
                        				<div class="header_email_home_icon" mc:edit="vc_vichy_confirm_icon_home_header_email_template">
					                 	 <a href="http://www.vichy.com.vn" target="_blank">
					                           <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/bac767e8-6c03-48c3-915a-23e516f700d5.png"></a>
					                           </div>
                        			</td>
                                    
                                                            			<td style="width:65%;">
                        				<div class="header_line_icon">
				                          <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/14557d45-7723-42f8-9d7b-12687f308908.png">
				                        </div>
                        			</td>
                                    
                                    <td style="width:7%;">
                        				<div class="header_email_facebook_icon" mc:edit="vc_vichy_confirm_icon_facebook_header_email_template">
			                          <a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/21014a26-5458-4c36-b30c-806fffe33167.png"></a></div>
                        			</td>
                                    
                                    <td>
                        				                            <div class="header_email_youtube_icon" mc:edit="vc_vichy_confirm_icon_youtube_header_email_template">
                                           <a href="https://www.youtube.com/user/VichyVietnam" target="_blank">       <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/936eacc9-e5ab-4fae-a03a-a4cc1ab34969.png"></a>
                                                  </div>
                        			</td>
                            </tr>
                            </tbody>
                        	</table>
                            </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--main-->
                <!--top-->
                <tr>
                    <td class="column_padding_left">
                    </td>
                     <td class="content-top" style="width:600px;
												margin:0 auto;background:#ffffff;">
                     <div class="wrap_image_top" style="height:180px;"><a href="http://www.vichy.com.vn/vi/san-pham/dong-san-pham/aqualia-thermal" target="_blank">
                       <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/fcb3dbef-c4a7-4f53-82d7-307f49856d14.png" mc:edit="imagebannertoptemplatenhanbantin"></a>
                       </div>
                    </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--center-->
                <tr class="content-center">
                    <td class="column_padding_left">
                    </td>
                     <td>
                     <table style="width:600px;border-collapse:collapse;background:#ffffff;font-family:arial">
                     	<tbody>
                        <tr>
                        <td style="
			width:307px;
			padding-left:4%;
			margin-top:-12px;
			font-size:8pt;
            " mc:edit="content_center_left_email_template_nhan_ban_tin">
                         <p>';
		$body_suf='</p>
   
                        </td>
                       <td>
                        <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/6b4fe5e9-48c5-4e12-a133-64b95345aa54.png" alt="" border="0" style="width: 100%;" mc:edit="image_center_info_email_subscribe_template">
                        </td>
                        </tr>
                        </tbody>
                       </table>
                     	
                     </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--logo-->
                <tr class="content-bottom" style="">
                    <td class="column_padding_left">
                    </td>
                    <td>
                     <table style="width:600px;
								margin:0 auto;
								table-layout:fixed;
								border-collapse:collapse;
                                height:95px;
                                background:#ffffff;
							" cellpadding="0">
                        	<tbody>
                        		<tr>
                        			<td style="width:21%;
														overflow:hidden;">
                        				<div class="bottom_left" style="font-size:10pt;">
                    <a href="http://www.vichy.com.vn/vi/my-skin/dang-ky" target="_blank" style="height:17px"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/22ec1c78-4e9a-481b-951b-a43c1fc8c60a.png" mc:edit="content_image_bottom_left_email_template_nhan_ban_tin"> </a>
                    </div><!--end of image-->
                        			</td>

                        			<td style="width:21%;
                        						padding-left:1%;
														overflow:hidden;">
                        				<div class="bottom_center">
                    <a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/d948158f-e21a-4d4a-b17d-5a4c36313e83.png" mc:edit="content_image_bottom_center_email_template_nhan_ban_tin"> </a>
                    </div><!--end of image-->
                        			</td>

                        			<td style="width:21%;
                        						padding-left:2%;
														overflow:hidden;">
		                             <div class="bottom_right" style="
                    
                    display:inline-block;
			background:#ffffff;
			padding:5px 0px 1px 0px;">
                    <a href="http://www.vichy.com.vn/vi/he-thong-cua-hang" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/f72ce317-4c75-456f-b600-9daff4fd5747.png" mc:edit="content_image_bottom_right_email_template_nhan_ban_tin"> </a>
                    </div>    <!--end of image-->
	
                        			</td>
                        		</tr>
                        	</tbody>
                        </table>
                    </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--footer-->
                <tr>
                     <td class="column_padding_left">
                    </td>
                 <td style="background:#ffffff;width:600px;">
						
						<table style="width:600px;
								margin:0 auto;
								height:90px;
								table-layout:fixed;
								border-collapse:collapse;
							" cellpadding="0">
							<tbody>
<tr>
        <td><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/755409e6-0408-4348-84eb-5653dd1ed34c.png"></td>
</tr>
								<tr>
	
									<td>
				                         <table style="width:600px;
								margin:0 auto;
								height:90px;
								table-layout:fixed;
								border-collapse:collapse;
							" cellpadding="0">
							<tbody>
                              <tr>
									<td style="width:60%;height:90px;"></td>
                                    <td>
                                                        
                                    </td>
                                    <td>
                                        <a class="go_gome" href="http://www.vichy.com.vn" target="_blank">Về Vichy</a>
                                    </td>

                                </tr>
                            </tbody>
                            </table>               
                                    </td>

								</tr>
<tr>
<td><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/755409e6-0408-4348-84eb-5653dd1ed34c.png"></td>
</tr>
	                </tbody>
	            </table>

					</td>
                     
                    <td class="column_padding_right">
                </td></tr>

            </tbody>
            </table>
';
		// Handle account activation/confirmation emails.
		if ($useractivation == 2)
		{
			// Set the link to confirm the user email.
			$uri = JURI::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);

			$emailSubject	= JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody_content = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
				$emailBody=$body_pre.$emailBody_content.$body_suf;
			}
			else
			{
				$emailBody_content = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username']
				);
				$emailBody=$body_pre.$emailBody_content.$body_suf;
			}
		}
		elseif ($useractivation == 1)
		{
			// Set the link to activate the user account.
			$uri = JURI::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);

			$emailSubject	= JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody_content = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
				$emailBody=$body_pre.$emailBody_content.$body_suf;
			}
			else
			{
				$emailBody_content = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username']
				);
				$emailBody=$body_pre.$emailBody_content.$body_suf;
			}
		}
		else
		{

			$emailSubject	= JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBody_content = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_BODY',
				$data['name'],
				$data['sitename'],
				$data['siteurl']
			);
			$emailBody=$body_pre.$emailBody_content.$body_suf;
		}

		// Send the registration email.
		$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

		//Send Notification mail to administrators
		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1)) {
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBodyAdmin = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
				$data['name'],
				$data['username'],
				$data['siteurl']
			);

			// get all admin users
			$query = 'SELECT name, email, sendEmail' .
					' FROM #__users' .
					' WHERE sendEmail=1';

			$db->setQuery( $query );
			$rows = $db->loadObjectList();

			// Send mail to all superadministrators id
			foreach( $rows as $row )
			{
				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);

				// Check for an error.
				if ($return !== true) {
					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
					return false;
				}
			}
		}
		// Check for an error.
		if ($return !== true) {
			$this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));

			// Send a system message to administrators receiving system mails
			$db = JFactory::getDBO();
			$q = "SELECT id
				FROM #__users
				WHERE block = 0
				AND sendEmail = 1";
			$db->setQuery($q);
			$sendEmail = $db->loadColumn();
			if (count($sendEmail) > 0) {
				$jdate = new JDate();
				// Build the query to add the messages
				$q = "INSERT INTO ".$db->quoteName('#__messages')." (".$db->quoteName('user_id_from').
				", ".$db->quoteName('user_id_to').", ".$db->quoteName('date_time').
				", ".$db->quoteName('subject').", ".$db->quoteName('message').") VALUES ";
				$messages = array();

				foreach ($sendEmail as $userid) {
					$messages[] = "(".$userid.", ".$userid.", '".$jdate->toSql()."', '".JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT')."', '".JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username'])."')";
				}
				$q .= implode(',', $messages);
				$db->setQuery($q);
				$db->query();
			}
			return false;
		}

		if ($useractivation == 1)
			return "useractivate";
		elseif ($useractivation == 2)
			return "adminactivate";
		else
			return $user->id;
	}
}
