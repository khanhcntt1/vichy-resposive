<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	Application
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

//Global definitions.
//Joomla framework path definitions.
$parts = explode(DIRECTORY_SEPARATOR, JPATH_BASE);
array_pop($parts);

//Defines.
define('JPATH_ROOT',			implode(DIRECTORY_SEPARATOR, $parts));
define('JPATH_SITE',			JPATH_ROOT);
define('JPATH_CONFIGURATION',	JPATH_ROOT);
define('JPATH_ADMINISTRATOR',	JPATH_ROOT . '/administrator');
define('JPATH_LIBRARIES',		JPATH_ROOT . '/libraries');
define('JPATH_PLUGINS',			JPATH_ROOT . '/plugins');
define('JPATH_INSTALLATION',	JPATH_ROOT . '/installation');
define('JPATH_THEMES',			JPATH_BASE . '/templates');
define('JPATH_CACHE',			JPATH_ROOT . '/cache');
define('JPATH_MANIFESTS',		JPATH_ADMINISTRATOR . '/manifests');
define('STORE','18');
define('SPA','19');
define('RANGE','8');
define('RANGE_NO_STEP','37');
define('HOME_POSITION_SLIDESHOW','1');
define('HOME_POSITION_MAIN_ROW_TOP_LEFT','2');
define('HOME_POSITION_MAIN_ROW_TOP_RIGHT','3');
define('HOME_POSITION_MAIN_ROW_BOTTOM_LEFT','4');
define('HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1','5');
define('HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2','6');

define('HOME_POSITION_SLIDESHOW_NAME','Slide show');
define('HOME_POSITION_MAIN_ROW_TOP_LEFT_NAME','Vị trí trái hàng đầu tiên');
define('HOME_POSITION_MAIN_ROW_TOP_RIGHT_NAME','Vị trí phải hàng đầu tiên');
define('HOME_POSITION_MAIN_ROW_BOTTOM_LEFT_NAME','Vị trí trái hàng cuối cùng');
define('HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1_NAME','Vị trí phải 1 hàng cuối cùng');
define('HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2_NAME','Vị trí phải 2 hàng cuối cùng');

// define('MAILCHIMP_KEY','8bbfa5e13a3f69135ef096e426e4197a-us8');
define('MAILCHIMP_KEY','42cfd9e26b3036c914ebfbcac84f6acd-us8');
define('ELEMENT_MC','0');