<?php
/**
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * The Categories List Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_categories
 * @since		1.6
 */
class CategoriesControllerCategories extends JControllerAdmin
{
	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	function getModel($name = 'Category', $prefix = 'CategoriesModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Rebuild the nested set tree.
	 *
	 * @return	bool	False on failure or error, true on success.
	 * @since	1.6
	 */
	public function rebuild()
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$extension = JRequest::getCmd('extension');
		$this->setRedirect(JRoute::_('index.php?option=com_categories&view=categories&extension='.$extension, false));

		// Initialise variables.
		$model = $this->getModel();

		if ($model->rebuild()) {
			// Rebuild succeeded.
			$this->setMessage(JText::_('COM_CATEGORIES_REBUILD_SUCCESS'));
			return true;
		} else {
			// Rebuild failed.
			$this->setMessage(JText::_('COM_CATEGORIES_REBUILD_FAILURE'));
			return false;
		}
	}

	/**
	 * Save the manual order inputs from the categories list page.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function saveorder()
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Get the arrays from the Request
		$order	= JRequest::getVar('order',	null, 'post', 'array');
		$originalOrder = explode(',', JRequest::getString('original_order_values'));

		// Make sure something has changed
		if (!($order === $originalOrder)) {
			parent::saveorder();
		} else {
			// Nothing to reorder
			$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&view='.$this->view_list, false));
			return true;
		}
	}
	/** Deletes and returns correctly.
 	 *
 	 * @return	void
 	 * @since	2.5.12
 	 */
 	public function delete()
 	{
 		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
 		
 		// Get items to remove from the request.
 		$cid = JRequest::getVar('cid', array(), '', 'array');
 		$extension = JRequest::getVar('extension', null);
 
 		if (!is_array($cid) || count($cid) < 1)
 		{
 			JError::raiseWarning(500, JText::_($this->text_prefix . '_NO_ITEM_SELECTED'));
 		}
 		else
 		{
 			// Get the model.
 			$model = $this->getModel();
 
 			// Make sure the item ids are integers
 			jimport('joomla.utilities.arrayhelper');
 			JArrayHelper::toInteger($cid);
 			// Remove the items.
 			if ($model->delete($cid))
 			{
 				$this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));
 				require_once JPATH_ROOT.'/MailChimp.php';
				$mailchimp = new Drewm\MailChimp(MAILCHIMP_KEY);
				$lists = $mailchimp->call('lists/list') ;
				$listid = $lists['data'][0]['id'];

				$groups = $mailchimp->call('lists/interest-groupings',array(
					'id' => $listid,
					'counts'=> 'false'
				)) ;
				$groups_id = $groups[0]['id'];
				$db = & JFactory::getDbo();
 				foreach ($cid as $v) {
 					$db->setQuery("SELECT catid, subcribe_group from vc_vichy_subcribe_group where catid = $v");
					$res = $db->loadObject();
					if(count($res)){
						$db->setQuery("DELETE FROM vc_vichy_subcribe_group where catid = $v");
						$db->query();
						$mailchimp->call('lists/interest-group-del',array(
							'id'         => $listid,
							'group_name' => $res->subcribe_group,
							'grouping_id'=> "$groups_id"
						)) ;
					}
 				}
 			}
 			else
 			{
 				$this->setMessage($model->getError());
 			}
 		}
 
 		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&extension=' . $extension, false));
 	}

 	function my_trash(){
 		$cid = $_GET['cid'];
 		$extension = $_GET['extension'];
 		$db = & JFactory::getDbo();
 		$db->setQuery('SELECT count(id) as count from #__categories where islock = 1 and id = '.$cid);
 		$res = $db->loadObject()->count;
 		if($res){
 			$this->setMessage('This category must not be deleted');
 			$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&extension=' . $extension, false));
 		}else{
 			$db->setQuery('UPDATE #__categories set published = -2 where id = '.$cid);
 			$db->query();
 			$db->setQuery('UPDATE #__vichy_subcribe_group set status = -2 where catid = '.$cid);
 			$db->query();
 			$this->setMessage('ITEM trashed');
 			$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&extension=' . $extension, false));
 		}
 	}
}