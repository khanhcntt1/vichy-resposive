<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');
    $doc->addStyleSheet('templates/bluestork/js/jtable/themes/lightcolor/gray/jtable.css');

    JToolBarHelper::save();
    $db =& JFactory::getDBO();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));
    $ciddef = $cid[0];

    if ($ciddef > 0){	
    	
    	$query = "SELECT * FROM #__vichy_product  WHERE id = ".$ciddef;
    	$db->setQuery($query);
    	$group = $db->loadObject();
        
    	JFilterOutput::objectHTMLSafe($group, ENT_QUOTES, '' );

        $query = "SELECT pc.id, pc.category_id, c.title FROM #__vichy_product_category as pc join #__categories as c on c.id = pc.category_id WHERE product_id = ".$ciddef;
        $db->setQuery($query);
        $category = $db->loadObjectList();
        $tmp = '';
        foreach ($category as $k => $v) {
            $tmp[] = $v->category_id;
        }
        if(!empty($tmp)){
            $str = implode('|',$tmp);
        }
    	
    	JToolBarHelper::title(JText::_( 'Edit Product  : '.$group->name ), 'generic.png');
    	JToolBarHelper::cancel( 'cancel', 'Close' ); 	
    }
    else{
    	JToolBarHelper::title(JText::_( 'Add Product' ), 'generic.png');
    	JToolBarHelper::cancel();
	
    }
    $listCategories = Vichy_productHelper::getCategories();
    $listLanguage = Vichy_productHelper::listLanguage();
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
	}
</script>
<fieldset>
    <legend>Product details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table>
            <tr>
                <td><strong>Name :</strong></td>
                <td>
                    <input type="text" value="<?php if(!empty($group->name))  echo($group->name);else echo ''; ?>"  name="name" size="100"/>
                </td>
            </tr>

            <tr>
                <td><strong>Link :</strong></td>
                <td>
                    <input type="text" value="<?php if(!empty($group->link))  echo($group->link);else echo ''; ?>"  name="link" size="100"/>
                </td>
            </tr>

            <tr>
                <td><strong>Language :</strong></td>
                <td>
                    <?php $selected = (!empty($group->language)) ? $group->language : 'vi-VN'; ?>
                    <select name="language">
                    <option value="*">All</option>
                    <?php
                        echo JHtml::_('select.options', $listLanguage, 'lang_code', 'title_native', $selected); 
                    ?>
                    </select>
                </td>
            </tr>
                        
            <tr>
                <td><strong>Photo :</strong></td>
                <td><input type="file" name="file" id="file"/> (Max size of file is 300Kb, width:168px, height:305px)</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if(!empty($group->image)){ ?>
                        <img width="300px" height="176px" src="<?php echo JURI::root();?>components/com_vichy_product/uploads/products/<?php echo $group->image;?>"/>
                    <?php } ?>
                    <input type="hidden" value="<?php if(!empty($group->image)) echo($group->image); else echo '';?>" name="file-image"/>
                </td>
            </tr>
            <tr>
                <td><strong>Description :</strong></td>
                <td>
                <?php 			
			         echo @$this->editor->display( 'description',  $group->description, '550;', '300', '75', '20', array() ) ; 
                 ?> 
                </td>
            </tr>

        </table>
        <table >
            <tr>
                <td><strong>Efficacy :</strong></td>
                <td>
                <?php 			
			         echo @$this->editor->display( 'effectiveness',  $group->effectiveness, '550;', '300', '75', '20', array() ) ; 
                 ?> 
                </td>
            </tr>
            <tr>
                <td><strong>Tolerance :</strong></td>
                <td>
                <?php 			
			         echo @$this->editor->display( 'tolerance',  $group->tolerance, '550;', '300', '75', '20', array() ) ; 
                 ?> 
                </td>
            </tr>
            <tr>
                <td><strong>Pleasure :</strong></td>
                <td>
                <?php 			
			         echo @$this->editor->display( 'pleasure',  $group->pleasure, '550;', '300', '75', '20', array() ) ; 
                 ?> 
                </td>
            </tr>
            <tr>
                <td><strong>Cách sử dụng :</strong></td>
                <td>
                <?php           
                     echo @$this->editor->display( 'video',  $group->video, '550;', '300', '75', '20', array() ) ; 
                 ?>
                </td>
            </tr>
            <tr>
                <td><strong>Categories :</strong></td>
                <td>  
                    <?php if(count($category)){ ?>
                    <a href="javascript:void(0)" id="category_add">Add</a>
                    <div id="wrap_category">
                        <?php foreach ($category as $k => $v) { ?>
                        <div class="category_item">
                            <span class="cate_name" id="name_<?php echo $v->id; ?>"><?php echo $v->title; ?></span>
                            <a href="javascript:void(0)" class="category_edit" data-cate="<?php echo $v->category_id; ?>" data-id="<?php echo $v->id; ?>">Edit</a>
                            <a href="javascript:void(0)" class="category_del" data-cate="<?php echo $v->category_id; ?>" data-id="<?php echo $v->id; ?>">Delete</a>
                        </div>
                        <?php } ?>
                    </div>
                    <?php }else{ ?>
                    <select name="category[]" class="inputbox" size="10" multiple>
                        <?php Vichy_productHelper::recursive($listCategories,'1',''); ?>
                    </select>    
                    <?php } ?>
                </td>
            </tr>
        </table>
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_product" />
        <input type="hidden" name="controller" value="products" />
        <input type="hidden" name="task" value=""/>
    </form>
</fieldset>
<?php if(count($category)){ ?>
<div id="dialog-form" style="width: 200px;display:none;">
    <div style="overflow:hidden;">
        <select id="category" name="category[]" class="inputbox" size="10">
          <?php Vichy_productHelper::recursive($listCategories,'1',''); ?>
        </select>
    </div>
</div>

<script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="templates/bluestork/js/jtable/jquery.jtable.js"></script>
<script type="text/javascript">
    var str = "<?php echo $str; ?>";
    var arr = str.split('|');
    var is_add = 1;
    var record_id;
    var old_category;
    jQuery(document).ready(function() {
        jQuery( "#dialog-form" ).dialog({
          autoOpen: false,
          height: 300,
          width: 240,
          modal: true,
          buttons: {
            "Select": function() {                
                cate_id = jQuery('#category').val();
                if(arr.indexOf(cate_id) > -1){
                    jQuery( this ).dialog( "close" );
                    alert("This category was chosen");
                }else{
                    cate_text = jQuery('#category option:selected').text();
                    cate_text = cate_text.replace("--", "");
                    if(is_add == 2){
                        html_code = '<div class="category_item">';
                        html_code += '<span class="cate_name">'+cate_text+'</span>';
                        html_code += '<input type="hidden" name="add_cate[]" value="'+cate_id+'" />';
                        html_code += '</div>';
                        jQuery('#wrap_category').append(html_code);
                        arr.push(cate_id);
                    }else{
                        inx = arr.indexOf(old_category);
                        if( inx > -1){
                            arr.splice( inx, 1 );
                        }
                        jQuery('#name_'+record_id).text(cate_text);
                        jQuery('#name_'+record_id).siblings('.input_hidden').remove();
                        jQuery('#name_'+record_id).parent().append('<input class="input_hidden" type="hidden" name="edit_id[]" value="'+record_id+'|'+cate_id+'" />');
                        jQuery('#name_'+record_id).siblings('.category_edit').attr('data-cate',cate_id);
                        arr.push(cate_id);
                    }
                    jQuery( this ).dialog( "close" );
                }
            },
            Cancel: function() {
               is_add = 1;
               jQuery( this ).dialog( "close" );
            }
          },
          close: function() {
                is_add = 1;
          }
        });
    });

    jQuery('#category_add').click(function() {
        is_add = 2;
        jQuery( "#dialog-form" ).dialog( "open" );
    });
    jQuery('#wrap_category').on('click','.category_edit',function() {
        record_id = jQuery(this).attr('data-id');
        old_category = jQuery(this).attr('data-cate');
        jQuery( "#dialog-form" ).dialog( "open" );
    });
    jQuery('#wrap_category').on('click', '.category_del', function() {
        id = jQuery(this).attr('data-id');
        if(window.confirm("Do you want to delete this category?")){
            jQuery.ajax({
                url: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=products&task=deleteCategory',
                type: 'POST',
                data: 'id='+id,
                success: function(kq){
                    if(kq == 1){
                        location.reload();
                    }
                }
            });
        }
    });
</script>

<?php } ?>