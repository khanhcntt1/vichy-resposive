<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');
    $doc->addStyleSheet('templates/bluestork/js/jtable/themes/lightcolor/gray/jtable.css');
    $doc->addStyleSheet('components/com_vichy_product/css/style.css');

    JToolBarHelper::save();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $db = & JFactory::getDbo();
    $db->setQuery("SELECT title from #__categories where id = ".$cid[0]);
    $db->query();
    $res = $db->loadObject();
    $product_step = Vichy_productHelper::getStepByCategory($cid[0]);
    JToolBarHelper::title(JText::_( 'Edit Step'), 'generic.png');
    JToolBarHelper::cancel( 'cancel', 'Close' );
    $arr_prod = array();
?>
<fieldset>
    <legend>Product details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
		<div><?php echo $res->title; ?></div>
        <a href="javascript:void(0)" id="add">Add</a>
        <div id="wrap_steps_categories">
        	<?php foreach($product_step as $k => $v){ ?>
            <?php $arr_prod[] = $v->product_id; ?>
        	<div class="wrap_step">
	            <div class="button_step">
		            <a href="javascript:void(0)" data-step="<?php echo $v->no_of_steps; ?>" data-id="<?php echo $v->id; ?>" class="open_popup">Step <?php echo $v->no_of_steps; ?></a>
	            </div>
	            <div class="step_product" id="SelectedRowList_<?php echo $v->no_of_steps; ?>">
	            	<div class="step_product_item">
	                    <div class="item-name">
	                    <?php echo $v->name; ?>
	                    </div>
	                    <div class="item-image">
	                    	<img style="width:150px;" src="<?php echo JURI::root().'components/com_vichy_product/uploads/products/'.$v->image ?>" />
	                    </div>
	                    <input type="hidden" name="step_product_item[]" value="<?php echo $v->no_of_steps.'|'.$v->product_id ?>" />
                    </div>
	            </div>
	            <a href="javascript:void(0)" class="delete" data-id="<?php echo $v->id ?>">Delete</a>
            </div>
        	<?php } ?>
        </div>
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="" />
        <input type="hidden" name="cid[]" value="<?php echo $cid[0]; ?>" />
        <input type="hidden" name="option" value="com_vichy_product" />
        <input type="hidden" name="controller" value="steps" />
        <input type="hidden" name="task" value=""/>
    </form>
</fieldset>
<div id="dialog-form" style="width: 600px;display:none;">
<div class="filtering">
    <form>
        Product name: <input type="text" name="name" id="name" />
        <button type="submit" id="LoadRecordsButton">Load records</button>
    </form>
</div>
<div id="ProductsTableContainer" style="width: 600px;"></div>
</div>
<script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="templates/bluestork/js/jtable/jquery.jtable.js"></script>
<script type="text/javascript">
    str = '<?php echo implode("|",$arr_prod); ?>';
    var json = str.split('|');
    var count = json.length;
    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'cancel') {
            submitform( pressbutton );
            return;
        }
    }

    Joomla.submitbutton = function(task){
        if (task == 'save'){
            ret = 1;
            for(i=0;i<count;i++){
                for (var j = i+1; j < count; j++) {
                    if(json[i] == json[j]){
                        ret = 0;
                        break;
                    }
                }
                if (ret == 0){
                    break;
                }
            }
            if(ret == 0){
                alert('Some steps have the same products');
            }else{
                Joomla.submitform(task);
            }
        } else Joomla.submitform(task);
        return false;
    };

    jQuery(document).ready(function() {

    	var current_step;
    	var edit_id;
        var add;
    	//Prepare jTable
        jQuery('#ProductsTableContainer').jtable({
            title: 'Table of product',
            paging: true,
            pageSize: 5,
            sorting: true,
            defaultSorting: 'name ASC',
            selecting: true, //Enable selecting
            multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
            selectOnRowClick: false, //Enable this to only select using checkboxes
            actions: {
                listAction: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=products&task=get_product_ajax'
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                name: {
                    title: 'Product Name',
                    width: '40%'
                },
                image: {
                    title: 'Image',
                    width: '40%'
                }
            }
        });

        //Load person list from server
        jQuery('#ProductsTableContainer').jtable('load');

        //Re-load records when user click 'load records' button.
        jQuery('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            jQuery('#ProductsTableContainer').jtable('load', {
                name: jQuery('#name').val()
            });
        });

        //add attribute step to class jtable-data-row


        jQuery( "#dialog-form" ).dialog({
          autoOpen: false,
          height: 600,
          width: 650,
          modal: true,
          buttons: {
            "Select": function() {                
                var $selectedRows = jQuery('#ProductsTableContainer').jtable('selectedRows');
                if ($selectedRows.length > 0) {
                    //Show selected rows
                    jQuery('#SelectedRowList_'+current_step).empty();
                    $selectedRows.each(function () {
                        var record = jQuery(this).data('record');
                        json[current_step-1] = record.id;
                        var step_product_item = '<div class="step_product_item">';
                        step_product_item += '<div class="item-name">';
                        step_product_item += record.name;
                        step_product_item += '</div>';
                        step_product_item += '<div class="item-image">';
                        step_product_item += record.image;
                        step_product_item += '</div>';
                        step_product_item += '<input type="hidden" name="step_product_item[]" value="'+current_step+'|'+record.id+'" />';
                        if(edit_id != '' && edit_id != null){
                            step_product_item += '<input type="hidden" name="edit_id[]" value="'+edit_id+'|'+current_step+'|'+record.id+'" />';
                        }
                        if(add != '' && add != null){
                            step_product_item += '<input type="hidden" name="add_id[]" value="'+current_step+'|'+record.id+'" />';
                            count = json.length;
                        }
                        step_product_item += '</div>';
                        jQuery('#SelectedRowList_'+current_step).append(step_product_item);
                    });
                }
                jQuery( this ).dialog( "close" );
            },
            Cancel: function() {
               jQuery('.jtable-column-header-container input[type=checkbox]').attr('checked', false);
               jQuery('.jtable-selecting-column input[type=checkbox]').attr('checked', false);
               jQuery('.jtable-row-selected').removeClass('jtable-row-selected');
               if(add != '' && add != null){
                    tmp = jQuery('#SelectedRowList_'+current_step).html();
                    if(tmp == '' || tmp == null){
                        jQuery('#SelectedRowList_'+current_step).parent().remove();
                    }
               }
               add = '';
               jQuery( this ).dialog( "close" );
            }
          },
          close: function() {
                if(add != '' && add != null){
                    tmp = jQuery('#SelectedRowList_'+current_step).html();
                    if(tmp == '' || tmp == null){
                        jQuery('#SelectedRowList_'+current_step).parent().remove();
                    }
                }
                add = '';
                jQuery('.jtable-column-header-container input[type=checkbox]').attr('checked', false);
                jQuery('.jtable-selecting-column input[type=checkbox]').attr('checked', false);
                jQuery('.jtable-row-selected').removeClass('jtable-row-selected');
          }
        });

		jQuery('#wrap_steps_categories').on('click','.open_popup',function(){
            current_step = jQuery(this).attr('data-step');
            edit_id = jQuery(this).attr('data-id');
            add = jQuery(this).attr('data-add');
            jQuery( "#dialog-form" ).dialog( "open" );
        });

        jQuery('#wrap_steps_categories').on('click','.delete',function(){
            id = jQuery(this).attr('data-id');
            if(window.confirm("Do you want to delete this step?")){
	            jQuery.ajax({
	            	url: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=steps&task=deleteStep',
	            	type: 'POST',
	            	data: 'id='+id+'&cid=<?php echo $cid[0] ?>',
	            	success: function(kq){
	            		if(kq == 1){
	            			location.reload();
	            		}
	            	}
	            });
        	}
            
        });

        jQuery('#add').click(function() {

            current_step = count + 1;
            add = current_step;
            html_code = '';
            html_code += '<div class="wrap_step">';
            html_code += '<div class="button_step">';
            html_code += '<a href="javascript:void(0)" data-step="'+current_step+'" data-add="'+add+'" class="open_popup">Step '+current_step;
            html_code += '</a>';
            html_code += '</div>';
            html_code += '<div class="step_product" id="SelectedRowList_'+current_step+'">';
            html_code += '</div>';
            html_code += '</div>';
            jQuery('#wrap_steps_categories').append(html_code);
            jQuery( "#dialog-form" ).dialog( "open" );
        });
    });
</script>