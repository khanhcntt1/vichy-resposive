<?php 
	defined('_JEXEC') or die;
	$doc = JFactory::getDocument();
    $doc->addStyleSheet('components/com_vichy_product/css/steps.css');

	JToolBarHelper::title(JText::_( 'Steps' ), 'generic.png');
	JToolBarHelper::addNewX();
?>
<script type="text/javascript" src="templates/bluestork/js/jcarousellite_1.0.1.min.js"></script>
<script type="text/javascript">
	function confirm_del(){
		if(!window.confirm("Do you want to delete?")){
			return false;
		}else{
			return true;
		}
	}
</script>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
	<table class="adminform">
	    <tr>
	      <td width="100%">
	        <?php echo JText::_( 'Tên danh mục' ); ?>
	        <input type="text" name="search" id="search" value="<?php echo $_POST['search'];?>"  onChange="document.adminForm.submit();" />
	        <button onclick="this.form.submit();"><?php echo JText::_( 'Tìm kiếm' ); ?></button>
	      </td>
	    </tr>
	</table>
	<table class="adminlist" cellspacing="1">
		<tbody>
			<?php 
	        $key = 0;
	        for ($i=0, $n=count($this->items); $i < $n; $i++)
	        {
	            $row    =& $this->items[$i];
	            $product_step = Vichy_productHelper::getStepByCategory($row->category_id);
	            $link = 'index.php?option=com_vichy_product&controller=steps&task=edit&cid[]='. $row->category_id;
	            $link_del = 'index.php?option=com_vichy_product&controller=steps&task=delete&cid[]='. $row->category_id;
	            $new_prod_step = array();
	            foreach ($product_step as $v) {
	            	$new_prod_step[$v->no_of_steps][] = $v;
	            }
	        ?>
            <tr class="<?php echo "row$key"; ?>"> 
               <td>
               		<div><a href="<?php echo $link_del; ?>" onclick="return confirm_del();">Xóa</a> <span style="padding:5px;">|</span>
            		<a href="<?php echo $link; ?>"><?php echo mb_strtoupper($row->title); ?></a></div>
            		<div class="wrap_all_step">
            			<ul class="tabs_<?php echo $i; ?>">
	            			<?php for($j=1;$j<=count($new_prod_step);$j++){ ?>
	            			<li>
	            				<a href="#step_<?php echo $i.'_'.$j; ?>" <?php echo ($j==1) ? 'class="active"' : ''; ?>>
	            					Step <?php echo $j; ?>
	            				</a>
	            			</li>
	            			<?php } ?>
            			</ul>
            			<div class="tab-container">
            				<?php for($j=1;$j<=count($new_prod_step);$j++){ ?>
            				<div id="step_<?php echo $i.'_'.$j; ?>" class="tab-content_<?php echo $i; ?>">
            					<?php if($new_prod_step[$j][0]->product_id == '0'){
            						echo "Không có sản phẩm";
            					}else{ ?>
            					<div class="wrap_carousel">
				            		<button class="prev_<?php echo $i.'_'.$j; ?>"><<</button>
									<button class="next_<?php echo $i.'_'.$j; ?>">>></button>
									        
									<div class="carousel_<?php echo $i.'_'.$j; ?>">
									    <ul>
									    	<?php
									    	$c = (count($new_prod_step[$j]) <= 8) ? count($new_prod_step[$j]) : 8;

									        foreach ($new_prod_step[$j] as $k => $v) {
									        	echo '<li>';
									        	echo '<div>';
									        	echo '<img style="width:150px;height:272px;" src="'.JURI::root().'components/com_vichy_product/uploads/products/'.$v->image.'" />';
									        	echo '<div style="width:150px;height:30px;overflow:hidden;">'.$v->name.'</div>';
									        	echo '</div>';
									        	echo '</li>';
									        }
									        ?>
									    </ul>
									</div>
								</div>
								<?php } ?>
	            			</div>
	            			<?php if($new_prod_step[$j][0]->product_id != '0'){ ?>
	            			<script type="text/javascript">
						        jQuery(document).ready(function() {
						        	jQuery(".carousel_<?php echo $i.'_'.$j; ?>").jCarouselLite({
								        btnNext: ".next_<?php echo $i.'_'.$j; ?>",
								        btnPrev: ".prev_<?php echo $i.'_'.$j; ?>",
								        visible: <?php echo $c; ?>
								    });
						        });
					        </script>
					        <?php } ?>
	            			<?php } //end loop for ?>
            			</div>
            			<script type="text/javascript">
					        jQuery(document).ready(function() {
							    jQuery('.tab-content_<?php echo $i; ?>:not(:first)').hide();
								jQuery('.tabs_<?php echo $i; ?> li a').click(function(){
									jQuery('.tabs_<?php echo $i; ?> li a').removeClass('active');
									jQuery(this).addClass('active');
									jQuery('.tab-content_<?php echo $i; ?>').hide();

									var activeTab = jQuery(this).attr('href');
									jQuery(activeTab).fadeIn();
									return false;
								});
					        });
				        </script>
            		</div>
               </td>
            </tr>
	        <?php $key = 1 - $key;}?>
		</tbody>
		<tfoot>
			<tr>
                <td colspan="5"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
		</tfoot>
	</table>

    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_product" />                        
    <input type="hidden" name="controller" value="steps" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>