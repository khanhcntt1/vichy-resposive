<?php 
	defined('_JEXEC') or die;
	$doc = JFactory::getDocument();
    // $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');

	JToolBarHelper::title(JText::_( 'Steps' ), 'generic.png');
	JToolBarHelper::addNewX();
?>
<script type="text/javascript" src="templates/bluestork/js/jcarousellite_1.0.1.min.js"></script>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
	<table class="adminform">
	    <tr>
	      <td width="100%">
	        <?php echo JText::_( 'Tên danh mục' ); ?>
	        <input type="text" name="search" id="search" value="<?php echo $_POST['search'];?>"  onChange="document.adminForm.submit();" />
	        <button onclick="this.form.submit();"><?php echo JText::_( 'Tìm kiếm' ); ?></button>
	      </td>
	    </tr>
	</table>
	<table class="adminlist" cellspacing="1">
		<tbody>
			<?php 
	        $k = 0;
	        for ($i=0, $n=count($this->items); $i < $n; $i++)
	        {
	            $row    =& $this->items[$i];
	            $product_step = Vichy_productHelper::getStepByCategory($row->category_id);
	            $link = 'index.php?option=com_vichy_product&controller=steps&task=edit&cid[]='. $row->category_id;
	        ?>
            <tr class="<?php echo "row$k"; ?>"> 
               <td>
            		<a href="<?php echo $link; ?>"><?php echo strtoupper($row->title); ?></a>
            		<div class="wrap_carousel">
	            		<button class="prev_<?php echo $i; ?>"><<</button>
						<button class="next_<?php echo $i; ?>">>></button>
						        
						<div class="carousel_<?php echo $i; ?>">
						    <ul>
						    	<?php
						    	$c = (count($product_step) <= 8) ? count($product_step) : 8;

						        foreach ($product_step as $k => $v) {
						        	echo '<li>';
						        	echo '<div>';
						        	echo '<div>Step '.$v->no_of_steps.'</div>';
						        	echo '<img style="width:150px;display:block;" src="'.JURI::root().'components/com_vichy_product/uploads/products/'.$v->image.'" />';
						        	echo '<div>'.$v->name.'</div>';
						        	echo '</div>';
						        	echo '</li>';
						        }
						        ?>
						    </ul>
						</div>
					</div>
               </td>
            </tr>
	        <script type="text/javascript">
		        jQuery(document).ready(function() {
		        	jQuery(".carousel_<?php echo $i; ?>").jCarouselLite({
				        btnNext: ".next_<?php echo $i; ?>",
				        btnPrev: ".prev_<?php echo $i; ?>",
				        visible: <?php echo $c; ?>
				    });
		        });
	        </script> 
	        <?php $k = 1 - $k;}?>
		</tbody>
		<tfoot>
			<tr>
                <td colspan="5"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
		</tfoot>
	</table>

    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_product" />                        
    <input type="hidden" name="controller" value="steps" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>
