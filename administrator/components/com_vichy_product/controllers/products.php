<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_productControllerProducts extends JController{
   
	function __construct(){
		parent::__construct();
		$this->registerTask('add', 'edit');
	}

	function display(){
		JRequest::setVar('view', 'Products');
    	parent::display();
	}
	function edit(){
    	JRequest::setVar('view', 'Products');
    	JRequest::setVar('layout', 'form');
    	JRequest::setVar('hidemainmenu', 1); 
    	parent::display();
    }
    function translate(){
        JRequest::setVar('view', 'Products');
        JRequest::setVar('layout', 'translate');
        JRequest::setVar('hidemainmenu', 1); 
        parent::display();
    }

    function cancel(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect('index.php?option=com_vichy_product' );
	}
	function save(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
	    $model = $this->getModel('Products');
	    $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $id = $cid[0];
        $data = JRequest::get( 'post' );
		$size = $_FILES['file']['size'];
        $max_size = 307200;
        if (empty($id)){
            if(empty($_POST['name'])){
                $this->setMessage(JText::_( 'Tên sản phẩm không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_product&controller=products&layout=form');
                return false;
            }

            if(empty($_FILES['file']['name'])){
                
                $this->setMessage(JText::_( 'Vui lòng chọn hình ảnh đại diện cho sản phẩm'),'error');
                $this->setRedirect('index.php?option=com_vichy_product&controller=products&layout=form');
                return false;
            }

            if ((($_FILES["file"]["type"] != "image/gif")
					&& ($_FILES["file"]["type"] != "image/jpeg")
					&& ($_FILES["file"]["type"] != "image/jpg")
					&& ($_FILES["file"]["type"] != "image/pjpeg")
					&& ($_FILES["file"]["type"] != "image/x-png")
					&& ($_FILES["file"]["type"] != "image/png"))
			)
			{
				$this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
                $this->setRedirect('index.php?option=com_vichy_product&controller=products&layout=form');
                return false;
			}
            
            if($size > $max_size){
                
                $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 300Kb !'),'error');
                $this->setRedirect('index.php?option=com_vichy_product&controller=products&layout=form');
                return false;     
            }
            
        }else{
            
            if(empty($_POST['name'])){
                $this->setMessage(JText::_( 'Tên sản phẩm không được để trống !'),'error');
                $this->setRedirect('index.php?option=com_vichy_product&controller=products&layout=form&task=edit&cid[]='.$id);
                return false;
            }
            
            if(!empty($_FILES['file']['name'])){
                if ((($_FILES["file"]["type"] != "image/gif")
					&& ($_FILES["file"]["type"] != "image/jpeg")
					&& ($_FILES["file"]["type"] != "image/jpg")
					&& ($_FILES["file"]["type"] != "image/pjpeg")
					&& ($_FILES["file"]["type"] != "image/x-png")
					&& ($_FILES["file"]["type"] != "image/png"))
				)
				{
					$this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
	                $this->setRedirect('index.php?option=com_vichy_product&controller=products&layout=form&task=edit&cid[]='.$id);
	                return false;
				}
                
                if($size > $max_size){
                    
                    $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 300Kb !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_product&controller=products&layout=form&task=edit&cid[]='.$id);
                    return false;     
                }
            }
        }
        $db = & JFactory::getDbo();

        if(isset($data['category'])){
            foreach ($data['category'] as $k => $v) {
                $sql = "Insert into #__vichy_product_category values (null, $v, $id, null, null)";
                $db->setQuery($sql);
                $db->query();
            }
        }

        if(isset($data['edit_id'])){
            foreach ($data['edit_id'] as $k => $v) {
                $tmp = explode('|', $v);
                $sql = "Update #__vichy_product_category set category_id = ".$tmp[1].", modify_date='".date('Y-m-d h:i:s')."' where id = ".$tmp[0];
                $db->setQuery($sql);
                $db->query();
            }
        }

        if(isset($data['add_cate'])){
            foreach ($data['add_cate'] as $k => $v) {
                $sql = "Insert into #__vichy_product_category values (null, $v, $id, null, null)";
                $db->setQuery($sql);
                $db->query();
            }
        }

        if ($model->store()) {
            $msg = JText::_( 'Lưu thành công!' );
    	} else {
            $msg = JText::_( 'Lưu thất bại !' );
        }

    	$link = 'index.php?option=com_vichy_product&controller=products';
    	$this->setRedirect($link, $msg);
	}

    function publish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
        } else {
    
            $model = $this->getModel('Products');
    
            if(!$model->publish($cid, 1)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) PUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_product');
            $cache->clean();
        }
    
        $this->setRedirect( 'index.php?option=com_vichy_product&controller=products', $msg );
    }
        
    function unpublish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
        } else {
    
            $model = $this->getModel('Products');
    
            if(!$model->publish($cid, 0)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) UNPUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_product');
            $cache->clean();
        }
            
        $this->setRedirect('index.php?option=com_vichy_product&controller=products', $msg);
    }
    function remove(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Products');
        $flag = $model->deleteRows();
        // if($flag == '1') {
        //     $msg = JText::_( 'Lỗi : Một hoặc nhiều sản phẩm không thể xoá');
        // } else {
        //     if($flag!='2'){
        //         $msg = JText::_('').$flag;
        //     }else{
        //         $msg = JText::_( 'Sản phẩm đã được xoá !' );
        //     }
        // }
        if($flag) {
            $msg = JText::_( 'Sản phẩm đã được xoá !' );
        } else {
            $msg = JText::_( 'Lỗi : Một hoặc nhiều sản phẩm không thể xoá');
        }
        
        $this->setRedirect('index.php?option=com_vichy_product&controller=products', $msg );
    }
    function featured(){
        $db = & JFactory::getDBO();
        $home_page = $_GET['home_page'];
        $id = $_GET['id'];
        $query ="update #__vichy_product set is_favorite=$home_page where id=".$id;
        $db->setQuery($query);
        $db->query();
        $this->setRedirect('index.php?option=com_vichy_product&view=products');
    }
    function product_new(){
        $db = & JFactory::getDBO();
        $home_page = $_GET['is_new'];
        $id = $_GET['id'];
        $query ="update #__vichy_product set is_new=$home_page where id=".$id;
        $db->setQuery($query);
        $db->query();
        $this->setRedirect('index.php?option=com_vichy_product&view=products');
    }
    function get_product_ajax(){
        $db = & JFactory::getDbo();
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $where = '';
        if($name != ''){
            $where = " and p.name like '%".$name."%'";
        }
        $range = isset($_POST['cid']) ? $_POST['cid']: '';
        if(!empty($range)){
            $sql = "SELECT p.id, p.name, p.image FROM vc_vichy_product_category as pc right join vc_vichy_product as p on pc.product_id = p.id where p.published = '1' and pc.category_id = $range $where ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"];
            $sql_count = "SELECT count(p.id) as count FROM vc_vichy_product_category as pc right join vc_vichy_product as p on pc.product_id = p.id where p.published = '1' and pc.category_id = $range $where";
        }else{
            $sql = "SELECT p.id, p.name, p.image FROM vc_vichy_product as p where p.published = '1' $where ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"];
            $sql_count = "select count(id) as count from vc_vichy_product where published = '1' $where";
        }
        $db->setQuery($sql);
        $db->query();
        $list = $db->loadObjectList();
        foreach ($list as $k => $v) {
            $list[$k]->image  = '<img style="width:150px;" src="'.JURI::root().'components/com_vichy_product/uploads/products/'.$v->image.'" />';
        }
        $array = json_decode(json_encode($list), true);
        $db->setQuery($sql_count);
        $db->query();
        $result_count = $db->loadObject();

        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $result_count->count;
        $jTableResult['Records'] = $array;
        print json_encode($jTableResult);
        exit();
    }

    function deleteCategory(){
        $id = $_POST['id'];
        $db = & JFactory::getDbo();
        $db->setQuery("DELETE from #__vichy_product_category where id = $id");
        $db->query();
        echo '1';
        exit();
    }
}