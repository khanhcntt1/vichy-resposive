<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_productModelProducts extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){
			$keyword = $_POST['search'];
			$where_cate = '';
			$where_state = '';
			$join = '';

			if(isset($_POST['filter_category']) && $_POST['filter_category'] != '0'){
				// $where_cate = ' left join vc_vichy_product_category as c on sl.id = c.product_id where c.category_id = '.$_POST['filter_category'];
				$join = 'left join vc_vichy_product_category as c on p.id = c.product_id';
				$where_cate = 'and c.category_id = '.$_POST['filter_category'];
			}
			if(isset($_POST['filter_state']) && $_POST['filter_state'] != 'default'){
				$where_state = 'and p.published = '.$_POST['filter_state'];
			}

			/* $query = "SELECT sl.* from (
				SELECT p.id, p.name, p.image, p.published, p.is_favorite, count(r.product_id) as no_rating, ROUND(AVG(r.rating)) as rating FROM vc_vichy_product as p left join vc_vichy_product_rating as r on p.id = r.product_id WHERE p.name LIKE '%$keyword%' $where_state group by r.product_id order by p.id DESC
				) as sl $where_cate";
				*/
			$query = "SELECT p.*,p2.name as translate_name from vc_vichy_product as p left join vc_vichy_product as p2 on p.id=p2.origin_id $join where p.name LIKE '%$keyword%' $where_state $where_cate";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {	
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );
	    	$data['name'] = JRequest::getVar( 'name', '', 'post', 'string', JREQUEST_ALLOWRAW );
            $data['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );
            $data['effectiveness'] = JRequest::getVar( 'effectiveness', '', 'post', 'string', JREQUEST_ALLOWRAW );
            $data['tolerance'] = JRequest::getVar( 'tolerance', '', 'post', 'string', JREQUEST_ALLOWRAW );
            $data['pleasure'] = JRequest::getVar( 'pleasure', '', 'post', 'string', JREQUEST_ALLOWRAW );
            $data['video'] = JRequest::getVar( 'video', '', 'post', 'string', JREQUEST_ALLOWRAW );
			$data['link'] = JRequest::getVar( 'link', '', 'post', 'string', JREQUEST_ALLOWRAW );
            
            if(empty($data['cid'][0])){

                $type = $_FILES['file']['type'];
                $extension = strtolower(substr($type, strpos($type,'/') + 1));
                $tmp_name = $_FILES['file']['tmp_name'];
                $new_name = md5(time()).'.'.$extension;
                $location = '../components/com_vichy_product/uploads/products/'.$new_name;
                if(move_uploaded_file($tmp_name, $location)){
                    $data['image'] =  $new_name;

                    if (!$row->bind($data)) {
                        $this->setError($this->_db->getErrorMsg());
                        return false;
                    }
                            	 
                    if (!$row->check()) {
                        $this->setError($this->_db->getErrorMsg());
                        return false;
                    }
                            	 
                    if (!$row->store()) {
                        $this->setError($this->_db->getErrorMsg());
                        return false;
                    }
    	 
                }else{
                	$this->setError("Lỗi không up được file");
                	return false;
                }

            }else{
            	if(!empty($_FILES['file']['name'])){
            		$type = $_FILES['file']['type'];
	                $extension = strtolower(substr($type, strpos($type,'/') + 1));
	                $tmp_name = $_FILES['file']['tmp_name'];
	                $new_name = md5(time()).'.'.$extension;
	                $location = '../components/com_vichy_product/uploads/products/'.$new_name;
	                if(move_uploaded_file($tmp_name, $location)){
	                    $data['image'] =  $new_name;
	                }else{
	                	$this->setError("Lỗi không up được file");
	                	return false;
	                }
            	}else{
            		$data['image'] = $data['file-image'];
            	}
            	if(!empty($data['file-image']) && $data['image']!=$data['file-image']){
            		$localtion_gallery = '../components/com_vichy_product/uploads/products/'.$data['file-image'];
                	unlink($localtion_gallery);
            	}
            	$data['modify_date'] = date('Y-m-d h:i:s');
            	
            	
            	if (!$row->bind($data)) {
                    $this->setError($this->_db->getErrorMsg());
                    return false;
                }
                        	 
                if (!$row->check()) {
                    $this->setError($this->_db->getErrorMsg());
                    return false;
                }
                        	 
                if (!$row->store()) {
                    $this->setError($this->_db->getErrorMsg());
                    return false;
                }
            }
	    	return true;
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));
		    $arr = array();	
	    	foreach ($cids as $cid) {
    			$flag = '';
    			try{
    				$db->transactionStart();
    				//Xoa product rating
    				$db->setQuery("DELETE FROM #__vichy_product_rating where product_id = ".$cid);
                	$db->query();

                	//Xoa product vichy_step
    				$db->setQuery("DELETE FROM #__vichy_step where product_id = ".$cid);
                	$db->query();

                	//Xoa product vichy_product_category
    				$db->setQuery("DELETE FROM #__vichy_product_category where product_id = ".$cid);
                	$db->query();

                	//Xoa product vichy_comment_product
    				$db->setQuery("DELETE FROM #__vichy_comment_product where product_id = ".$cid);
                	$db->query();

                	//xoa san pham trong bang product
	            	$query = "SELECT image FROM #__vichy_product where id=".$cid;
	                $db->setQuery($query);
	                $result = $db->loadObject();

	                $localtion = '../components/com_vichy_product/uploads/products/'.$result->image;

	                unlink($localtion);
	                $db->setQuery("DELETE FROM #__vichy_product where id = ".$cid);
                	$db->query();

    				$db->transactionCommit();
    				$flag = true;
    			}catch (Exception $e){
    				// catch any database errors.
				    $db->transactionRollback();
				    $flag = false; 
    			}
    			if($flag == false){
    				return false;
    			}
	    	}
	     	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_product'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>