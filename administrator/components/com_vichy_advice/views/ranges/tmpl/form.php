<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    // $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');

    JToolBarHelper::save();

    if(isset($_GET['id']) && !empty($_GET['id'])){
        $db = & JFactory::getDbo();
        $db->setQuery("SELECT a.*,c_y.title as yourneed from vc_vichy_advice_range a join vc_categories c_y on a.yourneed_id = c_y.id where yourneed_id = ".$_GET['id']);
        $group = $db->loadObjectList();
    }
    
    JToolBarHelper::title(JText::_( 'Add Range For Your Need' ), 'generic.png');
    JToolBarHelper::cancel();
    $listCategories = Vichy_adviceHelper::getYourNeed();
    $listRanges=Vichy_adviceHelper::getProductRanges();
    $new_cate = array();
    foreach ($listCategories as $v) {
        $pa = $v->parent_id;
        $new_cate[$pa][] = $v;
    }
?>
 <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'cancel') {
            submitform( pressbutton );
            return;
        }
    }
</script>
<fieldset>
    <legend></legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table>
            <tr>
                <td><strong>Thể loại :</strong></td>
                <td>
                    <?php if(empty($group[0]->yourneed_id)){ ?>
                    <select name="catid">
                        <?php
                        foreach ($new_cate[10] as $k => $v) {
                            $select = ($group->catid == $v->id) ? 'selected = "selected"' : '';
                            if($v->id == '11'){
                                $value = '';
                                $select = '';
                            }else{
                                $value = $v->id;
                            }
                            echo '<option value="'.$value.'" '.$select.'>'.$v->child_title.'</option>';
                            if(isset($new_cate[$v->id])){
                                foreach ($new_cate[$v->id] as $k1 => $v1) {
                                    $select_c = ($group->catid == $v1->id) ? 'selected = "selected"' : '';
                                    echo '<option value="'.$v1->id.'" '.$select_c.'>--'.$v1->child_title.'</option>';
                                }
                            }
                        } 
                        ?>
                    </select>
                    <?php }else{ echo $group[0]->yourneed; ?>
                        <input type="hidden" name="catid" value="<?php echo $group[0]->yourneed_id; ?>" />
                    <?php } ?>
                </td>
            </tr>
            <tr>
                    <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($listRanges); ?>);" /></th>
                    <th align="center">Dòng sản phấm</th>
             </tr>
                 <tbody>
                <?php 
                $k = 0;
                for ($i=0, $n=count($listRanges); $i < $n; $i++)
                {
                    $row    =& $listRanges[$i];
                    $checked = '';
                    foreach ($group as $v) {
                        if($row->id == $v->range_id){
                            $checked = 'checked = "checked"';
                        }
                    }
                ?>
                    <tr class="<?php echo "row$k"; ?>">
                        <td width="30"><input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>" <?php echo $checked; ?> /></td>                
                        <td align="center"><?php echo $row->title; ?></td>
                    </tr>
                        
                <?php $k = 1 - $k;}?>
                </tbody>
        </table>
        
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="option" value="com_vichy_advice" />
        <input type="hidden" name="controller" value="ranges" />
        <input type="hidden" name="task" value="save"/>
        <input type="hidden" name="id" value="<?php echo (!empty($group[0]->yourneed_id)) ? $group[0]->yourneed_id : ''; ?>" />
    </form>
</fieldset>