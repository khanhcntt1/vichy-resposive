<?php 
    defined('_JEXEC') or die;

    JToolBarHelper::title(JText::_( 'Diagnostics' ), 'generic.png');
    JToolBarHelper::addNewX();
    JToolBarHelper::publish();
    JToolBarHelper::unpublish();
    JToolBarHelper::deleteList();
  
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">

    <table class="adminlist" cellspacing="1">   
        <tr>
            <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->listDiagnostics); ?>);" /></th>
            <th width="30" align="center">Id</th>
            <th align="center">Nội dung</th>
            <th align="center">Loại</th>
            <th align="center">Xuất bản</th>
        </tr>
        <tbody>
        <?php 
        $k = 0;
        for ($i=0, $n=count($this->listDiagnostics); $i < $n; $i++)
        {
            $row    =& $this->listDiagnostics[$i];
            $link   = 'index.php?option=com_vichy_advice&controller=diagnostics&task=edit&cid[]='. $row->id;
            $published  = JHTML::_('grid.published', $row, $i );
        ?>
            <tr class="<?php echo "row$k"; ?>">
                <td width="30"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>
                <td align="center"><?php echo $row->id; ?></td>  
                <td align="center"><a href="<?php echo $link; ?>"><?php echo $row->text; ?></a></td>
                <td align="center"><?php echo ($row->is_question == 1) ? 'Câu hỏi' : 'Câu trả lời'; ?></td>
                <td align="center"><?php echo $published; ?></td>
            </tr>
                
        <?php $k = 1 - $k;}?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_advice" />                        
    <input type="hidden" name="controller" value="diagnostics" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>