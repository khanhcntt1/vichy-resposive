<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_adviceModelDiagnostics extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){

			$query = "SELECT * FROM #__vichy_qa where type = 'diagnostic'";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );
	    	$data['type'] = "diagnostic";
            $data['text'] = JRequest::getVar( 'text', '', 'post', 'string', JREQUEST_ALLOWRAW );
            if(!isset($data['parent_id'])){
            	$data['parent_id'] = 0;
            }
            $db = & JFactory::getDbo();
            if(empty($data['id'])){
            	$query = "Insert into #__vichy_qa values (null, '$data[text]', $data[parent_id], $data[is_question],'$data[type]',$data[catid],'$data[language]',1)";
            }else{
            	$query = "Update #__vichy_qa set `text` = '$data[text]', parent_id = $data[parent_id], is_question = $data[is_question], catid = $data[catid], language = '$data[language]' where id = $data[id]";
            }
            $db->setQuery($query);
            $ret =  $db->query();
            if($ret == 1){
            	return true;
            }else{
            	return false;
            }
	    	
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));
	    	foreach ($cids as $cid) {

                // if (!$row->delete( $cid )) {
  	            	// $this->setError($row->getErrorMsg());
  	            	// return false;
                // }
               
	    	}
	     	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_qa'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .') or parent_id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>