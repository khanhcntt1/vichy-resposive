<?php 
defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_adviceModelRanges extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}

		function _buildQuery(){

			$query = "SELECT a.*,c_y.title as yourneed,c_r.title as 'range' FROM #__vichy_advice_range a
						Join vc_categories c_y on a.yourneed_id = c_y.id
						Join vc_categories c_r on a.range_id = c_r.id
						";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {
	    	$db = & JFactory::getDBO();
	    	$data = JRequest::get( 'post' );
	    	// echo '<pre>';
	    	// print_r($data);
	    	// echo '</pre>';
	    	// die();
	    	// $id = $data['id'];
	    	$insert_value="";
	    	// if(empty($id)){
	    		$flag = '';
    			try{
    				$db->transactionStart();
    				
    				$db->setQuery("DELETE FROM #__vichy_advice_range where yourneed_id = ".$data['catid']);
                	$db->query();

                	foreach ($data['cid'] as $v) {
		    		$insert_value[] ="($data[catid],$v)";
			    	}
			    	$insert = implode(',', $insert_value);
			    	$query = "Insert into vc_vichy_advice_range (yourneed_id,range_id) values $insert";
			    	$db->setQuery($query);
			    	$db->query();

    				$db->transactionCommit();
    				$flag = true;
    			}catch (Exception $e){
    				// catch any database errors.
				    $db->transactionRollback();
				    $flag = false; 
    			}
    			if($flag == false){
    				return false;
    			}
		    	
			// }else{
			// 	echo 'edit';
			// }
	    	return true;
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));	
		    $row =& $this->getTable();		
	    	foreach ($cids as $cid) {

                if (!$row->delete( $cid )) {
  	            	$this->setError($row->getErrorMsg());
  	            	return false;
                }
               
	    	}
	     	return true;
		}
	}
?>