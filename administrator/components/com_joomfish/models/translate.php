<?php

/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003 - 2013, Think Network GmbH, Konstanz
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * @package joomfish
 * @subpackage Models
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

JLoader::register('JFModel', JOOMFISH_ADMINPATH . DS . 'models' . DS . 'JFModel.php');

/**
 * This is the corresponding module for translation management
 * @package		Joom!Fish
 * @subpackage	Translate
 */
class TranslateModelTranslate extends JFModel
{

	protected $_modelName = 'translate';

	/**
	*	return translation id from reference id
	**/
	public function get_translation_id($lang_code, $reference_id, $reference_table){
		$db  = & JFactory::getDBO();
		$translation_id = null;
		$sql = "select translation_id from #__jf_translationmap where language='".$lang_code."' and reference_id=".$reference_id." and reference_table='".$reference_table."'";
		$db->setQuery($sql);
		$translation_id = $db->loadResult();
		return $translation_id;
	}
	/**
	* update parent_id for categories
	**/
	public function update_parent_id_cate($parent_id=1, $cate_id){
		$db = & JFactory::getDBO();
		$sql = "update #__categories set parent_id = $parent_id where id = $cate_id";
		$db->setQuery($sql);
		if($db->query()){
			return true;
		}
		return false;
	}
	/**
	* save translate product
	**/
	public function save_translate_product($data=null){
		$db = JFactory::getDBO();
		if(!empty($data)){
			$modify_date = date('Y-m-d H:i:s');
			$lang_code = $this->getLangCodeByID($data['language_id']);
			if(!empty($data['translation_id'])){				
				if(!empty($_FILES['file']['name'])){
            		$type = $_FILES['file']['type'];
	                $extension = strtolower(substr($type, strpos($type,'/') + 1));
	                $tmp_name = $_FILES['file']['tmp_name'];
	                $new_name = md5(time()).'.'.$extension;
	                $location = '../components/com_vichy_product/uploads/products/'.$new_name;
	                if(move_uploaded_file($tmp_name, $location)){
	                    $data['image'] =  $new_name;
	                }else{
	                	$this->setError("Lỗi không up được file");
	                	return false;
	                }
            	}else{
            		$data['image'] = $data['file-image'];
            	}
            	if(!empty($data['file-image']) && $data['image']!=$data['file-image']){
            		$localtion_gallery = '../components/com_vichy_product/uploads/products/'.$data['file-image'];
                	unlink($localtion_gallery);
            	}
            	
            	$sql = "update #__vichy_product set name='".mysql_escape_string($data['refField_name'])."', description='".mysql_escape_string($data['refField_description'])."', image='".$data['image']."', effectiveness='".mysql_escape_string($data['refField_effectiveness'])."', tolerance='".mysql_escape_string($data['refField_tolerance'])."', pleasure='".mysql_escape_string($data['refField_pleasure'])."', video='".mysql_escape_string($data['refField_video'])."', buy_online_link='".$data['refField_buy_online_link']."', is_favorite='".$data['is_favorite']."', is_new='".$data['is_new']."', published='".$data['published']."', create_date='".$data['refField_create_date']."', modify_date='".$modify_date."', language='".$lang_code."' where id = ".$data['translation_id'];
				
				$db->setQuery($sql);
				if($db->query()){
					
					return true;
				}				
				return false;

			}else{	

				$type = $_FILES['file']['type'];
                $extension = strtolower(substr($type, strpos($type,'/') + 1));
                $tmp_name = $_FILES['file']['tmp_name'];
                $new_name = md5(time()).'.'.$extension;
                $location = '../components/com_vichy_product/uploads/products/'.$new_name;
                if(move_uploaded_file($tmp_name, $location)){
                    $data['image'] =  $new_name;

                    $sql = "insert into #__vichy_product (origin_id,name,description,image,effectiveness,tolerance,pleasure,video,buy_online_link,is_favorite,is_new,published,create_date,modify_date,language)
	              				values ('".$data['reference_id']. "','". mysql_escape_string($data['refField_name']) ."','". mysql_escape_string($data['refField_description']) ."','".$data['image']."', '".mysql_escape_string($data['refField_effectiveness'])."','".mysql_escape_string($data['refField_tolerance'])."','". mysql_escape_string($data['refField_pleasure']) ."','". mysql_escape_string($data['refField_video']) ."','".$data['refField_buy_online_link']."','".$data['is_favorite']."','".$data['is_new']."','". $data['published'] ."','". $data['refField_create_date']."','". $modify_date ."','". $lang_code ."')";
					$db->setQuery($sql);
			       	if($db->query()){
			       		$translation_id = $this->get_max_id_product();
			       		
			       		$list_cate_translate = $this->get_list_cate_translate($lang_code,$data['reference_id']);
			       		if(!empty($list_cate_translate)){	       			
			       			$values = array();
			       			foreach($list_cate_translate as $v){
			       				$values[] = "($v->category_id_translate, $translation_id, '$v->create_date', '$modify_date')";
			       			}
			       			if(!empty($values)){
			       				$values = implode(",", $values);
			       				$sql = "insert into #__vichy_product_category (category_id, product_id, create_date, modify_date) values ".$values;
			       				$db->setQuery($sql);
			       				$db->query();
			       			}
			       		}
			       		
			           	return true;
			       	}else{
			           	return false;
			       	}
    	 
                }else{
                	$this->setError("Lỗi không up được file");
                	return false;
                }                			
	            
			}
			
       	}
       	return false;
	}
	/**
	*	return array contain list category of product
	**/
	public function get_list_cate_of_product($lang_code='en-GB',$product_id){
		$db  = & JFactory::getDBO();		
		$list_cate = null;
		$sql = "select distinct pc.category_id,c.title, tm.translation_id as category_id_translate from (select * from #__vichy_product_category where product_id ='". $product_id ."') as pc left join #__categories as c on pc.category_id = c.id left join (select * from #__jf_translationmap where reference_table = 'categories' and  language ='". $lang_code ."')as tm on pc.category_id = tm.reference_id ";
		$db->setQuery($sql);
		$list_cate = $db->loadObjectList();
		return $list_cate;
	}
	/**
	*	return array contain list category translate of product
	**/
	public function get_list_cate_translate($lang_code='en-GB',$product_id){
		$db  = & JFactory::getDBO();
		
		$list_cate = null;
		$sql = "select distinct tm.translation_id as category_id_translate,pc.create_date,pc.modify_date from (select * from #__vichy_product_category where product_id='". $product_id ."') as pc inner join (select * from #__jf_translationmap where reference_table = 'categories' and  language ='". $lang_code ."')as tm on pc.category_id = tm.reference_id ";
		$db->setQuery($sql);
		$list_cate = $db->loadObjectList();
		return $list_cate;
	}
	
	/**
	*	return array contain list category of product
	**/
	public function get_category_product($product_id){
		$db  = & JFactory::getDBO();
		$cate_info = null;
		$sql = "select category_id,create_date,modify_date from #__vichy_product_category where product_id = $product_id ";
		$db->setQuery($sql);
		$cate_info = $db->loadObjectList();
		return $cate_info;
	}
	/**
	*	return array contain list category_id of category translate
	**/
	public function get_translate_catid($list_id){
		$db  = & JFactory::getDBO();
		$list_cat_id = array();
		$sql = "select translation_id from #__jf_translationmap where reference_id in($list_id) and reference_table = 'categories' ";
		$db->setQuery($sql);
		$list_cat_id = $db->loadResultArray();
		return $list_cat_id;
	}
	/**
	*	return category_id of category translate
	**/
	public function get_catid_translate($cate_id){
		$db  = & JFactory::getDBO();
		$cat_id = null;
		$sql = "select translation_id from #__jf_translationmap where reference_id =$cate_id and reference_table = 'categories' ";
		$db->setQuery($sql);
		$cat_id = $db->loadResult();
		return $cat_id;
	}
	/**
	*	insert data in table product_category
	**/
	public function add_product_cate($data=null){
		$db  = & JFactory::getDBO();
		$sql = "insert into #__vichy_product_category (category_id,product_id,create_date, modify_date)
                             values ('$data[translate_id]', '$data[product_translate_id]', '$data[create_date]', '$data[modify_date]')";
		
		$db->setQuery($sql);       
       	if($db->query()){
       		return true;
       	}else{
           	return false;
       	}
	}
	/**
	*	return max id of tables vichy_product
	**/
	public function get_max_id_product(){
		$db  = & JFactory::getDBO();
		$id_max = null;
		$sql = "select max(id) from #__vichy_product";
		$db->setQuery($sql);
		$id_max = $db->loadResult();
		return $id_max;
	}
	/**
	* return total number translation
	**/
	public function get_total_translation($table, $lang_code, $filters=null){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		if(!empty($filters)){
			$where = array();
			foreach ($filters as $filter)
			{				
				$sqlFilter = $filter->createFilter();
				if ($sqlFilter != ""){
					if($filter->filterField == 'category_id'){
						$sqlFilter = str_replace("c.category_id","pc.category_id", $sqlFilter);
						$where[] = $sqlFilter;						
					}else{
						$sqlFilter = str_replace("tm.translation_id","ct.id", $sqlFilter);
						$where[] = $sqlFilter;
					}
				}
			}
			if(!empty($where)){
				$where = ' and '.implode(' and ', $where);
			}else{
				$where = '';
			}
		}else{
			$where = '';
		}
		//die($where);
		$sql = 'select count(distinct c.id) from #__'.$table.' as c left join #__'.$table.' as ct on c.id=ct.origin_id and ct.language="'.$lang_code.'" left join #__vichy_product_category as pc on c.id = pc.product_id where c.published>=-1 and (c.language =  "*" or c.language =  '.$db->quote($jfManager->getDefaultLanguage()).' ) '.$where;
		$db->setQuery($sql);
		$count = $db->loadResult();
		
		return $count;
	}

	/**
	* return list translation
	**/
	public function get_list_translation($table, $lang_code, $limitstart, $limit, $filters=null){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		if(!empty($filters)){
			$where = array();
			foreach ($filters as $filter)
			{				
				$sqlFilter = $filter->createFilter();
				if ($sqlFilter != ""){
					if($filter->filterField == 'category_id'){
						$sqlFilter = str_replace("c.category_id","pc.category_id", $sqlFilter);
						$where[] = $sqlFilter;						
					}else{
						$sqlFilter = str_replace("tm.translation_id","ct.id", $sqlFilter);
						$where[] = $sqlFilter;
					}
				}
			}
			if(!empty($where)){
				$where = ' and '.implode(' and ', $where);
			}else{
				$where = '';
			}
		}else{
			$where = '';
		}
		//die($where);
		$sql = 'select distinct c.id, ct.id as jfc_id, c.name as title, ct.name as titleTranslation, c.name, ct.name as jfc_name, c.is_favorite, c.is_new, c.image, ct.image as jfc_image, c.description, ct.description as jfc_description, c.effectiveness, ct.effectiveness as jfc_effectiveness, c.tolerance, ct.tolerance as jfc_tolerance, c.pleasure, ct.pleasure as jfc_pleasure, c.video, ct.video as jfc_video, c.create_date, ct.create_date as jfc_create_date, ct.published as published, ct.id as jfc_refid,  ct.modify_date as lastchanged 
				from #__'.$table.' as c				
				left join #__'.$table.' as ct on c.id = ct.origin_id and ct.language="'.$lang_code.'"
				left join #__vichy_product_category as pc on c.id = pc.product_id
				where c.published >= -1
				and (c.language =  "*" or c.language =  '.$db->quote($jfManager->getDefaultLanguage()).' ) '. $where .' 
				order by name asc
				limit '. $limitstart .' , '. $limit;
		$db->setQuery($sql);
		$list = $db->loadObjectList();

		return $list;
	}
	
	/**
	* save translate press corner
	**/
	public function save_press_corner($data=null){
		$db = JFactory::getDBO();
		if(!empty($data)){			
			$lang_code = $this->getLangCodeByID($data['language_id']);			
			if(!empty($data['translation_id'])){
				
				$flag = '1';
            	if(!empty($_FILES['file']['name'])){
            		$type = $_FILES['file']['type'];
	                $extension = strtolower(substr($type, strpos($type,'/') + 1));
	                $tmp_name = $_FILES['file']['tmp_name'];
	                $new_name = md5(time()).'.'.$extension;
	                $location = '../components/com_vichy_about/uploads/images/'.$new_name;
	                if(move_uploaded_file($tmp_name, $location)){
	                    $data['image'] =  $new_name;
	                    $flag = '1';
	                }else{
	                	$flag = '0';
	                }
            	}else{
            		$data['image'] = $data['file-image'];
            	}

            	if(!empty($_FILES['pdf']['name'])){
            		$type = $_FILES['pdf']['type'];
	                $extension = strtolower(substr($type, strpos($type,'/') + 1));
	                $tmp_name = $_FILES['pdf']['tmp_name'];
	                $new_name = md5(time()).'.'.$extension;
	                $location = '../components/com_vichy_about/uploads/press_corner/'.$new_name;
	                if(move_uploaded_file($tmp_name, $location)){
	                    $data['file_pdf'] =  $new_name;
	                    $flag = '1';
	                }else{
	                	$flag = '0';
	                }
            	}else{
            		$data['file_pdf'] = $data['file-pdf'];
            	}
            	if($flag == '1'){
            		if(!empty($data['file-image']) && $data['image']!=$data['file-image']){
	            		$localtion_gallery = '../components/com_vichy_about/uploads/images/'.$data['file-image'];
	                	unlink($localtion_gallery);
	            	}
	            	if(!empty($data['file-pdf']) && $data['file_pdf']!=$data['file-pdf']){
	            		$localtion_gallery = '../components/com_vichy_about/uploads/press_corner/'.$data['file-pdf'];
	                	unlink($localtion_gallery);
	            	}

	            	$sql = "update #__vichy_press_corner set title='".mysql_escape_string($data['refField_title'])."', image='".$data['image']."', file_pdf='".$data['file_pdf']."', description='". mysql_escape_string($data['refField_description']) ."', language='".$lang_code."', published='".$data['published']."' where id = ".$data['translation_id'];					
					$db->setQuery($sql);
					if($db->query()){
						return true;
					}	
            	}

            	return false;

			}else{
				$type = $_FILES['file']['type'];
                $extension = strtolower(substr($type, strpos($type,'/') + 1));
                $tmp_name = $_FILES['file']['tmp_name'];
                $new_name = md5(time()).'.'.$extension;                
                $location = '../components/com_vichy_about/uploads/images/'.$new_name;
                $flag = '0'              ;
                if(move_uploaded_file($tmp_name, $location)){
                	$flag = '1';
                    $data['image'] =  $new_name;
                }else{
                	$flag = '0';
                }

                $type = $_FILES['pdf']['type'];
                $extension = strtolower(substr($type, strpos($type,'/') + 1));
                $tmp_name = $_FILES['pdf']['tmp_name'];
                $new_name = md5(time()).'.'.$extension;                
                $location = '../components/com_vichy_about/uploads/press_corner/'.$new_name;
                if(move_uploaded_file($tmp_name, $location)){
                	$flag = '1';
                    $data['file_pdf'] =  $new_name;
                    
                }else{
                	$flag = '0';
                }

                if($flag == '1'){
                	$sql = "insert into #__vichy_press_corner (origin_id, title, image, file_pdf, description, published, language)
	                              values ('".$data['origin_id']."','".mysql_escape_string($data['refField_title'])."','".$data['image']."','".$data['file_pdf']."','". mysql_escape_string($data['refField_description']) ."','".$data['published']."','". $lang_code ."')";
					$db->setQuery($sql);
			       	if($db->query()){
						return true;
					}
                }

                return false;				
			}
       	}
       	return false;
	}

	/**
	 * Deletes the selected translations (only the translations of course)
	 * @return string	message
	 */
	public function removeTranslationMap($cid, $reference_table)
	{
		$message = '';
		$db = JFactory::getDBO();
		foreach ($cid as $cid_row)
		{
			list($translationid, $contentid, $language_id) = explode('|', $cid_row);
			$lang_code = $this->getLangCodeByID($language_id);
			
			$sql = "delete from #__jf_translationmap where translation_id=".$translationid." and language='".$lang_code."' and reference_table='".$reference_table."'";
			$db->setQuery($sql);
			$result = $db->query();		
		}
		return $message;

	}
	

	/**
	 * Deletes the selected translations (only the translations of course)
	 * @return string	message
	 */
	public function removeCategogies($cid)
	{
		$message = '';
		$db = JFactory::getDBO();
		foreach ($cid as $cid_row)
		{
			list($translationid, $contentid, $language_id) = explode('|', $cid_row);
			$lang_code = $this->getLangCodeByID($language_id);
			
			$sql = "delete from #__categories where id=".$translationid." and language='".$lang_code."'";
			$db->setQuery($sql);
			if($db->query()){
				
				JFactory::getApplication()->enqueueMessage(JText::_('TRANSLATION_SUCCESSFULLY_DELETED'));
			}			
		}
		return $message;

	}

	/**
	 * Deletes the selected translations (only the translations of course)
	 * @return string	message
	 */
	public function removePressCorner($cid)
	{
		$message = '';
		$db = JFactory::getDBO();
		foreach ($cid as $cid_row)
		{
			list($translationid, $contentid, $language_id) = explode('|', $cid_row);
			$lang_code = $this->getLangCodeByID($language_id);

			//remove file
			$query = "SELECT image, file_pdf FROM #__vichy_press_corner where id=".$translationid;
            $db->setQuery($query);
            $result = $db->loadObject();
            $localtion_1 = '../components/com_vichy_about/uploads/images/'.$result->image;
            unlink($localtion_1);
            $localtion_2 = '../components/com_vichy_about/uploads/press_corner/'.$result->file_pdf;
            unlink($localtion_2);
            //end remove file
			
			$sql = "delete from #__vichy_press_corner where id=".$translationid." and language='".$lang_code."'";
			$db->setQuery($sql);
			if($db->query()){
				
				JFactory::getApplication()->enqueueMessage(JText::_('TRANSLATION_SUCCESSFULLY_DELETED'));
			}			
		}
		return $message;

	}

	/**
	*	return list translation press corner
	**/
	public function get_list_press_corner($lang_code='vi-VN', $limitstart, $limit, $filters=null){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		if(!empty($filters)){
			$where = array();
			foreach ($filters as $filter)
			{				
				$sqlFilter = $filter->createFilter();
				if ($sqlFilter != ""){
					$sqlFilter = str_replace("tm.translation_id","ct.id", $sqlFilter);						
					$where[] = $sqlFilter;
				}
			}
			if(!empty($where)){
				$where = ' and '.implode(' and ', $where);
			}else{
				$where = '';
			}
		}else{
			$where = '';
		}

		$sql = "select c.id, ct.id as jfc_id, c.title, ct.title as jfc_title,c.image,ct.image as jfc_image, c.file_pdf, ct.file_pdf as jfc_file_pdf, c.description, ct.description as jfc_description, ct.published as published, ct.language
				from #__vichy_press_corner as c
				left join #__vichy_press_corner as ct on c.id=ct.origin_id and ct.language='".$lang_code."'
				where (c.language =  '*' or c.language =  ".$db->quote($jfManager->getDefaultLanguage())." ) ". $where ." 
				order by c.id asc
				limit ". $limitstart ." , ". $limit;
		$db->setQuery($sql);
		$list = $db->loadObjectList();

		return $list;
	}
	/**
	* return total number translation
	**/
	public function get_total_press_corner($lang_code='vi-VN', $filters=null){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		if(!empty($filters)){
			$where = array();
			foreach ($filters as $filter)
			{				
				$sqlFilter = $filter->createFilter();
				if ($sqlFilter != ""){
					$sqlFilter = str_replace("tm.translation_id","ct.id", $sqlFilter);						
					$where[] = $sqlFilter;
				}
			}
			if(!empty($where)){
				$where = ' and '.implode(' and ', $where);
			}else{
				$where = '';
			}
		}else{
			$where = '';
		}
		//die($where);
		$sql = "select count(distinct c.id)
				from #__vichy_press_corner as c
				left join #__vichy_press_corner as ct on c.id=ct.origin_id and ct.language='".$lang_code."'
				where (c.language =  '*' or c.language =  ".$db->quote($jfManager->getDefaultLanguage())." ) ". $where;
		$db->setQuery($sql);
		$count = $db->loadResult();
		
		return $count;
	}

	/**
	* save translate store
	**/
	public function save_translate_store($data=null){
		$db = JFactory::getDBO();
		if(!empty($data)){
			$modify_date = date('Y-m-d H:i:s');
			$lang_code = $this->getLangCodeByID($data['language_id']);
			$type_translation = $this->get_translation_id($lang_code, $data['type'], 'categories');

			if(!empty($data['translation_id'])){
				$sql = "update #__vichy_store set name='".mysql_escape_string($data['refField_name'])."', type='".$type_translation."', image='".$data['refField_image']."', address='".mysql_escape_string($data['refField_address'])."', phone='".$data['refField_phone']."', email='".$data['refField_email']."', description='". mysql_escape_string($data['refField_description']) ."', create_date='".$data['refField_create_date']."', modify_date='".$modify_date."', language='".$lang_code."', published='".$data['published']."' where id = ".$data['translation_id'];
				//die($sql);
				$db->setQuery($sql);
				if($db->query()){
					return true;
				}
			}else{				
				$sql = "insert into #__vichy_store (origin_id, type,name, image, address, province, phone, email, lng, lat, description, published, create_date, modify_date, language)
	                              values ('".$data['origin_id']."','".$type_translation."','".mysql_escape_string($data['refField_name'])."','".$data['refField_image']."','".mysql_escape_string($data['refField_address'])."','".$data['province']."','".$data['refField_phone']."','".$data['refField_email']."','".$data['lng']."','".$data['lat']."','". mysql_escape_string($data['refField_description']) ."','".$data['published']."','".$data['refField_create_date']."', '".$modify_date."', '". $lang_code ."')";
				$db->setQuery($sql);
		       	if($db->query()){
					return true;
				}
			}
       	}
       	return false;
	}

	/**
	 * Deletes the selected translations (only the translations of course)
	 * @return string	message
	 */
	public function removeTranslation_Store($cid)
	{
		$message = '';
		$db = JFactory::getDBO();
		foreach ($cid as $cid_row)
		{
			list($translationid, $contentid, $language_id) = explode('|', $cid_row);
			$lang_code = $this->getLangCodeByID($language_id);
			
			$sql = "delete from #__vichy_store where id=".$translationid." and language='".$lang_code."'";
			$db->setQuery($sql);
			if($db->query()){
				
				JFactory::getApplication()->enqueueMessage(JText::_('TRANSLATION_SUCCESSFULLY_DELETED'));
			}			
		}
		return $message;

	}

	/**
	*	return list translation store
	**/
	public function get_list_translation_store($lang_code='vi-VN', $limitstart, $limit, $filters=null){
		$db = & JFactory::getDBO();
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		if(!empty($filters)){
			$where = array();
			foreach ($filters as $filter)
			{				
				$sqlFilter = $filter->createFilter();
				if ($sqlFilter != ""){
					$sqlFilter = str_replace("tm.translation_id","ct.id", $sqlFilter);						
					$where[] = $sqlFilter;
				}
			}
			if(!empty($where)){
				$where = ' and '.implode(' and ', $where);
			}else{
				$where = '';
			}
		}else{
			$where = '';
		}

		$sql = "select c.id, ct.id as jfc_id, c.name, ct.name as jfc_name,c.image,ct.image as jfc_image,c.address, ct.address as jfc_address, c.type, c.province,c.phone, ct.phone as jfc_phone, c.email, ct.email as jfc_email, c.lng,c.lat,c.description, ct.description as jfc_description, ct.published as published, c.create_date, ct.create_date as jfc_create_date, ct.modify_date as lastchanged, ct.language
				from #__vichy_store as c
				left join #__vichy_store as ct on c.id=ct.origin_id and ct.language='".$lang_code."'
				where (c.language =  '*' or c.language =  ".$db->quote($jfManager->getDefaultLanguage())." ) ". $where ." 
				order by c.id asc
				limit ". $limitstart ." , ". $limit;
		$db->setQuery($sql);
		$list = $db->loadObjectList();

		return $list;
	}
	/**
	* return total number translation
	**/
	public function get_total_translation_store($lang_code='vi-VN', $filters=null){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		if(!empty($filters)){
			$where = array();
			foreach ($filters as $filter)
			{				
				$sqlFilter = $filter->createFilter();
				if ($sqlFilter != ""){
					$sqlFilter = str_replace("tm.translation_id","ct.id", $sqlFilter);						
					$where[] = $sqlFilter;
				}
			}
			if(!empty($where)){
				$where = ' and '.implode(' and ', $where);
			}else{
				$where = '';
			}
		}else{
			$where = '';
		}
		//die($where);
		$sql = "select count(distinct c.id)
				from #__vichy_store as c
				left join #__vichy_store as ct on c.id=ct.origin_id and ct.language='".$lang_code."'
				where (c.language =  '*' or c.language =  ".$db->quote($jfManager->getDefaultLanguage())." ) ". $where;
		$db->setQuery($sql);
		$count = $db->loadResult();
		
		return $count;
	}

	/**
	* save translate advice
	**/
	public function save_translate_advice($data=null){
		$db = JFactory::getDBO();
		if(!empty($data)){
			$lang_code = $this->getLangCodeByID($data['language_id']);			
			if(!empty($data['translation_id'])){
				$sql = "update #__vichy_qa set text='".mysql_escape_string($data['refField_text'])."', catid='".$data['category_id_translation']."', language='".$lang_code."', published='".$data['published']."' where id = ".$data['translation_id'];
				//die($sql);
				$db->setQuery($sql);
				if($db->query()){
					return true;
				}
			}else{
				if($data['is_question'] == 0){
					$translation_parent_id = $this->get_translation_id_advice($lang_code, $data['parent_id']);
				}else{
					$translation_parent_id = 0;
				}
				$sql = "insert into #__vichy_qa (origin_id,text,parent_id,is_question,type,catid,language,published)
	                              values ('".$data['origin_id']. "','". mysql_escape_string($data['refField_text']) ."','". $translation_parent_id ."','".$data['is_question']."','".$data['type']."','".$data['category_id_translation']."', '". $lang_code ."','".$data['published']."')";
				$db->setQuery($sql);
		       	if($db->query()){
					return true;
				}
			}
       	}
       	return false;
	}
	/**
	 * Deletes the selected translations (only the translations of course)
	 * @return string	message
	 */
	public function removeTranslation_Advice($cid)
	{
		$message = '';
		$db = JFactory::getDBO();
		foreach ($cid as $cid_row)
		{
			list($translationid, $contentid, $language_id) = explode('|', $cid_row);
			$lang_code = $this->getLangCodeByID($language_id);
			
			$sql = "delete from #__vichy_qa where id=".$translationid." and language='".$lang_code."'";
			$db->setQuery($sql);
			if($db->query()){
				//delete vichy_product_category				
				$sql = "delete from #__vichy_qa where parent_id=".$translationid." and language='".$lang_code."'";
				$db->setQuery($sql);
				$db->query();				

				JFactory::getApplication()->enqueueMessage(JText::_('TRANSLATION_SUCCESSFULLY_DELETED'));
			}			
		}
		return $message;

	}

	/**
	*	return max id of tables vichy_qa
	**/
	public function get_translation_id_advice($lang_code, $id){
		$db  = & JFactory::getDBO();
		$translation_id = null;
		$sql = "select id from #__vichy_qa where language='".$lang_code."' and origin_id=".$id;
		$db->setQuery($sql);
		$translation_id = $db->loadResult();
		return $translation_id;
	}

	/**
	* return row data of step_name table from cate_id
	**/
	public function check_translation_question($lang_code='vi-VN', $id){
		$db = & JFactory::getDBO();
		$result = null;
		$sql = "select origin_id from vc_vichy_qa as qa where qa.is_question=1 and language='".$lang_code."' and origin_id=".$id;
		$db->setQuery($sql);
		$result = $db->loadObject();
		if(!empty($result)){
			return true;
		}
		return false;
	}

	/**
	*	check translation parent category
	**/
	public function check_translation_parent($lang_code='vi-VN', $id){
		if($id == '1'){
			return '1';
		}
		$db = & JFactory::getDBO();
		$result = null;
		$sql = "select translation_id from #__jf_translationmap as c where c.reference_table = 'categories' and c.language='".$lang_code."' and c.reference_id=".$id;
		$db->setQuery($sql);
		$result = $db->loadObject();
		if(!empty($result)){
			return $result->translation_id;
		}

		return null;
	}

	/**
	*	check translation category
	**/
	public function check_translation_cate($lang_code='vi-VN', $id){
		if($id == '1'){
			return '1';
		}
		$db = & JFactory::getDBO();
		$result = null;
		$sql = "select translation_id from #__jf_translationmap as c where c.reference_table = 'categories' and c.language='".$lang_code."' and c.reference_id=".$id;
		$db->setQuery($sql);
		$result = $db->loadObject();
		if(!empty($result)){
			return $result->translation_id;
		}

		return null;
	}

	/**
	*	return category_name from id
	**/
	public function get_cate_name($id){
		$db = & JFactory::getDBO();
		$result = null;
		$sql = "select title from #__categories where id='".$id."'";
		$db->setQuery($sql);
		$result = $db->loadResult();
		
		return $result;
	}

	/**
	* return row data of step_name table from cate_id
	**/
	public function loadRowAdvice($lang_code='vi-VN', $id){
		$db = & JFactory::getDBO();
		$sql = "select c.id, ct.id as translation_id, c.text, ct.text as translation_text, ct.published as published, c.parent_id, c.is_question, c.type, c.catid
				from #__vichy_qa as c
				left join #__vichy_qa as ct on c.id=ct.origin_id and ct.language='".$lang_code."'				
				where c.id = $id";
		$db->setQuery($sql);
		$result = $db->loadObject();

		return $result;
	}

	/**
	*	return list product steps
	**/
	public function get_list_translation_advice($lang_code='vi-VN', $limitstart, $limit, $filters=null){
		$db = & JFactory::getDBO();		
		$jfManager 		= JoomFishManager::getInstance();

		if(!empty($filters)){
			$where = array();
			foreach ($filters as $filter)
			{				
				$sqlFilter = $filter->createFilter();
				if ($sqlFilter != ""){
					$sqlFilter = str_replace("tm.translation_id","ct.id", $sqlFilter);
					$where[] = $sqlFilter;
				}
			}
			if(!empty($where)){
				$where = ' and '.implode(' and ', $where);
			}else{
				$where = '';
			}
		}else{
			$where = '';
		}

		$sql = 'select c.id, ct.id as translation_id, c.text, ct.text as translation_text, ct.published as published
				from #__vichy_qa as c			
				left join #__vichy_qa as ct on c.id=ct.origin_id and ct.language="'.$lang_code.'"
				left join #__categories as cat on c.catid=cat.id
				where (c.language =  "*" or c.language =  '.$db->quote($jfManager->getDefaultLanguage()).' ) '. $where .' 
				order by c.id asc
				limit '. $limitstart .' , '. $limit;
		$db->setQuery($sql);
		$list = $db->loadObjectList();

		return $list;
	}
	/**
	* return total number translation
	**/
	public function get_total_translation_advice($lang_code='vi-VN', $filters=null){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		if(!empty($filters)){
			$where = array();
			foreach ($filters as $filter)
			{				
				$sqlFilter = $filter->createFilter();
				if ($sqlFilter != ""){
					$sqlFilter = str_replace("tm.translation_id","ct.id", $sqlFilter);
					$where[] = $sqlFilter;
				}
			}
			if(!empty($where)){
				$where = ' and '.implode(' and ', $where);
			}else{
				$where = '';
			}
		}else{
			$where = '';
		}
		//die($where);
		$sql = 'select count(distinct c.id)
				from #__vichy_qa as c			
				left join #__vichy_qa as ct on c.id=ct.origin_id and ct.language="'.$lang_code.'"
				left join #__categories as cat on c.catid=cat.id
				where (c.language =  "*" or c.language =  '.$db->quote($jfManager->getDefaultLanguage()).' ) '. $where;
		$db->setQuery($sql);
		$count = $db->loadResult();
		
		return $count;
	}

	/**
	* save translate step
	**/
	public function save_translate_step($data=null){
		$db = JFactory::getDBO();
		if(!empty($data)){
			$lang_code = $this->getLangCodeByID($data['language_id']);
			$total_step = $data['total_step'];
			for($i = 1; $i <= $total_step; $i++){
				if(!empty($data['translate_id_step_'.$i])){
					$sql = "update #__vichy_step_name set step_name='".mysql_escape_string($data['refField_step_'.$i])."', language='".$lang_code."' where id = ".$data['translate_id_step_'.$i];
					//die($sql);
					$db->setQuery($sql);
					if(!$db->query()){
						return false;
					}

				}else{
					$sql = "insert into #__vichy_step_name (origin_id,category_id,no_of_steps,step_name,language)
		                              values ('".$data['original_id_step_'.$i]. "','". $data['category_id'] ."','". $data['no_of_steps_'.$i] ."','".mysql_escape_string($data['refField_step_'.$i])."', '". $lang_code ."')";
					$db->setQuery($sql);
			       	if(!$db->query()){
			       		return false;
			       	}

				}
			}
			return true;		
       	}
       	return false;
	}

	/**
	 * Deletes the selected translations (only the translations of course)
	 * @return string	message
	 */
	public function removeTranslation_Step($cid)
	{
		$message = '';
		$db = JFactory::getDBO();
		foreach ($cid as $cid_row)
		{
			list($translationid, $contentid, $language_id) = explode('|', $cid_row);
			$lang_code = $this->getLangCodeByID($language_id);

			$sql = "delete from #__vichy_step_name where category_id=".$contentid." and language='".$lang_code."'";
			$db->setQuery($sql);
			if($db->query()){			

				JFactory::getApplication()->enqueueMessage(JText::_('TRANSLATION_SUCCESSFULLY_DELETED'));
			}			
		}
		return $message;

	}

	/**
	* return row data of step_name table from cate_id
	**/
	public function loadRowStep($lang_code='vi-VN', $cate_id){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		$sql = "select sn.id as step_name_id, ct.id as translate_id, sn.category_id,sn.no_of_steps,sn.step_name,ct.step_name as step_name_translate from #__vichy_step_name as sn left join #__vichy_step_name as ct on sn.id = ct.origin_id and ct.language='".$lang_code."' where (sn.language =  '*' or sn.language =  ".$db->quote($jfManager->getDefaultLanguage())." ) and sn.category_id=".$cate_id." order by no_of_steps asc ";
		$db->setQuery($sql);
		$result = $db->loadObjectList();

		return $result;
	}

	/**
	*	return list product steps
	**/
	public function get_list_translation_steps($lang_code='vi-VN', $limitstart, $limit, $where){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		$sql = "select distinct c.id, c.title from vc_categories as c inner join vc_vichy_step_name as sn on c.id = sn.category_id and c.extension='com_vichy_product' and (sn.language =  '*' or sn.language =  ".$db->quote($jfManager->getDefaultLanguage())." ) ".$where." order by c.title asc limit ". $limitstart ." , ". $limit;
		$db->setQuery($sql);
		
		$list = $db->loadObjectList();
		if(!empty($list)){
			foreach($list as $v){
				$sql = "select sn.id as step_name_id from vc_vichy_step_name as sn inner join (select * from vc_vichy_step_name where language='".$lang_code."') as snt on sn.id = snt.origin_id where sn.category_id = $v->id";
				$db->setQuery($sql);
				$result = $db->loadObjectList();
				if(!empty($result)){
					$v->state = 1;
				}else{
					$v->state = -1;
				}
			}
		}
		return $list;
	}
	/**
	* return total number translation
	**/
	public function get_total_translation_steps($lang_code='vi-VN', $where){
		$db = & JFactory::getDBO();
		$jfManager 		= JoomFishManager::getInstance();

		$sql = "select count(distinct c.id) from vc_categories as c inner join vc_vichy_step_name as sn on c.id = sn.category_id and c.extension='com_vichy_product' and sn.language='".$lang_code." $where";
		$db->setQuery($sql);
		$count = $db->loadResult();
		
		return $count;
	}

	/**
	*	return lang title by lang_id from table languages
	**/
	public function getLangTitleByID($lang_id){
		$db = & JFactory::getDBO();

		$sql = "select title from #__languages where lang_id = $lang_id";
		$db->setQuery($sql);
		$langID = $db->loadResult();
		return $langID;
	}

	/**
	*	return lang code by lang_id from table languages
	**/
	public function getLangCodeByID($lang_id){
		$db = & JFactory::getDBO();

		$sql = "select lang_code from #__languages where lang_id = $lang_id";
		$db->setQuery($sql);
		$langID = $db->loadResult();
		return $langID;
	}

	/**
	*
	**/
	public function setPublished($table, $id, $published){
		$db = & JFactory::getDBO();

		$sql = "update #__".$table." set published=".$published." where id=".$id;
		$db->setQuery($sql);
		if($db->query()){
			return true;
		}
		return false;

	}

	/**
	 * Deletes the selected translations (only the translations of course)
	 * @return string	message
	 */
	public function removeTranslation_Product($cid)
	{
		$message = '';
		$db = JFactory::getDBO();
		foreach ($cid as $cid_row)
		{
			list($translationid, $contentid, $language_id) = explode('|', $cid_row);
			$lang_code = $this->getLangCodeByID($language_id);

			//remove image product on hosting
        	$query = "SELECT image FROM #__vichy_product where id=".$translationid;
            $db->setQuery($query);
            $result = $db->loadObject();
            $localtion = '../components/com_vichy_product/uploads/products/'.$result->image;
            unlink($localtion);
            //end remove image

			$sql = "delete from #__vichy_product where id=".$translationid;
			$db->setQuery($sql);
			if($db->query()){
				//delete vichy_product_category
				$sql = "delete from #__vichy_product_category where product_id=".$translationid;
				$db->setQuery($sql);
				$db->query();

				//delete translationmap
				/*$sql = "delete from #__jf_translationmap where language='".$lang_code."' and reference_table='vichy_product' and translation_id=".$translationid;
				$db->setQuery($sql);
				$db->query();*/

				JFactory::getApplication()->enqueueMessage(JText::_('TRANSLATION_SUCCESSFULLY_DELETED'));
			}			
		}
		return $message;

	}

	/**
	 * return the model name
	 */
	public function getName()
	{
		return $this->_modelName;

	}

	/**
	 * Method to prepare the language list for the translation backend
	 * The method defines that all languages are being presented except the default language
	 * if defined in the config.
	 * @return array of languages
	 */
	public function getLanguages()
	{
		$jfManager = JoomFishManager::getInstance();
		return $jfManager->getLanguages(false);

	}

	/**
	 * Deletes the selected translations (only the translations of course)
	 * @return string	message
	 */
	public function removeTranslation($catid, $cid)
	{
		$message = '';
		$db = JFactory::getDBO();
		foreach ($cid as $cid_row)
		{
			list($translationid, $contentid, $language_id) = explode('|', $cid_row);

			$jfManager = JoomFishManager::getInstance();
			$contentElement = $jfManager->getContentElement($catid);
			if ($contentElement->getTarget() == "joomfish")
			{
				$contentTable = $contentElement->getTableName();
				$contentid = intval($contentid);
				$translationid = intval($translationid);

				// safety check -- complete overkill but better to be safe than sorry
				// get the translation details
				JLoader::import('tables.JFContent', JOOMFISH_ADMINPATH);
				$translation = new jfContent($db);
				$translation->load($translationid);

				if (!isset($translation) || $translation->id == 0)
				{
					$this->setState('message', JText::sprintf('NO_SUCH_TRANSLATION', $translationid));
					continue;
				}

				// make sure translation matches the one we wanted
				if ($contentid != $translation->reference_id)
				{
					$this->setState('message', JText::_('SOMETHING_DODGY_GOING_ON_HERE'));
					continue;
				}

				$sql = "DELETE from #__jf_content WHERE reference_table='$catid' and language_id=$language_id and reference_id=$contentid";
				$db->setQuery($sql);
				$db->query();
				if ($db->getErrorNum() != 0)
				{
					$this->setError(JText::_('SOMETHING_DODGY_GOING_ON_HERE'));
					JError::raiseWarning(400, JTEXT::_('No valid table information: ') . $db->getErrorMsg());
					continue;
				}
				else
				{
					$this->setState('message', JText::_('TRANSLATION_SUCCESSFULLY_DELETED'));
				}
			}
			else
			{
				$db = JFactory::getDbo();
				$contentElement = $jfManager->getContentElement($catid);
				$tableclass = $contentElement->getTableClass();
				if ($tableclass && intval($translationid) > 0)
				{
					// load the translation and amend
					$table = JTable::getInstance($tableclass);
					$table->load(intval($translationid));
					if (!$table->delete())
					{
						$this->setError(JText::_('SOMETHING_DODGY_GOING_ON_HERE'));
						JError::raiseWarning(400, JTEXT::_('No valid table information: ') . $db->getErrorMsg());
						continue;
					}
					else
					{
						$this->setState('message', JText::_('TRANSLATION_SUCCESSFULLY_DELETED'));
					}
				}
			}
		}
		return $message;

	}
	
	/*
	 * Get a list of originals ids and titles for modal selector
	 */
	public function getSimpleOriginalItemList($table, $limitstart, $limit) {
		
		$db				= JFactory::getDbo();
		$jfManager 		= JoomFishManager::getInstance();
		$contentElement = $jfManager->getContentElement($table);
		$tableclass 	= $contentElement->getTableClass();
		$primarykey		= $contentElement->getReferenceId();
		
		$table			= $contentElement->getTable();
		
		foreach ($table->Fields as $tableField)
			{
				if ($tableField->Type == "titletext") {
					
					if (strtolower($tableField->Name) != "title")
					{
						
						$titlecolumn = $db->quoteName($tableField->Name) . ' as title';					
					} else {
						
						$titlecolumn	= $db->quoteName($tableField->Name);
					}
				}
			}
		
		
		// Build up the rows for the table
		$rows = null;
		$total = 0;
		$result = new stdClass();
		
		if (!$contentElement)
		{
			$table = "content";
			$contentElement = $jfManager->getContentElement->getContentElement($table);
		}
		

		$query	= $db->getQuery(true);
		
		$query->select($db->quoteName($primarykey).' AS id,'. $titlecolumn);
		$query->from($db->quoteName('#__'.$tableclass).' AS c');

		if ($table->Filter != '')
		{
			$query->where ($table->Filter);
		}
		
		$query->where ('(c.language="*" OR c.language='. $db->quote($jfManager->getDefaultLanguage()).')');
		
		$db->setQuery($query, $limitstart, $limit);	
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			JError::raiseWarning(200, JTEXT::_('No valid database connection: ') . $db->stderr());
			// should not stop the page here otherwise there is no way for the user to recover
			$rows = array();
		}
		
		$count = count($rows);
		
		$result->total 	= $total;
		$result->rows	= $rows;
		
		return $result;
		
	}

}

?>
