<?php
abstract class Vichy_adviceHelper{
	public static function addSubmenu($submenu) 
    {
            JSubMenuHelper::addEntry(JText::_('Advice'),
                                     'index.php?option=com_vichy_advice', $submenu == 'advices');
            JSubMenuHelper::addEntry(JText::_('Diagnostic'),
                                     'index.php?option=com_vichy_advice&controller=diagnostics', $submenu == 'diagnostics');
            JSubMenuHelper::addEntry(JText::_('Categories'),
                                     'index.php?option=com_categories&view=categories&extension=com_vichy_advice',
                                     $submenu == 'categories');
            // set some global property
            $document = JFactory::getDocument();
            if ($submenu == 'categories') 
            {
                    $document->setTitle(JText::_('COM_HELLOWORLD_ADMINISTRATION_CATEGORIES'));
            }
    }

    public static function getCategories(){
        $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        $query = "SELECT id, title, parent_id FROM #__categories where extension = 'com_vichy_advice' and published = '1' order by title ASC";
        $db->setQuery($query);
        $listCategories = $db->loadObjectList();
        return $listCategories;
    }

    public static function getYourNeed(){
        $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        $query = "SELECT g.id,c.parent_id as grand_id,g.title as child_title,g.parent_id,c.title from #__categories as c Join #__categories as g On c.id=g.parent_id where c.published > 0 and LOCATE('com_vichy', c.extension) > 0 and (c.parent_id=10 OR g.parent_id=10) and c.language = 'vi-VN' ORDER BY c.rgt";
        $db->setQuery($query);
        $listCategories = $db->loadObjectList();
        return $listCategories;
    }

    public static function listLanguage(){
        $db =& JFactory::getDbo();
        $db->setQuery(
            'SELECT lang_code, title_native' .
            ' FROM #__languages' .
            ' ORDER BY lang_code ASC'
        );
        $options = $db->loadObjectList();
        return $options;
    }

    public static function getListQuestionDiagnostics(){
         $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        $query = "SELECT id, `text`, parent_id FROM #__vichy_qa where type = 'diagnostic' and is_question = 1 and published = '1'";
        $db->setQuery($query);
        $listQuestion = $db->loadObjectList();
        foreach ($listQuestion as $k => $v) {
            $listQuestion[$k]->text = strip_tags($v->text);
        }
        return $listQuestion;
    }
}