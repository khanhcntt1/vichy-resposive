<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    // $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');

    JToolBarHelper::save();
    $db =& JFactory::getDBO();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));
    $ciddef = $cid[0];

    if ($ciddef > 0){	
    	
    	$query = "SELECT * FROM #__vichy_store WHERE id = ".$ciddef;
    	$db->setQuery($query);
    	$group = $db->loadObject();
        
    	JFilterOutput::objectHTMLSafe($group, ENT_QUOTES, '' );
    	
    	JToolBarHelper::title(JText::_( 'Edit Store  : '.$group->name ), 'generic.png');
    	JToolBarHelper::cancel( 'cancel', 'Close' ); 	
    }
    else{
    	JToolBarHelper::title(JText::_( 'Add Store' ), 'generic.png');
    	JToolBarHelper::cancel();
	
    }
    $type = Vichy_storeHelper::getCategories();
    $listLanguage = Vichy_storeHelper::listLanguage();
?>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script src="<?php echo JURI::root(); ?>administrator/templates/bluestork/js/jquery.geocomplete.min.js"></script>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
	}
    Joomla.submitbutton = function(task){
        if (task == 'save'){
            if(jQuery('#address').val() == '' && jQuery('#b_lng').val() != ''){
                alert('Địa chỉ của cửa hàng không được để trống');
            }else{
                Joomla.submitform(task);
            }
        } else Joomla.submitform(task);
        return false;
    };
</script>
<fieldset>
    <legend>Store details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table>
            <tr>
                <td><strong>Type :</strong></td>
                <td>
                    <select name="type">
                        <option value="">Choose type</option>
                        <?php foreach($type as $k => $v){ ?>
                        <option value="<?php echo $v->id; ?>" <?php echo ($v->id == $group->type) ? 'selected="selected"' : ''; ?>><?php echo $v->title; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td><strong>Name :</strong></td>
                <td>
                    <input type="text" value="<?php if(!empty($group->name))  echo($group->name);else echo ''; ?>"  name="name_s" size="100"/>
                </td>
            </tr>

            <tr>
                <td><strong>Language :</strong></td>
                <td>
                    <?php $selected = (!empty($group->language)) ? $group->language : 'vi-VN'; ?>
                    <select name="language">
                    <option value="*">All</option>
                    <?php
                        echo JHtml::_('select.options', $listLanguage, 'lang_code', 'title_native', $selected); 
                    ?>
                    </select>
                </td>
            </tr>
                        
            <tr>
                <td><strong>Photo :</strong></td>
                <td><input type="file" name="file" id="file"/> (Max size of file is 300Kb)</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if(!empty($group->image)){ ?>
                        <img width="300px" height="176px" src="<?php echo JURI::root();?>components/com_vichy_store/uploads/stores/<?php echo $group->image;?>"/>
                    <?php } ?>
                    <input type="hidden" value="<?php if(!empty($group->image)) echo($group->image); else echo '';?>" name="file-image"/>
                </td>
            </tr>
            <tr>
                <td><strong>Address :</strong></td>
                <td>
                    <input type="text" id="address" name="address" value="<?php echo $group->address; ?>" size="100" />
                </td>
            </tr>
            <tr>
                <td><strong>Phone :</strong></td>
                <td>
                    <input type="text" name="phone" value="<?php echo $group->phone; ?>" size="40" />
                </td>
            </tr>
            <tr>
                <td><strong>Email :</strong></td>
                <td>
                    <input type="text" name="email" value="<?php echo $group->email; ?>" size="40" />
                </td>
            </tr>
            <tr>
                <td><strong>Description :</strong></td>
                <td>
                <?php 			
			         echo @$this->editor->display( 'description',  $group->description, '550;', '300', '75', '20', array() ) ; 
                 ?> 
                </td>
            </tr>
            <input type="hidden" id="b_lng" value="<?php echo $group->lng; ?>" name="lng">
            <input type="hidden" id="b_lat" value="<?php echo $group->lat; ?>" name="lat">
        </table>
        
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_store" />
        <input type="hidden" name="controller" value="stores" />
        <input type="hidden" name="task" value=""/>
    </form>
    <form name="geo">
        <table>
            <tr>
                <td>
                    <input id="geocomplete" type="text" placeholder="Type in an address" value="<?php echo $group->address_vn; ?>" size="70" name="geocomplete" />

                    <div class="map_canvas" style="width:500px; height:300px;margin-top:30px;">
                </td>
            </tr>
        </table>
    </form>
</fieldset>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var mylng = jQuery('#b_lng').val();
        var mylat = jQuery('#b_lat').val();
        if(mylng != '' && mylat != ''){
            jQuery("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form",
                mapOptions: {
                    zoom: 17
                },
                location: [mylat,mylng],
                markerOptions: {
                    draggable: true
                }
            });
        }else{
            jQuery("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form",
                location: "VN",
                markerOptions: {
                    draggable: true
                }
            });
        }

        jQuery('#geocomplete').keypress(function(e) {
            if(e.which == 13){
                jQuery("#geocomplete").trigger("geocode");
                jQuery('#address').val(jQuery("#geocomplete").val());
            }
        });

        jQuery("#geocomplete").bind("geocode:dragged", function(event, latLng){
            jQuery("#b_lat").val(latLng.lat());
            jQuery("#b_lng").val(latLng.lng());
        });

        // if(jQuery('#b_lng').val() != '' && jQuery('#b_lat').val() != ''){
        //     var map = jQuery("#geocomplete").geocomplete("map"),
        //     center = new google.maps.LatLng(jQuery('#b_lat').val(), jQuery('#b_lng').val());
        //     map.setCenter(center);
        //     map.setZoom(17);

        //     var lat_and_long = jQuery('#b_lat').val()+", "+jQuery('#b_lng').val();
        //     jQuery("#geocomplete").geocomplete("find", lat_and_long);
        // }
    });
</script>