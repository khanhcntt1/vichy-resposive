<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_adviceModelAdvices extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){

			$query = "SELECT qa.*,c.title FROM #__vichy_qa as qa join #__categories as c on c.id = qa.catid where qa.type = 'advice'";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );
	    	$data['type'] = "advice";
            $data['text'] = JRequest::getVar( 'text', '', 'post', 'string', JREQUEST_ALLOWRAW );
            if ($data['is_question'] == '1'){
            	$data['parent_id'] = '0';
            }
            if (!$row->bind($data)) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }
                    	 
            if (!$row->check()) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }
                    	 
            if (!$row->store()) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }
	    	return true;
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));	
		    $row =& $this->getTable();		
	    	foreach ($cids as $cid) {

                if (!$row->delete( $cid )) {
  	            	$this->setError($row->getErrorMsg());
  	            	return false;
                }
               
	    	}
	     	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_qa'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .') or parent_id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>