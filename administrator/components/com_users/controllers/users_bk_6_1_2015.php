<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Users list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_users
 * @since       1.6
 */
class UsersControllerUsers extends JControllerAdmin
{
	/**
	 * @var    string  The prefix to use with controller messages.
	 * @since  1.6
	 */
	protected $text_prefix = 'COM_USERS_USERS';

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @return  UsersControllerUsers
	 *
	 * @since   1.6
	 * @see     JController
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		$this->registerTask('block',		'changeBlock');
		$this->registerTask('unblock',		'changeBlock');
		// $this->registerTask('export',		'export');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since	1.6
	 */
	public function getModel($name = 'User', $prefix = 'UsersModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * Method to change the block status on a record.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function changeBlock()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$ids	= JRequest::getVar('cid', array(), '', 'array');
		$values	= array('block' => 1, 'unblock' => 0);
		$task	= $this->getTask();
		$value	= JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('COM_USERS_USERS_NO_ITEM_SELECTED'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->block($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$this->setMessage(JText::plural('COM_USERS_N_USERS_BLOCKED', count($ids)));
				}
				elseif ($value == 0)
				{
					$this->setMessage(JText::plural('COM_USERS_N_USERS_UNBLOCKED', count($ids)));
				}
			}
		}

		$this->setRedirect('index.php?option=com_users&view=users');
	}

	/**
	 * Method to activate a record.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function activate()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$ids	= JRequest::getVar('cid', array(), '', 'array');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('COM_USERS_USERS_NO_ITEM_SELECTED'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->activate($ids))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				$this->setMessage(JText::plural('COM_USERS_N_USERS_ACTIVATED', count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_users&view=users');
	}

	public function export(){
		$ids = JRequest::getVar('cid');
		if(!empty($ids)){
			$db =& JFactory::getDBO();
			$query = "SELECT u.id,u.name, u.username, u.email, 
						(case when (u.gender = 0) then 'Female' else 'Male' end) as gender, 
						u.birthday, u.phone, vc_provinces.name as city, g.title as group_name,
						u.registerDate
					from vc_user_usergroup_map as map 
					join vc_users as u on map.user_id = u.id 
					join vc_usergroups as g on map.group_id = g.id 
					left join vc_provinces on u.address = vc_provinces.id 
					where u.id in ($ids) order by u.name asc";
			$db->setQuery($query);
			$rows = $db->loadObjectList();	
			
			// echo '<pre>'; print_r($rows); echo '</pre>';	exit;
		
			$now = time();
		

		
			require_once (JPATH_COMPONENT_ADMINISTRATOR.DS.'includes'.DS.'Classes/PHPExcel.php');
			require_once (JPATH_COMPONENT_ADMINISTRATOR.DS.'includes'.DS.'Classes/PHPExcel/IOFactory.php');
		
			$objPHPExcel = new PHPExcel();
		
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle('Export_users_'.$now);
		
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
	// 		$objPHPExcel->getActiveSheet()->getStyle('A1:FO1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
		
	// 		$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getFont()->setBold(true);
	// 		$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
		
	// 		$objPHPExcel->getActiveSheet()->getStyle('D2:FO2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,"ID");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,1,"Name");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,1,"Username");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,1,"Email");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,1,"Phone");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,1,"City");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,1,"Gender");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,1,"Birthday");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,1,"Registration Date");
			
			$i = 2;
			foreach ($rows as $v){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$v->id);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,$v->name);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$v->username);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$v->email);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$v->phone);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$v->city);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$v->gender);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$v->birthday);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$i,$v->registerDate);
				$i++;
			}
		
			// header("Content-type: application/vnd.ms-excel;charset=utf-8");
			// header("Content-Disposition: attachment; filename=Export_users_$now.csv");
			// // header('Cache-Control: max-age=0');
			// header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
			// header('Expires: 0');
			// header("Pragma: no-cache");
		
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save(JPATH_COMPONENT_ADMINISTRATOR.DS.'includes'.DS."Export_users_$now.csv");
			$result['success'] = '1';
			// $result['path'] = JURI::root()."administrator/index.php?option=com_users&task=users.download&file=Export_users_$now.csv";
			$result['path'] = JURI::root()."administrator/download.php?file=Export_users_$now.csv";
			$result['error'] = null;
			echo json_encode($result);
			exit();
		}else{
			$result['success'] = '0';
			$result['path'] = null;
			$result['error'] = 'Please choose some users';
			echo json_encode($result);
		}
		exit();
	}

	// public function export(){
	// 	JRequest::checkToken() or jexit( 'Invalid Token' );
	// 	$ids	= JRequest::getVar('cid', array(), '', 'array');
	// 	// $this->setRedirect('index.php?option=com_users&view=users&layout=export&cid[]='.$ids);
	// 	if (empty($ids))
	// 	{
	// 		JError::raiseWarning(500, JText::_('COM_USERS_USERS_NO_ITEM_SELECTED'));
	// 	}
	// 	else
	// 	{
	// 		$db =& JFactory::getDBO();
	// 		$query = "SELECT u.id,u.name, u.username, u.email, 
	// 					(case when (u.gender = 0) then 'Female' else 'Male' end) as gender, 
	// 					u.birthday, u.phone, vc_provinces.name as city, g.title as group_name,
	// 					u.registerDate
	// 				from vc_user_usergroup_map as map 
	// 				join vc_users as u on map.user_id = u.id 
	// 				join vc_usergroups as g on map.group_id = g.id 
	// 				left join vc_provinces on u.address = vc_provinces.id 
	// 				where u.id in (".implode(',', $ids).") order by u.name asc";
	// 		$db->setQuery($query);
	// 		$rows = $db->loadObjectList();	
			
	// 		// echo '<pre>'; print_r($rows); echo '</pre>';	exit;
		
	// 		// $now = Date("d-m-Y",time());
	// 		$now = time();
		

		
	// 		require_once (JPATH_COMPONENT_ADMINISTRATOR.DS.'includes'.DS.'Classes/PHPExcel.php');
	// 		require_once (JPATH_COMPONENT_ADMINISTRATOR.DS.'includes'.DS.'Classes/PHPExcel/IOFactory.php');
		
	// 		$objPHPExcel = new PHPExcel();
		
	// 		$objPHPExcel->setActiveSheetIndex(0);
	// 		$objPHPExcel->getActiveSheet()->setTitle('Export_users_'.$now);
		
		
	// 		$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
	// // 		$objPHPExcel->getActiveSheet()->getStyle('A1:FO1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
		
	// // 		$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getFont()->setBold(true);
	// // 		$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
		
	// // 		$objPHPExcel->getActiveSheet()->getStyle('D2:FO2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
			
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,"ID");
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,1,"Name");
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,1,"Username");
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,1,"Email");
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,1,"Phone");
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,1,"City");
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,1,"Gender");
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,1,"Birthday");
	// 		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,1,"Registration Date");
			
	// 		$i = 2;
	// 		foreach ($rows as $v){
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$v->id);
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,$v->name);
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$v->username);
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$v->email);
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$v->phone);
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$v->city);
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$v->gender);
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$v->birthday);
	// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$i,$v->registerDate);
	// 			$i++;
	// 		}
		
	// 		// header("Content-type: application/vnd.ms-excel;charset=utf-8");
	// 		// header("Content-Disposition: attachment; filename=Export_users_$now.csv");
	// 		// // header('Cache-Control: max-age=0');
	// 		// header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	// 		// header('Expires: 0');
	// 		// header("Pragma: no-cache");
		
	// 		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	// 		$objWriter->save(JPATH_COMPONENT_ADMINISTRATOR.DS.'includes'.DS."Export_users_$now.csv");
	// 		$this->setRedirect(JURI::root()."administrator/download.php?file=Export_users_$now.csv");
	// 		exit;
	// 	}
	// 	$this->setRedirect('index.php?option=com_users&view=users');
	// }
}
