<?php
    defined('_JEXEC') or die;
     
    class TableDiagnostics extends JTable {
        var $id = 0;
        var $text;
        var $parent_id;
        var $is_question;
        var $type;
        var $catid;
        var $language;
        var $published;
        var $background;
        var $icon;
        var $type_answer;
     
        function __construct(&$db) {
            parent::__construct('#__vichy_qa', 'id', $db);
        }
    }
?>
