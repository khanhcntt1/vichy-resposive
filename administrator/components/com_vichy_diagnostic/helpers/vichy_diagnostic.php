<?php
abstract class Vichy_diagnosticHelper{
	public static function addSubmenu($submenu) 
    {            
        JSubMenuHelper::addEntry(JText::_('Diagnostic'),
                                 'index.php?option=com_vichy_diagnostic&controller=diagnostics_users', $submenu == 'diagnostics');
        JSubMenuHelper::addEntry(JText::_('Questions & Answers'),
                                 'index.php?option=com_vichy_diagnostic&controller=diagnostics', $submenu == 'question_answer');
        JSubMenuHelper::addEntry(JText::_('Categories'),
                                 'index.php?option=com_categories&view=categories&extension=com_vichy_diagnostic',
                                 $submenu == 'categories');
        JSubMenuHelper::addEntry(JText::_('Mapping'),
                                 'index.php?option=com_vichy_diagnostic&controller=mapping',
                                 $submenu == 'mapping');
        // set some global property
        $document = JFactory::getDocument();
        if ($submenu == 'categories') 
        {
                $document->setTitle(JText::_('COM_HELLOWORLD_ADMINISTRATION_CATEGORIES'));
        }
    }

    public static function getCategories($language=null, $where=null){
        $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        if(!empty($language)){
            $where_language = " and language in('*', '". $language ."')";
        }
        if(!empty($where)){
            $where = " and $where ";
        }
        $query = "SELECT id, title, parent_id FROM #__categories where extension = 'com_vichy_diagnostic' and published = '1' $where_language $where order by title ASC";
        $db->setQuery($query);
        $listCategories = $db->loadObjectList();
        return $listCategories;
    }

    public static function getYourNeed(){
        $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        $query = "SELECT g.id,c.parent_id as grand_id,g.title as child_title,g.parent_id,c.title from #__categories as c Join #__categories as g On c.id=g.parent_id where c.published > 0 and LOCATE('com_vichy', c.extension) > 0 and (c.parent_id=10 OR g.parent_id=10) and c.language = 'vi-VN' ORDER BY c.rgt";
        $db->setQuery($query);
        $listCategories = $db->loadObjectList();
        return $listCategories;
    }

    public static function getProductRanges()
    {
        $db = & JFactory::getDBO();
        $query ="SELECT id,title from #__categories
                WHERE parent_id in (".RANGE.",".RANGE_NO_STEP.") and published>0
                ";
                // die($query);
        $db->setQuery($query);
        $listRanges = $db->loadObjectList();
        return $listRanges;
    }

    public static function listLanguage(){
        $db =& JFactory::getDbo();
        $db->setQuery(
            'SELECT lang_code, title_native' .
            ' FROM #__languages' .
            ' ORDER BY lang_code ASC'
        );
        $options = $db->loadObjectList();
        return $options;
    }

    public static function getListQuestionDiagnostics(){
        $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        $query = "SELECT id, `text`, parent_id FROM #__vichy_qa where type = 'diagnostic' and is_question = 1 and published = '1'";
        $db->setQuery($query);
        $listQuestion = $db->loadObjectList();
        foreach ($listQuestion as $k => $v) {
            $listQuestion[$k]->text = strip_tags($v->text);
        }
        return $listQuestion;
    }

    public static function getQuestionIDFromAnsID($answer_id){
        $db = & JFactory::getDbo();
        $query = "SELECT parent_id from #__vichy_qa where id = $answer_id";
        $db->setQuery($query);
        $id = $db->loadResult();
        return $id;
    }

    public static function getListAnswers($ques_id){
        $db = & JFactory::getDbo();
        $query = "SELECT parent_id, id as answer_id, q.text as answer_text from #__vichy_qa as q where is_question = 0 and parent_id = '".$ques_id."'";
        $db->setQuery($query);
        $list = $db->loadObjectList();
        return $list;
    }

    public static function getResultFromAnswerID($answer_id_1, $answer_id_2){
        $db = & JFactory::getDbo();
        $query1 = "SELECT m.parent_id 
                    from vc_vichy_diagnostic_map as m inner join vc_vichy_diagnostic_map as m2 on m. parent_id = m2.parent_id 
                    where m.answer_id = $answer_id_1 and m2.answer_id = $answer_id_2";
        $db->setQuery($query1);
        $map_id = $db->loadResult();

        $query2 = "SELECT r.*
                    from vc_vichy_diagnostic_map as m inner join vc_vichy_result as r on m.result_id = r.id inner join vc_vichy_result_product as rp on r.id = rp.result_id
                    where m.id = $map_id";
        $db->setQuery($query2);
        $result = $db->loadObject();

        return $result;
    }

    public static function getListProduct($result_id){
        $db = & JFactory::getDbo();
        $query = "SELECT p.id as product_id, p.name, p.image from #__vichy_product as p inner join #__vichy_result_product as rp on p.id = rp.product_id where rp.result_id = ".$result_id;
        $db->setQuery($query);
        $list = $db->loadObjectList();
        return $list;
    }

    public static function getListQuestion($language=null, $catid=null){
        $db =& JFactory::getDbo();
        $where_language = '';
        if(!empty($language)){
            $where_language = " and language in('*', '". $language ."')";
        }
        $where_cate = '';
        if(!empty($catid)){
            $where_cate = " and catid = '".$catid."'";
        }
        $sql = "Select * from vc_vichy_qa where type='diagnostic' and is_question = 1 and published = 1 $where_language $where_cate ";
        $db->setQuery($sql);
        $listQuestion = $db->loadObjectList();
        
        return $listQuestion;
    }

    public static function getListProductStep23($result_id){
        $db = & JFactory::getDbo();
        $query = "SELECT p.id as product_id, p.name as product_name, p.image as product_image, rp.product_id_2, p2.name as product_name_2, p2.image as product_image_2
                        from vc_vichy_product as p left join vc_vichy_result_product as rp on p.id = rp.product_id 
                        left join vc_vichy_product as p2 on rp.product_id_2 = p2.id 
                        where rp.result_id = ".$result_id;
        $db->setQuery($query);
        $list = $db->loadObjectList();
        return $list;
    }

    public static function getDiagnosticsUsers($user_id, $text=''){
        $db =& JFactory::getDbo();
        if(!empty($text)){
            $where_search = " and (qa.text like '%".$text."%' or c.title like '%".$text."%') ";
        }
        $query = "SELECT qa.*,c.title, qu.user_id, qu.question_id, qu.answer_id FROM vc_vichy_qa as qa join vc_categories as c on c.id = qa.catid 
                        left join vc_vichy_qa_user as qu on qa.id = qu.question_id
                        where qa.type = 'diagnostic' and qa.catid not in (60,155) and qu.user_id = $user_id $where_search ";
        $db->setQuery($query);
        $list = $db->loadObjectList();
        
        return $list;
    }

    public static function getDiagnosticsAnswers(){
        $db =& JFactory::getDbo();
        $query = "SELECT a.* FROM vc_vichy_qa as a 
                        where a.type = 'diagnostic' and a.catid not in (60,155) and a.is_question = 0";
        $db->setQuery($query);
        $list = $db->loadObjectList();

        return $list;
    }

    public static function getListAnswersUsers($user_id){
        $db =& JFactory::getDbo();
        $query = "SELECT qu.answer_id FROM vc_vichy_qa_user as qu 
                        where is_registration_question = 0 and user_id = $user_id ";
        $db->setQuery($query);
        $list = $db->loadColumn();
        
        return $list;
    }
}