<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_diagnosticViewDiagnostics_users extends JView{
	   
		function display($tpl = null){
	        // Set the submenu
            Vichy_diagnosticHelper::addSubmenu('diagnostics');
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor);
              
			$listDiagnosticsUsers = & $this->get('Data');
			$this->assignRef('listDiagnosticsUsers', $listDiagnosticsUsers);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>