<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_diagnosticViewMapping extends JView{
	   
		function display($tpl = null){
	        // Set the submenu
            Vichy_diagnosticHelper::addSubmenu('mapping');
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor); 
              
			$listDiagnostics = & $this->get('Data');
			$this->assignRef('listDiagnostics', $listDiagnostics);

			$model = $this->getModel('Mapping');
			$listAnswer = $model->getListAnswer();
			$this->assignRef('listAnswer', $listAnswer);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>