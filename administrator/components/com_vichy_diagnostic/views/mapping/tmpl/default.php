<?php 
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    JHTML::_('behavior.mootools');

    JToolBarHelper::title(JText::_( 'Diagnostics' ), 'generic.png');
    
    $listCategories = Vichy_diagnosticHelper::getCategories(null," parent_id in (170) ");    
    
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table class="adminform">
        <tr>
            <td nowrap="nowrap" style="text-align:left;">
                Tìm kiếm :
                <input name="filter_search" id="filter_search" class="inputbox" value="<?php echo $_POST['filter_search']; ?>" />
                <button type="submit" class="btn">Tìm</button>
                <button type="button" onclick="document.id('filter_search').value='';this.form.submit();">Xóa</button>
            </td>
            <td nowrap="nowrap" style="text-align:right;">
                Lọc theo danh mục :
                <select id="filter_category" name="filter_category" class="inputbox" size="1" onchange="Joomla.submitform();">   
                    <?php if($_POST['filter_category'] == '0'){?>
                    <option value="0" selected="true">Mặc định</option>
                    <?php }else{?>
                    <option value="0">Mặc định</option>
                    <?php } ?>
                    <?php
                        foreach ($listCategories as $k => $v) {
                            $select = ($_POST['filter_category'] == $v->id) ? 'selected = "selected"' : '';
                            $value = $v->id;
                            echo '<option value="'.$value.'" '.$select.'>'.$v->title.'</option>';                            
                        }
                    ?>
                </select>
                Kiểu :
                <?php
                    switch ($_POST['filter_style']) {
                        case 'advice':
                            $select_advice = 'selected="selected"';
                            break;
                        case 'product':
                            $select_product = 'selected="selected"';
                            break;
                        default:
                            $select_default = 'selected="selected"';
                            break;
                    }
                ?>
                <select id="filter_style" name="filter_style" class="inputbox" size="1" onchange="Joomla.submitform();">                       
                    <option value="0" <?php echo $select_default; ?>>Mặc định</option>
                    <option value="advice"  <?php echo $select_advice; ?>>Lời khuyên</option>                        
                    <option value="product" <?php echo $select_product; ?>>Sản phẩm</option>
                </select>

                <!-- Lọc theo trạng thái :
                <select id="filter_state" name="filter_state" class="inputbox" size="1" onchange="Joomla.submitform();">

                    <?php //if($_POST['filter_state'] == 'default'){?>
                    <option value="default" selected="true">Mặc định</option>
                    <?php //}else{?>
                    <option value="default">Mặc định</option>
                    <?php //} ?>

                    <?php //if($_POST['filter_state'] == '1'){?>
                    <option value="1" selected="true">New</option>
                    <?php //}else{?>
                    <option value="1" >New</option>
                    <?php //} ?>

                    <?php //if($_POST['filter_state'] == '0'){?>
                    <option value="0" selected="true">Edit</option>
                    <?php //}else{?>
                    <option value="0">Edit</option>
                    <?php //} ?>

                </select> -->
            </td>
            
        </tr>
    </table>

    <table class="adminlist" cellspacing="1">   
        <tr class="row1" style="line-height: 30px">
            <th width="30" align="center" style="width:4%"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->listDiagnostics); ?>);" /></th>
            <th align="center" style="width:30%">Nội dung</th>
            <th align="center" style="width:20%">Danh mục</th>
            <th align="center" style="width:15%">Loại</th>
            <th align="center" style="width:15">Kiểu</th>
            <th align="center" style="width:15%">Trạng thái</th>
            <th width="30" align="center" style="width:4%">ID</th>
        </tr>
        <tbody>
        <?php 
        $k = 1;
        $n = count($this->listDiagnostics);
        $m = count($this->listAnswer);
        // if($this->listDiagnostics[0]->is_question == 0){
        //     $question_id_first = Vichy_diagnosticHelper::getQuestionIDFromAnsID($this->listDiagnostics[0]->id);
        // }
        for ($i=0; $i < $n; $i++)
        {
            $row    =& $this->listDiagnostics[$i];            

            if($row->is_question == 1){
                if($row->style_diagnostic == 'advice'){
                    $style_diagnostic = 'Lời khuyên';
                }else if($row->style_diagnostic == 'product'){
                    $style_diagnostic = 'Sản phẩm';
                }else{
                    $style_diagnostic = '';
                }
                 
        ?>
            <tr class="<?php echo "row$k"; ?>" style="line-height: 30px">
                <td width="30"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>                  
                <td>
                    <?php
                        if($row->is_question == 1){
                    ?>
                    <b><?php echo strip_tags($row->text); ?></b>
                    <?php
                        }else{
                            // if(!empty($row->answer_id)){
                            //     $task = "add";
                            //     $map_title = "Mapping";
                            // }else{
                            //     $task = "edit";
                            //     $map_title = "Edit";
                            // }
                            // $link   = 'index.php?option=com_vichy_diagnostic&controller=mapping&task='.$task.'&cid[]='. $row->id;
                    ?>
                    <a href="<?php echo $link; ?>"><?php echo strip_tags($row->text); ?></a>
                    <?php
                        }
                    ?>
                </td>
                <td><?php echo $row->title; ?></td>
                <td align="center"><?php echo ($row->is_question == 1) ? 'Câu hỏi' : 'Câu trả lời'; ?></td>
                <td align="center"><?php echo $style_diagnostic; ?></td>
                <td align="center"></td>
                <td align="center"><?php echo $row->id; ?></td>
            </tr>               
        <?php
                $k = 1 - $k;
            }
                       
            for($j=0; $j < $m; $j++)
            {
                $row2 =& $this->listAnswer[$j];
                  

                if($row2->parent_id == $row->id){
                    if($row2->is_question == 0){
                        if(empty($row2->answer_id)){
                            $task = "add";
                            $map_title = "New";
                        }else{
                            $task = "edit";
                            $map_title = "Edit";
                        }
                        $link   = 'index.php?option=com_vichy_diagnostic&controller=mapping&task='.$task.'&cid[]='. $row2->id;
                        $title = '<a href="'.$link.'">'.strip_tags($row2->text).'</a>';
                        $map_title = '<a href="'. $link .'">'. $map_title .'</a>';
                    }else{
                        $link = '';
                        $title = strip_tags($row2->text);
                        $map_title = '';
                    }
        ?>
            <tr class="<?php echo "row$k"; ?>" style="line-height: 30px">
                <td width="30"><?php echo JHTML::_('grid.id', $j, $row2->id); ?></td>                  
                <td><span class="gi">|—</span><?php echo $title; ?></td>
                <td><?php echo $row->title; ?></td>
                <td align="center"><?php echo ($row2->is_question == 1) ? 'Câu hỏi' : 'Câu trả lời'; ?></td>                
                <td align="center"><?php echo $style_diagnostic; ?></td>
                <td align="center"><?php echo $map_title; ?></td>
                <td align="center"><?php echo $row2->id; ?></td>
            </tr>
        <?php
                $k = 1 - $k;
                }else if($row2->parent_id === $question_id_first && $question_id_first != null){
                    if($row2->is_question == 0){
                        if(empty($row2->answer_id)){
                            $task = "add";
                            $map_title = "New";
                        }else{
                            $task = "edit";
                            $map_title = "Edit";
                        }
                        $link   = 'index.php?option=com_vichy_diagnostic&controller=mapping&task='.$task.'&cid[]='. $row2->id;
                        $title = '<a href="'.$link.'">'.strip_tags($row2->text).'</a>';
                        $map_title = '<a href="'. $link .'">'. $map_title .'</a>';
                    }else{
                        $link = '';
                        $title = strip_tags($row2->text);
                        $map_title = '';
                    }
        ?>
            <tr class="<?php echo "row$k"; ?>" style="line-height: 30px">
                <td width="30"><?php echo JHTML::_('grid.id', $j, $row2->id); ?></td>                  
                <td><span class="gi">|—</span><a href="<?php echo $link; ?>"><?php echo strip_tags($row2->text); ?></a></td>
                <td><?php echo $row2->title; ?></td>
                <td align="center"><?php echo ($row2->is_question == 1) ? 'Câu hỏi' : 'Câu trả lời'; ?></td>                
                <td align="center"><?php echo $style_diagnostic; ?></td>
                <td align="center"><a href="<?php echo $link; ?>"><?php echo $map_title; ?></a></td>
                <td align="center"><?php echo $row2->id; ?></td>
            </tr>
        <?php 
                    $k = 1 - $k;
                }

            }
            $question_id_first = null;
        ?>

        <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_diagnostic" />                        
    <input type="hidden" name="controller" value="mapping" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>