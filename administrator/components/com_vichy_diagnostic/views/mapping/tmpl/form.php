<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    JHTML::_('behavior.mootools');
    $doc = JFactory::getDocument();
    $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');
    $doc->addStyleSheet('templates/bluestork/js/jtable/themes/lightcolor/gray/jtable.css');
    $doc->addStyleSheet('components/com_vichy_product/css/style.css');
    $doc->addStyleSheet('components/com_vichy_product/css/simplegrid.css');

    JToolBarHelper::save();
    $db =& JFactory::getDBO();
    $cid = JRequest::getVar('cid',  0, '', 'array');    
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));    
    $ciddef = $cid[0];    
    if ($ciddef > 0){   
        $query = "SELECT qa.*, r.id as result_id, r.name as result_text 
                    from vc_vichy_qa as qa left join vc_vichy_diagnostic_map as map on qa.id = map.answer_id 
                    left join vc_vichy_result as r on map.result_id = r.id 
                    where qa.id = $ciddef";
        $db->setQuery($query);
        $group = $db->loadObject();
        // $query = "SELECT * FROM #__vichy_qa WHERE id = ".$ciddef;
        // $db->setQuery($query);
        // $group = $db->loadObject();

        // $query = "SELECT * FROM #__vichy_result WHERE id = ".$riddef;
        // $db->setQuery($query);
        // $group = $db->loadObject();
        //JFilterOutput::objectHTMLSafe($group, ENT_QUOTES, '' );
        
        JToolBarHelper::title(JText::_( 'Mapping'), 'generic.png');
        JToolBarHelper::cancel( 'cancel', 'Close' );    
    }
    else{
        JToolBarHelper::title(JText::_( 'Mapping' ), 'generic.png');
        JToolBarHelper::cancel();
    
    }
    $sql = "SELECT * from vc_vichy_qa where type='diagnostic' and is_question = 1 and published = 1";
    $db->setQuery($sql);
    $listQuestion = $db->loadObjectList();
    // $listLanguage = Vichy_diagnosticHelper::listLanguage();
    // $listCategories = Vichy_diagnosticHelper::getCategories();

    if($group->parent_id == 187 || $group->parent_id == 192){
        $type = 'diagnostic';
        $question_id_2 = ($group->parent_id == 187) ? 192 : 187;
        $sql_ques_2 = "SELECT * from vc_vichy_qa where id = $question_id_2";
        $db->setQuery($sql_ques_2);
        $question_item = $db->loadObject();

        $list_ans = Vichy_diagnosticHelper::getListAnswers($question_id_2);

    }else if($group->parent_id == 202 || $group->parent_id == 203){
        $type = 'diagnostic_concern';
    }else{
        $type = 'diagnostic_advice';
    }

    // $db = & JFactory::getDbo();
    // $db->setQuery("SELECT p.id as product_id, p.name, p.image from #__vichy_product as p inner join #__vichy_result_product as rp on p.id = rp.product_id where rp.result_id = ".$group->result_id);
    // $db->query();
    // $res_product = $db->loadObjectList();

?>
<script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'cancel') {
            submitform( pressbutton );
            return;
        }
    }
</script>
<?php
    if($type == 'diagnostic_advice'){
?>

<fieldset>
    <legend>Mapping details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table style="padding-left:20px;">
            <tr>
                <td style="min-width: 128px"><strong>Câu hỏi :</strong></td>
                <td>
                    <?php
                        foreach ($listQuestion as $key => $value) {
                            $selected_q = '';
                            if($group->parent_id == $value->id){
                                $style_diagnostic = $value->style_diagnostic;
                                echo "<strong>".strip_tags($value->text)."</strong>";
                            }                            
                        }
                    ?>
                </td>                
            </tr>
            <tr>
                <td><strong>Câu trả lời :</strong></td>
                <td><?php echo strip_tags($group->text); ?></td>
            </tr>
            <tr>
                <td><strong>Kiểu câu hỏi :</strong></td>
                <td>
                <?php 
                    if($style_diagnostic == 'advice'){
                        $style_text = "Lời khuyên";
                    }else if($style_diagnostic == 'product'){
                        $style_text = "Sản phẩm";
                    }
                    echo $style_text; 
                ?>
                </td>
            </tr>
            <tr><td></td><td></td></tr>
            <tr>
                <td>
                    <strong>Lời khuyên :</strong></td> 
                </td>
                <td>
                <?php           
                     echo @$this->editor->display( 'name',  $group->result_text, '550', '300', '75', '20', array() ) ; 
                 ?>
                </td>
            </tr>
        </table>
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="result_id" value="<?php if(!empty($group->result_id))  echo($group->result_id);else echo '';  ?>" />
        <input type="hidden" name="answer_id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_diagnostic" />
        <input type="hidden" name="controller" value="mapping" />
        <input type="hidden" name="task" value=""/>
        <input type="hidden" name="type" value="advice" />
        <input type="hidden" name="form" value="add" />
    </form>
</fieldset>

<?php }else{ ?>

<fieldset>
    <legend>Mapping details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table style="padding-left:20px;">            
            <tr>
                <td style="min-width: 128px"><strong>Câu hỏi :</strong></td>
                <td>
                    <?php
                        foreach ($listQuestion as $key => $value) {
                            $selected_q = '';
                            if($group->parent_id == $value->id){
                                echo strip_tags($value->text);
                            }                            
                        }
                    ?>
                </td>                
            </tr>
            <tr>
                <td><strong>Câu trả lời :</strong></td>
                <td>
                    <?php echo strip_tags($group->text); ?>
                </td>
            </tr>
            <tr><td></td><td></td></tr>
            <?php if($type == 'diagnostic'){ ?>
            <tr>
                <td>
                    <strong>Câu hỏi: </strong></td>                    
                </td>
                <td>
                    <?php echo strip_tags($question_item->text); ?>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top"><strong>Câu trả lời :</strong></td>
                <td id="wrap_product">
                    <?php
                        foreach ($list_ans as $key => $value) {
                            $result = Vichy_diagnosticHelper::getResultFromAnswerID($ciddef, $value->answer_id);
                            if(!empty($result)){
                                $checked = "checked disabled";
                                $res_product = Vichy_diagnosticHelper::getListProduct($result->id);
                            }else{
                                $checked = '';
                            }

                    ?>
                        <div style="float:left;width:100%;">
                            <!-- <input type="hidden" name="result_id_<?php //echo $result->id; ?>" value="<?php //if(!empty($result->id))  echo($result->id);else echo '';  ?>" />
                            <input type="hidden" name="answer_id_<?php //echo $value->answer_id; ?>" value="<?php //if(!empty($value->answer_id))  echo($value->answer_id);else echo '';  ?>" /> -->
                            <input type="hidden" name="aid[]" value="<?php echo $value->answer_id; ?>" />
                            <input type="checkbox" <?php echo $checked; ?> name="answer_<?php echo $value->answer_id; ?>" class="ans_check" value="<?php echo $value->answer_id; ?>" id="<?php echo $value->answer_id; ?>" />
                            <label for="<?php echo $value->answer_id; ?>" style="clear:none;width:70%;"><?php echo $value->answer_text; ?></label>
                            <div style="float:left;width:100%;">
                                <div style="float:left;width:100%;">
                                    <div style="float:left;width:10%;line-height:25px;"><strong>Loại da :</strong></div>
                                    <div style="float:left;width: 70%;">
                                        <input type="text" name="type_skin_<?php echo $value->answer_id; ?>" id="type_skin_<?php echo $value->answer_id; ?>" value="" style="width: 50%;" />
                                    </div>
                                </div>
                                <div class="wrap_step" style="padding-left:0px;">                                    
                                    <!-- <div style="float:left;width:128px;margin-left:-20px;">
                                        <strong>Chọn sản phẩm :</strong>
                                    </div> -->
                                    <div style="float:left;">
                                        <a href="javascript:void(0)" data-step="<?php echo $key; ?>" data-result="<?php echo $result->id; ?>" data-answer="<?php echo $value->answer_id; ?>" class="open_popup">Thêm sản phẩm</a>
                                    </div>
                                    <div style="clear:both;float:left;width:100%;" id="SelectedRowList_<?php echo $key; ?>">
                                        <?php foreach($res_product as $row){ ?>
                                        <div class="step_product col-1-3" >
                                            <div class="step_product_item content">
                                                <div class="item-name">
                                                    <?php echo $row->name; ?>
                                                </div>
                                                <div class="item-image">
                                                    <img style="width:150px;" src="<?php echo JURI::root().'components/com_vichy_product/uploads/products/'.$row->image ?>" />
                                                </div>                                                
                                                <a style="float:right" href="javascript:void(0)" class="delete" data-step="<?php echo $key; ?>" data-id="<?php echo $row->product_id ?>" data-result="<?php echo $result->id; ?>" >Delete</a>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                </td>
            </tr>
            <!-- <tr>
                <td><strong>Loại da :</strong></td>
                <td>
                    <input type="text" name="type_skin" id="type_skin" value="" style="width: 100%;" />
                </td>
            </tr> -->
            <?php }else if($type == 'diagnostic_concern'){ ?>
            <tr>
                <td colspan="2">
                    <div id="wrap_product">
                        <div class="wrap_step" style="padding-left:0px;">                                    
                            <!-- <div style="float:left;width:128px;margin-left:-20px;">
                                <strong>Chọn sản phẩm :</strong>
                            </div> -->
                            <div style="float:left;">
                                <a href="javascript:void(0)" data-step="0" data-result="" data-answer="" class="open_popup">Thêm sản phẩm</a>
                            </div>
                            <div style="clear:both;float:left;width:100%;" id="SelectedRowList_0">
                                <?php foreach($res_product as $row){ ?>
                                <div class="step_product col-1-3" >
                                    <div class="step_product_item content">
                                        <div class="item-name">
                                            <?php echo $row->name; ?>
                                        </div>
                                        <div class="item-image">
                                            <img style="width:150px;" src="<?php echo JURI::root().'components/com_vichy_product/uploads/products/'.$row->image ?>" />
                                        </div>                                                
                                        <a style="float:right" href="javascript:void(0)" class="delete" data-step="<?php echo $key; ?>" data-id="<?php echo $row->product_id ?>" data-result="<?php echo $result->id; ?>" >Delete</a>
                                    </div>
                                </div>
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <?php } ?>
        </table>
        
        <?php echo JHTML::_( 'form.token' ); ?>        
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_diagnostic" />
        <input type="hidden" name="controller" value="mapping" />
        <input type="hidden" name="task" value=""/>
        <input type="hidden" name="type" value="diagnostic" />
        <input type="hidden" name="form" value="add" />
    </form>
</fieldset>

<div id="com-vichy-diagnostic-base-url" data-url="<?php echo JURI::root();?>administrator/index.php?option=com_vichy_diagnostic&controller=mapping&task=loadAnswers"></div>
<div id="dialog-form" style="width: 600px;display:none;">
<div class="filtering">
    <form>
        Tên sản phẩm: <input type="text" name="name" id="name" />
        <button type="submit" id="LoadRecordsButton">OK</button>
    </form>
</div>
<div id="ProductsTableContainer" style="width: 600px;"></div>
</div>
<script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="templates/bluestork/js/jtable/jquery.jtable.js"></script>
<script type="text/javascript">
    var arr = new Array();
    var current_step;
    var edit_id;
    var result_id;
    var answer_id;
    var result_id_old;
    var n;
    var category_id;
    
    jQuery(document).ready(function() {       
        
        //Prepare jTable
        jQuery('#ProductsTableContainer').jtable({
            title: 'Table of product',
            paging: true,
            pageSize: 5,
            sorting: true,
            defaultSorting: 'name ASC',
            selecting: true, //Enable selecting
            multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
            selectOnRowClick: false, //Enable this to only select using checkboxes
            actions: {
                listAction: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=products&task=get_product_ajax'
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                name: {
                    title: 'Product Name',
                    width: '40%'
                },
                image: {
                    title: 'Image',
                    width: '40%'
                }
            },
            recordsLoaded: function(event, data) {
                //add attribute step to class jtable-data-row
                for(i=0;i<arr.length;i++){
                    temp = arr[i].split('#');
                    jQuery('.jtable-data-row').each(function(index) {
                        if(jQuery(this).attr('data-record-key') == temp[1]){
                            jQuery(this).attr('step', temp[0]);
                        }
                    });
                }
            }
        });

        //Load person list from server
        jQuery('#ProductsTableContainer').jtable('load');

        //Re-load records when user click 'load records' button.
        jQuery('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            jQuery('#ProductsTableContainer').jtable('load', {
                name: jQuery('#name').val(),
                // cid: category_id
            });
        });

        jQuery( "#dialog-form" ).dialog({
          autoOpen: false,
          height: 600,
          width: 650,
          modal: true,
          buttons: {
            "Select": function() {
                flag = '';
                jQuery('.jtable-row-selected').each(function(index) {
                    var step = jQuery(this).attr('step');
                    if(step == '' || step == null){
                        jQuery(this).attr('step',current_step);
                    }else{
                        if(step != current_step){
                            flag = 1;
                            alert('One of these products was chosen for this result');
                        }else{
                            flag = 2;
                            alert('This product was chosen for this result');
                        }
                    }
                });

                if(flag == 1 || flag == 2){
                    jQuery( this ).dialog( "close" );
                }
                
                var $selectedRows = jQuery('#ProductsTableContainer').jtable('selectedRows');
                if ($selectedRows.length > 0) {
                    //Show selected rows                    
                    
                    $selectedRows.each(function () {
                        var record = jQuery(this).data('record');
                        arr.push(current_step+'#'+record.id);
                        var step_product_item = '<div class="col-1-3" ><div class="step_product_item content">';
                        step_product_item += '<div class="item-name">';
                        step_product_item += record.name;
                        step_product_item += '</div>';
                        step_product_item += '<div class="item-image">';
                        step_product_item += record.image;
                        step_product_item += '</div>';
                        step_product_item += '<a href="javascript:void(0)" class="del">Delete</a>';
                        step_product_item += '<input type="hidden" name="step_product_item_'+answer_id+'[]" value="'+current_step+'|'+record.id+'" />';
                        step_product_item += '</div></div>';
                        jQuery('#SelectedRowList_'+current_step).append(step_product_item);
                    });
                }
                jQuery( this ).dialog( "close" );
            },
            Cancel: function() {
               jQuery('.jtable-column-header-container input[type=checkbox]').attr('checked', false);
               jQuery('.jtable-selecting-column input[type=checkbox]').attr('checked', false);
               jQuery('.jtable-row-selected').removeClass('jtable-row-selected');
               jQuery( this ).dialog( "close" );
            }
          },
          close: function() {
                jQuery('.jtable-column-header-container input[type=checkbox]').attr('checked', false);
                jQuery('.jtable-selecting-column input[type=checkbox]').attr('checked', false);
                jQuery('.jtable-row-selected').removeClass('jtable-row-selected');
          }
        });

        jQuery('#wrap_product').on('click','.open_popup',function(){
            current_step = jQuery(this).attr('data-step');
            answer_id = jQuery(this).attr('data-answer');
            jQuery( "#dialog-form" ).dialog( "open" );
        });

        jQuery('#wrap_product').on('click','.del',function(){
            tmp = jQuery(this).siblings('input[type=hidden]').val();
            pro_id = tmp.substring(tmp.indexOf('|')+1);
            jQuery('.jtable-data-row').each(function(index) {
                if(jQuery(this).attr('data-record-key') == pro_id){
                    jQuery(this).attr('step', '');
                }
            });
            for(i=0;i<arr.length;i++){
                temp = arr[i].split('#');
                if(temp[1] == pro_id){
                    arr.splice(i,1);
                }
            }
            jQuery(this).parent().remove();
        });
    });
</script>

<?php } ?>