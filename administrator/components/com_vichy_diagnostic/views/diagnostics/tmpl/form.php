<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    // $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');

    JToolBarHelper::save();
    $db =& JFactory::getDBO();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));
    $ciddef = $cid[0];
    if ($ciddef > 0){   
        
        $query = "SELECT * FROM #__vichy_qa WHERE id = ".$ciddef;
        $db->setQuery($query);
        $group = $db->loadObject();
        JFilterOutput::objectHTMLSafe($group, ENT_QUOTES, '' );
        
        JToolBarHelper::title(JText::_( 'Edit Diagnostic'), 'generic.png');
        JToolBarHelper::cancel( 'cancel', 'Close' );    
    }
    else{
        JToolBarHelper::title(JText::_( 'Add Diagnostic' ), 'generic.png');
        JToolBarHelper::cancel();
    
    }
    
    $listLanguage = Vichy_diagnosticHelper::listLanguage();
    $lang = isset($_POST['language']) ? $_POST['language'] : 'vi-VN';
    $listQuestion = Vichy_diagnosticHelper::getListQuestion($lang);    
    $listCategories = Vichy_diagnosticHelper::getCategories($lang);
?>
<script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'cancel') {
            submitform( pressbutton );
            return;
        }
    }
</script>
<fieldset>
    <legend>Diagnostic details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table>
            <tr>
                <td style="min-width: 128px"><strong>Ngôn ngữ :</strong></td>
                <td>
                    <?php $selected = (!empty($group->language)) ? $group->language : 'vi-VN'; ?>
                    <select name="language" id="language" class="selected">
                    <option value="0">Tất cả</option>
                    <?php
                        echo JHtml::_('select.options', $listLanguage, 'lang_code', 'title_native', $selected); 
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><strong>Chọn danh mục :</strong></td>
                <td>
                    <?php $selected = (!empty($group->catid)) ? $group->catid : ''; ?>
                    <select name="catid" id="catid" class="selected">
                    <?php
                        echo JHtml::_('select.options', $listCategories, 'id', 'title', $selected); 
                    ?>
                    </select>
                </td>
            </tr>
            <?php if(!isset($group->is_question) || ($group->is_question == '1')){ ?>     
            <tr>
                <td><strong>Chọn loại</strong></td>
                <td>
                    <input type="radio" name="is_question" id="radio_ques_1" class="radio_ques" value="1" <?php echo ($group->is_question == '1') ? 'checked="checked"' : 'checked="checked"' ?> /><label for="radio_ques_1" style="clear:none">Câu hỏi</label>
                </td>
            </tr>
            <?php } ?>
            <?php if(count($listQuestion)){ ?>
            <?php if(!isset($group->is_question) || ($group->is_question == '0')){ ?>
            <tr>
                <td></td>
                <td>
                    <input type="radio" name="is_question" id="radio_ques_2" class="radio_ques" value="0" <?php echo ($group->is_question == '0') ? 'checked="checked"' : '' ?> /><label for="radio_ques_2" style="clear:none">Câu trả lời</label>
                </td>
            </tr>
            <?php } ?>
            <?php if(!isset($group->is_question) || ($group->is_question == '1')){ ?>
            <tr class="ques">
                <td><strong>Kiểu câu hỏi</strong></td>
                <td>
                    <?php
                        switch ($group->style_diagnostic) {
                            case 'advice':
                                $select_advice = 'selected="selected"';
                                break;
                            case 'product':
                                $select_product = 'selected="selected"';
                                break;
                        }
                    ?>
                    <select name="style_diagnostic">                        
                        <option value="advice"  <?php echo $select_advice; ?>>Lời khuyên</option>                        
                        <option value="product" <?php echo $select_product; ?>>Sản phẩm</option>
                    </select>
                </td>
            </tr>
            <tr class="ques">
                <td><strong>Loại câu trả lời :</strong></td>
                <td>
                    <?php
                        switch ($group->type_answer) {
                            case 'checkbox':
                                $select_checkbox = 'selected="selected"';
                                break;
                            case 'selectbox':
                                $select_selectbox = 'selected="selected"';
                                break;                            
                            default:
                                $select_radio = 'selected="selected"';
                                break;
                        }
                    ?>
                    <select name="type_answer">
                        <option value="radio" <?php echo $select_radio; ?>>Radio Button</option>
                        <option value="checkbox"  <?php echo $select_checkbox; ?>>Checkbox</option>
                        <option value="selectbox"  <?php echo $select_selectbox; ?>>Select Button</option>
                    </select>
                </td>
            </tr>
            <?php } ?>
            <tr class="choose_question">
                <td><strong id="ques_parent">Chọn cấp cha :</strong></td>
                <td>
                    <select name="parent_id" id="parent_id">
                    <option value="0">Không có cấp cha</option>
                    <?php
                        foreach ($listQuestion as $key => $value) {
                            $selected_q = '';
                            if($group->parent_id == $value->id){
                                $selected_q = 'selected="selected"';
                            }
                            echo '<option value="'.$value->id.'"'. $selected_q.'>'.strip_tags($value->text).'</option>';
                        }
                    ?>
                    </select>
                </td>
            </tr>
            <?php } ?>
            <?php if(!isset($group->is_question) || ($group->is_question == '1')){ ?>
            <tr class="ques">
                <td><strong>Chọn hình ảnh ICON :</strong></td>
                <td><input type="file" name="icon" id="icon"/></td>
            </tr>
            <tr class="ques">
                <td></td>
                <td>
                    <?php if(!empty($group->icon)){ ?>
                        <img width="300px" height="176px" src="<?php echo JURI::root();?>components/com_vichy_advice/uploads/diagnostics/<?php echo $group->icon;?>"/>
                    <?php } ?>
                    <input type="hidden" value="<?php if(!empty($group->icon)) echo($group->icon); else echo '';?>" name="icon-image"/>
                </td>
            </tr>
            <tr class="ques">
                <td><strong>Chọn hình nền câu hỏi :</strong></td>
                <td><input type="file" name="background" id="background"/></td>
            </tr>
            <tr class="ques">
                <td></td>
                <td>
                    <?php if(!empty($group->background)){ ?>
                        <img width="300px" height="176px" src="<?php echo JURI::root();?>components/com_vichy_advice/uploads/diagnostics/<?php echo $group->background;?>"/>
                    <?php } ?>
                    <input type="hidden" value="<?php if(!empty($group->background)) echo($group->background); else echo '';?>" name="background-image"/>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td><strong>Nội dung :</strong></td>
                <td>
                <?php           
                     echo @$this->editor->display( 'text',  $group->text, '550', '300', '75', '20', array() ) ; 
                 ?>
                </td>
            </tr>
        </table>
        
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_diagnostic" />
        <input type="hidden" name="controller" value="diagnostics" />
        <input type="hidden" name="task" value=""/>
    </form>
</fieldset>
<div id="com-vichy-diagnostic-base-url" data-url="<?php echo JURI::root();?>administrator/index.php?option=com_vichy_diagnostic&view=diagnostic"></div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.radio_ques').click(function(){
            if(jQuery(this).val() == '1'){
                jQuery('.ques').show();
                jQuery('#ques_parent').html('Chọn cấp cha :');
            }else{
                jQuery('.ques').hide();
                jQuery('#ques_parent').html('Chọn câu hỏi :');
            }
        });

        jQuery('#language').change(function(){
            var language = jQuery("#language").val();
            
            jQuery.ajax({
                type: "POST",
                url: jQuery('#com-vichy-diagnostic-base-url').attr('data-url')+"&task=loadCategories",
                dataType: "json",
                data: "language="+language,
                success: function(data_page){
                    jQuery("#catid").html(data_page.option_cate);
                    jQuery("#parent_id").html(data_page.option_question);
                }
            });

        });

        jQuery('#catid').change(function(){
            var language = jQuery("#language").val();
            var catid = jQuery("#catid").val();

            jQuery.ajax({
                type: "POST",
                url: jQuery('#com-vichy-diagnostic-base-url').attr('data-url')+"&task=loadQuestion",
                dataType: "json",
                data: "language="+language+"&catid="+catid,
                success: function(data_page){
                    jQuery("#parent_id").html(data_page.option_question);
                }
            });

        });
    });
</script>