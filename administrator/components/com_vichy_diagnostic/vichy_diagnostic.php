<?php 
	defined('_JEXEC') or die();
	// require helper file
	JLoader::register('Vichy_diagnosticHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'vichy_diagnostic.php');
	$controller = JRequest::getCmd('controller','diagnostics_users');
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if(file_exists($path)){
		require_once $path;
	}
	$controller = ucwords(strtolower($controller));
	$classname = 'Vichy_diagnosticController'.$controller;
	$controller = new $classname();
	$task = JRequest::getVar('task');
	$controller->execute($task);
	$controller->redirect();
?>