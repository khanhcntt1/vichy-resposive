<?php
defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');

JToolBarHelper::save();
JToolBarHelper::cancel();

$db = & JFactory::getDbo();
$db->setQuery("Select sg.catid, sg.subcribe_group, c.title from vc_vichy_subcribe_group as sg join vc_categories as c on sg.catid = c.id where sg.status = 1");
$group_name = $db->loadObjectList();


JToolBarHelper::title(JText::_( 'Soạn tin sẽ gửi' ), 'generic.png');

require_once(JPATH_ROOT.'/MailChimp.php');
$mailchimp = new Drewm\MailChimp(MAILCHIMP_KEY);
$lists = $mailchimp->call('lists/list',array(
	'sort_dir'   => 'ASC'
)) ;
$listid = $lists['data'][ELEMENT_MC]['id'];

$groups = $mailchimp->call('lists/interest-groupings',array(
		'id' => $listid,
		'counts'=> 'false'
	)) ;

?>

<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
	}
</script>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
	<table>
		<tr>
			<td>Chọn nhóm :</td>
			<td>
				<select name="groups">
				<?php
				foreach($group_name as $v){
					echo "<option value='".$v->subcribe_group."'>".$v->title.'</option>';
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Tiêu đề :</td>
			<td> 
				<input name="subject" size="40" type="text" /> 
            </td>
		</tr>
		<tr>
			<td>Nội dung :</td>
			<td> 
				<?php 			
			         echo @$this->editor->display( 'content',  "", '550;', '300', '75', '20', array() ) ; 
                 ?> 
             </td>
		</tr>
		
	</table>			
	<?php echo JHTML::_( 'form.token' ); ?>
	<input type="hidden" name="grouping_id" value="<?php echo $groups[0]['id']; ?>" />
	<input type="hidden" name="option" value="com_vichy_new_letter" />
	<input type="hidden" name="controller" value="newletter" />
	<input type="hidden" name="task" value="" />
</form>
