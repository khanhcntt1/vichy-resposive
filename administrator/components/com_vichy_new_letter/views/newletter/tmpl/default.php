<?php 
	defined('_JEXEC') or die;
	require_once JPATH_ROOT.'/MailChimp.php';
	JToolBarHelper::title(JText::_('Bản tin'), 'generic.png');
	JToolBarHelper::addNewX();

	$mailchimp = new Drewm\MailChimp(MAILCHIMP_KEY);
	$lists = $mailchimp->call('lists/list',array(
		'sort_dir'   => 'ASC'
	)) ;
	$list_id = $lists['data'][ELEMENT_MC]['id'];

	$chunk_size = 4096; //in bytes
	$url = 'http://us8.api.mailchimp.com/export/1.0/list?apikey='.MAILCHIMP_KEY.'&id='.$list_id;
?>
<form action="index.php?option=com_vichy_new_letter&controller=newletter" method="post" name="adminForm" enctype="multipart/form-data">
	<table class="adminlist" cellspacing="1">	
		<tr>
			<th align="center" width="500">Email</th>
			<th align="center">Nhu cầu bạn quan tâm</th>
		</tr>
		<?php
		/** a more robust client can be built using fsockopen **/
		$handle = @fopen($url,'r');
		if (!$handle) {
			echo "<tr><td colspan='2'>failed to access url</td></tr>";
		} else {
			$i = 0;
			$header = array();
			while (!feof($handle)) {
				$buffer = fgets($handle, $chunk_size);
				if (trim($buffer)!=''){
					$obj = json_decode($buffer);
					if ($i==0){
						//store the header row
						$header = $obj;
					} else {
						//echo, write to a file, queue a job, etc.
						// echo $header[0].': '.$obj[0]."<br />";
						echo '<tr>';
						echo '<td>'.$obj[0].'</td>';
						echo '<td>'.$obj[3].'</td>';
						echo '</tr>';
					}
					$i++;
				}
			}
			fclose($handle);
		}

		?>
	</table>
	<?php echo JHTML::_( 'form.token' ); ?>					
	<input type="hidden" name="view" value="" />							
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="option" value="com_vichy_new_letter" />						
	<input type="hidden" name="controller" value="newletter" />
	<input type="hidden" name="boxchecked" value="0" />	
</form>