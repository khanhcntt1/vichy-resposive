<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	jimport( 'joomla.html.pagination' );
	class Vichy_new_letterViewNewletter extends JView{
		function display($tpl = null){
			
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();			
			        
			$editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor);

			$ListSubscribe = & $this->get('Data');
			$this->assignRef('ListSubscribe', $ListSubscribe);

			$ListEmail = Vichy_new_letterHelper::getEmail();
			$this->assignRef('ListEmail', $ListEmail);
			
			// $Pagination = & $this->get('Pagination');
			// $this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>