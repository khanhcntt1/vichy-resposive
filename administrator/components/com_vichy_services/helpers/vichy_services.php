<?php
abstract class Vichy_servicesHelper{
	public static function addSubmenu($submenu) 
    {
            JSubMenuHelper::addEntry(JText::_('Dịch vụ'),
                                     'index.php?option=com_vichy_services', $submenu == 'services');

            JSubMenuHelper::addEntry(JText::_('Danh mục'),
                                     'index.php?option=com_categories&view=categories&extension=com_vichy_services',
                                     $submenu == 'categories');
            // set some global property
            $document = JFactory::getDocument();
            //$document->addStyleDeclaration('.icon-48-helloworld ' . '{background-image: url(../media/com_helloworld/images/tux-48x48.png);}');
            if ($submenu == 'categories') 
            {
                    $document->setTitle(JText::_('COM_HELLOWORLD_ADMINISTRATION_CATEGORIES'));
            }
    }
     public static function listLanguage(){
        $db =& JFactory::getDbo();
        $db->setQuery(
            'SELECT lang_code, title_native' .
            ' FROM #__languages' .
            ' ORDER BY lang_code ASC'
        );
        $options = $db->loadObjectList();
        return $options;
    }
    public static function getCategories(){
        $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        $query = "SELECT id, title, parent_id FROM #__categories where extension = 'com_vichy_services' and published = '1' and language = 'vi-VN' order by title ASC";
        $db->setQuery($query);
        $listCategories = $db->loadObjectList();
        return $listCategories;
    }
    public static function recursive($data,$parent='1',$space='',$select=''){
        if (count($data)) {
            foreach ($data as $k => $v) {
                if($v->parent_id == $parent){
                    $id = $v->id;
                    if($v->parent_id == '1'){
                        if($k>0) echo '</optgroup>';
                        echo '<optgroup label="'.$v->title.'">';
                    }else{
                        if(is_array($select)){
                            $flag = false;
                            foreach ($select as $k1 => $v2) {
                                if($id == $v2){
                                    $flag = true;
                                    break;
                                }
                            }
                            if($flag == true){
                                echo "<option value='$id' selected='selected'>$space".$v->title."</option>";
                            }else{
                                echo "<option value='$id'>$space".$v->title."</option>";
                            }
                        }else{
                            if($select != '' && $id == $select){
                                echo "<option value='$id' selected='selected'>$space".$v->title."</option>";
                            }else{
                                echo "<option value='$id'>$space".$v->title."</option>";
                            }
                        }
                    }
                    unset($data[$k]);
                    self::recursive($data,$id,$space.'--',$select);
                }
            }
        }
    }
    
}