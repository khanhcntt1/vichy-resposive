<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    // $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');

    JToolBarHelper::save();
    $db =& JFactory::getDBO();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));
    $ciddef = $cid[0];

    if ($ciddef > 0){	
    	
    	$query = "SELECT * FROM #__vichy_services WHERE id = ".$ciddef;
    	$db->setQuery($query);
    	$group = $db->loadObject();

        $image_link = JURI::root()."components/com_vichy_services/uploads/others/";
        if($group->position ==1)
            $image_link = JURI::root()."components/com_vichy_services/uploads/slide_show/";
        
    	JFilterOutput::objectHTMLSafe($group, ENT_QUOTES, '' );
    	
    	JToolBarHelper::title(JText::_( 'Chỉnh sửa ảnh  : '.$group->name ), 'generic.png');
    	JToolBarHelper::cancel( 'cancel', 'Close' ); 	
    }
    else{
    	JToolBarHelper::title(JText::_( 'Thêm ảnh trang chủ' ), 'generic.png');
    	JToolBarHelper::cancel();
	
    }
    $listCategories = Vichy_servicesHelper::getCategories();
    $listLanguage = Vichy_servicesHelper::listLanguage();
?>     


<fieldset>
    <legend>Services detail</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table>
           <tr>
                <td><strong>Service :</strong></td>
                <td>  
                    <?php $selected = (!empty($group->catid)) ? $group->catid : ''; ?>
                    <select name="category">
                    <option value="*">Dịch vụ</option>
                       <?php Vichy_servicesHelper::recursive($listCategories,'1','',$selected); ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td><strong>Language :</strong></td>
                <td>
                    <?php $selected = (!empty($group->language)) ? $group->language : 'vi-VN'; ?>
                    <select name="language">
                    <option value="*">All</option>
                    <?php
                        echo JHtml::_('select.options', $listLanguage, 'lang_code', 'title_native', $selected); 
                    ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td><strong>Description :</strong></td>
                <td>
                <?php           
                     echo @$this->editor->display( 'content',  $group->content, '550;', '300', '75', '20', array() ) ; 
                 ?> 
                </td>
            </tr>

        </table>
        
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_services" />
        <input type="hidden" name="controller" value="services" />
        <input type="hidden" name="task" value=""/>
    </form>
   
</fieldset>
