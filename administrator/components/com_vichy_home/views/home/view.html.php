<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_homeViewHome extends JView{
	   
		function display($tpl = null){
	        // Set the submenu            
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor);            
			$listHomeImages = & $this->get('Data');
			$this->assignRef('listHomeImages', $listHomeImages);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>