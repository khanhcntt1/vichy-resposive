<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    // $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');

    JToolBarHelper::save();
    $db =& JFactory::getDBO();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));
    $ciddef = $cid[0];

    if ($ciddef > 0){	
    	
    	$query = "SELECT * FROM #__vichy_home WHERE id = ".$ciddef;
    	$db->setQuery($query);
    	$group = $db->loadObject();

        $image_link = JURI::root()."components/com_vichy_home/uploads/others/";
        if($group->position ==1)
            $image_link = JURI::root()."components/com_vichy_home/uploads/slide_show/";
        
    	JFilterOutput::objectHTMLSafe($group, ENT_QUOTES, '' );
    	
    	JToolBarHelper::title(JText::_( 'Chỉnh sửa ảnh  : '.$group->name ), 'generic.png');
    	JToolBarHelper::cancel( 'cancel', 'Close' ); 	
    }
    else{
    	JToolBarHelper::title(JText::_( 'Thêm ảnh trang chủ' ), 'generic.png');
    	JToolBarHelper::cancel();
	
    }
    // $type = Vichy_storeHelper::getCategories();
    $listLanguage = Vichy_homeHelper::listLanguage();       

?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
	}
    // Joomla.submitbutton = function(task){
    //     if (task == 'save'){
    //         if(jQuery('#address').val() == '' && jQuery('#b_lng').val() != ''){
    //             alert('Địa chỉ của cửa hàng không được để trống');
    //         }else{
    //             Joomla.submitform(task);
    //         }
    //     } else Joomla.submitform(task);
    //     return false;
    // };
</script>
<fieldset>
    <legend>Upload ảnh cho trang chủ</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table>
            <tr>
                <td><strong>Vị trí hiển thị :</strong></td>
                <td>
                    <select name="position">
                        <option value="">Chọn vị trí</option>                        
                        <option value="<?php echo HOME_POSITION_SLIDESHOW;?>" <?php if($group->position==HOME_POSITION_SLIDESHOW) echo 'selected="selected"';?> ><?php echo HOME_POSITION_SLIDESHOW_NAME;?></option>
                        <option value="<?php echo HOME_POSITION_MAIN_ROW_TOP_LEFT;?>" <?php if($group->position==HOME_POSITION_MAIN_ROW_TOP_LEFT) echo 'selected="selected"';?>><?php echo HOME_POSITION_MAIN_ROW_TOP_LEFT_NAME;?></option>
                        <option value="<?php echo HOME_POSITION_MAIN_ROW_TOP_RIGHT;?>" <?php if($group->position==HOME_POSITION_MAIN_ROW_TOP_RIGHT) echo 'selected="selected"';?>><?php echo HOME_POSITION_MAIN_ROW_TOP_RIGHT_NAME;?></option>
                        <option value="<?php echo HOME_POSITION_MAIN_ROW_BOTTOM_LEFT;?>" <?php if($group->position==HOME_POSITION_MAIN_ROW_BOTTOM_LEFT) echo 'selected="selected"';?>><?php echo HOME_POSITION_MAIN_ROW_BOTTOM_LEFT_NAME;?></option>
                        <option value="<?php echo HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1;?>" <?php if($group->position==HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1) echo 'selected="selected"';?>><?php echo HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1_NAME;?></option>
                        <option value="<?php echo HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2;?>" <?php if($group->position==HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2) echo 'selected="selected"';?>><?php echo HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2_NAME;?></option>                        
                    </select>
                    (*Không thể chuyển ảnh từ vị trí Slide Show đến các vị trí khác và ngược lại)
                </td>
            </tr>
            <tr>
                <td><strong>Language :</strong></td>
                <td>
                    <?php $selected = (!empty($group->language)) ? $group->language : 'vi-VN'; ?>
                    <select name="language">
                    <option value="*">All</option>
                    <?php
                        echo JHtml::_('select.options', $listLanguage, 'lang_code', 'title_native', $selected); 
                    ?>
                    </select>
                </td>
            </tr>                    
            <tr>
                <td><strong>Ảnh :</strong></td>
                <td><input type="file" name="file" id="file"/> (*Dung lượng ảnh tối đa là 1Mb)</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if(!empty($group->image)){ ?>
                        <img width="300px" height="176px" src="<?php echo $image_link.$group->image;?>"/>
                    <?php } ?>
                    <input type="hidden" value="<?php if(!empty($group->image)) echo($group->image); else echo '';?>" name="file-image"/>
                </td>
            </tr>
            <tr>
                <td><strong>Liên kết :</strong></td>
                <td>
                    <input type="text" id="link" name="link" value="<?php if(!empty($group->link)) echo($group->link); else echo '';?>" size="100" />
                </td>
            </tr>
           
        </table>
        
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_home" />
        <input type="hidden" name="controller" value="home" />
        <input type="hidden" name="task" value=""/>
    </form>
   
</fieldset>
