<?php 
	defined('_JEXEC') or die();
	// require helper file
	JLoader::register('Vichy_homeHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'vichy_home.php');
	$controller = JRequest::getCmd('controller','home');
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if(file_exists($path)){
		require_once $path;
	}
	$controller = ucwords(strtolower($controller));
	$classname = 'Vichy_homeController'.$controller;	
	$controller = new $classname();
	$task = JRequest::getVar('task');
	$controller->execute($task);
	$controller->redirect();
?>