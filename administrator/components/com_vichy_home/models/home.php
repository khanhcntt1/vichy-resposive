<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_homeModelHome extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){

			$query = "SELECT * FROM #__vichy_home";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );

            $data['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );

            $data['name'] = $data['name_s'];
            
            $data['published_date'] = date('Y-m-d h:i:s');
            if(empty($data['cid'][0])){
               	$type = $_FILES['file']['type'];
                $extension = strtolower(substr($type, strpos($type,'/') + 1));
                $tmp_name = $_FILES['file']['tmp_name'];
                $new_name = md5(time()).'.'.$extension;                
                $location = '../components/com_vichy_home/uploads/others/'.$new_name;
                if($data['position']==1)
                	$location = '../components/com_vichy_home/uploads/slide_show/'.$new_name;
                
                if(move_uploaded_file($tmp_name, $location)){
                    $data['image'] =  $new_name;
                    $db = & JFactory::getDBO();
                    $sql = "INSERT into #__vichy_home(position,image,publish,published_date,link, language) 
                    values ('$data[position]','$data[image]', '$data[publish]', '$data[published_date]','$data[link]', '$data[language]')";
                    $db->setQuery($sql);
                    $db->query();
                }else{
                	$this->setError("Lỗi không up được file");
                	return false;
                }

            }else{

            	if(!empty($_FILES['file']['name'])){
            		$type = $_FILES['file']['type'];
	                $extension = strtolower(substr($type, strpos($type,'/') + 1));
	                $tmp_name = $_FILES['file']['tmp_name'];
	                $new_name = md5(time()).'.'.$extension;
	                $data['published_date'] = date('Y-m-d h:i:s');
	                $location = '../components/com_vichy_home/uploads/others/'.$new_name;
	                if($data['position']==1)
                		$location = '../components/com_vichy_home/uploads/slide_show/'.$new_name;
	                if(move_uploaded_file($tmp_name, $location)){
	                    $data['image'] =  $new_name;
	                }else{
	                	$this->setError("Lỗi không up được file");
	                	return false;
	                }
            	}else{
            		$data['image'] = $data['file-image'];
            	}
            	if(!empty($data['file-image']) && $data['image']!=$data['file-image']){
            		$localtion_gallery = '../components/com_vichy_home/uploads/others/'.$data['file-image'];
            		if($data['position']==1)
                		$localtion_gallery = '../components/com_vichy_home/uploads/slide_show/'.$data['file-image'];
                	unlink($localtion_gallery);
            	}
            	$db = & JFactory::getDBO();
                $sql = "UPDATE #__vichy_home 
                set position = $data[position], link='$data[link]',image = '$data[image]', publish = '$data[publish]', published_date = '$data[published_date]', language = '$data[language]' where id = ".$data['cid'][0];
                $db->setQuery($sql);
                $db->query();
            }
	    	return true;
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));	
		    $row =& $this->getTable();		
	    	foreach ($cids as $cid) {

                $query = "SELECT image FROM #__vichy_home where id=".$cid;
                $db->setQuery($query);
                $result = $db->loadObject();

                $localtion = '../components/com_vichy_store/uploads/others/'.$result->image;

                if($data['position']==1)
                		$location = '../components/com_vichy_home/uploads/slide_show/'.$result->image;

                unlink($localtion);

                if (!$row->delete( $cid )) {
  	            	$this->setError($row->getErrorMsg());
  	            	return false;
                }
               
	    	}
	     	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_home'
					. ' SET publish = ' . (int) $publish
					. ' WHERE id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>