<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_storeControllerSpa extends JController{
   
	function __construct(){
		parent::__construct();
		$this->registerTask('add', 'edit');
	}

	function display(){
		JRequest::setVar('view', 'spa');
    	parent::display();
	}
	function edit(){
    	JRequest::setVar('view', 'spa');
    	JRequest::setVar('layout', 'form');
    	JRequest::setVar('hidemainmenu', 1); 
    	parent::display();
    }
    function cancel(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect('index.php?option=com_vichy_store&controller=spa' );
	}
	function save(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
	    $model = $this->getModel('Spa');
	    $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $id = $cid[0];
        $data = JRequest::get( 'post' );
		$size = $_FILES['file']['size'];
        $max_size = 307200;
        if (empty($id)){
            if(empty($data['type'])){
                $this->setMessage(JText::_( 'Vui lòng chọn loại cho cửa hàng'),'error');
                $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form');
                return false;
            }

            if(empty($data['name_s'])){
                $this->setMessage(JText::_( 'Tên cửa hàng không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form');
                return false;
            }

            if(empty($_FILES['file']['name'])){
                
                $this->setMessage(JText::_( 'Vui lòng chọn hình ảnh đại diện cho cửa hàng'),'error');
                $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form');
                return false;
            }

            if ((($_FILES["file"]["type"] != "image/gif")
					&& ($_FILES["file"]["type"] != "image/jpeg")
					&& ($_FILES["file"]["type"] != "image/jpg")
					&& ($_FILES["file"]["type"] != "image/pjpeg")
					&& ($_FILES["file"]["type"] != "image/x-png")
					&& ($_FILES["file"]["type"] != "image/png"))
			)
			{
				$this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
                $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form');
                return false;
			}
            
            if($size > $max_size){
                
                $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 300Kb !'),'error');
                $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form');
                return false;     
            }
            
        }else{

            if(empty($data['type'])){
                $this->setMessage(JText::_( 'Vui lòng chọn loại cho cửa hàng'),'error');
                $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form&task=edit&cid[]='.$id);
                return false;
            }
            
            if(empty($data['name_s'])){
                $this->setMessage(JText::_( 'Tên cửa hàng không được để trống !'),'error');
                $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form&task=edit&cid[]='.$id);
                return false;
            }
            
            if(!empty($_FILES['file']['name'])){
                if ((($_FILES["file"]["type"] != "image/gif")
					&& ($_FILES["file"]["type"] != "image/jpeg")
					&& ($_FILES["file"]["type"] != "image/jpg")
					&& ($_FILES["file"]["type"] != "image/pjpeg")
					&& ($_FILES["file"]["type"] != "image/x-png")
					&& ($_FILES["file"]["type"] != "image/png"))
				)
				{
					$this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
	                $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form&task=edit&cid[]='.$id);
	                return false;
				}
                
                if($size > $max_size){
                    
                    $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 300Kb !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_store&controller=spa&layout=form&task=edit&cid[]='.$id);
                    return false;     
                }
            }
        }

        if ($model->store()) {
            $msg = JText::_( 'Lưu thành công!' );
    	} else {
            $msg = JText::_( 'Lưu thất bại !' );
        }

    	$link = 'index.php?option=com_vichy_store&controller=spa';
    	$this->setRedirect($link, $msg);
	}

    function publish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
        } else {
    
            $model = $this->getModel('Spa');
    
            if(!$model->publish($cid, 1)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) PUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_store');
            $cache->clean();
        }
    
        $this->setRedirect( 'index.php?option=com_vichy_store&controller=spa', $msg );
    }
        
    function unpublish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
        } else {
    
            $model = $this->getModel('Spa');
    
            if(!$model->publish($cid, 0)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) UNPUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_store');
            $cache->clean();
        }
            
        $this->setRedirect('index.php?option=com_vichy_store&controller=spa', $msg);
    }

    function remove(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Spa');
        
        if(!$model->deleteRows()) {
            $msg = JText::_( 'Lỗi : Một hoặc nhiều cửa hàng không thể xoá');
        } else {
            $msg = JText::_( 'Cửa hàng này đã được xoá !' );
        }
        
        $this->setRedirect('index.php?option=com_vichy_store&controller=spa', $msg );
    }
}