<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_storeViewSpa extends JView{
	   
		function display($tpl = null){
	        // Set the submenu
            Vichy_storeHelper::addSubmenu('spa');
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor); 
              
			$listStores = & $this->get('Data');
			$this->assignRef('listStores', $listStores);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>