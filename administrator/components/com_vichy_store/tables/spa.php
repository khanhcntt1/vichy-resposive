<?php
    defined('_JEXEC') or die;
     
    class TableSpa extends JTable {
    	var $id = 0;
        var $type;
        var $name;
        var $image;
        var $address;
        var $phone;
        var $email;
        var $lng;
        var $lat;
        var $description;
        var $published;
        var $create_date;
        var $modify_date;
        var $language;
     
        function __construct(&$db) {
        	parent::__construct('#__vichy_store', 'id', $db);
        }
    }
?>
