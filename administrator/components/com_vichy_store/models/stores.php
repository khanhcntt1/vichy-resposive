<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_storeModelStores extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){

			$query = "SELECT s.*, c.parent_id FROM `vc_vichy_store` as s join vc_categories as c on c.id = s.type where c.parent_id = ".STORE;
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );

            $data['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );

            $data['name'] = $data['name_s'];
            if(empty($data['cid'][0])){
               	// $type = $_FILES['file']['type'];
                // $extension = strtolower(substr($type, strpos($type,'/') + 1));
                // $tmp_name = $_FILES['file']['tmp_name'];
                // $new_name = md5(time()).'.'.$extension;
                // $location = '../components/com_vichy_store/uploads/stores/'.$new_name;
                // if(move_uploaded_file($tmp_name, $location)){
                    $data['image'] =  $new_name;
                    $db = & JFactory::getDBO();
                    $sql = "INSERT into #__vichy_store 
                    values (null, $data[type], '$data[name]', '1', '$data[address]', $data[province], '$data[phone]', '$data[email]', '$data[lng]', '$data[lat]', '$data[description]',  '1', null, null, '$data[language]')";
                    $db->setQuery($sql);
                    $db->query();
                // }else{
                // 	$this->setError("Lỗi không up được file");
                // 	return false;
                // }

            }else{

            	// if(!empty($_FILES['file']['name'])){
            	// 	$type = $_FILES['file']['type'];
	            //     $extension = strtolower(substr($type, strpos($type,'/') + 1));
	            //     $tmp_name = $_FILES['file']['tmp_name'];
	            //     $new_name = md5(time()).'.'.$extension;
	            //     $location = '../components/com_vichy_store/uploads/stores/'.$new_name;
	            //     if(move_uploaded_file($tmp_name, $location)){
	            //         $data['image'] =  $new_name;
	            //     }else{
	            //     	$this->setError("Lỗi không up được file");
	            //     	return false;
	            //     }
            	// }else{
            	// 	$data['image'] = $data['file-image'];
            	// }
            	// if(!empty($data['file-image']) && $data['image']!=$data['file-image']){
            	// 	$localtion_gallery = '../components/com_vichy_store/uploads/stores/'.$data['file-image'];
             //    	unlink($localtion_gallery);
            	// }
            	$db = & JFactory::getDBO();
                $sql = "UPDATE #__vichy_store 
                set type = $data[type], name = '$data[name]', address = '$data[address]', province = $data[province], phone = '$data[phone]', email = '$data[email]', lng = '$data[lng]', lat = '$data[lat]', description = '$data[description]', modify_date = '".date('Y-m-d h:i:s')."', language = '$data[language]' where id = ".$data['cid'][0];
                $db->setQuery($sql);
                $db->query();
            }
	    	return true;
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));	
		    $row =& $this->getTable();		
	    	foreach ($cids as $cid) {

                // $query = "SELECT image FROM #__vichy_store where id=".$cid;
                // $db->setQuery($query);
                // $result = $db->loadObject();

                // $localtion = '../components/com_vichy_store/uploads/stores/'.$result->image;
                // unlink($localtion);

                if (!$row->delete( $cid )) {
  	            	$this->setError($row->getErrorMsg());
  	            	return false;
                }
               
	    	}
	     	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_store'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>