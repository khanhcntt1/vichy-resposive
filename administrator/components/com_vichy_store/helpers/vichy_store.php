<?php
abstract class Vichy_storeHelper{
	public static function addSubmenu($submenu) 
    {
            JSubMenuHelper::addEntry(JText::_('VICHY_STORES'),
                                     'index.php?option=com_vichy_store', $submenu == 'stores');

            JSubMenuHelper::addEntry(JText::_('Spa'),
                                     'index.php?option=com_vichy_store&controller=spa', $submenu == 'spa');

            JSubMenuHelper::addEntry(JText::_('VICHY_STORES_CATEGORIES'),
                                     'index.php?option=com_categories&view=categories&extension=com_vichy_store',
                                     $submenu == 'categories');
            // set some global property
            $document = JFactory::getDocument();
            if ($submenu == 'categories') 
            {
                    $document->setTitle(JText::_('COM_HELLOWORLD_ADMINISTRATION_CATEGORIES'));
            }
    }

    public static function getCategories($parent_id=''){
        $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        $where = "";
        if($parent_id != ''){
            $where = " and parent_id = $parent_id";
        }
        $query = "SELECT id, title, parent_id FROM #__categories where extension = 'com_vichy_store' and published = '1' $where order by title ASC";
        $db->setQuery($query);
        $listCategories = $db->loadObjectList();
        return $listCategories;
    }

    public static function listLanguage(){
        $db =& JFactory::getDbo();
        $db->setQuery(
            'SELECT lang_code, title_native' .
            ' FROM #__languages' .
            ' ORDER BY lang_code ASC'
        );
        $options = $db->loadObjectList();
        return $options;
    }

    public static function listProvince(){
        $db =& JFactory::getDbo();
        $db->setQuery(
            'SELECT id, name' .
            ' FROM #__provinces' .
            ' ORDER BY name ASC'
        );
        $options = $db->loadObjectList();
        return $options;
    }
}