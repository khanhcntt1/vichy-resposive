<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();

    JToolBarHelper::save();
    $db =& JFactory::getDBO();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));
    $ciddef = $cid[0];

    if ($ciddef > 0){   
        
        $query = "SELECT * FROM #__vichy_press_corner  WHERE id = ".$ciddef;
        $db->setQuery($query);
        $group = $db->loadObject();
        
        JFilterOutput::objectHTMLSafe($group, ENT_QUOTES, '' );
        
        JToolBarHelper::title(JText::_( 'Edit Press corner  : '.$group->title ), 'generic.png');
        JToolBarHelper::cancel( 'cancel', 'Close' );    
    }
    else{
        JToolBarHelper::title(JText::_( 'Add Press corner' ), 'generic.png');
        JToolBarHelper::cancel();
    
    }
    $listLanguage = Vichy_aboutHelper::listLanguage();
?>
<script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'cancel') {
            submitform( pressbutton );
            return;
        }
    }
</script>
<fieldset>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table>
            <tr>
                <td><strong>Name :</strong></td>
                <td>
                    <input type="text" value="<?php if(!empty($group->title))  echo($group->title);else echo ''; ?>"  name="title" size="100"/>
                </td>
            </tr>

            <tr>
                <td><strong>Language :</strong></td>
                <td>
                    <?php $selected = (!empty($group->language)) ? $group->language : 'vi-VN'; ?>
                    <select name="language">
                    <option value="*">All</option>
                    <?php
                        echo JHtml::_('select.options', $listLanguage, 'lang_code', 'title_native', $selected); 
                    ?>
                    </select>
                </td>
            </tr>
                        
            <tr>
                <td><strong>Photo :</strong></td>
                <td><input type="file" name="file" id="file"/> (Max size of file is 300Kb, width:284px, height:238px)</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if(!empty($group->image)){ ?>
                        <img width="284px" height="238px" src="<?php echo JURI::root();?>components/com_vichy_about/uploads/images/<?php echo $group->image;?>"/>
                    <?php } ?>
                    <input type="hidden" value="<?php if(!empty($group->image)) echo($group->image); else echo '';?>" name="file-image"/>
                </td>
            </tr>
            <tr>
                <td><strong>File pdf :</strong></td>
                <td>
                    <input type="file" name="pdf" id="pdf"/>
                    <input type="hidden" value="<?php if(!empty($group->file_pdf)) echo($group->file_pdf); else echo '';?>" name="file-pdf"/>
                </td>
            </tr>
            <tr>
                <td><strong>Description :</strong></td>
                <td>
                <?php           
                     echo @$this->editor->display( 'description',  $group->description, '550', '300', '75', '20', array() ) ; 
                 ?> 
                </td>
            </tr>

        </table>
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_about" />
        <input type="hidden" name="controller" value="press_corner" />
        <input type="hidden" name="task" value=""/>
    </form>
</fieldset>