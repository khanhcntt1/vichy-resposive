<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_aboutViewPress_corner extends JView{
	   
		function display($tpl = null){
			// Vichy_aboutHelper::addSubmenu('press_corner');
	        // Set the submenu            
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor);            
			$list = & $this->get('Data');
			$this->assignRef('list', $list);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>