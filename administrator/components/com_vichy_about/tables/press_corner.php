<?php
    defined('_JEXEC') or die;
     
    class TablePress_corner extends JTable {
    	var $id = 0;
        var $title;
        var $description;
        var $image;
        var $file_pdf;
        var $language;
        var $published;  
     
        function __construct(&$db) {
        	parent::__construct('#__vichy_press_corner', 'id', $db);
        }
    }
?>
