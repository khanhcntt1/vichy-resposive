<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_aboutModelPress_corner extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){

			$query = "SELECT * FROM #__vichy_press_corner";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );

            $data['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );
            if(empty($data['cid'][0])){
               	$type = $_FILES['file']['type'];
                $extension = strtolower(substr($type, strpos($type,'/') + 1));
                $tmp_name = $_FILES['file']['tmp_name'];
                $new_name = md5(time()).'.'.$extension;                
                $location = '../components/com_vichy_about/uploads/images/'.$new_name;
                $flag = '0'              ;
                if(move_uploaded_file($tmp_name, $location)){
                	$flag = '1';
                    $data['image'] =  $new_name;
                }else{
                	$flag = '0';
                }

                $type = $_FILES['pdf']['type'];
                $extension = strtolower(substr($type, strpos($type,'/') + 1));
                $tmp_name = $_FILES['pdf']['tmp_name'];
                $new_name = md5(time()).'.'.$extension;                
                $location = '../components/com_vichy_about/uploads/press_corner/'.$new_name;
                if(move_uploaded_file($tmp_name, $location)){
                	$flag = '1';
                    $data['file_pdf'] =  $new_name;
                    
                }else{
                	$flag = '0';
                }

                if($flag == '1'){
                	$db = & JFactory::getDBO();
                    $sql = "INSERT into vc_vichy_press_corner values (null,'$data[title]','$data[description]','$data[image]', '$data[file_pdf]', '$data[language]',1)";
                    $db->setQuery($sql);
                    $db->query();
                }else{
                	return false;
                }

            }else{
            	$flag = '1';
            	if(!empty($_FILES['file']['name'])){
            		$type = $_FILES['file']['type'];
	                $extension = strtolower(substr($type, strpos($type,'/') + 1));
	                $tmp_name = $_FILES['file']['tmp_name'];
	                $new_name = md5(time()).'.'.$extension;
	                $location = '../components/com_vichy_about/uploads/images/'.$new_name;
	                if(move_uploaded_file($tmp_name, $location)){
	                    $data['image'] =  $new_name;
	                    $flag = '1';
	                }else{
	                	$flag = '0';
	                }
            	}else{
            		$data['image'] = $data['file-image'];
            	}

            	if(!empty($_FILES['pdf']['name'])){
            		$type = $_FILES['pdf']['type'];
	                $extension = strtolower(substr($type, strpos($type,'/') + 1));
	                $tmp_name = $_FILES['pdf']['tmp_name'];
	                $new_name = md5(time()).'.'.$extension;
	                $location = '../components/com_vichy_about/uploads/press_corner/'.$new_name;
	                if(move_uploaded_file($tmp_name, $location)){
	                    $data['file_pdf'] =  $new_name;
	                    $flag = '1';
	                }else{
	                	$flag = '0';
	                }
            	}else{
            		$data['file_pdf'] = $data['file-pdf'];
            	}
            	if($flag == '1'){
            		if(!empty($data['file-image']) && $data['image']!=$data['file-image']){
	            		$localtion_gallery = '../components/com_vichy_about/uploads/images/'.$data['file-image'];
	                	unlink($localtion_gallery);
	            	}
	            	if(!empty($data['file-pdf']) && $data['file_pdf']!=$data['file-pdf']){
	            		$localtion_gallery = '../components/com_vichy_about/uploads/press_corner/'.$data['file-pdf'];
	                	unlink($localtion_gallery);
	            	}
	            	$db = & JFactory::getDBO();
	                $sql = "UPDATE #__vichy_press_corner
	                set title = '$data[title]', description='$data[description]',image = '$data[image]', file_pdf = '$data[file_pdf]', language = '$data[language]' where id = ".$data['cid'][0];
	                $db->setQuery($sql);
	                $db->query();
            	}else{
            		return false;
            	}
            	
            }
	    	return true;
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));	
		    $row =& $this->getTable();		
	    	foreach ($cids as $cid) {

                $query = "SELECT image, file_pdf FROM #__vichy_press_corner where id=".$cid;
                $db->setQuery($query);
                $result = $db->loadObject();

                $localtion_1 = '../components/com_vichy_about/uploads/images/'.$result->image;

                unlink($localtion_1);

                $localtion_2 = '../components/com_vichy_about/uploads/press_corner/'.$result->file_pdf;

                unlink($localtion_2);

                if (!$row->delete( $cid )) {
  	            	$this->setError($row->getErrorMsg());
  	            	return false;
                }
               
	    	}
	     	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_press_corner'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>