<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	jimport( 'joomla.html.pagination' );
	class Vichy_comment_productViewFeedback extends JView{
		function display($tpl = null){
			
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
			
			JSubMenuHelper::addEntry( JText::_( 'Feedback' ), 'index.php?option=com_vichy_comment_product&controller=feedback',true);	        
			
			$ListFeedback = & $this->get('Data');
			$this->assignRef('ListFeedback', $ListFeedback);
			
			$Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>