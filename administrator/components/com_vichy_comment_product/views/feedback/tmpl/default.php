<?php 
	defined('_JEXEC') or die;
	JToolBarHelper::title(JText::_('Feedback'), 'generic.png');
	JToolBarHelper::editListX();
	JToolBarHelper::publish();
	JToolBarHelper::unpublish();
	JToolBarHelper::deleteList();
?>
<form action="index.php?option=com_vichy_comment_product&controller=feedback" method="post" name="adminForm" enctype="multipart/form-data">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('Tìm kiếm theo username:'); ?></label>
			<input type="text" size="30" name="filter_search" id="filter_search" value="<?php echo $this->escape($_SESSION['default_search_comment']); ?>" />

			<button type="submit" class="btn"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
	</fieldset>
	<table class="adminlist" cellspacing="1">	
			<tr>
				<th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->ListFeedback); ?>);" /></th>
				<th width="30" align="center">STT</th>
				<th align="center">Bình luận</th>
				<th align="center">Trả lời</th>
				<th align="center">Sản phẩm</th>
				<th align="center">Tài khoản</th>
				<th align="center">Ngày tháng</th>
				<th align="center">Ẩn/Hiện</th>
			</tr>
		<tbody>
<?php 
$k = 0;
$cate ='';
for ($i=0, $n=count($this->ListFeedback); $i < $n; $i++)
{
	$row 	=& $this->ListFeedback[$i];
	$published 	= JHTML::_('grid.published', $row, $i );

?>
			<tr class="<?php echo "row$k"; ?>">
				<td width="20"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>
				<td width="30" align="center"><?php echo $i+1; ?></td>
				<td align="center"><?php echo $row->comment; ?></td>
				<td align="center"><?php echo $row->answer; ?></td>
				<td align="center" width='20%'><?php echo $row->name; ?></td>
				<td align="center"><?php echo $row->username; ?></td>
				<td align="center"><?php echo $row->date; ?></td>	
				<td align="center"><?php echo $published; ?></td>
			</tr>
			
<?php

	$k = 1 - $k;
	
}
?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
			</tr>
		</tfoot>
	</table>
	<?php echo JHTML::_( 'form.token' ); ?>					
	<input type="hidden" name="view" value="" />							
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="option" value="com_vichy_comment_product" />						
	<input type="hidden" name="controller" value="feedback" />
	<input type="hidden" name="boxchecked" value="0" />	
</form>