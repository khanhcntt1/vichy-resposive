<?php 
	defined('_JEXEC') or die();
	$controller = JRequest::getCmd('controller','feedback');
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if(file_exists($path)){
		require_once $path;
	}
	
	$controller = ucwords(strtolower($controller));
	$classname = 'Vichy_comment_productController'.$controller; 
	$controller = new $classname();
	$task = JRequest::getVar('task');
	$controller->execute($task);
	$controller->redirect();