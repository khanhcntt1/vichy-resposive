<?php
defined('_JEXEC') or die('Restricted access');
 
class TableFeedback extends JTable {
	var $id = 0;
	var $username;
	var $product_id;
    var $comment;
    var $answer;
    var $date;
    var $published;
 
    function __construct( &$db ) {
    	parent::__construct('#__vichy_comment_product', 'id', $db);
    }
}
?>