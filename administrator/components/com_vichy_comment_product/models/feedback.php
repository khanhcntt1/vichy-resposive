<?php

	defined( '_JEXEC' ) or die( 'Restricted Access' );
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_comment_productModelFeedback extends JModel{
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){
			if(isset($_POST['filter_search']) && !empty($_POST['filter_search'])){
				$username = $_POST['filter_search'];
				$_SESSION['default_search_comment']=$_POST['filter_search'];
				$query = "select cp.*,p.name from `#__vichy_comment_product` as cp join `#__vichy_product` as p on cp.product_id = p.id where cp.username = '$username' order by id DESC";
			}else{
				$_SESSION['default_search_comment'] = "";
				$query ='select cp.*,p.name from `#__vichy_comment_product` as cp join `#__vichy_product` as p on cp.product_id = p.id order by id DESC';
			}
			return $query;
    	}
    	
    	//get total item in table
		function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}
		//get start paging
		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
 		//paging table
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		//set state
 		function populateState()
		{ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}
 	
 	
    	
	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
		    JArrayHelper::toInteger($cids, array(0));
	    	
		    $row =& $this->getTable();
	 
	    	foreach ($cids as $cid) {
	        	if (!$row->delete( $cid )) {
	            	$this->setError($row->getErrorMsg());
	            	return false;
	        	}
	    	}
	     	return true;
		}
		
		function publish($cid = array(), $publish = 1)
		{
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_comment_product'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}

		function store() {
		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );
	    	$data['answer'] = JRequest::getVar( 'answer', '', 'post', 'string', JREQUEST_ALLOWRAW );
	    	if (!$row->bind($data)) {
	        	$this->setError($this->_db->getErrorMsg());
	        	return false;
	    	}
	 
	    	if (!$row->check()) {
	        	$this->setError($this->_db->getErrorMsg());
	        	return false;
	    	}
	 
	    	if (!$row->store()) {
	        	$this->setError($this->_db->getErrorMsg());
	        	return false;
	    	}
	 
	    	return true;
		}
	}
	
?>