<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.controller');

	class Vichy_comment_productControllerFeedback extends JController{
		function __construct() {
			parent::__construct($config);
			$this->registerTask('add', 'edit');
		}
  
		function display() {
			JRequest::setVar('view', 'feedback');
	    	parent::display();
		}		
	    
    	function remove() {
		    JRequest::checkToken() or jexit( 'Invalid Token' );
			$model = $this->getModel('Feedback');
			
	    	if(!$model->deleteRows()) {
	        	$msg = JText::_( 'Lỗi: Xóa không thành công !');
	    	} else {
	        	$msg = JText::_( 'Xóa thành công !' );
	    	}
	    	
	    	$this->setRedirect( 'index.php?option=com_vichy_comment_product&controller=feedback', $msg );
    	}
    	
		function publish(){
			JRequest::checkToken() or jexit( 'Invalid Token' );
			
			$cid 	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
	
			if (!is_array( $cid ) || count( $cid ) < 1) {
				$msg = '';
				JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
			} else {
	
				$model = $this->getModel('Feedback');
	
				if(!$model->publish($cid, 1)) {
					JError::raiseError(500, $model->getError());
				}
	
				$msg 	= JText::_( 'ITEM(S) PUBLISHED');
			
				$cache = &JFactory::getCache('com_vichy_comment_product');
				$cache->clean();
			}
	
			$this->setRedirect( 'index.php?option=com_vichy_comment_product&controller=feedback', $msg );
		}
		
		function unpublish()
		{
			JRequest::checkToken() or jexit( 'Invalid Token' );
			
			$cid 	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
	
			if (!is_array( $cid ) || count( $cid ) < 1) {
				$msg = '';
				JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
			} else {
	
				$model = $this->getModel('Feedback');

				if(!$model->publish($cid, 0)) {
					JError::raiseError(500, $model->getError());
				}
	
				$msg 	= JText::_( 'ITEM(S) UNPUBLISHED');
			
				$cache = &JFactory::getCache('com_vichy_comment_product');
				$cache->clean();
			}
			
			$this->setRedirect( 'index.php?option=com_vichy_comment_product&controller=feedback', $msg );
		}

		function edit() 
		{
	    	JRequest::setVar('view', 'feedback');
	    	JRequest::setVar('layout', 'form');
	    	JRequest::setVar('hidemainmenu', 1); 
	    	parent::display();
	    }
	    
		function save(){
			JRequest::checkToken() or jexit( 'Invalid Token' );
	    	$model = $this->getModel('Feedback');
	 		$post = JRequest::get( 'post' );
	 		$post['answer'] = JRequest::getVar( 'answer', '', 'post', 'string', JREQUEST_ALLOWRAW );
	    	if ($model->store()) {
	        	$msg = JText::_( 'Lưu thành công!' );
	        	$config = JFactory::getConfig();
	        	$data['fromname'] = $config->get('fromname');
				$data['mailfrom'] = $config->get('mailfrom');
				$emailSubject	= 'Vichy trả lời';
				$emailBody = "Chào bạn ".$post['username'].',<br />'.$post['answer'];
				$mailer =& JFactory::getMailer();
				$mailer->IsHTML(true);
	        	$return = $mailer->sendMail($data['mailfrom'], $data['fromname'], $post['email'], $emailSubject, $emailBody,1);

				// Check for an error.
				if ($return !== true) {
					$msg = "Gửi mail không thành công";
					$link = 'index.php?option=com_vichy_comment_product&controller=feedback';
	    			$this->setRedirect($link, $msg);
					return false;
				}
	    	} else {
	        	$msg = JText::_( 'Lưu không thành công !' );
	    	}
	 
	    	$link = 'index.php?option=com_vichy_comment_product&controller=feedback';
	    	$this->setRedirect($link, $msg);
		}
		
		function cancel(){
			JRequest::checkToken() or jexit( 'Invalid Token' );
			$this->setRedirect('index.php?option=com_vichy_comment_product&controller=feedback');
		}

	}
	
?>