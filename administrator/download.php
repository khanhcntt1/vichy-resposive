<?php
$fn = $_GET['file'];
if ($fn) {
	//Perform security checks
	$result = $_SERVER['DOCUMENT_ROOT'].'/administrator/components/com_users/includes/'.$fn;
	// echo $result;
	if (file_exists($result)) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/force-download');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));

		ob_clean();
		flush();
		readfile($result);
		@unlink($result);
	}

}